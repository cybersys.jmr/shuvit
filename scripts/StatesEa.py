import bge
import random
#====================================     

State = type("State", (object,), {})
#====================================     
class State(object):
    def __init__(self, FSM):
        self.FSM = FSM
        self.timer = 0
        self.startTime = 0
    def Enter(self):
        self.timer = 0
        self.startTime = 0
    def Execute(self):
        print('Executing')
    def Exit(self):
        print('Exiting')
#==================================== 
            
class Example(State):
    def __init__(self,FSM):
        super(Example, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Example, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #print('doing example', self.FSM.owner['EaRequest'])

        if self.FSM.owner['EaRequest'] == 'land':
            self.FSM.ToTransition('toLand')
        
    def Exit(self):
        pass

#==================================== 
            
class Land(State):
    def __init__(self,FSM):
        super(Land, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        #self.strength = random.choice([.1, .2, .3, .4, .5, .6, .7, .8, .9, .9, .9, .9])
        self.strength = random.choice([.1, .2, .3, .4, .5, .6, .7, .8, .9])
        self.act = random.choice(['ea_land2', 'ea_land3'])
        #self.strength = random.choice([.1])
        super(Land, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        print('land')
        arm = bge.logic.globalDict['p1']

        if self.FSM.stateLife > 80:
            if self.strength < 1:
                self.strength += .05
                arm.stopAction(8)
        
        arm.playAction(self.act, 0,80, layer=8, play_mode=0, speed=1, blendin=10, layer_weight=self.strength)


        if self.FSM.stateLife > 100:
            self.FSM.owner['EaRequest'] = None
            self.FSM.ToTransition('toExample')    


    def Exit(self):
        pass

