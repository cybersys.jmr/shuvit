import bge

def place():
	scene = bge.logic.getCurrentScene()
	for x in scene.objects:
		if 'occluder_tmp' in x.name:
			print('place occluder')
			o = scene.addObject('occluder_obj', x)
			o.worldOrientation = x.worldOrientation
			o.worldScale = x.worldScale
			x.endObject()