#zskcam
import bge
import mathutils
from mathutils import *
scene = bge.logic.getCurrentScene()
import camFSM
import birds
import sound_man
import FSM
import cars
import walkers
import occluders

def main(cont):
    own = cont.owner
    dict = bge.logic.globalDict
    camempty = scene.objects['camEmpty.001']
    controlcube = scene.objects['control_cube.002']
    camCube = scene.objects['camCube']
    dropinCol = controlcube.sensors['dropinCol']
    LAST_GRIND = controlcube['LAST_GRIND']
    up = cont.sensors['up']
    ray = cont.sensors['up']
    down = cont.sensors['down']
    left = cont.sensors['left']
    right = cont.sensors['right']
    distance = 0.12
    cam = cont.actuators["Camera"]
    near = cont.sensors["Near"]
    move = own.actuators["move"]
    cam_def_height = dict['cam_height']
    cam_def_min = dict['cam_min']
    cam_def_max = dict['cam_max']
    cam_height = cam.height
    cam_min = cam.min
    cam_max = cam.max
    lasty = controlcube['lasty']
    cam_moved = 0
    lastCamheight = controlcube['lastCamheight']
    raised = 0
    walk = controlcube['walk']
    cam.axis = 4
    ccheight = controlcube.worldPosition[2]
    camwpz = own.worldPosition[2]
    zdist = camwpz - ccheight
    cam1 = scene.objects['Camera.003']
    cam2 = scene.objects['freecam']
    cam3 = scene.objects['followcam']
    camList = scene.cameras
    freecam = camList["freecam"]

    if 'init' not in own:
        own['init'] = 1
        own['last_move_x'] = 0
        own['last_move_y'] = 0
        own['last_move_z'] = 0
        own['last_rot_x'] = 0
        own['last_rot_y'] = 0
        own['last_rot_z'] = 0
        own['idlecampos_x'] = 0
        own['speed_mult'] = 1.00
        controlcube['driving'] = False
        dict['cur_ccH_min'] = dict['cam_walk_min']
        dict['cur_ccH_max'] = dict['cam_walk_max']
        cam.height = dict['cam_height']
        dict['cam_state'] = 'walking'
        controlcube['ragdoll_active'] = False
        dict['camera'] = cam1
        #own['NpcPedFSM'] = FSM.NpcPed(own)
        own['CamFSM'] = FSM.CameraFSM(own)
        own['life'] = 0
        occluders.place()
    acam = scene.active_camera

    def get_cam_state():
        if  dict['npause']:
            if dict['menu_idle_timer'] > 300:
                #dict['cam_state'] = 'pause_idle'
                dict['cam_state'] = 'PauseIdleCam'
            else:    
                dict['cam_state'] = 'PauseCam'
        elif walk == 1:
            if controlcube['ragdoll_active']:
                dict['cam_state'] = 'RagdollCam'
            else:
                dict['cam_state'] = 'WalkCam'
        elif controlcube['driving']:
            dict['cam_state'] = 'driving'
        else:
            dict['cam_state'] = 'RollCam'   

    if controlcube['driving'] == False:
        LAST_GRIND = False
        if down.triggered == True and LAST_GRIND == False and walk == 0:
            hitPosition = down.hitPosition
            distance = own.getDistanceTo(hitPosition)
            cam_moved = 1

        if down.triggered == False and LAST_GRIND == False and cam_moved == 0 and walk == 0:
            camempty['hitdown'] = False
            if cam_height > (cam_def_height + .08) and zdist > -.2 and raised == 0:
                cam.height = cam_height - .02

        if cam_height < -.6 and cam_moved == 0 and LAST_GRIND == False and zdist > -.2 and raised == 0 and walk == 0:
            cam_height = .1
        if LAST_GRIND == True and walk == 0:
            if cam.height < -.5 or zdist < -.2:
                cam.height = cam.height + .013
            if cam.height >= -.5 and not down.triggered:
                pass

        controlcube['lastCamheight'] = cam.height
        cont.activate(own.actuators['Camera'])

        if near.triggered == True and walk == 0:
            if cam.min < 1:
                cam.min = cam.min + .1
                cam.max = cam.max + .1
            cam.height = cam_height + .01
            cam.damping = .001
            cont.activate(move)

        else:
            cont.deactivate(move)
            if cam.min > cam_def_min:
                cam.min = cam.min - .05
                cam.max = cam.max - .05
            cam.damping = .0
        if cam.damping != 0 and down.triggered == False and near.triggered == False:
            cam.damping = 0.91

        obj = cont.owner
        cube = controlcube
        to = cube
        to2 = to.worldPosition
        pos = [to.worldPosition.x, to.worldPosition.y, to.worldPosition.z]
        pos[2] = pos[2] - .2
        from2 = obj
        from3 = [from2.worldPosition.x, from2.worldPosition.y, from2.worldPosition.z]
        from3[2] = from3[2] - .2
        distance = 0.0
        property = ""
        face = 1
        xray = 0
        poly = 0
        hit = obj.rayCast( pos, from3, distance, property, face, xray, poly)
        control = "control_cube.002"
        hitobj = hit[0]
        hitobj = str(hitobj)
        
        if hit[0]:
            if hitobj != control and walk == 0 and 'vert' not in hit[0] and 'ground' in hit[0]:
                cam.damping = .01
                if cam.height < 2:
                    cam.height = cam_height + .1
                    cam.max = cam.max - .2
                elif cam.height >= 2 and cam.height < 4:
                    cam.height = cam_height + .05
                    cam.max = cam.max - .05

    if dict['menu_idle_timer'] > 300:
        move_len = 2048
        if own['idlecampos_x'] < move_len:
            own['idlecampos_x'] += 1
        if own['idlecampos_x'] == move_len:
           own['idlecampos_x'] = 0

        if own['idlecampos_x'] < (move_len / 2):
            move = [.0001, 0, 0]
            freecam.applyMovement( move, True)
            freecam.applyRotation([.0001, 0, .0001], True)
        if own['idlecampos_x'] > (move_len / 2):
            move = [-.0001, 0, 0]
            freecam.applyMovement(move, True)
            freecam.applyRotation([-.0001, 0, -.0001], True)

    if dict['npause'] == True:
        cont.deactivate(own.actuators['Camera'])
        controlcube['camera'] = 2
    if controlcube['camera'] == 2 and dict['joy_con'] == 1:
        cont.deactivate(own.actuators['Camera'])
        scene.active_camera = freecam
        cont.activate(cube.actuators['freecam'])
        lLR = dict['lLR'] / .082 * 100
        lUD = dict['lUD'] / .082 * 100 - 20 / 80
        rLR = dict['rLR'] / .082 * 100 - 20 / 80
        rUD = dict['rUD'] / .082 * 100 - 20 / 80
        lTrig = dict['lTrig'] / .082 * 100 - 20 / 80
        rTrig = dict['rTrig'] / .082 * 100 - 20 / 80

        if lLR < -20:
            lmLR = round((lLR + 20) / 80 * 100, 0)
        elif lLR > 20:
            lmLR = round((lLR - 20) / 80 * 100, 0)
        else: lmLR = 0

        if lUD > 20:
            lmUD = round((lUD - 20) / 80 * 100, 0)
        elif lUD < -20:
            lmUD = round((lUD + 20) / 80 * 100, 0)
        else: lmUD = 0

        if rLR < -20:
            rmLR = round((rLR + 20) / 80 * 100, 0)
        elif rLR > 20:
            rmLR = round((rLR - 20) / 80 * 100, 0)
        else: rmLR = 0

        if rUD > 20:
            rmUD = round((rUD - 20) / 80 * 100, 0)
        elif rUD < -20:
            rmUD = round((rUD + 20) / 80 * 100, 0)
        else: rmUD = 0

        if lTrig > 3:
            mTrig = lTrig * -1
        elif rTrig > 3:
            mTrig = rTrig
        else: mTrig = 0

        #move camera
        damping = .95
        damping2 = 1.005
        mult = .0005 * own['speed_mult']
        move_x = lmUD * mult
        move_y = lmLR * mult
        move_z = (mTrig * -mult) / 2#4

        rot_mult = -.00015 * own['speed_mult']
        rot_x = rmUD * rot_mult
        rot_y = rmLR * rot_mult

        if move_x == 0 and own['last_move_x'] != 0:
            move_x = own['last_move_x'] * damping
        if move_y == 0 and own['last_move_y'] != 0:
            move_y = own['last_move_y'] * damping
        if move_z == 0 and own['last_move_z'] != 0:
            move_z = own['last_move_z'] * damping
        if rot_x == 0 and own['last_rot_x'] != 0:
            rot_x = own['last_rot_x'] * damping
        if rot_y == 0 and own['last_rot_y'] != 0:
            rot_y = own['last_rot_y'] * damping

        move = [move_y, 0, move_x]
        freecam.applyMovement(move, True)
        freecam.applyMovement([0, 0, move_z], False)
        freecam.applyRotation([rot_x, 0, 0], True)
        freecam.applyRotation([0, 0, rot_y], False)


################
    multer = .02
    if dict['walk'] == 1 and controlcube['ragdoll_active'] == False:
        if dict['cur_ccH_targetHeight'] < dict['cam_walk_height']:
            dist = dict['cam_walk_height'] - dict['cur_ccH_targetHeight']

            dict['cur_ccH_targetHeight'] = dict['cur_ccH_targetHeight'] + (dist * multer)
            if dict['cur_ccH_targetHeight'] > dict['cam_walk_height']:
                dict['cur_ccH_targetHeight'] = dict['cam_walk_height']

        cam.min = dict['cam_walk_min']
        cam.max = dict['cam_walk_max']

    else:
        if dict['cur_ccH_targetHeight'] < dict['cch_targetHeight']:
            dist = dict['cur_ccH_targetHeight'] - dict['cch_targetHeight']
            dict['cur_ccH_targetHeight'] = dict['cur_ccH_targetHeight'] - (dist * multer)
            if dict['cur_ccH_targetHeight'] > dict['cch_targetHeight'] - .001:
                dict['cur_ccH_targetHeight'] = dict['cch_targetHeight']

        if dict['cur_ccH_targetHeight'] > dict['cch_targetHeight']:
            dist = dict['cch_targetHeight'] - dict['cur_ccH_targetHeight']
            dict['cur_ccH_targetHeight'] = dict['cur_ccH_targetHeight'] + (dist * multer)
            if dict['cur_ccH_targetHeight'] < dict['cch_targetHeight'] + .001:
                dict['cur_ccH_targetHeight'] = dict['cch_targetHeight']

    ccH_targetHeight = dict['cur_ccH_targetHeight']
    ccH = camCube.worldPosition.z
    pH = camCube.parent.worldPosition.z
    ccheight = (round((ccH - pH), 2) - .4)
    localPos = camCube.localPosition.z

    if localPos != ccH_targetHeight:
        num = ccH_targetHeight - localPos
        camCube.localPosition.z += num
    num = ccH_targetHeight - ccheight

    try:
        if dict['npause'] == False and controlcube['ragdoll_active'] == False:
            cont.activate(own.actuators['Camera'])
            controlcube['camera'] = 0
        if controlcube['ragdoll_active'] == True:
            own.actuators['Camera'].object = scene.objects['ragdoll_parent']
            cont.activate(own.actuators['Camera'])
            controlcube['camera'] = 0
    except:
        pass

    def set_lens_dist():
        cur_lens = cam1.lens
        cur_min = cam.min
        cur_max = cam.max
        inc = .025
        if walk == 1:
            #lens
            if dict['walk_focal_length'] > cur_lens:
                new_lens = cur_lens + inc
            else:
                new_lens = cur_lens - inc
            if cur_lens > (dict['walk_focal_length'] - .1) and cur_lens < (dict['walk_focal_length'] + .1):
                new_lens = dict['walk_focal_length']

            #distance
            inc = .025
            if cur_min > (dict['cam_walk_min'] - inc):
                new_min = cur_min - inc
            if cur_min < (dict['cam_walk_min'] + inc):
                new_min = cur_min + inc
            if cur_max > (dict['cam_walk_max'] - inc):
                new_max = cur_min - inc
            if cur_max < (dict['cam_walk_max'] + inc):
                new_max = cur_min + inc
        else:
            #lens
            if dict['focal_length'] > cur_lens:
                new_lens = cur_lens + inc
            else:
                new_lens = cur_lens - inc
            if cur_lens > (dict['focal_length'] - .1) and cur_lens < (dict['focal_length'] + .1):
                new_lens = dict['focal_length']

            #distance
            inc = .025
            if cur_min > (dict['cam_min'] - inc):
                new_min = cur_min - inc
            if cur_min < (dict['cam_min'] + inc):
                new_min = cur_min + inc
            if cur_max > (dict['cam_max'] - inc):
                new_max = cur_min - inc
            if cur_max < (dict['cam_max'] + inc):
                new_max = cur_min + inc

        focallength = new_lens
        cam1.lens = focallength
        cam2.lens = focallength
        cam3.lens = focallength
        try:
            cam.min = new_min
            cam.max = new_max
        except:
            pass

    set_lens_dist()        
    get_cam_state()
    camFSM.main(cont)
    birds.main(cont, scene)
    sound_man.main(cont)
    
    if own['life'] % 2 == 1:
        #cars.Execute(cont)
        own['CamFSM'].Execute()
        #walkers.Execute(cont)
    else:
        pass

    own['life'] += 1


    # mo = scene.objects['Suzanne']
    # print(mo.culled)

    # if dict['kb_a'] == True and dict['last_kb_a'] == False:
        
    #     if scene.active_camera == scene.objects['tmp_cam2']:
    #         scene.active_camera = scene.objects['tmp_cam']
    #         print('change cam tmp', scene.active_camera)
    #     else:
    #         scene.active_camera = scene.objects['tmp_cam2']

    #         print('change cam og', scene.active_camera)