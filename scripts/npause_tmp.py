import bge
import menuV3
import Settings


#replay huds:
#add_hud
#remove_hud (on control_cube)

def main():

    cont = bge.logic.getCurrentController()
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict 
    
    skater = scene.objects["Char4"]
    deck = scene.objects["deck"]
    trucks = scene.objects["trucks"]
    own = scene.objects["control_cube.002"]
    
    camList = scene.cameras
    freecam = camList["freecam"] 
    mcam = camList['Camera.003']   
    
    cube = scene.objects['control_cube.002']
    
    try:
        pause_state = dict['npause']
        last_pause = dict['last_npause']
    except:
        dict['npause'] = 0
        dict['last_npause'] = 0
        pause_state = 0
        last_pause = 0


    def printplaying():
        splaying_layers = "S: "
        playing_layers = "D: "
        tplaying_layers = "T: "
        for x in range(1000):
            if skater.isPlayingAction(x):
            #if trucks.isPlayingAction(x):
            #if skater.isPlayingAction(x):                        
                splaying_layers += str(x)
                splaying_layers += " "        
            if deck.isPlayingAction(x):
            #if trucks.isPlayingAction(x):
            #if skater.isPlayingAction(x):                        
                playing_layers += str(x)
                playing_layers += " "
            if trucks.isPlayingAction(x):
            #if trucks.isPlayingAction(x):
            #if skater.isPlayingAction(x):                        
                tplaying_layers += str(x)
                tplaying_layers += " "            
        print(splaying_layers, playing_layers, tplaying_layers)

    
    #switch pause state
    if dict['stBut'] == True and dict['last_stBut'] == False:
        if pause_state == 0:
            pause_state = 1
        else:
            pause_state = 0    
        dict['npause'] = pause_state
    
    #enter pause    
    if pause_state == 1 and last_pause == 0:
        own.suspendDynamics()
        own['pre_pause_linvel'] = [own['linvelx'], own['linVely'], 0]
        own.setLinearVelocity([0,0,0],1)  
        cont.activate(cont.actuators['empty'])
        if mcam['playback'] == False:
            cont.activate(cont.actuators['add_overlay']) 
        cont.activate(cont.actuators['remove_stance'])  
        cube['camera'] = 2
        cube['camnum'] = 2 
        scene.active_camera = freecam 
        freecam.lens = mcam['focal_length'] 
        freecam.worldPosition = mcam.worldPosition
        freecam.worldOrientation = mcam.worldOrientation                      

    if pause_state == 1: 
        if mcam['playback'] == False:       
            layer = 2
            if skater.isPlayingAction(layer):
                skater.stopAction(layer)
            if deck.isPlayingAction(layer):    
                deck.stopAction(layer)
            if trucks.isPlayingAction(layer):    
                trucks.stopAction(layer)
            layer = 3
            if skater.isPlayingAction(layer):
                skater.stopAction(layer)
            if deck.isPlayingAction(layer):    
                deck.stopAction(layer)
            if trucks.isPlayingAction(layer):    
                trucks.stopAction(layer)
            layer = 4
            if skater.isPlayingAction(layer):
                skater.stopAction(layer)
            if deck.isPlayingAction(layer):    
                deck.stopAction(layer)
            if trucks.isPlayingAction(layer):    
                trucks.stopAction(layer)
        menuV3.main(cont)                                                    
                
    #exit pause     
    if pause_state == 0 and last_pause == 1:
        Settings.writeSettings()
        Settings.writeChar()
        own.restoreDynamics()

        if own['walk'] == False:
            cont.activate(cont.actuators['roll'])
        else:
            cont.activate(cont.actuators['walk'])    
        try:
            own.setLinearVelocity(own['pre_pause_linvel'], 1) 
        except:
            pass    
        cont.activate(cont.actuators['remove_overlay'])  
        cont.activate(cont.actuators['add_stance']) 
        
    if pause_state == 1 and dict['rtsBut'] == False and dict['last_rtsBut'] == True:
        scenes = bge.logic.getSceneList()
        if 'pause' in scenes:    
            cont.activate(cont.actuators['remove_overlay']) 
        else:
            cont.activate(cont.actuators['add_overlay'])                                   
    dict['last_npause'] = pause_state
    
main()
