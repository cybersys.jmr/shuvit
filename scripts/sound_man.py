#sound_man.py
#shuvit sound effects manager

import bge
import aud

dict = bge.logic.globalDict

def init_sounds(self):
	#pop

	self.pop = sounds('pop')
	self.land = sounds('land')
	self.roll = sounds('roll')
	self.s_l_1 = sounds('s_l_1')

	self.s_r_1 = sounds('s_r_1')
	self.s_l_2 = sounds('s_l_2')
	self.s_r_2 = sounds('s_r_2')
	self.s_l_1.volume = 0.3
	self.s_r_1.volume = 0.3
	self.s_l_1.pitch = 3
	self.s_r_1.pitch = 3

	self.walk_grunt = sounds('walk_grunt')
	self.walk_land = sounds('walk_land')
	self.walk_hang = sounds('walk_hang')
	self.walk_climb = sounds('walk_climb')
	self.revert = sounds('revert')
	self.engine_idle = sounds('engine_idle')
	self.roll.loop = True
	self.grind_cement = sounds('grind_cement')
	self.grind_cement.loop = True
	self.grind_cement.pingpong = True
	self.grind_rail = sounds('grind_rail')
	self.grind_rail.loop = True
	self.grind_rail.pingpong = True
	self.engine_idle.loop = True
	#self.pop.name = 'pop'



class sound_manager:
	def __init__(self):
		self.queue = []
		self.history = []
		self.h_queue = []
		self.gd = bge.logic.globalDict
		init_sounds(self)
		#self.pop = sounds()

	def queue_sound(self, sound):
		self.queue.append(sound)

	def remove_sound(self, sound):
		self.queue.remove(sound)

	def play_queue(self):
		if self.queue is not []:
			#pass
			self.h_queue = self.queue.copy()
			#print('og', self.h_queue, self.queue)
			for ev in self.queue:
				#get function from string at position 0 of event
				result = getattr(self, ev[0])
				#print(result)
				#print(result.name)
				if result.loop:
					result.loop_play(ev[1], ev[2], ev[3], ev[4])
				else:
					result.play(ev[1], ev[2])

				self.queue.remove(ev)
			

			self.queue = []	

	def play_hist(self):
		if self.h_queue is not []:
			#pass
			
			for ev in self.h_queue:
				#get function from string at position 0 of event
				result = getattr(self, ev[0])
				#print(result)
				#print(result.name)
				#if result.name == 'roll':
				if result.loop:
					result.loop_play(ev[1], ev[2], ev[3], ev[4])
				else:
					result.play(ev[1], ev[2])

				#self.queue.remove(ev)				

	def update(self):
		self.play_queue()
		#print('queued sounds', self.queue)

	def stop_sound(self, sound):
		c = getattr(self, sound[0])
		if c.handle_buffered:
			c.handle_buffered.stop()

	def Record(self):
		

		self.history.append(self.h_queue)
		self.history.append(self.h_queue)
		#self.history.insert(0, self.h_queue)
		l = len(self.history)
		#print(self.h_queue, 'appended to history', l)
		#self.history.append(self.h_queue)
		#print('appending', self.h_queue)
		l = len(self.history)
		if l > ((self.gd['replay_record_length'])):
			#del self.history[0:1]
			self.history.pop(0)
			self.history.pop(0)
			#self.history.pop(self.gd['replay_record_length'])
			#self.history.pop(0)
		#print(l, 'history length', ((self.gd['replay_record_length']) -1))

	def h_Playback(self, frame):
		#frame *= 2
		#frame = int(frame /2)
		try:	
			self.h_queue = self.history[frame]
			#self.h_queue2 = self.history[frame-1]
			#print(self.history[frame], self.queue, frame, 'replay queue')
		except:
			print('replay audio not playing', frame, len(self.history))
		#print('h_queue', self.h_queue, frame, (self.history))
		#print('h_queue', self.h_queue, frame, len(self.history))
		if self.h_queue is not []:
			for ev in self.h_queue:
				result = getattr(self, ev[0])
				#print('ev0', ev[0])
				if result.loop:
					result.loop_play(ev[1], ev[2], ev[3], ev[4])
					#pass
				else:
					result.play(ev[1], ev[2])
					#print('... playing', result.name, frame, len(self.history))
		# if self.h_queue2 is not []:
		# 	for ev in self.h_queue2:
		# 		result = getattr(self, ev[0])
		# 		if result.loop:
		# 			result.loop_play(ev[1], ev[2], ev[3], ev[4])
		# 		else:
		# 			result.play(ev[1], ev[2])					


		#print(self.history)

class sounds(sound_manager):
	def __init__(self, name):
		self.name = name
		self.obj = None
		self.loop = False
		self.pingpong = False
		self.listener_obj = None
		self.pitch = 1.0	
		self.volume = 0.9
		self.device = aud.device()
		self.path = bge.logic.expandPath('//sounds')
		self.factory = aud.Factory(self.path + '/' + self.name + '.wav')
		self.factory_buffered = aud.Factory.buffer(self.factory)
		self.handle_buffered = None
		#self.handle = self.device.play(factory)

	def play(self, obj, listener):
		
		scene = bge.logic.getCurrentScene()

		self.device.distance_model = aud.AUD_DISTANCE_MODEL_LINEAR
		self.device.listener_location = obj.worldPosition
		self.device.listener_velocity = obj.getLinearVelocity()
		self.device.volume = self.volume

		handle_buffered = self.device.play(self.factory_buffered)
		handle_buffered.relative = False
		handle_buffered.location = listener.worldPosition
		handle_buffered.velocity = listener.getLinearVelocity()
		handle_buffered.distance_maximum = 50
		handle_buffered.distance_reference = .1


	def loop_play(self, obj, listener, v = 0.0, p = 0.0):
		
		scene = bge.logic.getCurrentScene()

		self.device.distance_model = aud.AUD_DISTANCE_MODEL_LINEAR
		self.device.listener_location = obj.worldPosition
		self.device.listener_velocity = obj.getLinearVelocity()
		#self.device.volume = self.volume
		
		if not self.handle_buffered or not self.handle_buffered.status:
			self.handle_buffered = self.device.play(self.factory_buffered)
		#print(self.handle_buffered.status)
		self.factory_buffered.loop(-1)
		
		if self.pingpong:
			self.factory_buffered.pingpong()
		try:
			self.handle_buffered.volume = v
			self.handle_buffered.pitch = p
			self.handle_buffered.relative = False
			self.handle_buffered.location = listener.worldPosition
			self.handle_buffered.velocity = listener.getLinearVelocity()
			self.handle_buffered.distance_maximum = 50
			self.handle_buffered.distance_reference = .1	
			
		except:
			pass	


def main(cont):
	own = cont.owner
	if 'sound_man_inited' not in own:
		own['sound_man_inited'] = True

		sm = sound_manager()
		own['sndmgr'] = sm

	dict = bge.logic.globalDict
	
	#if not dict['playback']:
		#own['sndmgr'].Record()
	if not dict['playback']:
		own['sndmgr'].update()
		#print('updating sound manager', dict['game_life'])

	#if dict['playback']:
		#own['sndmgr'].Playback()	
	