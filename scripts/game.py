import bge
import FSM
import requests
import platform
import os

def phone_home():
	url = 'https://shuvit.org/phone_home.php'
	n = platform.uname()
	n = platform.platform()
	myobj = {'somekey': n}
	x = requests.post(url, data = myobj, timeout=3.50)
	print('phone home response')
	print(x.text)

def get_map_list():
	out = []
	ldir = bge.logic.expandPath("//assets/maps")
	out = os.listdir(ldir)
	print('$$$$$$$$$$$$$$$$$$$$$$$out', out)
	return out

def main(cont):
	own = cont.owner
	dict = bge.logic.globalDict
	if 'game_inited' not in own:
		info = bge.logic.PrintGLInfo()
		own['game_inited'] = True
		own['cont'] = cont
		dict['gameFSM'] = FSM.GameFSM(own)
		dict['game_life'] = 0
		dict['maps'] = get_map_list()
		#phone_home()
	if 'game_life' in dict:
		dict['game_life'] += 1
	dict['gameFSM'].Execute()
	