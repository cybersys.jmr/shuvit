TRANS_LAYER = 12
MAIN_LAYER = 10
fliplay = 3

reg_fliplist = ['reg_ollie', 'reg_nollie', 'reg_nollie', 'reg_kickflip', 'reg_varial_kickflip', 'reg_nollie_varial_kickflip', 'reg_nollie_varial_heelflip', 'fak_nollie_varial_heelflip', 'reg_varial_heelflip', 'reg_nollie_kickflip',  'reg_heelflip', 'reg_nollie_heelflip', 'reg_shuvit', 'reg_shuvit360', 'reg_fsshuvit360', 'reg_nollie_shuvit', 'fak_nollie_shuvit', 'reg_fsshuvit', 'fak_fsshuvit',  'reg_nollie_fsshuvit', 'reg_nollie_shuvit360', 'reg_nollie_fsshuvit', 'reg_brfoot', 'reg_frfoot', 'reg_blfoot', 'reg_flfoot', 'reg_inward_heelflip', 'reg_hardflip', 'reg_nollie_inward_heelflip', 'reg_nollie_hardflip', 'reg_offboard', 'reg_nollie_fsshuvit_360'] 
fak_fliplist = ['fak_ollie', 'fak_nollie', 'fak_nollie', 'fak_kickflip', 'fak_varial_kickflip', 'fak_nollie_varial_kickflip', 'fak_nollie_varial_heelflip', 'fak_nollie_varial_heelflip', 'fak_varial_heelflip', 'fak_nollie_kickflip',  'fak_heelflip','fak_nollie_heelflip', 'fak_shuvit', 'fak_shuvit360', 'fak_fsshuvit360', 'fak_nollie_shuvit', 'fak_nollie_shuvit', 'fak_fsshuvit', 'fak_fsshuvit',  'fak_nollie_fsshuvit', 'fak_nollie_shuvit360', 'fak_nollie_fsshuvit', 'fak_brfoot', 'fak_frfoot', 'fak_blfoot', 'fak_flfoot', 'fak_inward_heelflip', 'fak_hardflip', 'fak_nollie_inward_heelflip', 'fak_nollie_hardflip', 'fak_offboard', 'fak_nollie_fsshuvit_360']     

revertlist = ['revert1', 'revert2', 'fak_revert1', 'fak_revert2', 'revert3', 'revert4']

reg_manuallist = ['reg_manual', 'reg_nmanual', 'fak_manual', 'fak_nmanual']

reg_grindlist = ['reg_5050', 'reg_fsboard', 'reg_tailg', 'reg_tailgr', 'reg_tailgl', 'reg_noseg', 'reg_nosegr', 'reg_nosegl', 'reg_noseslide', 'reg_tailslide', 'reg_bsboard', 'reg_back_invert']

fak_grindlist = ['fak_5050', 'fak_fsboard', 'fak_tailg', 'fak_tailgr', 'fak_tailgl', 'fak_noseg', 'fak_nosegr', 'fak_nosegl', 'fak_noseslide', 'fak_tailslide', 'fak_bsboard']

walk_exits = ['reg_walk', 'reg_walkFast', 'reg_idle', 'reg_jump', 'reg_onboard', 'reg_idle2', 'reg_idle3', 'reg_idle4', 'reg_idle6', 'reg_idle7', 'reg_walk_air', 'reg_walk_air_out', 'reg_sit', 'reg_dropin', 'reg_air', 'fak_walk', 'fak_walkFast', 'fak_idle', 'fak_jump', 'fak_onboard', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle6', 'fak_idle7', 'fak_walk_air', 'fak_walk_air_out', 'fak_sit', 'fak_air']

lands = ['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_manual_left', 'fak_manual_right', 'fak_manual', 'fak_nmanual', 'fak_nmanual_left', 'fak_nmanual_right'] + ['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_offboard', 'reg_manual_left', 'reg_manual_right', 'reg_manual', 'reg_nmanual', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop']

class a_class:
	def __init__(self, name, start, end, dname, dstart, dend, layer, speed, mode, blendin, intro, intro_frames, exits, fe, fef, opposite):
		self.name = name
		self.start = start
		self.end = end
		self.dname = dname
		self.dstart = dstart
		self.dend = dend
		self.layer = layer
		self.speed = speed
		self.mode = mode
		self.blendin = blendin
		self.intro = intro
		self.intro_frames = intro_frames
		self.exits = exits
		self.force_exit = fe
		self.fef = fef
		self.opposite = opposite

reg_walk = a_class(
	#player armature action name, start, end frames
	'reg_nwalk2', 0, 30,
	#deck action name, start, end frames  
	'b_reg_walk', 0, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walkFast', 'reg_idle', 'reg_jump', 'reg_onboard', 'reg_idle2', 'reg_idle3', 'reg_idle4', 'reg_idle6', 'reg_idle7', 'reg_walk_air', 'reg_walk_air_out']
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_walkFast = a_class(
	#player armature action name, start, end frames, start, end frames
	'reg_run.001', 51, 90,
	#deck action name, start, end frames  
	'b_reg_run', 51, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10, 
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle', 'reg_jump', 'reg_onboard']
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle = a_class(
	#player armature action name, start, end frames
	'reg_idle1', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle1', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_walkFast', 'reg_idle2', 'reg_idle3', 'reg_idle4', 'reg_idle5', 'reg_idle6', 'reg_idle7', 'reg_jump', 'reg_sit', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle2 = a_class(
	#player armature action name, start, end frames
	'reg_idle2_', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle2', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle', 'reg_idle3', 'reg_idle4', 'reg_idle5', 'reg_idle6', 'reg_idle7', 'reg_jump', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle3 = a_class(
	#player armature action name, start, end frames
	'reg_idle3', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle3', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle2', 'reg_idle', 'reg_idle4', 'reg_idle5', 'reg_idle6', 'reg_idle7', 'reg_jump', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle4 = a_class(
	#player armature action name, start, end frames
	'reg_idle4', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle4', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle2', 'reg_idle3', 'reg_idle', 'reg_idle5', 'reg_idle6', 'reg_idle7', 'reg_jump', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle5 = a_class(
	#player armature action name, start, end frames
	'reg_idle5', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle5', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle2', 'reg_idle3', 'reg_idle4', 'reg_idle', 'reg_idle6', 'reg_idle7', 'reg_jump', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle6 = a_class(
	#player armature action name, start, end frames
	'reg_idle6', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle6', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle2', 'reg_idle3', 'reg_idle', 'reg_idle5', 'reg_idle4', 'reg_idle7', 'reg_jump', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_idle7 = a_class(
	#player armature action name, start, end frames
	'reg_idle7', 1, 120,
	#deck action name, start, end frames  
	'b_reg_idle7', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['reg_walk', 'reg_idle2', 'reg_idle3', 'reg_idle4', 'reg_idle5', 'reg_idle6', 'reg_idle', 'reg_jump', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_jump = a_class(
	#player armature action name, start, end frames
	#'reg_jump', 1, 10,
	'a_jump_t', 9, 27,
	#deck action name, start, end frames  
	'b_reg_jump', 1, 5,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .75, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_idle', 'reg_walk_air', 'reg_onboard'],
	#force exit, frame
	'reg_walk_air', 12,
	#opposite
	None)

reg_walk_air = a_class(
	#player armature action name, start, end frames
	#'reg_walk_air', 10, 10,
	'a_jump_t', 27, 27,
	#deck action name, start, end frames  
	'b_reg_walk_air', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	#['reg_walk_air_out', 'reg_onboard', 'reg_walk', 'reg_walkFast'],
	walk_exits,
	#'reg_walk_air_out',
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_walk_air_out = a_class(
	#player armature action name, start, end frames
	#'reg_walk_air', 10, 40,
	'a_jump_t', 27, 49,
	#deck action name, start, end frames  
	'b_reg_walk_air', 10, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1.5, 1, 5,
	#intro, length
	None, 0,
	#exits
	#['reg_idle', 'reg_walk', 'reg_walkFast', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	'reg_idle', 20,
	#opposite
	None)

reg_sit_in = a_class(
	#player armature action name, start, end frames
	'reg_sit', 1, 65,
	#deck action name, start, end frames  
	'b_reg_jump', 1, 65,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_idle', 'reg_walk'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_sit = a_class(
	#player armature action name, start, end frames
	'reg_sit', 65, 65,
	#deck action name, start, end frames  
	'b_reg_jump', 65, 65,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_idle', 'reg_walk'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_onboard = a_class(
	#player armature action name, start, end frames
	#'reg_noffboard', 24, 1,
	'reg_onboard', 1, 100,
	#deck action name, start, end frames  
	#'b_reg_offboard', 24, 1,
	'b_reg_onboard', 1, 100,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, 2, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_roll'],
	#force exit, frame
	'reg_roll', 98,
	#opposite
	None)

reg_offboard = a_class(
	#player armature action name, start, end frames
	'reg_noffboard', 1, 40,
	#deck action name, start, end frames  
	'b_reg_offboard', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, 1, 0, 5,
	#intro, length
	None, 0,
	#exits
	#['reg_idle', 'reg_walkFast', 'reg_walk', 'reg_onboard'],
	walk_exits,
	#force exit, frame
	'reg_idle', 39,
	#opposite
	None)

reg_roll = a_class(
	#player armature action name, start, end frames
	'nreg', 1, 60,
	#deck action name, start, end frames  
	'b_reg', 1, 60,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_idle', 'fak_offboard' 'reg_offboard', 'reg_turnLeft', 'reg_turnRight', 'reg_opos', 'reg_nopos', 'reg_pump', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	'fak_roll')

reg_turnLeft = a_class(
	#player armature action name, start, end frames
	'nreg_left', 10, 30,
	#deck action name, start, end frames  
	'b_reg_left', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_opos', 'reg_nopos', 'reg_pump', 'reg_pump_left', 'reg_pump_right', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'fak_turnRight', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_turnRight = a_class(
	#player armature action name, start, end frames
	'nreg_right', 10, 30,
	#deck action name, start, end frames  
	'b_reg_right', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_opos', 'reg_nopos', 'reg_pump','reg_pump_left', 'reg_pump_right',  'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'fak_turnLeft', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_manual_left = a_class(
	#player armature action name, start, end frames
	'reg_manual_left', 10, 90,
	#deck action name, start, end frames  
	'b_reg_manual_left', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_opos', 'reg_nopos', 'reg_pump', 'reg_pump_left', 'reg_pump_right', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_manual_right = a_class(
	#player armature action name, start, end frames
	'reg_manual_right', 10, 90,
	#deck action name, start, end frames  
	'b_reg_manual_right', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_opos', 'reg_nopos', 'reg_pump','reg_pump_left', 'reg_pump_right',  'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_opos_in = a_class(
	#player armature action name, start, end frames
	'noposin', 1, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_opos'],
	#force exit, frame
	'reg_opos', 78,
	#opposite
	'fak_opos')

reg_opos = a_class(
	#player armature action name, start, end frames
	'nopos', 1, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	'reg_opos_in', 80,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_pump', 'reg_pump_left', 'reg_pump_right', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	'fak_opos')

reg_nopos = a_class(
	#player armature action name, start, end frames
	'nnopos', 20, 20,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_pump', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_pump = a_class(
	#player armature action name, start, end frames
	'nopos', 1, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_pump_left', 'reg_pump_right', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_pump_left = a_class(
	#player armature action name, start, end frames
	'nreg_pump_left', 10, 30,
	#deck action name, start, end frames  
	'b_reg_left', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_opos', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_pump', 'reg_pump_right', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_pump_right = a_class(
	#player armature action name, start, end frames
	'nreg_pump_right', 10, 30,
	#deck action name, start, end frames  
	'b_reg_right', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_opos', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_pump', 'reg_pump_left', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_push = a_class(
	#player armature action name, start, end frames
	'reg_push2', 1, 70,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_push_goof = a_class(
	#player armature action name, start, end frames
	'reg_push_goof', 1, 35,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_stop'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_stop = a_class(
	#player armature action name, start, end frames
	'reg_stop', 1, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_powerslide', 'reg_fs_powerslide', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_powerslide_in = a_class(
	#player armature action name, start, end frames
	'nreg_powerslide2', 0, 20,
	#deck action name, start, end frames  
	'b_reg_powerslide2', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_fs_powerslide_in = a_class(
	#player armature action name, start, end frames
	'nreg_powerslide', 0, 20,
	#deck action name, start, end frames  
	'b_reg_powerslide', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	'None', 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_powerslide = a_class(
	#player armature action name, start, end frames
	'nreg_powerslide2', 20, 20,
	#deck action name, start, end frames  
	'b_reg_powerslide2', 20, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	'reg_powerslide_in', 20,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_fs_powerslide = a_class(
	#player armature action name, start, end frames
	'nreg_powerslide', 20, 20,
	#deck action name, start, end frames  
	'b_reg_powerslide', 20, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	'reg_fs_powerslide_in', 20,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_brfoot = a_class(
	#player armature action name, start, end frames
	'brfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual', 'reg_nmanual', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	'reg_air', 20,
	#opposite
	None)

reg_frfoot = a_class(
	#player armature action name, start, end frames
	'frfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual', 'reg_nmanual', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	'reg_air', 20,
	#opposite
	None)

reg_blfoot = a_class(
	#player armature action name, start, end frames
	'blfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual', 'reg_nmanual', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	'reg_air', 20,
	#opposite
	None)


reg_flfoot = a_class(
	#player armature action name, start, end frames
	'flfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual', 'reg_nmanual', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_manual', 'reg_nmanual', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	'reg_air', 20,
	#opposite
	None)


reg_manual = a_class(
	#player armature action name, start, end frames
	'reg_manual', 10, 90,
	#deck action name, start, end frames  
	'b_reg_manual', 10, 70,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_nmanual = a_class(
	#player armature action name, start, end frames
	'reg_nmanual', 10, 90,
	#deck action name, start, end frames  
	'b_fak_manual', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_nmanual_left = a_class(
	#player armature action name, start, end frames
	'reg_nmanual_left', 10, 90,
	#deck action name, start, end frames  
	'b_fak_manual_right', 10, 70,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_nmanual_right = a_class(
	#player armature action name, start, end frames
	'reg_nmanual_right', 10, 90,
	#deck action name, start, end frames  
	'b_fak_manual_left', 10, 70,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'reg_turnRight', 'reg_turnLeft', 'reg_push', 'reg_push_goof', 'reg_pump', 'reg_nopos', 'reg_opos', 'reg_offboard', 'reg_air', 'reg_air_nose', 'reg_air_tail', 'reg_manual_left', 'reg_manual_right', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_land = a_class(
	#player armature action name, start, end frames
	'reg_land', 1, 50,
	#deck action name, start, end frames  
	'b_reg', 1, 50,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'reg_roll', 25,
	#opposite
	None)

reg_landL = a_class(
	#player armature action name, start, end frames
	'reg_landL', 1, 40,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'reg_roll', 20,
	#opposite
	None)

reg_landR = a_class(
	#player armature action name, start, end frames
	'reg_landR', 1, 40,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'reg_roll', 20,
	#opposite
	None)

reg_landLb = a_class(
	#player armature action name, start, end frames
	'reg_landLb', 1, 40,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'reg_roll', 20,
	#opposite
	None)

reg_landRb = a_class(
	#player armature action name, start, end frames
	'reg_landRb', 1, 40,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'reg_roll', 20,
	#opposite
	None)

reg_air = a_class(
	#player armature action name, start, end frames
	'reg_air', 1, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_air_nose', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_judo', 'reg_frigid', 'reg_onefoot', 'reg_fsonefoot', 'reg_airwalk', 'reg_wall_r', 'reg_wall_l', 'reg_manual_left', 'reg_manual_right', 'reg_manual', 'reg_nmanual', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_air_nb = a_class(
	#player armature action name, start, end frames
	'reg_air', 1, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 0,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_air_nose', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_judo', 'reg_frigid', 'reg_onefoot', 'reg_fsonefoot', 'reg_airwalk', 'reg_wall_r', 'reg_wall_l', 'reg_manual_left', 'reg_manual_right', 'reg_manual', 'reg_nmanual', 'reg_nmanual_left', 'reg_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_air_nose = a_class(
	#player armature action name, start, end frames
	'reg_noseg.002', 40, 80,
	#deck action name, start, end frames  
	'b_reg_noseg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual', 'reg_nmanual', 'reg_air_tail', 'reg_ollie_north', 'reg_ollie_south', 'reg_airwalk', 'reg_wall_r', 'reg_wall_l', 'reg_manual_left', 'reg_manual_right', 'reg_manual', 'reg_nmanual', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_air_tail = a_class(
	#player armature action name, start, end frames
	'reg_tailg.001', 40, 80,
	#deck action name, start, end frames  
	'b_reg_tailg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'reg_manual', 'reg_nmanual', 'reg_air_nose', 'reg_ollie_north', 'reg_ollie_south', 'reg_airwalk', 'reg_wall_r', 'reg_wall_l', 'reg_manual_left', 'reg_manual_right', 'reg_manual', 'reg_nmanual', 'reg_nmanual_left', 'reg_nmanual_right', 'reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

#**********************#
#**********************#
#**********************#
#*******fliptricks*****#
#**********************#
#**********************#
#**********************#


reg_ollie = a_class(
	#player armature action name, start, end frames
	'reg_ollie', 1, 40,
	#deck action name, start, end frames  
	'b_reg_ollie', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 36,
	#opposite
	None)


reg_heelflip = a_class(
	#player armature action name, start, end frames
	'reg_heelflip', 0, 20,
	#deck action name, start, end frames  
	'b_reg_heelflip', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)


reg_kickflip = a_class(
	#player armature action name, start, end frames
	'reg_kickflip', 0, 20,
	#deck action name, start, end frames  
	'b_reg_kickflip', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_shuvit = a_class(
	#player armature action name, start, end frames
	'reg_shuvit', 1, 40,
	#deck action name, start, end frames  
	'b_reg_shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_shuvit360 = a_class(
	#player armature action name, start, end frames
	'reg_360shuvit', 1, 20,
	#deck action name, start, end frames  
	'b_reg_360shuvit', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_fsshuvit = a_class(
	#player armature action name, start, end frames
	'reg_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_reg_fsshuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_fsshuvit360 = a_class(
	#player armature action name, start, end frames
	'reg_360fsshuvit', 1, 20,
	#deck action name, start, end frames  
	'b_reg_360fsshuvit', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_hardflip = a_class(
	#player armature action name, start, end frames
	'reg_hardflip', 1, 40,
	#deck action name, start, end frames  
	'b_reg_hardflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_inward_heelflip = a_class(
	#player armature action name, start, end frames
	'reg_inward_heelflip', 1, 40,
	#deck action name, start, end frames  
	'b_reg_inward_heelflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_varial_kickflip = a_class(
	#player armature action name, start, end frames
	'reg_varialkickflip', 1, 40,
	#deck action name, start, end frames  
	'b_reg_varialkickflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_varial_heelflip = a_class(
	#player armature action name, start, end frames
	'reg_varialheelflip', 1, 40,
	#deck action name, start, end frames  
	'b_reg_varialheelflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)



#-----------------------





reg_nollie = a_class(
	#player armature action name, start, end frames
	'nollie', 1, 20,
	#deck action name, start, end frames  
	'b_fak_ollie', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air_nb', 38,
	#opposite
	None)

reg_nollie_kickflip = a_class(
	#player armature action name, start, end frames
	'nollie_kickflip', 1, 20,
	#deck action name, start, end frames  
	'b_fak_kickflip', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air_nb', 38,
	#opposite
	None)

reg_nollie_heelflip = a_class(
	#player armature action name, start, end frames
	'nollie_heelflip', 1, 20,
	#deck action name, start, end frames  
	'b_fak_heelflip', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air_nb', 38,
	#opposite
	None)

reg_nollie_shuvit = a_class(
	#player armature action name, start, end frames
	'nollie_shuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air_nb', 38,
	#opposite
	None)

reg_nollie_fsshuvit = a_class(
	#player armature action name, start, end frames
	'nollie_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_fsshuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air_nb', 38,
	#opposite
	None)

reg_nollie_shuvit360 = a_class(
	#player armature action name, start, end frames
	'nollie_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_360fsshuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)

reg_nollie_fsshuvit_360 = a_class(
	#player armature action name, start, end frames
	'nollie_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_360shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_offboard', 'reg_air_nb'],
	#force exit, frame
	'reg_air', 38,
	#opposite
	None)


#**********************#
#**********************#
#**********************#
#*******grabs**********#
#**********************#
#**********************#
#**********************#

frontside_grab_in = a_class(
	#player armature action name, start, end frames
	'reg_fg', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['frontside_grab'],
	#force exit, frame
	'frontside_grab', 20,
	#opposite
	None)

frontside_grab = a_class(
	#player armature action name, start, end frames
	'reg_fg', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'frontside_grab_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

backside_grab_in = a_class(
	#player armature action name, start, end frames
	'reg_bsg2', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['backside_grab'],
	#force exit, frame
	'backside_grab', 20,
	#opposite
	None)

backside_grab = a_class(
	#player armature action name, start, end frames
	'reg_bsg2', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'backside_grab_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

frontside_nose_grab_in = a_class(
	#player armature action name, start, end frames
	'frontside_nose_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['frontside_nose_grab'],
	#force exit, frame
	'frontside_nose_grab', 20,
	#opposite
	None)

frontside_nose_grab = a_class(
	#player armature action name, start, end frames
	'frontside_nose_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg',  1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'frontside_nose_grab_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

backside_nose_grab_in = a_class(
	#player armature action name, start, end frames
	'backside_nose_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['backside_nose_grab'],
	#force exit, frame
	'backside_nose_grab', 20,
	#opposite
	None)

backside_nose_grab = a_class(
	#player armature action name, start, end frames
	'backside_nose_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'backside_nose_grab_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

frontside_tail_grab_in = a_class(
	#player armature action name, start, end frames
	'frontside_tail_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['frontside_tail_grab'],
	#force exit, frame
	'frontside_tail_grab', 20,
	#opposite
	None)

frontside_tail_grab = a_class(
	#player armature action name, start, end frames
	'frontside_tail_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'frontside_tail_grab_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

backside_tail_grab_in = a_class(
	#player armature action name, start, end frames
	'backside_tail_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['backside_tail_grab'],
	#force exit, frame
	'backside_tail_grab', 20,
	#opposite
	None)

backside_tail_grab = a_class(
	#player armature action name, start, end frames
	'backside_tail_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'backside_tail_grab_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_judo_in = a_class(
	#player armature action name, start, end frames
	'reg_judo', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_judo'],
	#force exit, frame
	'reg_judo', 20,
	#opposite
	None)

reg_judo = a_class(
	#player armature action name, start, end frames
	'reg_judo', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'reg_judo_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_frigid_in = a_class(
	#player armature action name, start, end frames
	'reg_frigid', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_frigid'],
	#force exit, frame
	'reg_frigid', 20,
	#opposite
	None)

reg_frigid = a_class(
	#player armature action name, start, end frames
	'reg_frigid', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'reg_frigid_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_fsonefoot_in = a_class(
	#player armature action name, start, end frames
	'reg_fsonefoot', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_fsonefoot'],
	#force exit, frame
	'reg_fsonefoot', 20,
	#opposite
	None)

reg_fsonefoot = a_class(
	#player armature action name, start, end frames
	'reg_fsonefoot', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'reg_fsonefoot_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_onefoot_in = a_class(
	#player armature action name, start, end frames
	'reg_onefoot', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_onefoot'],
	#force exit, frame
	'reg_onefoot', 20,
	#opposite
	None)

reg_onefoot = a_class(
	#player armature action name, start, end frames
	'reg_onefoot', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'reg_onefoot_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_ollie_north_in = a_class(
	#player armature action name, start, end frames
	'reg_ollie_north', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_ollie_north'],
	#force exit, frame
	'reg_ollie_north', 20,
	#opposite
	None)

reg_ollie_north = a_class(
	#player armature action name, start, end frames
	'reg_ollie_north', 10, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	'reg_ollie_north', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_ollie_south_in = a_class(
	#player armature action name, start, end frames
	'reg_ollie_south', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_ollie_south'],
	#force exit, frame
	'reg_ollie_south', 20,
	#opposite
	None)

reg_ollie_south = a_class(
	#player armature action name, start, end frames
	'reg_ollie_south', 10, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	'reg_ollie_south', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_airwalk_in = a_class(
	#player armature action name, start, end frames
	'reg_airwalk', 1, 10,
	#deck action name, start, end frames  
	'b_reg_airwalk', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_airwalk'],
	#force exit, frame
	'reg_airwalk', 20,
	#opposite
	None)

reg_airwalk = a_class(
	#player armature action name, start, end frames
	'reg_airwalk', 10, 10,
	#deck action name, start, end frames  
	'b_reg_airwalk', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	'reg_airwalk_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_wall_r_in = a_class(
	#player armature action name, start, end frames
	'reg_wall_r', 1, 10,
	#deck action name, start, end frames  
	'b_reg_wall_r', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_wall_r'],
	#force exit, frame
	'reg_wall_r', 20,
	#opposite
	None)

reg_wall_r = a_class(
	#player armature action name, start, end frames
	'reg_wall_r', 10, 10,
	#deck action name, start, end frames  
	'b_reg_wall_r', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 5,
	#intro, length
	'reg_wall_r_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_wall_l_in = a_class(
	#player armature action name, start, end frames
	'reg_wall_l', 1, 10,
	#deck action name, start, end frames  
	'b_reg_wall_l', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_wall_l'],
	#force exit, frame
	'reg_wall_l', 20,
	#opposite
	None)

reg_wall_l = a_class(
	#player armature action name, start, end frames
	'reg_wall_l', 10, 10,
	#deck action name, start, end frames  
	'b_reg_wall_l', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 5,
	#intro, length
	'reg_wall_l_in', 10,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)




#**********************#
#*****fliptricks*******#
#**********************#


reg_back_invert_in = a_class(
	#player armature action name, start, end frames
	'reg_back_invert_in.001', 1, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_5050'],
	#force exit, frame
	'reg_5050', 19,
	#opposite
	'fak_5050')

reg_back_invert = a_class(
	#player armature action name, start, end frames
	'reg_back_invert_in.001', 30, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'reg_back_invert_in', 30,
	#exits
	['reg_air', 'reg_roll'] + reg_fliplist,
	#force exit, frame
	None, 0,
	#opposite
	'fak_5050')



#**********************#
#*******grinds*********#
#**********************#



reg_5050_in = a_class(
	#player armature action name, start, end frames
	'reg_5050', 1, 20,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_5050'],
	#force exit, frame
	'reg_5050', 19,
	#opposite
	'fak_5050')

reg_5050 = a_class(
	#player armature action name, start, end frames
	'reg_5050', 30, 60,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'reg_5050_in', 20,
	#exits
	['reg_air', 'reg_roll'] + reg_fliplist,
	#force exit, frame
	None, 0,
	#opposite
	'fak_5050')

reg_bsboard_in = a_class(
	#player armature action name, start, end frames
	'reg_BS_Board3', 1, 40,
	#deck action name, start, end frames  
	'b_reg_board', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_bsboard'],
	#force exit, frame
	'reg_bsboard', 39,
	#opposite
	'fak_bsboard')

reg_bsboard = a_class(
	#player armature action name, start, end frames
	'reg_BS_Board3', 41, 80,
	#deck action name, start, end frames  
	'b_reg_board', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_bsboard_in', 40,
	#exits
	['reg_air', 'reg_roll'] + reg_fliplist,
	#force exit, frame
	None, 0,
	#opposite
	None)

# reg_fsboard = a_class(
# 	#player armature action name, start, end frames
# 	'reg_FS_Board2', 1, 30,
# 	#deck action name, start, end frames  
# 	'b_reg', 1, 1,
# 	#layer, speed, mode (0 = play, 1 = loop), blendin
# 	1, .5, 1, 15,
# 	#intro, length
# 	None, 0,
# 	#exits
# 	['reg_air', 'reg_roll'],
# 	#force exit, frame
# 	None, 0,
# 	#opposite
# 	None)

reg_noseg_in = a_class(
	#player armature action name, start, end frames
	'reg_noseg.002', 1, 40,
	#deck action name, start, end frames  
	'b_reg_noseg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_noseg'],
	#force exit, frame
	'reg_noseg', 39,
	#opposite
	'fak_noseg')

reg_noseg = a_class(
	#player armature action name, start, end frames
	'reg_noseg.002', 41, 80,
	#deck action name, start, end frames  
	'b_reg_noseg', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_noseg_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_nosegr_in = a_class(
	#player armature action name, start, end frames
	'reg_nosegR', 1, 40,
	#deck action name, start, end frames  
	'b_reg_nosegR', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_nosegr'],
	#force exit, frame
	'reg_nosegr', 39,
	#opposite
	'fak_nosegl')


reg_nosegr = a_class(
	#player armature action name, start, end frames
	'reg_nosegR', 41, 80,
	#deck action name, start, end frames  
	'b_reg_nosegR', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_nosegr_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_nosegl_in = a_class(
	#player armature action name, start, end frames
	'reg_nosegL', 1, 40,
	#deck action name, start, end frames  
	'b_reg_nosegL', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_nosegl'],
	#force exit, frame
	'reg_nosegl', 39,
	#opposite
	'fak_nosegr')

reg_nosegl = a_class(
	#player armature action name, start, end frames
	'reg_nosegL', 41, 80,
	#deck action name, start, end frames  
	'b_reg_nosegL', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_nosegl_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_tailg_in = a_class(
	#player armature action name, start, end frames
	'reg_tailg.001', 1, 40,
	#deck action name, start, end frames  
	'b_reg_tailg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_tailg'],
	#force exit, frame
	'reg_tailg', 39,
	#opposite
	'fak_tailg')

reg_tailg = a_class(
	#player armature action name, start, end frames
	'reg_tailg.001', 41, 80,
	#deck action name, start, end frames  
	'b_reg_tailg', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_tailg_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_tailgr_in = a_class(
	#player armature action name, start, end frames
	'reg_tailgR', 1, 40,
	#deck action name, start, end frames  
	'b_reg_tailgR', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_tailgr'],
	#force exit, frame
	'reg_tailgr', 39,
	#opposite
	'fak_nosegl')

reg_tailgr = a_class(
	#player armature action name, start, end frames
	'reg_tailgR', 41, 80,
	#deck action name, start, end frames  
	'b_reg_tailgR', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_tailgr_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)


reg_tailgl_in = a_class(
	#player armature action name, start, end frames
	'reg_tailgL', 1, 40,
	#deck action name, start, end frames  
	'b_reg_tailgL', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_tailgl'],
	#force exit, frame
	'reg_tailgl', 39,
	#opposite
	'fak_nosegl')

reg_tailgl = a_class(
	#player armature action name, start, end frames
	'reg_tailgL', 41, 80,
	#deck action name, start, end frames  
	'b_reg_tailgL', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_tailgl_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_tailslide_in = a_class(
	#player armature action name, start, end frames
	'fak_noses', 1, 40,
	#deck action name, start, end frames  
	'b_fak_noses', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_tailslide'],
	#force exit, frame
	'reg_tailslide', 39,
	#opposite
	'fak_noseslide')

reg_tailslide = a_class(
	#player armature action name, start, end frames
	'fak_noses', 41, 80,
	#deck action name, start, end frames  
	'b_fak_noses', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_tailslide_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_noseslide_in = a_class(
	#player armature action name, start, end frames
	'fak_tails', 1, 40,
	#deck action name, start, end frames  
	'b_fak_tails', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['reg_noseslide'],
	#force exit, frame
	'reg_noseslide', 39,
	#opposite
	'fak_tailslide')

reg_noseslide = a_class(
	#player armature action name, start, end frames
	'fak_tails', 41, 80,
	#deck action name, start, end frames  
	'b_fak_tails', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'reg_noseslide_in', 40,
	#exits
	['reg_air', 'reg_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)


revert1 = a_class(
	#player armature action name, start, end frames
	'revert1', 1, 10,
	#deck action name, start, end frames  
	'b_revert1', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .3, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'fak_roll'],
	#force exit, frame
	'fak_roll', 11,
	#opposite
	None)

revert2 = a_class(
	#player armature action name, start, end frames
	'revert2', 1, 10,
	#deck action name, start, end frames  
	'b_revert2', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .3, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'fak_roll'],
	#force exit, frame
	'fak_roll', 11,
	#opposite
	None)

fak_revert1 = a_class(
	#player armature action name, start, end frames
	'fak_revert1', 1, 10,
	#deck action name, start, end frames  
	'b_revert1', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .3, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'fak_roll'],
	#force exit, frame
	'reg_roll', 11,
	#opposite
	None)

fak_revert2 = a_class(
	#player armature action name, start, end frames
	'fak_revert2', 1, 10,
	#deck action name, start, end frames  
	'b_revert1', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .3, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'fak_roll'],
	#force exit, frame
	'reg_roll', 11,
	#opposite
	None)

revert3 = a_class(
	#player armature action name, start, end frames
	'revert1', 1, 10,
	#deck action name, start, end frames  
	'b_revert1', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .3, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'fak_roll'],
	#force exit, frame
	'fak_roll', 11,
	#opposite
	None)

revert4 = a_class(
	#player armature action name, start, end frames
	'revert2', 1, 10,
	#deck action name, start, end frames  
	'b_revert2', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .3, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_roll', 'fak_roll'],
	#force exit, frame
	'fak_roll', 11,
	#opposite
	None)







reg_ollie_north_in = a_class(
	#player armature action name, start, end frames
	'reg_ollie_north', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	None, 0,
	#exits
	['reg_ollie_north'],
	#force exit, frame
	'reg_ollie_north', 20,
	#opposite
	None)

reg_dropin_in = a_class(
	#player armature action name, start, end frames
	'reg_dropin3', 30, 50,
	#deck action name, start, end frames  
	'b_reg_dropin', 30, 50,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 15,
	#intro, length
	None, 0,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

reg_dropin = a_class(
	#player armature action name, start, end frames
	'reg_dropin3', 50, 60,
	#deck action name, start, end frames  
	'b_reg_dropin', 50, 60,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	'reg_dropin_in', 20,
	#exits
	['reg_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

















#******************(((((((((((((((((())))))))))))))))))************




















































fak_walk = a_class(
	#player armature action name, start, end frames
	'fak_walk_new', 0, 30,
	#deck action name, start, end frames  
	'b_fak_walk', 0, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	['fak_walkFast', 'fak_idle', 'fak_jump', 'fak_onboard', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle6', 'fak_idle7', 'fak_walk_air', 'fak_walk_air_out'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_walkFast = a_class(
	#player armature action name, start, end frames, start, end frames
	'fak_run', 51, 90,
	#deck action name, start, end frames  
	'b_fak_run', 51, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10, 
	#intro, length
	None, 0,
	#exits
	['fak_walk', 'fak_idle', 'fak_jump', 'fak_onboard'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle = a_class(
	#player armature action name, start, end frames
	'fak_idle1', 1, 120,
	#deck action name, start, end frames  
	'b_fak_idle1', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle5', 'fak_idle6', 'fak_idle7', 'fak_jump', 'fak_sit', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle2 = a_class(
	#player armature action name, start, end frames
	'fak_idle2', 1, 120,
	#deck action name, start, end frames  
	'b_fak_walk', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle', 'fak_idle3', 'fak_idle4', 'fak_idle5', 'fak_idle6', 'fak_idle7', 'fak_jump', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle3 = a_class(
	#player armature action name, start, end frames
	'fak_idle3', 1, 120,
	#deck action name, start, end frames  
	'b_fak_walk', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle2', 'fak_idle', 'fak_idle4', 'fak_idle5', 'fak_idle6', 'fak_idle7', 'fak_jump', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle4 = a_class(
	#player armature action name, start, end frames
	'fak_idle4', 1, 120,
	#deck action name, start, end frames  
	'b_fak_idle4', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle2', 'fak_idle3', 'fak_idle', 'fak_idle5', 'fak_idle6', 'fak_idle7', 'fak_jump', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle5 = a_class(
	#player armature action name, start, end frames
	'fak_idle5', 1, 120,
	#deck action name, start, end frames  
	'b_fak_idle5', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle6', 'fak_idle7', 'fak_idle', 'fak_jump', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle6 = a_class(
	#player armature action name, start, end frames
	'fak_idle6', 1, 120,
	#deck action name, start, end frames  
	'b_fak_idle6', 1, 120,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle7', 'fak_idle', 'fak_jump', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_idle7 = a_class(
	#player armature action name, start, end frames
	'fak_idle7', 1, 120,
	#deck action name, start, end frames  
	'b_fak_idle7', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 20,
	#intro, length
	None, 0,
	#exits
	#['fak_walk', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle6', 'fak_idle', 'fak_jump', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_jump = a_class(
	#player armature action name, start, end frames
	'fak_jump', 1, 10,
	#deck action name, start, end frames  
	'b_fak_jump', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_idle', 'fak_walk_air', 'fak_onboard'],
	#force exit, frame
	'fak_walk_air', 20,
	#opposite
	None)

fak_walk_air = a_class(
	#player armature action name, start, end frames
	'fak_walk_air', 10, 10,
	#deck action name, start, end frames  
	'b_fak_walk_air', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	#['fak_walk_air_out', 'fak_onboard'],
	walk_exits,
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_walk_air_out = a_class(
	#player armature action name, start, end frames
	'fak_walk_air', 10, 40,
	#deck action name, start, end frames  
	'b_fak_walk_air', 10, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1.5, 1, 5,
	#intro, length
	None, 0,
	#exits
	['fak_idle', 'fak_walk', 'fak_walkFast', 'fak_onboard'],
	#force exit, frame
	'fak_idle', 10,
	#opposite
	None)

fak_sit_in = a_class(
	#player armature action name, start, end frames
	'fak_sit', 1, 65,
	#deck action name, start, end frames  
	'b_fak_jump', 1, 65,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['fak_idle', 'fak_walk'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_sit = a_class(
	#player armature action name, start, end frames
	'fak_sit', 65, 65,
	#deck action name, start, end frames  
	'b_fak_jump', 65, 65,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['fak_idle', 'fak_walk'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_onboard = a_class(
	#player armature action name, start, end frames
	'fak_noffboard', 10, 1,
	#deck action name, start, end frames  
	'b_fak_offboard', 10, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	4, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	['fak_roll'],
	#force exit, frame
	'fak_roll', 18,
	#opposite
	None)

fak_offboard = a_class(
	#player armature action name, start, end frames
	'fak_noffboard', 1, 24,
	#'fak_noffboard', 
	#deck action name, start, end frames  
	'b_reg_offboard', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	2, .5, 0, 0,
	#intro, length
	None, 0,
	#exits
	['reg_idle', 'fak_idle', 'fak_walkFast', 'fak_walk', 'fak_onboard'],
	#force exit, frame
	'reg_idle', 24,
	#opposite
	None)

fak_roll = a_class(
	#player armature action name, start, end frames
	'nfak', 1, 60,
	#deck action name, start, end frames  
	'b_reg', 1, 60,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_idle', 'fak_offboard', 'reg_offboard','fak_turnLeft', 'fak_turnRight', 'fak_opos', 'fak_nopos', 'fak_pump', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	'reg_roll')

#not working
fak_turnLeft = a_class(
	#player armature action name, start, end frames
	'nfak_left', 10, 30,
	#deck action name, start, end frames  
	'b_reg_right', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	['fak_turnRight', 'fak_roll', 'fak_opos', 'fak_nopos', 'fak_pump', 'fak_pump_left', 'fak_pump_right', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right', 'reg_turnRight'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_turnRight = a_class(
	#player armature action name, start, end frames
	'nfak_right', 10, 30,
	#deck action name, start, end frames  
	'b_reg_left', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 20,
	#intro, length
	None, 0,
	#exits
	['fak_turnLeft', 'fak_roll', 'fak_opos', 'fak_nopos', 'fak_pump', 'fak_pump_left', 'fak_pump_right', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right', 'reg_turnLeft'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_opos = a_class(
	#player armature action name, start, end frames
	'fak_opos', 1, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_pump', 'fak_pump_left', 'fak_pump_right', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	'reg_opos')

fak_nopos = a_class(
	#player armature action name, start, end frames
	'fak_noposin', 20, 20,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_pump', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	'reg_nopos')

fak_pump = a_class(
	#player armature action name, start, end frames
	'fak_opos', 1, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 15,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_pump_left', 'fak_pump_right', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_pump_left = a_class(
	#player armature action name, start, end frames
	'nfak_pump_left', 10, 30,
	#deck action name, start, end frames  
	'b_reg_right', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_opos', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_pump', 'fak_pump_right', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_pump_right = a_class(
	#player armature action name, start, end frames
	'nfak_pump_right', 10, 30,
	#deck action name, start, end frames  
	'b_reg_left', 10, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_opos', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_pump', 'fak_pump_left', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_push = a_class(
	#player armature action name, start, end frames
	'fak_push', 1, 70,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_push_goof = a_class(
	#player armature action name, start, end frames
	'fak_push_goof', 1, 35,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_powerslide', 'fak_fs_powerslide', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_powerslide_in = a_class(
	#player armature action name, start, end frames
	'nfak_powerslide2', 0, 20,
	#deck action name, start, end frames  
	'b_fak_powerslide2', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_fs_powerslide_in = a_class(
	#player armature action name, start, end frames
	'nfak_powerslide', 0, 20,
	#deck action name, start, end frames  
	'b_fak_powerslide', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	'None', 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_powerslide = a_class(
	#player armature action name, start, end frames
	'nfak_powerslide2', 20, 20,
	#deck action name, start, end frames  
	'b_fak_powerslide2', 20, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	'fak_powerslide_in', 20,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_fs_powerslide = a_class(
	#player armature action name, start, end frames
	'nfak_powerslide', 20, 20,
	#deck action name, start, end frames  
	'b_fak_powerslide', 20, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	'fak_fs_powerslide_in', 20,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_brfoot = a_class(
	#player armature action name, start, end frames
	'fak_brfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual', 'fak_nmanual', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	'fak_air', 20,
	#opposite
	None)

fak_frfoot = a_class(
	#player armature action name, start, end frames
	'fakbfrfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual', 'fak_nmanual', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	'fak_air', 20,
	#opposite
	None)

fak_blfoot = a_class(
	#player armature action name, start, end frames
	'fakflfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual', 'fak_nmanual', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	'fak_air', 20,
	#opposite
	None)


fak_flfoot = a_class(
	#player armature action name, start, end frames
	'fak_flfoot', 1, 30,
	#deck action name, start, end frames  
	'b_brfoot', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .75, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'backside_nose_grab', 'frontside_tail_grab', 'backside_tail_grab', 'fak_manual', 'fak_nmanual', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_manual', 'fak_nmanual', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	'fak_air', 20,
	#opposite
	None)


fak_manual = a_class(
	#player armature action name, start, end frames
	'fak_manual', 10, 90,
	#deck action name, start, end frames  
	'b_fak_manual', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_manual_left = a_class(
	#player armature action name, start, end frames
	'fak_manual_left', 10, 90,
	#deck action name, start, end frames  
	'b_fak_manual_left', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_manual_right = a_class(
	#player armature action name, start, end frames
	'fak_manual_right', 10, 90,
	#deck action name, start, end frames  
	'b_fak_manual_right', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)



fak_nmanual = a_class(
	#player armature action name, start, end frames
	'fak_nmanual', 10, 70,
	#deck action name, start, end frames  
	'b_reg_manual', 10, 70,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_nmanual_left = a_class(
	#player armature action name, start, end frames
	'fak_nmanual_left', 10, 90,
	#deck action name, start, end frames  
	'b_reg_manual_right', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_nmanual_right = a_class(
	#player armature action name, start, end frames
	'fak_nmanual_right', 10, 90,
	#deck action name, start, end frames  
	'b_reg_manual_left', 10, 90,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_roll', 'fak_turnRight', 'fak_turnLeft', 'fak_push', 'fak_push_goof', 'fak_pump', 'fak_nopos', 'fak_opos', 'fak_offboard', 'fak_air', 'fak_air_nose', 'fak_air_tail', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_land = a_class(
	#player armature action name, start, end frames
	'fak_land', 1, 46,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'fak_roll', 20,
	#opposite
	None)

fak_landL = a_class(
	#player armature action name, start, end frames
	'fak_landL', 1, 46,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'fak_roll', 20,
	#opposite
	None)

fak_landR = a_class(
	#player armature action name, start, end frames
	'fak_landR', 1, 46,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'fak_roll', 20,
	#opposite
	None)

fak_landLb = a_class(
	#player armature action name, start, end frames
	'fak_landLb', 1, 46,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'fak_roll', 20,
	#opposite
	None)

fak_landRb = a_class(
	#player armature action name, start, end frames
	'fak_landRb', 1, 46,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	lands,
	#force exit, frame
	'fak_roll', 20,
	#opposite
	None)

fak_air = a_class(
	#player armature action name, start, end frames
	'fak_air', 1, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'fak_frontside_grab', 'fak_backside_grab', 'fak_frontside_nose_grab', 'fak_backside_nose_grab', 'fak_frontside_tail_grab', 'fak_backside_tail_grab', 'fak_air_nose', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_judo', 'fak_frigid', 'fak_onefoot', 'fak_fsonefoot', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right', 'fak_wall_l', 'fak_wall_r'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_air_nb = a_class(
	#player armature action name, start, end frames
	'fak_air', 1, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 30,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 0,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'fak_frontside_grab', 'fak_backside_grab', 'fak_frontside_nose_grab', 'fak_backside_nose_grab', 'fak_frontside_tail_grab', 'fak_backside_tail_grab', 'fak_air_nose', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_judo', 'fak_frigid', 'fak_onefoot', 'fak_fsonefoot', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right', 'fak_wall_l', 'fak_wall_r'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_air_nose = a_class(
	#player armature action name, start, end frames
	'fak_noseg', 5, 5,
	#deck action name, start, end frames  
	'b_fak_noseg', 5, 5,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'fak_frontside_grab', 'fak_backside_grab', 'fak_frontside_nose_grab', 'fak_backside_nose_grab', 'fak_frontside_tail_grab', 'fak_backside_tail_grab', 'fak_air_nose', 'fak_air_tail', 'fak_ollie_north', 'fak_ollie_south', 'fak_judo', 'fak_frigid', 'fak_onefoot', 'fak_fsonefoot', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right', 'fak_wall_l', 'fak_wall_r'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_air_tail = a_class(
	#player armature action name, start, end frames
	'fak_tailg', 35, 35,
	#deck action name, start, end frames  
	'b_fak_tailg', 35, 35,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 1, 20,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_land', 'fak_frontside_grab', 'fak_backside_grab', 'fak_frontside_nose_grab', 'fak_backside_nose_grab', 'fak_frontside_tail_grab', 'fak_backside_tail_grab', 'fak_manual', 'fak_nmanual', 'fak_air_nose', 'fak_ollie_north', 'fak_ollie_south', 'fak_manual_left', 'fak_manual_right', 'fak_nmanual_left', 'fak_nmanual_right'],
	#force exit, frame
	None, 0,
	#opposite
	None)

#**********************#
#**********************#
#**********************#
#*******fliptricks*****#
#**********************#
#**********************#
#**********************#

fak_ollie = a_class(
	#player armature action name, start, end frames
	'fak_ollie', 1, 20,
	#deck action name, start, end frames  
	'b_fak_ollie', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air', 30,
	#opposite
	None)


fak_heelflip = a_class(
	#player armature action name, start, end frames
	'fak_heelflip', 0, 20,
	#deck action name, start, end frames  
	'b_fak_heelflip', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)


fak_kickflip = a_class(
	#player armature action name, start, end frames
	'fak_kickflip', 0, 20,
	#deck action name, start, end frames  
	'b_fak_kickflip', 0, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_shuvit = a_class(
	#player armature action name, start, end frames
	'fak_shuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air', 38,
	#opposite
	None)

fak_shuvit360 = a_class(
	#player armature action name, start, end frames
	'fak_360shuvit', 1, 20,
	#deck action name, start, end frames  
	'b_fak_360shuvit', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air', 38,
	#opposite
	None)

fak_fsshuvit = a_class(
	#player armature action name, start, end frames
	'fak_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_fsshuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air', 38,
	#opposite
	None)

fak_fsshuvit360 = a_class(
	#player armature action name, start, end frames
	'fak_360fsshuvit', 1, 20,
	#deck action name, start, end frames  
	'b_fak_360fsshuvit', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air', 38,
	#opposite
	None)

fak_hardflip = a_class(
	#player armature action name, start, end frames
	'fak_hardflip', 1, 40,
	#deck action name, start, end frames  
	'b_fak_hardflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_inward_heelflip = a_class(
	#player armature action name, start, end frames
	'fak_inward_heelflip', 1, 40,
	#deck action name, start, end frames  
	'b_fak_inward_heelflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)


fak_varial_kickflip = a_class(
	#player armature action name, start, end frames
	'fak_varialkickflip', 1, 40,
	#deck action name, start, end frames  
	'b_fak_varialkickflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_varial_heelflip = a_class(
	#player armature action name, start, end frames
	'fak_varialheelflip', 1, 40,
	#deck action name, start, end frames  
	'b_fak_varialheelflip', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

#-----------------------





fak_nollie = a_class(
	#player armature action name, start, end frames
	'fak_nollie', 1, 40,
	#deck action name, start, end frames  
	'b_reg_ollie', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_nollie_kickflip = a_class(
	#player armature action name, start, end frames
	'fak_nollie_kickflip', 1, 20,
	#deck action name, start, end frames  
	'b_reg_kickflip', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_nollie_heelflip = a_class(
	#player armature action name, start, end frames
	'fak_nollie_heelflip', 1, 20,
	#deck action name, start, end frames  
	'b_reg_heelflip', 1, 20,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, .5, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 30,
	#opposite
	None)

fak_nollie_shuvit = a_class(
	#player armature action name, start, end frames
	'fak_nollie_shuvit', 1, 40,
	#deck action name, start, end frames  
	'b_reg_fsshuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	3, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air', 38,
	#opposite
	None)

fak_nollie_fsshuvit = a_class(
	#player armature action name, start, end frames
	'fak_nollie_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_reg_shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_nollie_shuvit360 = a_class(
	#player armature action name, start, end frames
	'fak_nollie_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_reg_360shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)

fak_nollie_fsshuvit_360 = a_class(
	#player armature action name, start, end frames
	'fak_nollie_fsshuvit', 1, 40,
	#deck action name, start, end frames  
	'b_fak_360shuvit', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	fliplay, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_offboard', 'fak_air_nb'],
	#force exit, frame
	'fak_air_nb', 38,
	#opposite
	None)


#**********************#
#**********************#
#**********************#
#*******grabs**********#
#**********************#
#**********************#
#**********************#

fak_frontside_grab_in = a_class(
	#player armature action name, start, end frames
	'fak_fg', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_frontside_grab'],
	#force exit, frame
	'fak_frontside_grab', 20,
	#opposite
	None)

fak_frontside_grab = a_class(
	#player armature action name, start, end frames
	'fak_fg', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_frontside_grab_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_backside_grab_in = a_class(
	#player armature action name, start, end frames
	'fak_bg', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_backside_grab'],
	#force exit, frame
	'fak_backside_grab', 20,
	#opposite
	None)

fak_backside_grab = a_class(
	#player armature action name, start, end frames
	'fak_bg', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_backside_grab_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_frontside_nose_grab_in = a_class(
	#player armature action name, start, end frames
	'fak_frontside_nose_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_frontside_nose_grab'],
	#force exit, frame
	'fak_frontside_nose_grab', 20,
	#opposite
	None)

fak_frontside_nose_grab = a_class(
	#player armature action name, start, end frames
	'fak_frontside_nose_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg',  1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_frontside_nose_grab_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_backside_nose_grab_in = a_class(
	#player armature action name, start, end frames
	'fak_backside_nose_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_backside_nose_grab'],
	#force exit, frame
	'fak_backside_nose_grab', 20,
	#opposite
	None)

fak_backside_nose_grab = a_class(
	#player armature action name, start, end frames
	'fak_backside_nose_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_backside_nose_grab_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_frontside_tail_grab_in = a_class(
	#player armature action name, start, end frames
	'fak_frontside_tail_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_frontside_tail_grab'],
	#force exit, frame
	'fak_frontside_tail_grab', 20,
	#opposite
	None)

fak_frontside_tail_grab = a_class(
	#player armature action name, start, end frames
	'fak_frontside_tail_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_frontside_tail_grab_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_backside_tail_grab_in = a_class(
	#player armature action name, start, end frames
	'fak_backside_tail_grab', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_backside_tail_grab'],
	#force exit, frame
	'backside_tail_grab', 20,
	#opposite
	None)

fak_backside_tail_grab = a_class(
	#player armature action name, start, end frames
	'fak_backside_tail_grab', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_backside_tail_grab_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_judo_in = a_class(
	#player armature action name, start, end frames
	'fak_judo', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_judo'],
	#force exit, frame
	'fak_judo', 20,
	#opposite
	None)

fak_judo = a_class(
	#player armature action name, start, end frames
	'fak_judo', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_judo_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_frigid_in = a_class(
	#player armature action name, start, end frames
	'fak_frigid', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_frigid'],
	#force exit, frame
	'fak_frigid', 20,
	#opposite
	None)

fak_frigid = a_class(
	#player armature action name, start, end frames
	'fak_frigid', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_frigid_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_fsonefoot_in = a_class(
	#player armature action name, start, end frames
	'fak_fsonefoot', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_fsonefoot'],
	#force exit, frame
	'fak_fsonefoot', 20,
	#opposite
	None)

fak_fsonefoot = a_class(
	#player armature action name, start, end frames
	'fak_fsonefoot', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_fsonefoot_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_onefoot_in = a_class(
	#player armature action name, start, end frames
	'fak_onefoot', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .4, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_onefoot'],
	#force exit, frame
	'fak_onefoot', 20,
	#opposite
	None)

fak_onefoot = a_class(
	#player armature action name, start, end frames
	'fak_onefoot', 10, 30,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 10,
	#intro, length
	'fak_onefoot_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_ollie_north_in = a_class(
	#player armature action name, start, end frames
	'fak_ollie_north', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	None, 0,
	#exits
	['fak_ollie_north'],
	#force exit, frame
	'fak_ollie_north', 20,
	#opposite
	None)

fak_ollie_north = a_class(
	#player armature action name, start, end frames
	'fak_ollie_north', 10, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	'fak_ollie_north', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)


fak_ollie_south_in = a_class(
	#player armature action name, start, end frames
	'fak_ollie_south', 1, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	None, 0,
	#exits
	['fak_ollie_south'],
	#force exit, frame
	'fak_ollie_south', 20,
	#opposite
	None)

fak_ollie_south = a_class(
	#player armature action name, start, end frames
	'fak_ollie_south', 10, 10,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 5,
	#intro, length
	'fak_ollie_south', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)




#**********************#
#*****fliptricks*******#
#**********************#






#**********************#
#*******grinds*********#
#**********************#






fak_5050_in = a_class(
	#player armature action name, start, end frames
	'fak_5050', 1, 40,
	#deck action name, start, end frames  
	'b_reg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_5050'],
	#force exit, frame
	'fak_5050', 39,
	#opposite
	'fak_5050')

fak_5050 = a_class(
	#player armature action name, start, end frames
	'fak_5050', 41, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_5050_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	'reg_5050')


fak_bsboard_in = a_class(
	#player armature action name, start, end frames
	'fak_BS_Board2', 1, 40,
	#deck action name, start, end frames  
	'b_fak_bsboard', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_bsboard'],
	#force exit, frame
	'fak_bsboard', 39,
	#opposite
	'fak_bsboard')

fak_bsboard = a_class(
	#player armature action name, start, end frames
	'fak_BS_Board2', 41, 80,
	#deck action name, start, end frames  
	'b_fak_bsboard', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_bsboard_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_fsboard_in = a_class(
	#player armature action name, start, end frames
	'fak_FS_Board2', 1, 40,
	#deck action name, start, end frames  
	'b_fak_bsboard', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_fsboard'],
	#force exit, frame
	'fak_fsboard', 39,
	#opposite
	'fak_fsboard')

fak_fsboard = a_class(
	#player armature action name, start, end frames
	'fak_FS_Board2', 41, 80,
	#deck action name, start, end frames  
	'b_reg', 1, 1,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_fsboard_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_noseg_in = a_class(
	#player armature action name, start, end frames
	'fak_noseg', 1, 40,
	#deck action name, start, end frames  
	'b_fak_noseg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_noseg'],
	#force exit, frame
	'fak_noseg', 39,
	#opposite
	'fak_noseg')

fak_noseg = a_class(
	#player armature action name, start, end frames
	'fak_noseg', 41, 80,
	#deck action name, start, end frames  
	'b_fak_noseg', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_noseg_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_nosegr_in = a_class(
	#player armature action name, start, end frames
	'fak_nosegR', 1, 40,
	#deck action name, start, end frames  
	'b_fak_nosegR', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_nosegr'],
	#force exit, frame
	'fak_nosegr', 39,
	#opposite
	'fak_nosegr')

fak_nosegr = a_class(
	#player armature action name, start, end frames
	'fak_nosegR', 41, 80,
	#deck action name, start, end frames  
	'b_fak_nosegR', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_nosegr', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_nosegl_in = a_class(
	#player armature action name, start, end frames
	'fak_nosegL', 1, 40,
	#deck action name, start, end frames  
	'b_fak_nosegL', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_nosegl'],
	#force exit, frame
	'fak_nosegl', 39,
	#opposite
	'fak_nosegl')

fak_nosegl = a_class(
	#player armature action name, start, end frames
	'fak_nosegL', 41, 80,
	#deck action name, start, end frames  
	'b_fak_nosegL', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_nosegl', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_tailg_in = a_class(
	#player armature action name, start, end frames
	'fak_tailg', 1, 40,
	#deck action name, start, end frames  
	'b_fak_tailg', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_tailg'],
	#force exit, frame
	'fak_tailg', 39,
	#opposite
	'fak_tailg')

fak_tailg = a_class(
	#player armature action name, start, end frames
	'fak_tailg', 41, 80,
	#deck action name, start, end frames  
	'b_fak_tailg', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_tailg_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_tailgr_in = a_class(
	#player armature action name, start, end frames
	'fak_tailgR', 1, 40,
	#deck action name, start, end frames  
	'b_fak_tailgR', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_tailgr'],
	#force exit, frame
	'fak_tailgr', 39,
	#opposite
	'fak_tailgr')

fak_tailgr = a_class(
	#player armature action name, start, end frames
	'fak_tailgR', 41, 80,
	#deck action name, start, end frames  
	'b_fak_tailgR', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_tailgr_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_tailgl_in = a_class(
	#player armature action name, start, end frames
	'fak_tailgL', 1, 40,
	#deck action name, start, end frames  
	'b_fak_tailgL', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_tailgl'],
	#force exit, frame
	'fak_tailgl', 39,
	#opposite
	'fak_tailgl')

fak_tailgl = a_class(
	#player armature action name, start, end frames
	'fak_tailgL', 41, 80,
	#deck action name, start, end frames  
	'b_fak_tailgL', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_tailgl_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_tailslide_in = a_class(
	#player armature action name, start, end frames
	'reg_noses', 1, 40,
	#deck action name, start, end frames  
	'b_reg_noses', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_tailslide'],
	#force exit, frame
	'fak_tailslide', 39,
	#opposite
	'fak_tailslide')

fak_tailslide = a_class(
	#player armature action name, start, end frames
	'reg_noses', 41, 80,
	#deck action name, start, end frames  
	'b_reg_noses', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_tailslide_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_noseslide_in = a_class(
	#player armature action name, start, end frames
	'reg_tails', 1, 40,
	#deck action name, start, end frames  
	'b_reg_tails', 1, 40,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, 1, 0, 10,
	#intro, length
	None, 0,
	#exits
	['fak_noseslide'],
	#force exit, frame
	'fak_noseslide', 39,
	#opposite
	'fak_noseslide')

fak_noseslide = a_class(
	#player armature action name, start, end frames
	'reg_tails', 41, 80,
	#deck action name, start, end frames  
	'b_reg_tails', 41, 80,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 15,
	#intro, length
	'fak_noseslide_in', 40,
	#exits
	['fak_air', 'fak_roll'],
	#force exit, frame
	None, 0,
	#opposite
	None)



fak_wall_r_in = a_class(
	#player armature action name, start, end frames
	'fak_wall_r', 1, 10,
	#deck action name, start, end frames  
	'b_reg_wall_l', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	['fak_wall_r'],
	#force exit, frame
	'fak_wall_r', 20,
	#opposite
	None)

fak_wall_r = a_class(
	#player armature action name, start, end frames
	'fak_wall_r', 10, 10,
	#deck action name, start, end frames  
	'b_reg_wall_l', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 5,
	#intro, length
	'fak_wall_r_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)

fak_wall_l_in = a_class(
	#player armature action name, start, end frames
	'fak_wall_l', 1, 10,
	#deck action name, start, end frames  
	'b_reg_wall_r', 1, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 0, 5,
	#intro, length
	None, 0,
	#exits
	['fak_wall_l'],
	#force exit, frame
	'fak_wall_l', 20,
	#opposite
	None)

fak_wall_l = a_class(
	#player armature action name, start, end frames
	'fak_wall_l', 10, 10,
	#deck action name, start, end frames  
	'b_reg_wall_r', 10, 10,
	#layer, speed, mode (0 = play, 1 = loop), blendin
	1, .5, 1, 5,
	#intro, length
	'fak_wall_l_in', 10,
	#exits
	['fak_air'],
	#force exit, frame
	None, 0,
	#opposite
	None)
