import bge
cont = bge.logic.getCurrentController()
own = cont.owner
dict = bge.logic.globalDict

lLR = dict['lLR']
lUD = dict['lUD']
rLR = dict['rLR']
rUD = dict['rUD']
lTrig = dict['lTrig']
rTrig = dict['rTrig']
aBut = dict['aBut']
bBut = dict['bBut']
xBut = dict['xBut']
yBut = dict['yBut']
lBump = dict['lBump']
rBump = dict['rBump']
bkBut = dict['bkBut']
stBut = dict['stBut']
xbBut = dict['xbBut']
ltsBut = dict['ltsBut']
rtsBut = dict['rtsBut']
ldPad = dict['ldPad']
rdPad = dict['rdPad']
udPad = dict['udPad']
ddPad = dict['ddPad']

#print(lUD)
axis_sens = .06
#print(aBut)
lasta = own["lasta"]

if lUD > axis_sens: down = 1
else: down = 0    
if lUD < -axis_sens: 
    up = 1 
    #print("up")
else: up = 0    
#print(down, up)
if lLR > axis_sens: right = 1
else: right = 0 
if lLR < -axis_sens: left = 1
else: left = 0 

fileName = "Settings.dat"
scenes = bge.logic.getSceneList()
#print(scenes)
main_scene = [scene for scene in scenes if scene.name=="main"][0]
main_empty = main_scene.objects['Empty']

def main():

    cont = bge.logic.getCurrentController()
    own = cont.owner
    #down = cont.sensors["down"]
    #up = cont.sensors["up"]
    #bBut = cont.sensors["bBut"]
    #aBut = cont.sensors["aBut"]
    pstate = own["pstate"]
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
#    resume = scene.objects["resume"]
#    settings = scene.objects["settings"]
    #level = scene.objects["level"]
#    dict['level'] = level
#    levelselect = scene.objects["levelselect"]
#    restart = scene.objects["restart"]
#    exit = scene.objects["exit"]
#    submenu_on = own["submenu_on"]
#    level_text = scene.objects["level_text"]
#    level_text.resolution = 8
    lastup = own["lastup"]
    lastdown = own["lastdown"]
    lastleft = own["lastleft"]
    lastright = own["lastright"]    
    lastpstate = own["lastpstate"]
    playspeed = 2
    #sSelect = cont.actuators["sSelect"]
    cont.deactivate(own.actuators['restart'])                                        
    #joy down
#    if down == 1 and lastdown != 1:
#        #own.stopAction(0)
#        pstate = own["pstate"]
#                    
#        if pstate == 0 and submenu_on == 0:
#            resume.playAction("p_resume", 10,0, layer=1, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 1 and submenu_on == 0:
#            settings.playAction("p_settings", 10,0, layer=1, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 2 and submenu_on == 0:
#            level.playAction("p_level", 10,0, layer=1, priority=0, play_mode=0, speed=playspeed)
#            #level.playAction("p_level_select", 10,0, layer=1, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 3 and submenu_on == 0:
#            restart.playAction("p_restart", 10,0, layer=1, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 4 and submenu_on == 0:
#            exit.playAction("p_exit", 10,0, layer=1, priority=0, play_mode=0, speed=playspeed)           
#            
#        if pstate < 4 and submenu_on == 0:
#            own["pstate"] = own["pstate"] + 1
#        if pstate == 4 and submenu_on == 0:
#            own["pstate"] = 0    
#        pstate = own["pstate"]
#        #print(pstate)   
#        cont.activate(own.actuators['sSelect']) 
#    #joy up    
#    if up == 1 and lastup != 1:
#        pstate = own["pstate"]
#                    
#        if pstate == 0 and submenu_on == 0:
#            resume.playAction("p_resume", 10,0, layer=0, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 1 and submenu_on == 0:
#            settings.playAction("p_settings", 10,0, layer=0, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 2 and submenu_on == 0:
#            level.playAction("p_level", 10,0, layer=0, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 3 and submenu_on == 0:
#            restart.playAction("p_restart", 10,0, layer=0, priority=0, play_mode=0, speed=playspeed)
#        if pstate == 4 and submenu_on == 0:
#            exit.playAction("p_exit", 10,0, layer=0, priority=0, play_mode=0, speed=playspeed)
#            
#            
#        if pstate > 0 and submenu_on == 0:
#            own["pstate"] = own["pstate"] - 1
#        if pstate == 0 and submenu_on == 0:
#            own["pstate"] = 4    
#        pstate = own["pstate"]
#        #print(pstate)    
#        cont.activate(own.actuators['sSelect'])  
#    if right == 1 and lastright !=1:
#        cont.activate(own.actuators['sSelect']) 
#    if left == 1 and lastleft != 1:    
#        cont.activate(own.actuators['sSelect'])
        
#level*****************************************************

#    if own["submenu"] == "level":
#        #print("submenu is level")
#        active_level_select = own["active_level_select"]
#        
#        import Settings
#        if left == 1 and lastleft != 1:
#            if active_level_select >= 0:
#               active_level_select -= 1
#            if active_level_select < 0:
#                active_level_select = 8    
#            #if active_level_select == -1:    
#                #active_level_select = 0
#        #print(active_level_select)        
#            #move up
#            #play up            
#        if right == 1 and lastright != 1:    
#            if active_level_select < 9:
#               active_level_select += 1               
#            if active_level_select == 9:
#                active_level_select = 0
#            if active_level_select == -1:    
#                active_level_select = 0
#        #print(active_level_select)                        
#            #move down
#            #play down
#        if active_level_select == 0:
#            level_text.text = "Empty Lot"
#        if active_level_select == 1:
#            level_text.text = "Waterfall"
#        if active_level_select == 2:
#            level_text.text = "Warehouse" 
#        if active_level_select == 3:
#            level_text.text = "City"          
#        if active_level_select == 4:
#            level_text.text = "Training"          
#        if active_level_select == 5:
#            level_text.text = "Spine" 
#        if active_level_select == 6:
#            level_text.text = "Apricot Park" 
#        if active_level_select == 7:
#            level_text.text = "Newday Bowl"                           
#        if active_level_select == 8:
#            level_text.text = "Garage 18"                                                                      
#        own["active_level_select"] = active_level_select 
#        load_timer = 100
##indoor
#        if aBut == False and active_level_select == 0 and lasta == 1:
#            level_text.text = ""
#            cont.activate(own.actuators['loading']) 
#            level = "lot"
#            main_empty["level"] = "lot"
#            own["level"] = level
#            Settings.writeSettings()
#            #print("not")
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart'])
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel() 
#            dict['load_timer'] = load_timer
#        else: 
#            
#            cont.deactivate(own.actuators['restart']) 
##woodbowl               
#        if aBut == False and active_level_select == 1 and lasta == 1:
#            level_text.text = ""
#            #cont.activate(own.actuators['loading']) 
#            level = "mini_scene"
#            main_empty["level"] = "mini_scene"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart']) 
##mini        
#        if aBut == False and active_level_select == 2 and lasta == 1:
#            level_text.text = ""
#            #cont.activate(own.actuators['loading']) 
#            level = "warehouse"
#            main_empty["level"] = "warehouse"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart'])
##level3        
#        if aBut == False and active_level_select == 3 and lasta == 1:
#            level_text.text = ""
#            level = "city2"
#            main_empty["level"] = "city2"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart']) 
##hubbas rail        
#        if aBut == False and active_level_select == 4 and lasta == 1:
#            level_text.text = ""
#            level = "train"
#            main_empty["level"] = "train"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart']) 
##training        
#        if aBut == False and active_level_select == 5 and lasta == 1:
#            level_text.text = ""
#            level = "spine"
#            main_empty["level"] = "spine"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart'])  
#        if aBut == False and active_level_select == 6 and lasta == 1:
#            level_text.text = ""
#            level = "gt"
#            main_empty["level"] = "gt"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart']) 
#        if aBut == False and active_level_select == 7 and lasta == 1:
#            level_text.text = ""
#            level = "newday"
#            main_empty["level"] = "newday"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#        else: cont.deactivate(own.actuators['restart']) 
#        if aBut == False and active_level_select == 8 and lasta == 1:
#            level_text.text = ""
#            level = "garage18"
#            main_empty["level"] = "garage18"
#            own["level"] = level
#            Settings.writeSettings()
#            Settings.readSettings()
#            #cont.activate(own.actuators['restart']) 
#            #Settings.loadlevel()
#            dict['load_timer'] = load_timer
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1
#        else: cont.deactivate(own.actuators['restart'])                                               
#    else:
#        own["active_level_select"] = -1 
#    if bBut == True:    
#        levelselect.playAction("p_level_select", 10,0, layer=3, priority=0, play_mode=0, speed=playspeed)                                         
#**********************************************************   



 

#    if pstate == 0 and submenu_on == 0 and lastpstate != 0:
#        resume.playAction("p_resume", -10,10, layer=0, priority=0, play_mode=0, speed=playspeed)
#    if pstate == 1 and submenu_on == 0 and lastpstate != 1:
#        settings.playAction("p_settings", -10,10, layer=0, priority=0, play_mode=0, speed=playspeed)
#    if pstate == 2 and submenu_on == 0 and lastpstate != 2:
#        level.playAction("p_level", -10,10, layer=0, priority=0, play_mode=0, speed=playspeed)
#        #level.playAction("p_level_select", -10,10, layer=0, priority=0, play_mode=0, speed=playspeed)
#    if pstate == 3 and submenu_on == 0 and lastpstate != 3:
#        restart.playAction("p_restart", -10,10, layer=0, priority=0, play_mode=0, speed=playspeed)
#    if pstate == 4 and submenu_on == 0 and lastpstate != 4:
#        exit.playAction("p_exit", -10,10, layer=0, priority=0, play_mode=0, speed=playspeed)                


    #find the scene and object
    scenes = bge.logic.getSceneList()
    for scene in scenes :
        #if scene.name == 'lot' or scene.name == 'train' or scene.name == 'mini_scene' or scene.name == 'city' or scene.name == 'city2' or scene.name == 'spine':
        #if scene.name == main_empty["level"]: 
        if scene.name == 'lot':    
            #print("*****stop sounds")
            cc = scene.objects['control_cube.002']
            cc.actuators["sroll"].volume = .0001
            #cc.deactivate(own.actuators["sroll"])
            cc.actuators["sroll"].stopSound()            
 
    #print(pstate) 
    own["lastup"] = up
    own["lastdown"] = down 
    own["lastleft"] = left
    own["lastright"] = right 
    own["lasta"] = aBut 
    own["lastpstate"] = own["pstate"]
main()
