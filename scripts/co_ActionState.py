import bge
import actionsFSM
import actionPlayer

def main():

    cont = bge.logic.getCurrentController()
    own = cont.owner
#     scene = bge.logic.getCurrentScene()
#     skater = scene.objects['Char4']
#     deck = scene.objects['deck']
#     trucks = scene.objects['trucks']
#     bdeck = scene.objects['deck_arm']
#     loop_layer = 2
#     trans_layer = 3
#     flip_layer = 4
#     throw_layer = 5
#     trans_playing = skater.isPlayingAction(trans_layer)  
#     #set possible requestActions
#     requestActions_list = ['reg_roll', 'fak_roll', 'reg_turnLeft', 'reg_turnLeft_out', 'reg_turnRight', 'reg_turnRight_out', 'fak_turnLeft', 'fak_turnRight', 'reg_pump', 'fak_pump', 'reg_land', 'reg_landL', 'reg_landR',  'reg_landLb', 'reg_landRb', 'fak_land', 'fak_landL', 'fak_landR',  'fak_landLb', 'fak_landRb',  'reg_idle', 'reg_idle_nb', 'fak_idle', 'fak_idle_nb', 'reg_walk', 'reg_walk_nb', 'reg_walkFast', 'reg_walkFast_nb', 'fak_walk', 'fak_walk_nb', 'fak_walkFast', 'fak_walkFast_nb', 'reg_pump', 'fak_pump', 'reg_opos', 'fak_opos', 'reg_nopos', 'fak_nopos', 'reg_manual', 'fak_manual', 'reg_nmanual', 'fak_nmanual', 'reg_air', 'fak_air', 'frontside_grab', 'backside_grab', 'fak_backside_grab', 'fak_frontside_grab', 'frontside_nose_grab', 'frontside_tail_grab', 'backside_nose_grab', 'backside_tail_grab', 'fak_frontside_nose_grab', 'fak_frontside_tail_grab', 'fak_backside_nose_grab', 'fak_backside_tail_grab', 'reg_noseg', 'reg_air_nose', 'fak_air_nose', 'reg_air_tail', 'fak_air_tail', 'reg_nosegr','reg_nosegl',  'fak_noseg','fak_nosegr', 'fak_nosegl', 'reg_nosegl', 'fak_tailg', 'fak_tailgr', 'fak_tailgl', 'reg_tailg', 'reg_tailgr', 'reg_tailgl', 'reg_tailslide', 'fak_tailslide', 'reg_noseslide', 'fak_noseslide', 'nose_stall','tail_stall', 'reg_5050', 'fak_5050', 'reg_tailg', 'reg_bsboard', 'fak_bsboard', 'reg_fsboard', 'reg_powerslide', 'fak_powerslide', 'reg_fs_powerslide', 'fak_fs_powerslide', 'reg_stop', 'fak_stop', 'fak_fp_rback', 'reg_fp_rback', 'reg_back_invert', 'fak_fr_invert', 'reg_wall_l', 'reg_wall_r', 'fak_wall_l', 'fak_wall_r', 'reg_hippy_in', 'fak_hippy_in', 'reg_dropin_pos', 'fak_dropin_pos', 'reg_idle1', 'reg_idle2', 'reg_idle3', 'reg_idle4', 'reg_idle5', 'reg_idle6', 'reg_idle7', 'fak_idle1', 'fak_idle2', 'fak_idle3', 'fak_idle4', 'fak_idle5', 'fak_idle6', 'fak_idle7', 'reg_idle2_nb', 'reg_ollie_north', 'fak_ollie_north', 'reg_ollie_south', 'fak_ollie_south', 'reg_sit', 'fak_sit', 'reg_walk_air', 'fak_walk_air', 'reg_judo', 'fak_judo', 'reg_frigid', 'reg_fsonefoot', 'reg_onefoot', 'fak_frigid', 'fak_fsonefoot', 'fak_onefoot', 'reg_airwalk', 'reg_pump_left', 'reg_pump_right', 'fak_pump_left', 'fak_pump_right']
#     oneshotActions_list = ['reg_jump', 'fak_jump', 'reg_onboard', 'fak_onboard', 'reg_dropin', 'fak_dropin', 'reg_land', 'reg_landL', 'reg_landR',  'reg_landLb', 'reg_landRb',  'fak_land', 'fak_landL', 'fak_landR',  'fak_landLb', 'fak_landRb',   'reg_push', 'fak_push', 'reg_push_goof', 'fak_push_goof', 'reg_manual_revert_ccw', 'revert1', 'revert2', 'revert3', 'revert4', 'fak_revert1', 'fak_revert2', 'fak_revert3', 'fak_revert4', 'reg_ollie', 'reg_nollie', 'reg_nollie', 'fak_ollie', 'fak_nollie', 'reg_kickflip', 'fak_kickflip', 'reg_varial_kickflip', 'fak_varial_kickflip', 'reg_nollie_varial_kickflip', 'fak_nollie_varial_kickflip', 'reg_nollie_varial_heelflip', 'fak_nollie_varial_heelflip', 'reg_varial_heelflip', 'fak_varial_heelflip', 'reg_nollie_kickflip', 'fak_nollie_kickflip', 'reg_heelflip', 'fak_heelflip', 'reg_nollie_heelflip', 'fak_nollie_heelflip', 'reg_shuvit', 'fak_shuvit', 'reg_shuvit360', 'fak_shuvit360', 'reg_fsshuvit360', 'fak_fsshuvit360', 'reg_nollie_shuvit', 'fak_nollie_shuvit', 'reg_fsshuvit', 'fak_fsshuvit',  'reg_nollie_fsshuvit', 'fak_nollie_fsshuvit', 'reg_nollie_shuvit360', 'fak_nollie_shuvit360', 'reg_nollie_fsshuvit', 'fak_nollie_fsshuvit', 'reg_turnRight_out', 'reg_turnLeft_out', 'fak_turnRight_out', 'fak_turnLeft_out', 'reg_opos_out', 'fak_opos_out', 'reg_nopos_out', 'fak_nopos_out', 'reg_pump_out', 'fak_pump_out', 'reg_powerslide_out', 'fak_powerslide_out', 'reg_fs_powerslide_out', 'fak_fs_powerslide_out', 'reg_stop_out', 'fak_stop_out', 'reg_noseslide_out', 'fak_noseslide_out', 'reg_tailslide_out', 'fak_tailslide_out', 'reg_5050_out', 'fak_5050_out', 'reg_tailg_out', 'fak_tailg_out', 'reg_noseg_out', 'fak_noseg_out', 'reg_tailgr_out', 'fak_tailgr_out', 'reg_nosegr_out', 'fak_nosegr_out', 'reg_tailgl_out', 'fak_tailgl_out', 'reg_nosegl_out', 'fak_nosegl_out', 'reg_brfoot', 'reg_frfoot', 'reg_blfoot', 'reg_flfoot', 'fak_brfoot', 'fak_frfoot', 'fak_blfoot', 'fak_flfoot', 'fak_hippy', 'reg_hippy', 'reg_hippy_nccw', 'fak_hippy_nccw', 'reg_hippy_ncw', 'fak_hippy_ncw', 'reg_manual_revert_cw', 'fak_manual_revert_cw', 'reg_manual_revert_ccw', 'fak_manual_revert_ccw', 'reg_nmanual_revert_cw', 'fak_nmanual_revert_cw', 'reg_nmanual_revert_ccw', 'fak_nmanual_revert_ccw', 'fak_fp_rback', 'reg_fp_rback', 'reg_back_invert_out', 'fak_fr_invert_out', 'fak-reg_roll', 'reg-fak_roll', 'fak-reg_5050', 'reg-fak_5050', 'fak-reg_noseslide', 'reg-fak_tailslide', 'reg-fak_noseslide', 'fak-reg_tailslide', 'fak-reg_tailslide', 'reg-fak_noseslide' , 'reg_manual_out', 'fak_manual_out', 'reg_nmanual_out', 'fak_nmanual_out', 'reg-fak_nmanual', 'fak-reg_nmanual', 'reg-fak_manual', 'fak-reg_manual', 'reg_air-roll', 'fak_air-roll', 'reg_roll-air', 'fak_roll-air', 'reg_5050-roll', 'fak_5050-roll', 'reg_back_invert_out', 'fak_fr_invert_out', 'reg_offboard', 'fak_offboard', 'reg_throw', 'fak_throw', 'reg_fall1', 'fak_fall1', 'reg_air_tail to manual', 'fak_air_tail to manual', 'reg_air_nose to nmanual', 'fak_air_nose to nmanual', 'reg_ollie_north_out', 'fak_ollie_north_out', 'reg_ollie_south_out', 'fak_ollie_south_out', 'reg_sit_out', 'fak_sit_out', 'frontside_grab_out', 'backside_grab_out', 'fak_backside_grab_out', 'fak_frontside_grab_out', 'frontside_nose_grab_out', 'frontside_tail_grab_out', 'backside_nose_grab_out', 'backside_tail_grab_out', 'fak_frontside_nose_grab_out', 'fak_frontside_tail_grab_out', 'fak_backside_nose_grab_out', 'fak_backside_tail_grab_out', 'reg_walk_air_out', 'fak_walk_air_out', 'reg_air-walk_air', 'fak_air-walk_air', 'reg_judo_out', 'fak_judo_out', 'reg_frigid_out', 'reg_fsonefoot_out', 'reg_onefoot_out', 'fak_frigid_out', 'fak_fsonefoot_out', 'fak_onefoot_out', 'reg_airwalk_out', 'reg_inward_heelflip', 'fak_inward_heelflip', 'reg_hardflip', 'fak_hardflip', 'reg_nollie_inward_heelflip', 'fak_nollie_inward_heelflip', 'reg_nollie_hardflip', 'fak_nollie_hardflip', 'reg_nollie_fsshuvit_360', 'fak_nollie_fsshuvit_360', 'reg_pump_left_out', 'reg_pump_right_out', 'fak_pump_left_out', 'fak_pump_right_out', 'reg_pump_left-left', 'reg_pump_right-right', 'fak_pump_left-left', 'fak_pump_right-right', 'reg_left-pump', 'reg_right-pump', 'fak_left-pump', 'fak_right-pump', 'reg_pump_right-rightb', 'reg_pump_left-leftb', 'fak_pump_right-rightb', 'fak_pump_left-leftb', 'reg_fak_noseg', 'fak_reg_tailg', 'reg_fak_tailg', 'fak_reg_noseg', 'reg_fak_nosegl', 'fak_reg_tailgl', 'reg_fak_tailgl', 'fak_reg_nosegl', 'reg_fak_nosegr', 'fak_reg_tailgr', 'reg_fak_tailgr', 'fak_reg_nosegr']

#     jump_overrideList = ['reg_ollie', 'reg_nollie', 'reg_nollie', 'fak_ollie', 'fak_nollie', 'reg_kickflip', 'fak_kickflip', 'reg_varial_kickflip', 'fak_varial_kickflip', 'reg_nollie_varial_kickflip', 'fak_nollie_varial_kickflip', 'reg_nollie_varial_heelflip', 'fak_nollie_varial_heelflip', 'reg_varial_heelflip', 'fak_varial_heelflip', 'reg_nollie_kickflip', 'fak_nollie_kickflip', 'reg_heelflip', 'fak_heelflip', 'reg_nollie_heelflip', 'fak_nollie_heelflip', 'reg_shuvit', 'fak_shuvit', 'reg_shuvit360', 'fak_shuvit360', 'reg_fsshuvit360', 'fak_fsshuvit360', 'reg_nollie_shuvit', 'fak_nollie_shuvit', 'reg_fsshuvit', 'fak_fsshuvit',  'reg_nollie_fsshuvit', 'fak_nollie_fsshuvit', 'reg_nollie_shuvit360', 'fak_nollie_shuvit360', 'reg_nollie_fsshuvit', 'fak_nollie_fsshuvit', 'reg_brfoot', 'reg_frfoot', 'reg_blfoot', 'reg_flfoot', 'fak_brfoot', 'fak_frfoot', 'fak_blfoot', 'fak_flfoot', 'reg_manual_revert_ccw', 'revert1', 'revert2', 'revert3', 'revert4', 'fak_revert1', 'fak_revert2', 'fak_revert3', 'fak_revert4', 'fak_hippy', 'reg_hippy', 'reg_hippy_nccw', 'fak_hippy_nccw', 'reg_hippy_ncw', 'fak_hippy_ncw', 'reg_manual_revert_cw', 'fak_manual_revert_cw', 'reg_manual_revert_ccw', 'fak_manual_revert_ccw',  'reg_nmanual_revert_cw', 'fak_nmanual_revert_cw', 'reg_nmanual_revert_ccw', 'fak_nmanual_revert_ccw', 'reg_offboard', 'fak_offboard', 'reg_onboard', 'fak_onboard', 'reg_inward_heelflip', 'fak_inward_heelflip', 'reg_hardflip', 'fak_hardflip', 'reg_nollie_inward_heelflip', 'fak_nollie_inward_heelflip', 'reg_nollie_hardflip', 'fak_nollie_hardflip', 'reg_nollie_fsshuvit_360', 'fak_nollie_fsshuvit_360', 'reg_fak_noseg', 'fak_reg_tailg', 'reg_fak_tailg', 'fak_reg_noseg', 'reg_pump_right_out', 'reg_pump_left_out', 'reg_turnRight_out', 'reg_turnLeft_out', 'reg_pump_right-rightb', 'reg_pump_right-right', 'reg_pump_left-left', 'reg_pump_left-leftb', 'fak_pump_right_out', 'fak_pump_left_out', 'fak_turnRight_out', 'fak_turnLeft_out', 'fak_pump_right-rightb', 'fak_pump_right-right', 'fak_pump_left-left', 'fak_pump_left-leftb', 'reg_pump_out', 'fak_pump_out']   
    
#     pump_out_overList = ['reg_nosegr','reg_nosegl',  'fak_noseg','fak_nosegr', 'fak_nosegl', 'reg_nosegl', 'fak_tailg', 'fak_tailgr', 'fak_tailgl', 'reg_tailg', 'reg_tailgr', 'reg_tailgl', 'reg_tailslide', 'fak_tailslide', 'reg_noseslide', 'fak_noseslide', 'nose_stall','tail_stall', 'reg_5050', 'fak_5050', 'reg_tailg', 'reg_bsboard', 'fak_bsboard', 'reg_fsboard'] 
    
#     reg_fliplist = ['reg_ollie', 'reg_nollie', 'reg_nollie', 'reg_kickflip', 'reg_varial_kickflip', 'reg_nollie_varial_kickflip', 'reg_nollie_varial_heelflip', 'fak_nollie_varial_heelflip', 'reg_varial_heelflip', 'reg_nollie_kickflip',  'reg_heelflip', 'reg_nollie_heelflip', 'reg_shuvit', 'reg_shuvit360', 'reg_fsshuvit360', 'reg_nollie_shuvit', 'fak_nollie_shuvit', 'reg_fsshuvit', 'fak_fsshuvit',  'reg_nollie_fsshuvit', 'reg_nollie_shuvit360', 'reg_nollie_fsshuvit', 'reg_brfoot', 'reg_frfoot', 'reg_blfoot', 'reg_flfoot', 'reg_inward_heelflip', 'reg_hardflip', 'reg_nollie_inward_heelflip', 'reg_nollie_hardflip', 'reg_offboard', 'reg_nollie_fsshuvit_360'] 
#     fak_fliplist = ['fak_ollie', 'fak_nollie', 'fak_nollie', 'fak_kickflip', 'fak_varial_kickflip', 'fak_nollie_varial_kickflip', 'fak_nollie_varial_heelflip', 'fak_nollie_varial_heelflip', 'fak_varial_heelflip', 'fak_nollie_kickflip',  'fak_heelflip','fak_nollie_heelflip', 'fak_shuvit', 'fak_shuvit360', 'fak_fsshuvit360', 'fak_nollie_shuvit', 'fak_nollie_shuvit', 'fak_fsshuvit', 'fak_fsshuvit',  'fak_nollie_fsshuvit', 'fak_nollie_shuvit360', 'fak_nollie_fsshuvit', 'fak_brfoot', 'fak_frfoot', 'fak_blfoot', 'fak_flfoot', 'fak_inward_heelflip', 'fak_hardflip', 'fak_nollie_inward_heelflip', 'fak_nollie_hardflip', 'fak_offboard', 'fak_nollie_fsshuvit_360']     

#     #initialize variables
#     try:
#         actionState = own['actionState']
#         l_actionState = own['l_actionState']
#         requestAction = own['requestAction']
#         og_request = requestAction
#         l_requestAction = own['l_requestAction']
#         queueAction = own['queueAction']
#         actionTimer = own['actionTimer']
#     except:
#         own['actionState'] = 'reg_walk'
#         own['l_actionState'] = 'empty'  
#         og_request = 'empty'              
#         own['requestAction'] = 'reg_walk'    
#         own['l_requestAction'] = 'empty'
#         own['queueAction'] = 'empty'
#         own['actionTimer'] = 0
#         own['clear_request'] = 0
#         actionTimer = 0
#         queueAction = 'empty'
#         requestAction = 'reg_walk'
#         actionState = 'empty'
#         l_actionState = 'empty'
#         l_requestAction = 'empty'
        
#     skater.playAction('blink', 0,100, layer=7, play_mode=0, speed=.25)
#     #print('reqqqqqqqqes1', requestAction)
#     def stopActs():
#         skater.stopAction(loop_layer)
#         trucks.stopAction(loop_layer)
#         deck.stopAction(loop_layer)
#         skater.stopAction(trans_layer)
#         trucks.stopAction(trans_layer)
#         deck.stopAction(trans_layer)
#         actionTimer = 0
#         own['actionTimer'] = 0   
                  
#     if requestAction == 'empty':
#         #print('*******empty request****')
#         pass
#     isplaying = skater.isPlayingAction(flip_layer)
#     transPlaying = skater.isPlayingAction(trans_layer)
        
#     if own['clear_request'] == 1:
#         requestAction = 'empty'
#         own['clear_request'] = 0    
#     if own['actionTimer'] != 0 and transPlaying == False and isplaying == False:
#         own['actionTimer'] = 0
#         actionTimer = 0
#         print('resetting actionTimer')
        
#     if own['actionTimer'] != 0 and (own['actionState'] in ['reg_land', 'fak_land', 'reg_landL', 'reg_landR',  'reg_landLb', 'reg_landRb' 'fak_landL', 'fak_landR',  'fak_landLb', 'fak_landRb', ]) and own['requestAction'] not in reg_fliplist and own['requestAction'] not in fak_fliplist:
#         requestAction = 'empty'
#         own['requestAction'] = 'empty'              
# #================
#     #stance change detection
#     if (requestAction == 'reg_roll' and l_actionState in ('fak_roll', 'fak_land', 'fak_landL', 'fak_landLb', 'fak_landR', 'fak_landRb', 'fak_pump_out', 'fak_nopos_out', 'fak_opos_out', 'fak_air-roll', 'fak_fs_powerslide_out', 'fak_bs_powerslide_out', 'fak_nmanual_out', 'fak_manual_out', 'fak_tailslide_out', 'fak_noseslide_out', 'fak_5050_out')) or l_actionState == 'fak_land' and (requestAction in ['reg_turnLeft', 'reg_turnRight', 'reg_turnLeft_out', 'reg_turnRight_out', 'reg_opos', 'reg_nopos', 'reg_pump', 'reg_push', 'reg_push_goof', 'fak_5050-roll']) and requestAction not in ['reg_push', 'reg_push_goof', 'fak_push', 'fak_push_goof'] or (requestAction in ('reg_turnLeft', 'reg_turnRight') and l_actionState in ('fak_turnLeft', 'fak_turnRight')):
#         #print('fak-reg_roll')
#         requestAction = 'fak-reg_roll'
#         own['requestAction'] = 'fak-reg_roll' 
#     if (requestAction == 'fak_roll' and l_actionState in ('reg_roll', 'reg_land', 'reg_landL', 'reg_landR',  'reg_landLb', 'reg_landRb',  'reg_pump_out', 'reg_nopos_out', 'reg_opos_out', 'reg_air-roll', 'reg_fs_powerslide_out', 'reg_bs_powerslide_out', 'reg_nmanual_out', 'reg_manual_out', 'reg_tailslide_out', 'reg_noseslide_out', 'reg_5050_out')) or l_actionState in ['reg_land', 'reg_landL', 'reg_landR'] and (requestAction in ['fak_turnLeft', 'fak_turnRight', 'fak_turnLeft_out', 'fak_turnRight_out', 'fak_opos', 'fak_nopos', 'fak_pump', 'fak_push', 'reg_push_goof', 'reg_5050-roll']) and requestAction not in ['reg_push', 'reg_push_goof', 'fak_push', 'fak_push_goof'] or (requestAction in ('fak_turnLeft', 'fak_turnRight') and l_actionState in ('reg_turnLeft', 'reg_turnRight')):
#         #print('reg-fak_roll')
#         requestAction = 'reg-fak_roll'
#         own['requestAction'] = 'reg-fak_roll'
    
#     if requestAction in ['reg_pump'] and l_actionState in ['fak_roll']:
#         requestAction = 'fak-reg_roll'
#     if requestAction in ['fak_pump'] and l_actionState in ['reg_roll']:
#         requestAction = 'reg-fak_roll'   
    
#     if l_actionState == 'reg_roll' and requestAction in ['fak_pump', 'fak_pump_left', 'fak_pump_right', 'fak_opos', 'fak_nopos', 'fak_turnLeft', 'fak_turnRight']:
#         requestAction = 'reg-fak_roll'
#     if l_actionState == 'fak_roll' and requestAction in ['reg_pump', 'reg_pump_left', 'reg_pump_right', 'reg_opos', 'reg_nopos', 'reg_turnLeft', 'reg_turnRight']:
#         requestAction = 'fak-reg_roll'                     
    
    
#     if requestAction in ['reg_push', 'reg_push_goof', 'reg_turnRight', 'reg_turnLeft'] and l_actionState == 'fak_land':
#         requestAction = 'fak-reg_roll'
#     if requestAction in ['fak_push', 'fak_push_goof', 'fak_turnRight', 'fak_turnLeft'] and l_actionState in ['reg_land', 'reg_landL', 'reg_landR',  'reg_landLb', 'reg_landRb']:
#         requestAction = 'reg-fak_roll'         
    
#     if requestAction == 'reg_5050' and (l_actionState == 'fak_5050' or l_actionState == 'fak_air' or l_actionState in fak_fliplist) or (l_actionState == 'fak-reg_5050' and actionTimer > 1):
#         #print('fak-reg_5050')
#         requestAction = 'fak-reg_5050'
#         own['requestAction'] = 'fak-reg_5050'
#     if requestAction == 'fak_5050' and (l_actionState == 'reg_5050' or l_actionState == 'reg_air' or l_actionState in reg_fliplist) or (l_actionState == 'reg-fak_5050' and actionTimer > 1):
#         #print('reg-fak_5050')
#         requestAction = 'reg-fak_5050'
#         own['requestAction'] = 'reg-fak_5050'                                

#     if requestAction in ['reg_offboard', 'fak_offboard', 'reg_onboard', 'fak_onboard']:
#         own['actionTimer'] = 0
#         actionTimer = 0
#     if l_actionState == 'reg_dropin_pos' and requestAction != 'reg_dropin_pos':
#         own['requestAction'] = 'reg_dropin'
#         requestAction = 'reg_dropin'
#     if l_actionState == 'fak_dropin_pos' and requestAction != 'fak_dropin_pos':
#         own['requestAction'] = 'fak_dropin'
#         requestAction = 'fak_dropin'        

# #===============        
#     #land action over ride
#     if (requestAction in ['reg_land', 'reg_landL', 'reg_landR', 'fak_land',  'reg_landLb', 'reg_landRb',  'fak_landL', 'fak_landR',  'fak_landLb', 'fak_landRb']) and own['grindHit'] == True:
#         if l_requestAction in ['reg_land', 'fak_land', 'reg_landL', 'reg_landR']:
#             pass
#         else:    
#             requestAction = l_requestAction
#             own['requestAction'] = requestAction
#             own['l_actionState'] = requestAction
#             actionState = requestAction
#             own['actionTimer'] = 0
#             actionTimer = 0 
            
#     if requestAction == 'reg_land' and l_actionState == 'reg_back_invert_out' and actionTimer > 0:
#         requestAction = 'reg_back_invert_out' 
#     if requestAction == 'fak_land' and l_actionState == 'fak_fr_invert_out' and actionTimer > 0:
#         requestAction = 'fak_fr_invert_out'                
                       
        
#     #dirt invert fix
#     if 'invert' in requestAction:
#         actionTimer = 0
#         own['actionTimer'] = 0    
    
    
#     #dirty stall fix
#     fixer = 0
#     if requestAction in ['reg_5050', 'reg_tailslide', 'reg_noseslide'] and actionState in ['reg_pump_out', 'fak_nopos_out'] and actionTimer > 10:
#         fixer = 1 
#     if requestAction in ['fak_5050', 'fak_tailslide', 'fak_noseslide'] and actionState in ['fak_pump_out', 'fak_nopos_out'] and actionTimer > 10:
#         fixer = 1             
        
#     if fixer == 1:    
#         actionTimer = 0
#         own['actionTimer'] = 0  
#         stopActs()              
    
#     if requestAction == 'revert1' or requestAction == 'revert2' or requestAction == 'revert3' or requestAction == 'revert4' or requestAction == 'fak_revert1' or requestAction == 'fak_revert2' or requestAction == 'fak_revert3' or requestAction == 'fak_revert4':
#         actionState = requestAction
#         own['actionState'] = requestAction 
#         if l_requestAction != 'revert1' and l_requestAction != 'revert2' and l_requestAction != 'revert3' and l_requestAction != 'revert4' and l_requestAction != 'fak_revert1' and l_requestAction != 'fak_revert2' and l_requestAction != 'fak_revert3' and l_requestAction != 'fak_revert4':   
#             #print('stopping revert layers')     
#             skater.stopAction(loop_layer)
#             trucks.stopAction(loop_layer)
#             deck.stopAction(loop_layer)
#             skater.stopAction(trans_layer)
#             trucks.stopAction(trans_layer)
#             deck.stopAction(trans_layer)  
            
#     if 'wall' in requestAction and actionTimer < 10:
#         actionTimer = 0
#         own['actionTimer'] = 0
#         skater.stopAction(flip_layer)
#         trucks.stopAction(flip_layer)
#         deck.stopAction(flip_layer)
#         skater.stopAction(trans_layer)
#         trucks.stopAction(trans_layer)
#         deck.stopAction(trans_layer)        
                                   
            
#     #check last actionState to see if an out action is needed    
#     if requestAction not in jump_overrideList:

#         # if l_actionState == 'fak_turnRight' and requestAction != 'fak_turnRight':
#         #     if requestAction not in ['fak_turnRight', 'fak_pump_right', 'fak_pump']:
#         #         requestAction = 'fak_turnRight_out'
#         #         actionState = 'fak_turnRight_out'
#         #     elif requestAction == 'fak_pump_right':
#         #         requestAction = 'fak_right-pump'    
#         # if l_actionState == 'fak_turnLeft' and requestAction != 'fak_turnLeft':
#         #     if requestAction not in ['fak_turnLeft', 'fak_pump_left', 'fak_pump']:
#         #         requestAction = 'fak_turnLeft_out'
#         #         actionState = 'fak_turnLeft_out'                        
#         #     elif requestAction == 'fak_pump_left':
#         #         requestAction = 'fak_left-pump'    
                
        
#         #-----
#         #need to do something different for nopos
#         # if requestAction == 'reg_opos':
#         #     requestAction = 'reg_pump'
#         # if requestAction == 'fak_opos':
#         #     requestAction = 'fak_pump'
                        
#         #-----
        
                     

#         if l_actionState == 'reg_opos' and requestAction != 'reg_opos':
#             requestAction = 'reg_opos_out'
#             actionState = 'reg_opos_out' 
#         if l_actionState == 'fak_opos' and requestAction != 'fak_opos':
#             requestAction = 'fak_opos_out'
#             actionState = 'fak_opos_out' 
#         if l_actionState == 'reg_nopos' and requestAction != 'reg_nopos':
#             requestAction = 'reg_nopos_out'
#             actionState = 'reg_nopos_out' 
#         if l_actionState == 'fak_nopos' and requestAction != 'fak_nopos':
#             requestAction = 'fak_nopos_out'
#             actionState = 'fak_nopos_out' 
# #*********************************            

#         # if l_actionState == 'fak_turnLeft' and requestAction == 'fak_pump_left':
#         #     requestAction = 'fak_pump_left-left'
#         #     actionState = 'fak_pump_left-left'
#         # if l_actionState == 'fak_turnRight' and requestAction == 'fak_pump_right':
#         #     requestAction = 'fak_pump_right-right'
#         #     actionState = 'fak_pump_right-right'                        
            
# #**********************************            

#         if l_actionState == 'reg_pump' and requestAction == 'reg_5050':
#             requestAction = 'reg_5050'
#             actionState = 'reg_5050'
#         if l_actionState == 'fak_pump' and requestAction == 'fak_5050':
#             requestAction = 'fak_5050'
#             actionState = 'fak_5050'                
#         # if l_actionState == 'fak_pump' and requestAction not in ['fak_pump', 'fak_pump_right', 'fak_pump_left'] and requestAction not in pump_out_overList:
#         #     requestAction = 'fak_pump_out'
#         #     actionState = 'fak_pump_out'            
            
#         if l_actionState == 'reg_powerslide' and requestAction != 'reg_powerslide':
#             requestAction = 'reg_powerslide_out'
#             actionState = 'reg_powerslide_out' 
#         if l_actionState == 'fak_powerslide' and requestAction != 'fak_powerslide':
#             requestAction = 'fak_powerslide_out'
#             actionState = 'fak_powerslide_out'
#         if l_actionState == 'reg_fs_powerslide' and requestAction != 'reg_fs_powerslide':
#             requestAction = 'reg_fs_powerslide_out'
#             actionState = 'reg_fs_powerslide_out' 
#         if l_actionState == 'fak_fs_powerslide' and requestAction != 'fak_fs_powerslide':
#             requestAction = 'fak_fs_powerslide_out'
#             actionState = 'fak_fs_powerslide_out' 
#         if l_actionState == 'reg_stop' and requestAction != 'reg_stop':
#             requestAction = 'reg_stop_out'
#             actionState = 'reg_stop_out' 
#         if l_actionState == 'fak_stop' and requestAction != 'fak_stop':
#             requestAction = 'fak_stop_out'
#             actionState = 'fak_stop_out' 
#         if l_actionState == 'reg_noseslide' and requestAction != 'reg_noseslide' and requestAction != 'fak_tailslide':
#             requestAction = 'reg_noseslide_out'
#             actionState = 'reg_noseslide_out'
#         if l_actionState == 'fak_noseslide' and requestAction != 'fak_noseslide' and requestAction != 'reg_tailslide':
#             requestAction = 'fak_noseslide_out'
#             actionState = 'fak_noseslide_out'
#         if l_actionState == 'reg_tailslide' and requestAction != 'reg_tailslide' and requestAction != 'fak_noseslide':
#             requestAction = 'reg_tailslide_out'
#             actionState = 'reg_tailslide_out'     
#         if l_actionState == 'fak_tailslide' and requestAction != 'fak_tailslide' and requestAction != 'reg_noseslide':
#             requestAction = 'fak_tailslide_out'
#             actionState = 'fak_tailslide_out' 
#         if l_actionState == 'reg_5050' and requestAction != 'reg_5050' and requestAction != 'reg-fak_5050':
#             requestAction = 'reg_5050_out'
#             actionState = 'reg_5050_out' 
#         if l_actionState == 'fak_5050' and requestAction != 'fak_5050' and requestAction != 'fak-reg_5050':
#             requestAction = 'fak_5050_out'
#             actionState = 'fak_5050_out'            
#         if l_actionState == 'reg_tailg' and requestAction not in ['reg_tailg', 'fak_noseg']:
#             requestAction = 'reg_tailg_out'
#             actionState = 'reg_tailg_out'
            
             
#         if l_actionState == 'fak_tailg' and requestAction not in ['fak_tailg', 'reg_noseg']:
#             requestAction = 'fak_tailg_out'
#             actionState = 'fak_tailg_out'   
#         if l_actionState == 'reg_noseg' and requestAction not in ['reg_noseg', 'fak_tailg']:
#             requestAction = 'reg_noseg_out'
#             actionState = 'reg_noseg_out'
            
             
#         if l_actionState == 'fak_noseg' and requestAction not in ['fak_noseg', 'reg_tailg']:
#             requestAction = 'fak_noseg_out'
#             actionState = 'fak_noseg_out'                                                         
#         if l_actionState == 'reg_tailgr' and requestAction not in ['reg_tailgr', 'fak_nosegr']:
#             requestAction = 'reg_tailgr_out'
#             actionState = 'reg_tailgr_out' 
#         if l_actionState == 'fak_tailgr' and requestAction not in ['fak_tailgr', 'reg_nosegr']:
#             requestAction = 'fak_tailgr_out'
#             actionState = 'fak_tailgr_out'   
#         if l_actionState == 'reg_nosegr' and requestAction not in ['reg_nosegr', 'fak_tailgr']:
#             requestAction = 'reg_nosegr_out'
#             actionState = 'reg_nosegr_out' 
#         if l_actionState == 'fak_nosegr' and requestAction not in ['fak_nosegr', 'reg_tailgr']:
#             requestAction = 'fak_nosegr_out'
#             actionState = 'fak_nosegr_out'             
#         if l_actionState == 'reg_tailgl' and requestAction not in ['reg_tailgl', 'fak_nosegl']:
#             requestAction = 'reg_tailgl_out'
#             actionState = 'reg_tailgl_out' 
#         if l_actionState == 'fak_tailgl' and requestAction not in ['fak_tailgl', 'reg_nosegl']:
#             requestAction = 'fak_tailgl_out'
#             actionState = 'fak_tailgl_out'   
#         if l_actionState == 'reg_nosegl' and requestAction not in ['reg_nosegl', 'fak_tailgl']:
#             requestAction = 'reg_nosegl_out'
#             actionState = 'reg_nosegl_out' 
#         if l_actionState == 'fak_nosegl' and requestAction not in ['fak_nosegl', 'reg_tailgl']:
#             requestAction = 'fak_nosegl_out'
#             actionState = 'fak_nosegl_out' 
            
            
            
#         if l_actionState == 'reg_back_invert' and requestAction != 'reg_back_invert':
#             requestAction = 'reg_back_invert_out'
#             actionState = 'reg_back_invert_out'
#         if l_actionState == 'fak_fr_invert' and requestAction != 'fak_fr_invert':
#             requestAction = 'fak_fr_invert_out'
#             actionState = 'fak_fr_invert_out'  
            
#         if l_actionState == 'reg_sit' and requestAction != 'reg_sit':
#             requestAction = 'reg_sit_out'
#             actionState = 'reg_sit_out'
#         if l_actionState == 'fak_sit' and requestAction != 'fak_sit':
#             requestAction = 'fak_sit_out'
#             actionState = 'fak_sit_out'                            
                
#         if l_actionState == 'fak_nmanual_out' and requestAction == 'fak_nmanual':
#             actionTimer = 0
#             own['actionTimer'] = 0
#             requestAction = 'fak_nmanual' 
#             skater.stopAction(trans_layer)
#             trucks.stopAction(trans_layer)
#             deck.stopAction(trans_layer)  
#         if l_actionState == 'fak_manual_out' and requestAction == 'fak_manual':
#             actionTimer = 0
#             own['actionTimer'] = 0
#             requestAction = 'fak_manual' 
#             skater.stopAction(trans_layer)
#             trucks.stopAction(trans_layer)
#             deck.stopAction(trans_layer) 
#         if l_actionState == 'reg_nmanual_out' and requestAction == 'reg_nmanual':
#             actionTimer = 0
#             own['actionTimer'] = 0
#             requestAction = 'reg_nmanual' 
#             skater.stopAction(trans_layer)
#             trucks.stopAction(trans_layer)
#             deck.stopAction(trans_layer)  
#         if l_actionState == 'reg_manual_out' and requestAction == 'reg_manual':
#             actionTimer = 0
#             own['actionTimer'] = 0
#             requestAction = 'reg_manual' 
#             skater.stopAction(trans_layer)
#             trucks.stopAction(trans_layer)
#             deck.stopAction(trans_layer)                                                     
        
#         if l_actionState == 'reg_air_tail' and requestAction == 'reg_5050':
#             requestAction = 'reg_tailg'
#         if l_actionState == 'fak_air_tail' and requestAction == 'fak_5050':
#             requestAction = 'fak_tailg'
#         if l_actionState == 'reg_air_nose' and requestAction == 'reg_5050':
#             requestAction = 'reg_noseg'
#         if l_actionState == 'fak_air_nose' and requestAction == 'fak_5050':
#             requestAction = 'fak_noseg'                        
                    
#         if (l_actionState in ('reg_air', 'reg_dropin', 'reg_air_nose', 'reg_air_tail', 'reg_5050')) and requestAction == 'reg_roll' and actionTimer == 0:
#             requestAction = 'reg_air-roll'
#             actionState = 'reg_air-roll'  
#         if (l_actionState in ('fak_air', 'fak_dropin', 'fak_air_nose', 'fak_air_tail', 'fak_5050')) and requestAction == 'fak_roll' and actionTimer == 0:
#             requestAction = 'fak_air-roll'
#             actionState = 'fak_air-roll' 

#         if l_actionState in ('reg_roll') and requestAction == 'reg_air':
#             requestAction = 'reg_roll-air'
#         if l_actionState in ('fak_roll') and requestAction == 'fak_air':
#             requestAction = 'fak_roll-air'    


                
#         if l_actionState == 'reg_dropin_pos' and requestAction != 'reg_dropin_pos':
#             requestAction = 'reg_dropin'
#             actionState = 'reg_dropin'  
#         if l_actionState == 'fak_dropin_pos' and requestAction != 'fak_dropin_pos':
#             requestAction = 'fak_dropin'
#             actionState = 'fak_dropin'             
             
#         if (l_actionState == 'reg_5050_out' and requestAction == 'reg_roll'):        
#             requestAction = 'reg_5050-roll'
#             actionState = 'reg_5050-roll' 
        
#         if (l_actionState == 'fak_5050_out' and requestAction == 'fak_roll'): 
#             requestAction = 'fak_5050-roll'
#             actionState = 'fak_5050-roll' 

# #####
            
#         if l_actionState == 'fak_air_tail' and requestAction in ('reg_roll', 'reg_manual', 'fak_land'):
#             requestAction = 'fak_air_tail to manual'

#         if l_actionState == 'reg_air_tail' and requestAction in ('fak_manual', 'fak_roll', 'reg_land'):
#             requestAction = 'reg_air_tail to manual'

#         if l_actionState == 'fak_air_nose' and requestAction in ('reg_roll', 'reg_nmanual', 'fak_land'):
#             requestAction = 'fak_air_nose to nmanual'
            
#         if l_actionState == 'reg_air_nose' and requestAction in ('fak_roll', 'fak_nmanual', 'reg_land'):
#             requestAction = 'reg_air_nose to nmanual'


# ####


#         #print(requestAction)
#         if l_actionState == 'reg_air_tail to manual' and requestAction in ['fak_nmanual', 'fak_manual']:
#             requestAction = 'reg-fak_nmanual'
#         if l_actionState == 'fak_air_tail to manual' and requestAction in ['reg_nmanual', 'reg_manual']:
#             requestAction = 'fak-reg_nmanual' 
#         if l_actionState == 'fak_air_tail to nmanual' and requestAction in ['reg_nmanual', 'reg_manual']:
#             requestAction = 'fak-reg_manual'   
            
#         if l_actionState == 'reg_air_nose to nmanual' and requestAction in ['fak_manual', 'fak_nmanual']:
#             requestAction = 'reg-fak_manual'
#         if l_actionState == 'fak_air_nose to nmanual' and requestAction in ['reg_manual', 'reg_nmanual']:
#             requestAction = 'fak-reg_manual' 
                        
#         if l_actionState in ['reg_manual'] and requestAction != 'reg_manual':
#             if requestAction == 'fak_nmanual':
#                 requestAction = 'reg-fak_nmanual'
#                 actionState = 'reg-fak_nmanual'
#             else:    
#                 requestAction = 'reg_manual_out'
#                 actionState = 'reg_manual_out' 
#         if l_actionState in ['fak_manual'] and requestAction != 'fak_manual':
#             if requestAction == 'reg_nmanual':
#                 requestAction = 'fak-reg_nmanual'
#                 actionState = 'fak-reg_manual'            
#             else:    
#                 requestAction = 'fak_manual_out'
#                 actionState = 'fak_manual_out'                        
#         if l_actionState in ['reg_nmanual'] and requestAction != 'reg_nmanual':
#             if requestAction == 'fak_manual':
#                 requestAction = 'reg-fak_manual'
#                 actionState = 'reg-fak_manual'
#             else:    
#                 requestAction = 'reg_nmanual_out'
#                 actionState = 'reg_nmanual_out' 
                
#         if l_actionState in ['fak_nmanual'] and requestAction != 'fak_nmanual':
#             if requestAction == 'reg_manual':
#                 requestAction = 'fak-reg_manual'
#                 actionState = 'fak-reg_manual'
#             else:
#                 requestAction = 'fak_nmanual_out'
#                 actionState = 'fak_nmanual_out'                   
            
#         if l_actionState == 'reg_ollie_north' and requestAction != 'reg_ollie_north':
#             requestAction = 'reg_ollie_north_out'
#             actionState = 'reg_ollie_north_out'
#         if l_actionState == 'fak_ollie_north' and requestAction != 'fak_ollie_north':
#             requestAction = 'fak_ollie_north_out'
#             actionState = 'fak_ollie_north_out'  
#         if l_actionState == 'reg_ollie_south' and requestAction != 'reg_ollie_south':
#             requestAction = 'reg_ollie_south_out'
#             actionState = 'reg_ollie_south_out'
#         if l_actionState == 'fak_ollie_south' and requestAction != 'fak_ollie_south':
#             requestAction = 'fak_ollie_south_out'
#             actionState = 'fak_ollie_south_out'  

#         if l_actionState == 'reg_air_nose to nmanual' and requestAction in  ['fak_roll', 'reg_roll']:
#             requestAction = 'reg_nmanual_out'
#         if l_actionState == 'fak_air_nose to nmanual' and requestAction in  ['reg_roll', 'fak_roll']:
#             requestAction = 'fak_nmanual_out'                                   
#         if l_actionState == 'reg_air_tail to manual' and requestAction in  ['fak_roll', 'reg_roll']:
#             requestAction = 'reg_manual_out'
#         if l_actionState == 'fak_air_tail to manual' and requestAction in ['reg_roll', 'fak_roll']:
#             requestAction = 'fak_manual_out'            

#         if l_actionState == 'frontside_grab' and requestAction != 'frontside_grab':
#             requestAction = 'frontside_grab_out'
#             actionState = 'frontside_grab_out'
#         if l_actionState == 'backside_grab' and requestAction != 'backside_grab':
#             requestAction = 'backside_grab_out'
#             actionState = 'backside_grab_out'            
#         if l_actionState == 'fak_frontside_grab' and requestAction != 'fak_frontside_grab':
#             requestAction = 'fak_frontside_grab_out'
#             actionState = 'fak_frontside_grab_out'
#         if l_actionState == 'fak_backside_grab' and requestAction != 'fak_backside_grab':
#             requestAction = 'fak_backside_grab_out'
#             actionState = 'fak_backside_grab_out'  

#         if l_actionState == 'frontside_nose_grab' and requestAction != 'frontside_nose_grab':
#             requestAction = 'frontside_nose_grab_out'
#             actionState = 'frontside_nose_grab_out'
#         if l_actionState == 'backside_nose_grab' and requestAction != 'backside_nose_grab':
#             requestAction = 'backside_nose_grab_out'
#             actionState = 'backside_nose_grab_out'            
#         if l_actionState == 'fak_frontside_nose_grab' and requestAction != 'fak_frontside_nose_grab':
#             requestAction = 'fak_frontside_nose_grab_out'
#             actionState = 'fak_frontside_nose_grab_out'
#         if l_actionState == 'fak_backside_nose_grab' and requestAction != 'fak_backside_nose_grab':
#             requestAction = 'fak_backside_nose_grab_out'
#             actionState = 'fak_backside_nose_grab_out'  

#         if l_actionState == 'frontside_tail_grab' and requestAction != 'frontside_tail_grab':
#             requestAction = 'frontside_tail_grab_out'
#             actionState = 'frontside_tail_grab_out'
#         if l_actionState == 'backside_tail_grab' and requestAction != 'backside_tail_grab':
#             requestAction = 'backside_tail_grab_out'
#             actionState = 'backside_tail_grab_out'            
#         if l_actionState == 'fak_frontside_tail_grab' and requestAction != 'fak_frontside_tail_grab':
#             requestAction = 'fak_frontside_tail_grab_out'
#             actionState = 'fak_frontside_tail_grab_out'
#         if l_actionState == 'fak_backside_tail_grab' and requestAction != 'fak_backside_tail_grab':
#             requestAction = 'fak_backside_tail_grab_out'
#             actionState = 'fak_backside_tail_grab_out'  
            
#         if l_actionState == 'reg_judo' and requestAction != 'reg_judo':
#             requestAction = 'reg_judo_out'
#             actionState = 'reg_judo_out'              
#         if l_actionState == 'fak_judo' and requestAction != 'fak_judo':
#             requestAction = 'fak_judo_out'
#             actionState = 'fak_judo_out'
#         if l_actionState == 'reg_frigid' and requestAction != 'reg_frigid':
#             requestAction = 'reg_frigid_out'
#             actionState = 'reg_frigid_out'              
#         if l_actionState == 'fak_frigid' and requestAction != 'fak_frigid':
#             requestAction = 'fak_frigid_out'
#             actionState = 'fak_frigid_out'
#         if l_actionState == 'reg_fsonefoot' and requestAction != 'reg_fsonefoot':
#             requestAction = 'reg_fsonefoot_out'
#             actionState = 'reg_fsonefoot_out'              
#         if l_actionState == 'fak_fsonefoot' and requestAction != 'fak_fsonefoot':
#             requestAction = 'fak_fsonefoot_out'
#             actionState = 'fak_fsonefoot_out'                         
#         if l_actionState == 'reg_onefoot' and requestAction != 'reg_onefoot':
#             requestAction = 'reg_onefoot_out'
#             actionState = 'reg_onefoot_out'              
#         if l_actionState == 'fak_onefoot' and requestAction != 'fak_onefoot':
#             requestAction = 'fak_onefoot_out'
#             actionState = 'fak_onefoot_out' 
#         if l_actionState == 'reg_airwalk' and requestAction != 'reg_airwalk':
#             requestAction = 'reg_airwalk_out'
#             actionState = 'reg_airwalk_out'                           
                       
            
#         # if l_actionState == 'fak_pump_right' and requestAction != 'fak_pump_right':
#         #     requestAction = 'fak_pump_right_out'
#         #     actionState = 'fak_pump_right_out'                                       

#         # if l_actionState == 'fak_pump_left' and requestAction != 'fak_pump_left':
#         #     requestAction = 'fak_pump_left_out'
#         #     actionState = 'fak_pump_left_out' 

#         # if l_actionState == 'fak_pump_left_out' and requestAction != 'fak_pump_left_out':
#         #     requestAction = 'fak_pump'
#         #     actionState = 'fak_pump'
#         # if l_actionState == 'fak_pump_right_out' and requestAction != 'fak_pump_right_out':
#         #     requestAction = 'fak_pump'
#         #     actionState = 'fak_pump'                        
                        
#         if l_actionState == 'reg_walk_air' and requestAction != 'reg_walk_air':
#             requestAction = 'reg_walk_air_out'
#             actionState = 'reg_walk_air_out'      
            
#         if l_actionState == 'fak_walk_air' and requestAction != 'fak_walk_air':
#             requestAction = 'fak_walk_air_out'
#             actionState = 'fak_walk_air_out'                                  
        
#         if l_actionState in ['reg_noseg', 'reg_noseg_out'] and requestAction == 'fak_tailg':
#             requestAction = 'reg_fak_noseg'
#             actionState = 'reg_fak_noseg'   
#         if l_actionState in ['fak_tailg', 'fak_tailg_out'] and requestAction == 'reg_noseg':
#             requestAction = 'fak_reg_tailg'
#             actionState = 'fak_reg_tailg' 
#         if l_actionState in ['fak_noseg', 'fak_noseg_out'] and requestAction == 'reg_tailg':
#             requestAction = 'fak_reg_noseg'
#             actionState = 'fak_reg_noseg'   
#         if l_actionState in ['reg_tailg', 'reg_tailg_out'] and requestAction == 'fak_noseg':
#             requestAction = 'reg_fak_tailg'
#             actionState = 'reg_fak_tailg'     
            
            
#         if l_actionState in ['reg_nosegl', 'reg_nosegl_out'] and requestAction == 'fak_tailgl':
#             requestAction = 'reg_fak_nosegl'
#             actionState = 'reg_fak_nosegl'  
#         if l_actionState in ['reg_nosegr', 'reg_nosegr_out'] and requestAction == 'fak_tailgr':
#             requestAction = 'reg_fak_nosegr'
#             actionState = 'reg_fak_nosegr'    
            
#         if l_actionState in ['reg_tailgl', 'reg_tailgl_out'] and requestAction == 'fak_nosegl':
#             requestAction = 'reg_fak_tailgl'
#             actionState = 'reg_fak_tailgl'  
#         if l_actionState in ['reg_tailgr', 'reg_tailgr_out'] and requestAction == 'fak_nosegr':
#             requestAction = 'reg_fak_tailgr'
#             actionState = 'reg_fak_tailgr'    
            
            
            
#         if l_actionState in ['fak_nosegl', 'fak_nosegl_out'] and requestAction == 'reg_tailgl':
#             requestAction = 'fak_reg_nosegl'
#             actionState = 'fak_reg_nosegl'  
#         if l_actionState in ['fak_nosegr', 'fak_nosegr_out'] and requestAction == 'reg_tailgr':
#             requestAction = 'fak_reg_nosegr'
#             actionState = 'fak_reg_nosegr'    
            
#         if l_actionState in ['fak_tailgl', 'fak_tailgl_out'] and requestAction == 'reg_nosegl':
#             requestAction = 'fak_reg_tailgl'
#             actionState = 'fak_reg_tailgl'  
#         if l_actionState in ['fak_tailgr', 'fak_tailgr_out'] and requestAction == 'reg_nosegr':
#             requestAction = 'fak_reg_tailgr'
#             actionState = 'fak_reg_tailgr'                                                               
        
        
        
#         if requestAction in ('reg_roll', 'fak_roll') and l_actionState in ('reg_ollie_north_out', 'reg_ollie_north', 'reg_ollie_south_out', 'reg_ollie_south', 'frontside_grab', 'backside_grab', 'frontside_nose_grab', 'frontside_tail_grab', 'backside_nose_grab', 'backside_tail_grab', 'frontside_grab_out', 'backside_grab_out', 'frontside_nose_grab_out', 'frontside_tail_grab_out', 'backside_nose_grab_out', 'backside_tail_grab_out', 'reg_judo_out', 'reg_frigid_out', 'reg_fsonefoot_out', 'reg_onefoot_out', 'reg_airwalk_out') and actionTimer < 2:
#             requestAction = 'reg_land'
#         if requestAction in ('fak_roll', 'reg_roll') and l_actionState in ('fak_ollie_north_out', 'fak_ollie_north', 'fak_ollie_south_out', 'fak_ollie_south', 'fak_backside_grab', 'fak_frontside_grab', 'fak_frontside_nose_grab', 'fak_frontside_tail_grab', 'fak_backside_nose_grab', 'fak_backside_tail_grab', 'fak_backside_grab_out', 'fak_frontside_grab_out', 'fak_frontside_nose_grab_out', 'fak_frontside_tail_grab_out', 'fak_backside_nose_grab_out', 'fak_backside_tail_grab_out', 'fak_judo_out', 'fak_frigid_out', 'fak_fsonefoot_out', 'fak_onefoot_out', 'reg_airwalk_out') and actionTimer < 2:
#             requestAction = 'fak_land'  
            
#         if l_actionState == 'reg_air' and requestAction == 'fak_air':
#             requestAction = 'reg_air'
#         if l_actionState == 'fak_air' and requestAction == 'reg_air':
#             requestAction = 'fak_air'   
#         if l_actionState == 'reg_air' and requestAction == 'fak_land':
#             requestAction = 'reg_land'
#         if l_actionState == 'fak_air' and requestAction == 'reg_land':
#             requestAction = 'fak_land'
            
        
#         if l_actionState == 'fak_5050-roll' and requestAction == 'reg_land':
#             requestAction = 'fak_land'
#         if l_actionState == 'reg_5050-roll' and requestAction == 'fak_land':
#             requestAction = 'reg_land'            
        
        
#         if requestAction in ('reg_pump', 'reg_opos', 'reg_nopos', 'reg_turnL', 'reg_turnR') and l_actionState in ('fak_nmanual_out', 'fak_manual_out'):
#             requestAction = 'fak-reg_roll'  
#         if requestAction in ('fak_pump', 'fak_opos', 'fak_nopos', 'fak_turnL', 'fak_turnR') and l_actionState in ('fak_nmanual_out', 'fak_manual_out'):
#             requestAction = 'reg-fak_roll'             
                                             
#         if l_actionState == 'reg_back_invert_out' and requestAction == 'fak_roll' and actionTimer < 1:
#             requestAction = 'reg_air-roll'              
            
            
#         if l_actionState == 'reg_land' and requestAction in ['reg_pump_left', 'reg_pump_right']:
#             requestAction = 'reg_pump_in'
#         if l_actionState == 'fak_land' and requestAction in ['fak_pump_left', 'fak_pump_right']:
#             requestAction = 'fak_pump_in'  
             
#         if l_actionState in ['fak_pump_right', 'fak_pump_left'] and requestAction == 'fak_roll':
#             requestAction = 'fak_pump_out'     
#         if l_actionState in ['fak_left-pump', 'fak_right-pump'] and requestAction == 'reg_roll':
#             requestAction = 'fak_pump_out' 

# #---new pump turns reg

#         if l_actionState == 'reg_pump' and requestAction == 'reg_pump_right' and trans_playing != 0:
#             requestAction = 'reg_pump'
#         if l_actionState == 'reg_turnRight' and requestAction == 'reg_pump_right' and trans_playing != 0:
#             requestAction = 'reg_turnRight'  
#         if l_actionState == 'reg_turnLeft' and requestAction == 'reg_pump_left' and trans_playing != 0:
#             requestAction = 'reg_turnLeft' 
            
#         if l_actionState == 'reg_turnRight' and requestAction in ['reg_roll', 'reg_pump']:
#             requestAction = 'reg_turnRight_out'
#         if l_actionState == 'reg_turnLeft' and requestAction in ['reg_roll', 'reg_pump']:
#             requestAction = 'reg_turnLeft_out'    
#         if l_actionState == 'reg_pump' and requestAction == 'reg_roll':
#             requestAction = 'reg_pump_out' 
        
#         if l_actionState == 'reg_turnRight' and requestAction in ['reg_pump_right']:
#             requestAction = 'reg_pump_right'    
#         if l_actionState == 'reg_turnLeft' and requestAction in ['reg_pump_left']:
#             requestAction = 'reg_pump_left'    
        
#         if l_actionState == 'reg_pump_right' and requestAction in ['reg_pump', 'reg_roll', 'reg_turnRight', 'reg_turnLeft']:
#             requestAction = 'reg_pump_right_out'
#             own['actionState'] = 'reg_pump_right_out'
            
#         if l_actionState == 'reg_pump_left' and requestAction in ['reg_pump', 'reg_roll', 'reg_turnRight', 'reg_turnLeft']:
#             requestAction = 'reg_pump_left_out'
#             own['actionState'] = 'reg_pump_left_out'    

#         if l_actionState == 'reg_pump_right_out' and requestAction == 'reg_roll':
#             requestAction = 'reg_pump'
#         if l_actionState == 'reg_pump_left_out' and requestAction == 'reg_roll':
#             requestAction = 'reg_pump'    
            
#         if l_actionState == 'reg_pump_right-rightb' and requestAction == 'reg_roll':
#             requestAction = 'reg_turnRight'   
#         if l_actionState == 'reg_pump_left-leftb' and requestAction == 'reg_roll':
#             requestAction = 'reg_turnLeft'   
        
#         if l_actionState == 'reg_turnRight' and requestAction == 'reg_pump_right':
#             requestAction = 'reg_pump_right-right' 
#         if l_actionState == 'reg_turnLeft' and requestAction == 'reg_pump_left':
#             requestAction = 'reg_pump_left-left'                
                                                                    
#         if l_actionState == 'reg_pump_right' and requestAction == 'reg_turnRight':
#             if trans_playing == False:
#                 requestAction = 'reg_pump_right-rightb'
#             else:
#                 requestAction == 'reg_pump_right'
                
#         if l_actionState == 'reg_pump_left' and requestAction == 'reg_turnLeft':
#             if trans_playing == False:
#                 requestAction = 'reg_pump_left-leftb'            
#             else:
#                 requestAction == 'reg_pump_left'    
            
#         if l_actionState == 'reg_turnRight_out' and requestAction == 'fak_roll':
#             requestAction = 'reg_roll' 
#         if l_actionState == 'reg_turnLeft_out' and requestAction == 'fak_roll':
#             requestAction = 'reg_roll'      
            
#         if l_actionState == 'reg_pump' and requestAction == 'reg_turnRight':
#             requestAction = 'reg_pump_out' 
#         if l_actionState == 'reg_pump' and requestAction == 'reg_turnLeft':
#             requestAction = 'reg_pump_out'               
            
#         if l_actionState == 'reg_pump_right-right' and requestAction == 'reg_turnRight':
#             requestAction = 'reg_pump_right-rightb'
#         if l_actionState == 'reg_pump_left-left' and requestAction == 'reg_turnLeft':
#             requestAction = 'reg_pump_left-leftb'    
        
#         if l_actionState == 'reg_pump_right-rightb' and requestAction == 'reg_pump_right':
#             requestAction = 'reg_pump_right-right' 
#         if l_actionState == 'reg_pump_left-leftb' and requestAction == 'reg_pump_left':
#             requestAction = 'reg_pump_left-left'                     
        
#         if l_actionState == 'reg_pump_right-right' and requestAction not in [ 'reg_pump_right-right', 'reg_pump_right-rightb']:
#             requestAction = 'reg_pump_right'         
            
#         if l_actionState == 'reg_pump_left-left' and requestAction not in [ 'reg_pump_left-left', 'reg_pump_left-leftb']:
#             requestAction = 'reg_pump_left'        
            
#         if l_actionState == 'reg_pump_right_out' and requestAction in ['reg_turnRight', 'reg_turnLeft']:
#             requestAction = 'reg_pump'
#         if l_actionState == 'reg_pump_left_out' and requestAction in ['reg_turnLeft', 'reg_turnRight']:
#             requestAction = 'reg_pump'    
                      
#         if l_actionState == 'reg_pump_out' and requestAction == 'reg_pump_right':
#             requestAction = 'reg_roll'
#         if l_actionState == 'reg_pump_out' and requestAction == 'reg_pump_left':
#             requestAction = 'reg_roll'  
            
#         if l_actionState == 'reg_roll' and requestAction in ['reg_pump_right', 'reg_pump_left']:
#             requestAction = 'reg_pump' 
            
#         if l_actionState == 'reg_turnLeft_out' and requestAction in ['reg_pump_right', 'reg_pump_left']:
#             requestAction = 'reg_pump'
        
#         if l_actionState == 'reg_turnRight_out' and requestAction in ['reg_pump_left', 'reg_pump_right']:
#             requestAction = 'reg_pump'                 
              


#         #fak

#         if l_actionState == 'fak_pump' and requestAction == 'fak_pump_right' and trans_playing != 0:
#             requestAction = 'fak_pump'
#         if l_actionState == 'fak_turnRight' and requestAction == 'fak_pump_right' and trans_playing != 0:
#             requestAction = 'fak_turnRight'  
#         if l_actionState == 'fak_turnLeft' and requestAction == 'fak_pump_left' and trans_playing != 0:
#             requestAction = 'fak_turnLeft'   

#         if l_actionState == 'fak_turnRight' and requestAction in ['fak_roll', 'fak_pump']:
#             requestAction = 'fak_turnRight_out'
#         if l_actionState == 'fak_turnLeft' and requestAction in ['fak_roll', 'fak_pump']:
#             requestAction = 'fak_turnLeft_out'    
#         if l_actionState == 'fak_pump' and requestAction == 'fak_roll':
#             requestAction = 'fak_pump_out' 
        
#         if l_actionState == 'fak_turnRight' and requestAction in ['fak_pump_right']:
#             requestAction = 'fak_pump_right'    
#         if l_actionState == 'fak_turnLeft' and requestAction in ['fak_pump_left']:
#             requestAction = 'fak_pump_left'    
        
#         if l_actionState == 'fak_pump_right' and requestAction in ['fak_pump', 'fak_roll', 'reg_pump_out', 'fak_turnRight', 'fak_turnLeft']:
#             requestAction = 'fak_pump_right_out'
#             own['actionState'] = 'fak_pump_right_out'
            
            
#         if l_actionState == 'fak_pump_left' and requestAction in ['fak_pump', 'fak_roll', 'fak_pump_out', 'fak_turnRight', 'fak_turnLeft']:
#             requestAction = 'fak_pump_left_out'
#             own['actionState'] = 'fak_pump_left_out'    

#         if l_actionState == 'fak_pump_right_out' and requestAction == 'fak_roll':
#             requestAction = 'fak_pump'
#         if l_actionState == 'fak_pump_left_out' and requestAction == 'fak_roll':
#             requestAction = 'fak_pump'    
            
#         if l_actionState == 'fak_pump_right-rightb' and requestAction == 'fak_roll':
#             requestAction = 'fak_turnRight'   
#         if l_actionState == 'fak_pump_left-leftb' and requestAction == 'fak_roll':
#             requestAction = 'fak_turnLeft'   
        
#         if l_actionState == 'fak_turnRight' and requestAction == 'fak_pump_right':
#             requestAction = 'fak_pump_right-right' 
#         if l_actionState == 'fak_turnLeft' and requestAction == 'fak_pump_left':
#             requestAction = 'fak_pump_left-left'                
                                                                    
#         if l_actionState == 'fak_pump_right' and requestAction == 'fak_turnRight':
#             if trans_playing == False:
#                 requestAction = 'fak_pump_right-rightb'
#             else:
#                 requestAction == 'fak_pump_right'
                
#         if l_actionState == 'fak_pump_left' and requestAction == 'fak_turnLeft':
#             if trans_playing == False:
#                 requestAction = 'fak_pump_left-leftb'            
#             else:
#                 requestAction == 'fak_pump_left'    
                   
#         if l_actionState == 'fak_turnRight_out' and requestAction == 'reg_roll':
#             requestAction = 'fak_roll' 
#         if l_actionState == 'fak_turnLeft_out' and requestAction == 'reg_roll':
#             requestAction = 'fak_roll'      
            
#         if l_actionState == 'fak_pump' and requestAction == 'fak_turnRight':
#             requestAction = 'fak_pump_out' 
#         if l_actionState == 'fak_pump' and requestAction == 'fak_turnLeft':
#             requestAction = 'fak_pump_out'               
            
#         if l_actionState == 'fak_pump_right-right' and requestAction == 'fak_turnRight':
#             requestAction = 'fak_pump_right-rightb'
#         if l_actionState == 'fak_pump_left-left' and requestAction == 'fak_turnLeft':
#             requestAction = 'fak_pump_left-leftb'    
        
#         if l_actionState == 'fak_pump_right-rightb' and requestAction == 'fak_pump_right':
#             requestAction = 'fak_pump_right-right' 
#         if l_actionState == 'fak_pump_left-leftb' and requestAction == 'fak_pump_left':
#             requestAction = 'fak_pump_left-left'                     
        
#         if l_actionState == 'fak_pump_right-right' and requestAction not in [ 'fak_pump_right-right', 'fak_pump_right-rightb']:
#             requestAction = 'fak_pump_right' 
                  
#         if l_actionState == 'fak_pump_left-left' and requestAction not in [ 'fak_pump_left-left', 'fak_pump_left-leftb']:
#             requestAction = 'fak_pump_left'        
            
#         if l_actionState == 'fak_pump_right_out' and requestAction in ['fak_turnRight', 'fak_turnLeft']:
#             requestAction = 'fak_pump'
#         if l_actionState == 'fak_pump_left_out' and requestAction in ['fak_turnLeft', 'fak_turnRight']:
#             requestAction = 'fak_pump'    
                      
#         if l_actionState == 'fak_pump_out' and requestAction == 'fak_pump_right':
#             requestAction = 'fak_roll'
#         if l_actionState == 'fak_pump_out' and requestAction == 'fak_pump_left':
#             requestAction = 'fak_roll'  
            
#         if l_actionState == 'fak_roll' and requestAction in ['fak_pump_right', 'fak_pump_left']:
#             requestAction = 'fak_pump' 
            
#         if l_actionState == 'fak_turnLeft_out' and requestAction in ['fak_pump_right', 'fak_pump_left']:
#             requestAction = 'fak_pump'
        
#         if l_actionState == 'fak_turnRight_out' and requestAction in ['fak_pump_left', 'fak_pump_right']:
#             requestAction = 'fak_pump'                 


#         if l_actionState == 'reg_pump' and requestAction in ['reg_pump_left', 'reg_pump_right'] and trans_playing == True:
#             requestAction = 'reg_pump'
#         if l_actionState == 'fak_pump' and requestAction in ['fak_pump_left', 'fak_pump_right'] and trans_playing == True:
#             requestAction = 'fak_pump'            



#         if l_actionState in ['reg_push', 'reg_push_goof', 'reg_land'] and requestAction != 'reg_roll':
#             requestAction = 'reg_roll'
#         if l_actionState in ['fak_push', 'fak_push_goof', 'fak_land'] and requestAction != 'fak_roll':
#             requestAction = 'fak_roll'                       
            
#         if l_actionState == 'fak-reg_roll' and requestAction in ['fak_pump_right', 'fak_pump_left']:
#             requestAction = 'fak_pump'
#         if l_actionState == 'reg-fak_roll' and requestAction in ['reg_pump_right', 'reg_pump_left']:
#             requestAction = 'reg_pump'  

#         if l_actionState == 'reg_air-roll' and requestAction == 'reg_5050' and actionTimer > 5:
#             requestAction = 'reg_5050'
#             #print('doing the thing')
#         tl = ['reg_turnLeft', 'reg_turnRight', 'reg_turnLeft_out', 'reg_turnRight_out']    
#         if l_actionState in tl and requestAction not in tl:
#             requestAction = 'reg_roll' 
#             #print('doing the other thing') 

#         if l_actionState == 'reg_air-roll' and requestAction not in ['reg_air-roll', 'reg_land', 'reg_landL', 'reg_landLb', 'reg_landR', 'reg_landRb', 'fak_land', 'fak_landL', 'fak_landLb', 'fak_landR', 'fak_landRb']:
#             requestAction = 'reg_roll'      
#             #print('doing the third thing')

#         # if l_actionState == 'reg_roll' and (requestAction == 'reg_5050' or requestAction == 'reg_air'):
#         #     requestAction = 'reg_air'
#             #print('doing the 4th thing')    
#         if l_actionState == 'reg_roll' and requestAction == 'reg_air':
#             requestAction = 'reg_roll'

#         if l_actionState == 'fak_tailslide' and requestAction == 'reg_noseslide':
#             requestAction = 'fak-reg_noseslide'    
#         if l_actionState == 'reg_noseslide' and requestAction == 'fak_tailslide':
#             requestAction = 'reg-fak_tailslide'   

#         if l_actionState == 'fak_noseslide' and requestAction == 'reg_tailslide':
#             requestAction = 'fak-reg_tailslide'    
#         if l_actionState == 'reg_tailslide' and requestAction == 'fak_noseslide':
#             requestAction = 'reg-fak_noseslide'                    
#         #print('reqqqqqqqqes2', requestAction)
#         if l_actionState in ['reg_air-roll', 'fak_air-roll'] and requestAction in ['reg_landL', 'reg_landLb', 'reg_landR', 'reg_landRb', 'fak_landL', 'fak_landLb', 'fak_landR', 'fak_landRb', 'fak_land', 'reg_land']:
#             actionTimer = 0    
#             own['actionTimer'] = 0 


#         if l_actionState == 'reg_air-roll' and requestAction == 'fak_land':
#             requestAction = 'reg_land'    
                                                                         

#         if l_actionState == 'reg_air_tail to manual' and requestAction in ['reg_land', 'fak_land'] and own['actionTimer'] > 1:
#             requestAction = 'reg_air_tail to manual'                                    
            
#         if l_actionState == 'reg_air_nose to nmanual' and requestAction  in ['reg_land', 'fak_land'] and own['actionTimer'] > 1:
#             requestAction = 'reg_air_nose to nmanual'
#         if l_actionState == 'fak_air_tail to manual' and requestAction in ['reg_land', 'fak_land'] and own['actionTimer'] > 1:
#             requestAction = 'fak_air_tail to manual'                                    
            
#         if l_actionState == 'fak_air_nose to nmanual' and requestAction in ['reg_land', 'fak_land'] and own['actionTimer'] > 1:
#             requestAction = 'fak_air_nose to nmanual'            
        
#         if requestAction in ['fak_land', 'reg_land']:  
#             requestAction = own['lland']
#         if l_actionState == 'reg_air-roll' and requestAction == 'fak_landL':
#             requestAction = 'reg_landL'    
#         if l_actionState == 'reg_air-roll' and requestAction == 'fak_landLb':
#             requestAction = 'reg_landLb'
#         if l_actionState == 'reg_air-roll' and requestAction == 'fak_landR':
#             requestAction = 'reg_landR'    
#         if l_actionState == 'reg_air-roll' and requestAction == 'fak_landRb':
#             requestAction = 'reg_landRb'    
#         if l_actionState == 'reg_air-roll' and requestAction == 'fak_land':
#             requestAction = 'reg_land'            
#         if l_actionState == 'fak_air-roll' and requestAction == 'reg_landL':
#             requestAction = 'fak_landL'    
#         if l_actionState == 'fak_air-roll' and requestAction == 'reg_landLb':
#             requestAction = 'fak_landLb'
#         if l_actionState == 'fak_air-roll' and requestAction == 'reg_landR':
#             requestAction = 'fak_landR'    
#         if l_actionState == 'fak_air-roll' and requestAction == 'reg_landRb':
#             requestAction = 'fak_landRb'            
#         if l_actionState == 'fak_air-roll' and requestAction == 'reg_land':
#             requestAction = 'fak_land'           



#        #if own['jump_stance'] == 0 and requestAction == 'fak_land'          
# #---------------------------------------------------                                                                                  
                                                                                  
                                                                                                                                                        
#     else:
#         print('action over written') 
#         #pass              
#     def updateAction(requestAction, actionState):
#         try:
#             flip_start_frame = own['flip_start_lay']
#             flipspeed = own['flipspeed']
            
#         except:
#             flip_start_frame = 1  
#             flipspeed = .6  
# ###############################        
#         #one shot actions
# ###############################
#         if requestAction in oneshotActions_list:
            
#             if requestAction == 'reg_land' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 #print('stopping loop and trans')                                
#                 actionState = 'reg_land'
#                 requestAction = 'reg_land'
#                 own['actionTimer'] = 37
#                 skater.playAction('reg_land', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)                                        
#                 #print('play land') 

#             if requestAction == 'reg_landL' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)                            
#                 actionState = 'reg_landL'
#                 requestAction = 'reg_landL'
#                 own['actionTimer'] = 37
#                 skater.playAction('reg_landL', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#             if requestAction == 'reg_landR' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 actionState = 'reg_landR'
#                 requestAction = 'reg_landR'
#                 own['actionTimer'] = 37
#                 skater.playAction('reg_landR', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1) 
#             if requestAction == 'reg_landLb' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 actionState = 'reg_landLb'
#                 requestAction = 'reg_landLb'
#                 own['actionTimer'] = 37
#                 skater.playAction('reg_landLb', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#             if requestAction == 'reg_landRb' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)                      
#                 actionState = 'reg_landRb'
#                 requestAction = 'reg_landRb'
#                 own['actionTimer'] = 37
#                 skater.playAction('reg_landRb', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)                                

#             if requestAction == 'fak_land' and own['grindHit'] == False:
#                 actionState = 'fak_land'
#                 requestAction = 'fak_land'
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)                                
#                 own['actionTimer'] = 37
#                 skater.playAction('fak_land', 1,40, layer=trans_layer, play_mode=0, speed=1)                        
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)                        
#                 #print('play land')





#             if requestAction == 'fak_landL' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)                            
#                 actionState = 'fak_landL'
#                 requestAction = 'fak_landL'
#                 own['actionTimer'] = 37
#                 skater.playAction('fak_landL', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#             if requestAction == 'fak_landR' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 actionState = 'fak_landR'
#                 requestAction = 'fak_landR'
#                 own['actionTimer'] = 37
#                 skater.playAction('fak_landR', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1) 
#             if requestAction == 'fak_landLb' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 actionState = 'fak_landLb'
#                 requestAction = 'fak_landLb'
#                 own['actionTimer'] = 37
#                 skater.playAction('fak_landLb', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#             if requestAction == 'fak_landRb' and own['grindHit'] == False:
#                 skater.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 skater.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)                      
#                 actionState = 'fak_landRb'
#                 requestAction = 'fak_landRb'
#                 own['actionTimer'] = 37
#                 skater.playAction('fak_landRb', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=1)  









#             if requestAction == 'reg_dropin':
#                 actionState = 'reg_dropin'
#                 own['actionTimer'] = 28
#                 skater.playAction('reg_dropin3', 60,80, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_reg_dropin3', 60,80, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_reg_dropin3', 60,80, layer=trans_layer, play_mode=0, speed=.75)    
#             if requestAction == 'fak_dropin':
#                 actionState = 'fak_dropin'
#                 own['actionTimer'] = 28
#                 skater.playAction('nfak_dropin', 60,80, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_fak_dropin', 60,80, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_fak_dropin', 60,80, layer=trans_layer, play_mode=0, speed=.75)    

#             if requestAction == 'reg_turnRight_out':
#                 #stopActs()
#                 actionState = 'reg_turnRight_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
                                  
#                 if trans_playing and cur_frame > .5 and cur_frame < 39: 
#                     #cur_frame = (10 - cur_frame)                
#                     skater.playAction('nreg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #own['actionTimer'] = (10 - cur_frame) 
#                     own['actionTimer'] = cur_frame
#                 else:                    
#                     skater.playAction('nreg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                   
                    
#             if requestAction == 'reg_turnLeft_out':
#                 actionState = 'reg_turnLeft_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)        
#                 if trans_playing and cur_frame > .5 and cur_frame < 39:             
#                     #cur_frame = (10 - cur_frame) 
#                     skater.playAction('nreg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #deck.playAction('a_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     ##trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('nreg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #deck.playAction('a_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     ##trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)
                
#             if requestAction == 'fak_turnRight_out':
#                 actionState = 'fak_turnRight_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)          
#                 if trans_playing and cur_frame > .5 and cur_frame < 39: 
#                     #cur_frame = (10 - cur_frame) 
#                     skater.playAction('nfak_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('nfak_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                   
                    
#             if requestAction == 'fak_turnLeft_out':
#                 actionState = 'fak_turnLeft_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > .5 and cur_frame < 39:     
#                     #cur_frame = (10 - cur_frame) 
#                     skater.playAction('nfak_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('nfak_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)              


#             if requestAction == 'reg_pump_left-left':
#                 actionState = 'reg_pump_left-left'
#                 own['actionTimer'] = 19
                
#                 if l_actionState == 'reg_pump_left-leftb':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)       
#                         skater.playAction('nreg_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         #deck.playAction('a_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         ##trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         own['actionTimer'] = cur_frame * 2    
#                     else:
#                         skater.playAction('nreg_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         #deck.playAction('a_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         ##trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)                                               
#                 else:    
#                     skater.playAction('nreg_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     ##trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)          
                
#             if requestAction == 'reg_pump_right-right':
#                 actionState = 'reg_pump_right-right'
#                 own['actionTimer'] = 19
                
#                 if l_actionState == 'reg_pump_right-rightb':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)       
#                         skater.playAction('nreg_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         own['actionTimer'] = cur_frame * 2    
#                     else:
#                         skater.playAction('nreg_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)                                               
#                 else:    
#                     skater.playAction('nreg_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)  
                         
#             if requestAction == 'fak_pump_left-left':
#                 actionState = 'fak_pump_left-left'
#                 own['actionTimer'] = 19
                
#                 if l_actionState == 'fak_pump_left-leftb':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)       
#                         skater.playAction('nfak_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                         own['actionTimer'] = cur_frame * 2    
#                     else:
#                         skater.playAction('nfak_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                                
#                 else:    
#                     skater.playAction('nfak_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                   

                
#             if requestAction == 'fak_pump_right-right':
#                 actionState = 'fak_pump_right-right'
#                 own['actionTimer'] = 19
#                 if l_actionState == 'fak_pump_right-rightb':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)       
#                         skater.playAction('nfak_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                         own['actionTimer'] = cur_frame * 2    
#                     else:
#                         skater.playAction('nfak_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                                
#                 else:    
#                     skater.playAction('nfak_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)      
                
#             if requestAction == 'reg_pump_right-rightb':
#                 actionState = 'reg_pump_right-rightb'
#                 own['actionState'] = 'reg_pump_right-rightb'
#                 own['actionTimer'] = 20
                
#                 if l_actionState == 'reg_pump_right-right':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         skater.playAction('nreg_pump_right', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                         own['actionTimer'] = cur_frame * 2 
#                     else:    
#                         skater.playAction('nreg_pump_right', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                                             
#                 else:    
#                     skater.playAction('nreg_pump_right', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                    
                    
#             if requestAction == 'reg_pump_left-leftb':
#                 actionState = 'reg_pump_left-leftb'
#                 own['actionState'] = 'reg_pump_left-leftb'
#                 own['actionTimer'] = 20
                
#                 if l_actionState == 'reg_pump_left-left':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         skater.playAction('nreg_pump_left', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         ##trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                         own['actionTimer'] = cur_frame * 2 
#                     else:    
#                         skater.playAction('nreg_pump_left', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         ##trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                                             
#                 else:    
#                     skater.playAction('nreg_pump_left', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     ##trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                     
                    
#             if requestAction == 'fak_pump_right-rightb':
#                 actionState = 'fak_pump_right-rightb'
#                 own['actionState'] = 'fak_pump_right-rightb'
#                 own['actionTimer'] = 20
                
#                 if l_actionState == 'fak_pump_right-right':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         skater.playAction('nfak_pump_right', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                         own['actionTimer'] = cur_frame * 2 
#                     else:    
#                         skater.playAction('nfak_pump_right', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                                             
#                 else:    
#                     skater.playAction('nfak_pump_right', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                    
                    
#             if requestAction == 'fak_pump_left-leftb':
#                 actionState = 'fak_pump_left-leftb'
#                 own['actionState'] = 'fak_pump_left-leftb'
#                 own['actionTimer'] = 20
                
#                 if l_actionState == 'fak_pump_left-left':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         skater.playAction('nfak_pump_left', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
#                         own['actionTimer'] = cur_frame * 2 
#                     else:    
#                         skater.playAction('nfak_pump_left', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                                             
#                 else:    
#                     skater.playAction('nfak_pump_left', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                
                
# #*************************

#             if requestAction == 'reg_left-pump':
#                 actionState = 'reg_left-pump'
#                 own['actionTimer'] = 19
                    
#                 skater.playAction('nreg_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 ##trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)  
                
#             if requestAction == 'reg_right-pump':
#                 actionState = 'reg_right-pump'
#                 own['actionTimer'] = 19
                    
#                 skater.playAction('nreg_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)       
#             if requestAction == 'fak_left-pump':
#                 actionState = 'fak_left-pump'
#                 own['actionTimer'] = 19
                    
#                 skater.playAction('nfak_pump_left', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_right', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)  
                
#             if requestAction == 'fak_right-pump':
#                 actionState = 'fak_right-pump'
#                 own['actionTimer'] = 19
                    
#                 skater.playAction('nfak_pump_right', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_left', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg', 10,10, layer=trans_layer, play_mode=0, speed=.5)   
                
                
# #*****************                                                       


#             if requestAction == 'reg_pump_right_out':
#                 actionState = 'reg_pump_right_out'
#                 requestAction = 'reg_pump_right_out'
#                 own['actionState'] = 'reg_pump_right_out'
#                 own['actionTimer'] = 20
#                 trans_playing = skater.isPlayingAction(trans_layer)
                
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                     stopActs()
#                     if cur_frame >= 32 and cur_frame <= 40 and l_actionState == 'reg_pump_right':
#                         cur_frame = cur_frame - 30   
#                     if cur_frame < 32 and cur_frame > 26:
#                         cur_frame = 10
#                     if trans_playing and cur_frame > .5:      
#                         #cur_frame = 20 - cur_frame         
#                         skater.playAction('nreg_pump_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         own['actionTimer'] = cur_frame *2
#                         #own['actionTimer'] = 19
#                 else:                     
#                     skater.playAction('nreg_pump_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                    
                    
                    
#             if requestAction == 'reg_pump_left_out':
#                 actionState = 'reg_pump_left_out'
#                 own['actionState'] = 'reg_pump_left_out'
#                 requestAction = 'reg_pump_left_out'
#                 own['actionTimer'] = 20
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                     stopActs()
#                     if cur_frame >= 32 and cur_frame <= 40 and l_actionState == 'reg_pump_left':
#                         cur_frame = cur_frame - 30   
#                     if cur_frame < 32 and cur_frame > 26:
#                         cur_frame = 10
#                     if trans_playing and cur_frame > .5:      
#                         #cur_frame = 20 - cur_frame         
#                         skater.playAction('nreg_pump_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         own['actionTimer'] = cur_frame *2
#                         #own['actionTimer'] = 19
#                 else:                     
#                     skater.playAction('nreg_pump_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                  
                    
                     
#             if requestAction == 'fak_pump_right_out':
#                 actionState = 'fak_pump_right_out'
#                 requestAction = 'fak_pump_right_out'
#                 own['actionState'] = 'fak_pump_right_out'
#                 own['actionTimer'] = 20
#                 trans_playing = skater.isPlayingAction(trans_layer)
                
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                     stopActs()
#                     if cur_frame >= 32 and cur_frame <= 40 and l_actionState == 'fak_pump_right':
#                         cur_frame = cur_frame - 30   
#                     if cur_frame < 32 and cur_frame > 26:
#                         cur_frame = 10
#                     if trans_playing and cur_frame > .5:      
#                         #cur_frame = 20 - cur_frame         
#                         skater.playAction('nfak_pump_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         own['actionTimer'] = cur_frame *2
#                         #own['actionTimer'] = 19
#                 else:                     
#                     skater.playAction('nfak_pump_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                    
                    
                    
#             if requestAction == 'fak_pump_left_out':
#                 actionState = 'fak_pump_left_out'
#                 own['actionState'] = 'fak_pump_left_out'
#                 requestAction = 'fak_pump_left_out'
#                 own['actionTimer'] = 20
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                     stopActs()
#                     if cur_frame >= 32 and cur_frame <= 40 and l_actionState == 'fak_pump_left':
#                         cur_frame = cur_frame - 30   
#                     if cur_frame < 32 and cur_frame > 26:
#                         cur_frame = 10
#                     if trans_playing and cur_frame > .5:      
#                         #cur_frame = 20 - cur_frame         
#                         skater.playAction('nfak_pump_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         own['actionTimer'] = cur_frame *2
#                         #own['actionTimer'] = 19
#                 else:                     
#                     skater.playAction('nfak_pump_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)  
                                      

#             if requestAction == 'reg_opos_out':
#                 actionState = 'reg_opos_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                
#                 if trans_playing and cur_frame > .5:                
#                     skater.playAction('noposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('noposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)

#             if requestAction == 'fak_opos_out':
#                 actionState = 'fak_opos_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                 
#                 if trans_playing and cur_frame > .5:                
#                     skater.playAction('fak_oposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_oposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)

#             if requestAction == 'reg_nopos_out':
#                 actionState = 'reg_nopos_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > .5:                
#                     skater.playAction('nnoposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('nnoposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)

#             if requestAction == 'fak_nopos_out':
#                 actionState = 'fak_nopos_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                  
#                 if trans_playing and cur_frame > .5:                
#                     skater.playAction('fak_noposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_noposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)    
                    
#             if requestAction == 'reg_pump_out':
#                 actionState = 'reg_pump_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if l_actionState in ['reg_pump_left_out', 'reg_pump_right_out']:
#                     cur_frame = 0
#                     skater.stopAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)  
#                     if l_actionState in ['reg_pump_right', 'reg_pump_left']:
#                         cur_frame = cur_frame * 2              
#                 if trans_playing and cur_frame > .5:                
#                     skater.playAction('nreg_pump_in', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=1, speed=1)
                    
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('nreg_pump_in', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=1, speed=1)                   
                    
#             if requestAction == 'fak_pump_out':
#                 actionState = 'fak_pump_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if l_actionState in ['fak_pump_left_out', 'fak_pump_right_out']:
#                     cur_frame = 0
#                     skater.stopAction(trans_layer)                                
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)  
#                     if l_actionState in ['fak_pump_right', 'fak_pump_left']:
#                         cur_frame = cur_frame * 2              
#                 if trans_playing and cur_frame > .5:                
#                     skater.playAction('nfak_pump_in', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=1, speed=1)
                    
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('nfak_pump_in', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=1, speed=1) 

#             if requestAction == 'reg_stop_out':
#                 actionState = 'reg_stop_out'
#                 own['actionTimer'] = 30
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)              
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_stopin', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_stopin', 15,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 15,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 15,1, layer=trans_layer, play_mode=0, speed=.5)       

#             if requestAction == 'fak_stop_out':
#                 actionState = 'fak_stop_out'
#                 own['actionTimer'] = 30
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_stopin', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_stopin', 15,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 15,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 15,1, layer=trans_layer, play_mode=0, speed=.5)                           
                    
#             if requestAction == 'reg_tailslide_out':
#                 actionState = 'reg_tailslide_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_noses', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_fak_noses', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_fak_noses', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_noses', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_fak_noses', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_fak_noses', 30,40, layer=trans_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak_tailslide_out':
#                 actionState = 'fak_tailslide_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                  
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_noses', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_noses', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('reg_noses', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_noses', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_noses', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg_noses', 30,40, layer=trans_layer, play_mode=0, speed=.5) 
#             if requestAction == 'reg_noseslide_out':
#                 actionState = 'reg_noseslide_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_tails', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_fak_tails', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_fak_tails', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_tails', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_fak_tails', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_fak_tails', 30,40, layer=trans_layer, play_mode=0, speed=.5)                 
#             if requestAction == 'fak_noseslide_out':
#                 actionState = 'fak_noseslide_out'
#                 own['actionTimer'] = 19
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_tails', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_tails', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg_tails', cur_frame,40, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_tails', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_tails', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg_tails', 30,40, layer=trans_layer, play_mode=0, speed=.5) 
                    
#             if requestAction == 'reg_5050_out':
#                 actionState = 'reg_5050_out'
#                 own['actionTimer'] = 5
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                    #cur_frame -= 2                    
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_5050', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_5050', 20,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=.5)
#             if requestAction == 'fak_5050_out':
#                 actionState = 'fak_5050_out'
#                 own['actionTimer'] = 5
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)              
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_5050', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_5050', 20,1, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_noseg_out':
#                 actionState = 'reg_noseg_out'
#                 own['requestAction'] = 'reg_noseg_out'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)              
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_noseg.002', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_noseg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_noseg.002', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_noseg.002', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_noseg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_noseg.002', 10,1, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'fak_noseg_out':
#                 actionState = 'fak_noseg_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'fak_noseg_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)               
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_noseg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_noseg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_noseg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_noseg', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_noseg', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_noseg', 30,40, layer=trans_layer, play_mode=0, speed=1)        

#             if requestAction == 'reg_tailg_out':
#                 actionState = 'reg_tailg_out'
#                 own['requestAction'] = 'reg_tailg_out'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                 
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_tailg.001', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailg.001', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_tailg.001', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailg', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailg.001', 30,40, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'fak_tailg_out':
#                 actionState = 'fak_tailg_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'fak_tailg_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_tailg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailg', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_tailg', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailg', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailg', 30,40, layer=trans_layer, play_mode=0, speed=1)
                    
#             if requestAction == 'reg_nosegr_out':
#                 actionState = 'reg_nosegr_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'reg_nosegr_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                     #cur_frame -= 2                    
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_nosegR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_nosegR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_nosegR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_nosegR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_nosegR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_nosegR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'fak_nosegr_out':
#                 actionState = 'fak_nosegr_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'fak_nosegr_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)             
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_nosegR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_nosegR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_nosegR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_nosegR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_nosegR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_nosegR', 30,40, layer=trans_layer, play_mode=0, speed=1)        

#             if requestAction == 'reg_tailgr_out':
#                 actionState = 'reg_tailgr_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'reg_tailgr_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_tailgR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailgR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailgR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_tailgR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailgR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailgR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'fak_tailgr_out':
#                 actionState = 'fak_tailgr_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'fak_tailgr_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                 
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_tailgR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailgR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailgR', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_tailgR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailgR', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailgR', 30,40, layer=trans_layer, play_mode=0, speed=1) 
                    
#             if requestAction == 'reg_nosegl_out':
#                 actionState = 'reg_nosegl_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'reg_nosegl_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer) 
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_nosegL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_nosegL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_nosegL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_nosegL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_nosegL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_nosegL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'fak_nosegl_out':
#                 actionState = 'fak_nosegl_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'fak_nosegl_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer) 
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_nosegL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_nosegL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_nosegL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_nosegL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_nosegL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_nosegL', 30,40, layer=trans_layer, play_mode=0, speed=1)        

#             if requestAction == 'reg_tailgl_out':
#                 actionState = 'reg_tailgl_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'reg_tailgl_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer) 
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_tailgL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailgL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailgL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('reg_tailgL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailgL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailgL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'fak_tailgl_out':
#                 actionState = 'fak_tailgl_out'
#                 own['actionTimer'] = 9
#                 own['requestAction'] = 'fak_tailgl_out'
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_tailgL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailgL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailgL', cur_frame,40, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                      
#                     skater.playAction('fak_tailgL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailgL', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailgL', 30,40, layer=trans_layer, play_mode=0, speed=1)
                    
#             if requestAction == 'reg_manual_out':
#                 actionState = 'reg_manual_out'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 skater.playAction('reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)                                     
                    
#             if requestAction == 'fak_manual_out':
#                 actionState = 'fak_manual_out'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 skater.playAction('fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)                     
                
#             if requestAction == 'reg_nmanual_out':
#                 actionState = 'reg_nmanual_out'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 skater.playAction('reg_nmanual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)                     
                    
                    
#             if requestAction == 'fak_nmanual_out':
#                 actionState = 'fak_nmanual_out'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer) 
#                 skater.playAction('fak_nmanual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
            
#             if requestAction == 'reg-fak_nmanual':
#                 own['actionTimer'] = 9
#                 actionState = 'reg-fak_nmanual'
#                 skater.playAction('reg_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)            
#             if requestAction == 'fak-reg_nmanual':
#                 own['actionTimer'] = 9
#                 actionState = 'fak-reg_nmanual'
#                 skater.playAction('fak_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_fak_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_fak_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)                 
#             if requestAction == 'reg-fak_manual':
#                 own['actionTimer'] = 9
#                 actionState = 'reg-fak_manual'
#                 skater.playAction('reg_nmanual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_fak_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_fak_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)                    
#             if requestAction == 'fak-reg_manual':
#                 own['actionTimer'] = 9 
#                 actionState = 'fak-reg_manual'
#                 skater.playAction('fak_nmanual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_manual', 70,80, layer=trans_layer, play_mode=0, speed=1)

#             if requestAction == 'reg_brfoot':
#                 actionState = 'reg_brfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#             if requestAction == 'fak_brfoot':
#                 actionState = 'fak_brfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('fak_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#             if requestAction == 'reg_frfoot':
#                 actionState = 'reg_frfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('frfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#             if requestAction == 'fak_frfoot':
#                 actionState = 'fak_frfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('fakbfrfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)

#             if requestAction == 'reg_blfoot':
#                 #print('playing blfoot broke')
#                 actionState = 'reg_blfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('blfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#             if requestAction == 'fak_blfoot':
#                 actionState = 'fak_blfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('fakflfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#             if requestAction == 'reg_flfoot':
#                 actionState = 'reg_flfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('flfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#             if requestAction == 'fak_flfoot':
#                 actionState = 'fak_flfoot'
#                 own['actionTimer'] = 40
#                 skater.playAction('fak_flfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 deck.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)
#                 trucks.playAction('a_brfoot', 1, 30, layer=trans_layer, play_mode=0, speed=.75)                

#             if requestAction == 'reg_push' and own['actionTimer'] == 0:
#                 own['actionTimer'] = 70
#                 actionState = 'reg_push'
#                 skater.playAction('reg_push', 1,35, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#             if requestAction == 'reg_push_goof' and own['actionTimer'] == 0:
#                 own['actionTimer'] = 70
#                 actionState = 'reg_push_goof'
#                 skater.playAction('reg_push_goof', 1,35, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_push' and own['actionTimer'] == 0:
#                 own['actionTimer'] = 70
#                 actionState = 'fak_push'
#                 skater.playAction('fak_push', 1,35, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_push_goof' and own['actionTimer'] == 0:
#                 own['actionTimer'] = 70
#                 actionState = 'fak_push_goof'
#                 skater.playAction('fak_push_goof', 1,35, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 1,40, layer=trans_layer, play_mode=1, speed=.5)
                
#             if requestAction == 'reg_manual_revert_ccw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'reg_manual_revert_ccw'
#                 actionState = 'reg_manual_revert_ccw'
#                 skater.playAction('reg_manual_revert_ccw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_reg_manual_revert_ccw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_reg_manual_revert_ccw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3)          
#             if requestAction == 'fak_manual_revert_ccw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'fak_manual_revert_ccw'
#                 actionState = 'fak_manual_revert_ccw'
#                 skater.playAction('fak_manual_revert_ccw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_fak_manual_revert_ccw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_fak_manual_revert_ccw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3)
               

#             if requestAction == 'reg_manual_revert_cw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'reg_manual_revert_cw'
#                 actionState = 'reg_manual_revert_cw'
#                 skater.playAction('reg_manual_revert_cw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_reg_manual_revert_cw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_reg_manual_revert_cw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3)     
                     
#             if requestAction == 'fak_manual_revert_cw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'fak_manual_revert_cw'
#                 actionState = 'fak_manual_revert_cw'
#                 skater.playAction('fak_manual_revert_cw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_fak_manual_revert_cw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_fak_manual_revert_cw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3)           
                                      
#             if requestAction == 'fak_nmanual_revert_ccw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'fak_nmanual_revert_ccw'
#                 actionState = 'fak_nmanual_revert_ccw'
#                 skater.playAction('fak_nmanual_revert_ccw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_reg_manual_revert_ccw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_reg_manual_revert_ccw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3)             
#             if requestAction == 'fak_nmanual_revert_cw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'fak_nmanual_revert_cw'
#                 actionState = 'fak_nmanual_revert_cw'
#                 skater.playAction('fak_nmanual_revert_cw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_reg_manual_revert_cw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_reg_manual_revert_cw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3) 
#             if requestAction == 'reg_nmanual_revert_ccw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'reg_nmanual_revert_ccw'
#                 actionState = 'reg_nmanual_revert_ccw'
#                 skater.playAction('reg_nmanual_revert_ccw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_fak_manual_revert_ccw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_fak_manual_revert_ccw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3) 
                
#             if requestAction == 'reg_nmanual_revert_cw':
#                 own['actionTimer'] = 21
#                 own['actionState'] = 'reg_nmanual_revert_cw'
#                 actionState = 'reg_nmanual_revert_cw'
#                 skater.playAction('reg_nmanual_revert_cw', 70,10, layer=trans_layer, priority=8, play_mode=0, speed=3)
#                 deck.playAction('a_fak_manual_revert_cw', 70,10, layer=trans_layer, priority=1, play_mode=0, speed=3)
#                 trucks.playAction('a_fak_manual_revert_cw', 70,10, layer=trans_layer, priority=0, play_mode=0, speed=3) 
#             if requestAction == 'revert1':
#                 own['actionTimer'] = 18
#                 own['actionState'] = 'revert1'
#                 skater.playAction('revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 deck.playAction('a_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 trucks.playAction('a_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3) 
#             if requestAction == 'fak_revert1':
#                 own['actionTimer'] = 18
#                 own['actionState'] = 'fak_revert1'
#                 skater.playAction('fak_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 deck.playAction('a_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 trucks.playAction('a_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)                 
#             if requestAction == 'revert2':
#                 own['actionTimer'] = 18 
#                 own['actionState'] = 'revert2'
#                 skater.playAction('revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 deck.playAction('a_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 trucks.playAction('a_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3) 
#             if requestAction == 'fak_revert2':
#                 own['actionTimer'] = 18 
#                 own['actionState'] = 'fak_revert2'
#                 skater.playAction('fak_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 deck.playAction('a_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 trucks.playAction('a_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)                                
                 
#             if requestAction == 'revert3':
#                 own['actionTimer'] = 18 
#                 own['actionState'] = 'revert3'
#                 skater.playAction('revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 deck.playAction('a_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 trucks.playAction('a_revert1', 1,10, layer=trans_layer, play_mode=0, speed=.3)                                     
                
#             if requestAction == 'revert4':
#                 own['actionTimer'] = 18 
#                 own['actionState'] = 'revert4'
#                 skater.playAction('revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 deck.playAction('a_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)
#                 trucks.playAction('a_revert2', 1,10, layer=trans_layer, play_mode=0, speed=.3)                

#             if requestAction == 'reg_ollie':
#                 #print('*****reg ollie act')
#                 actionState = 'reg_ollie'
#                 own['actionState'] = 'reg_ollie'
#                 own['actionTimer'] = 38           
#                 skater.playAction('reg_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('t_reg_ollie', flip_start_frame,40, layer=flip_layer, play_mode=0, speed=1)
                
#             if requestAction == 'fak_ollie':
#                 actionState = 'fak_ollie'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('t_fak_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
            
#             if requestAction == 'reg_nollie':
#                 actionState = 'reg_nollie'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('t_fak_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)               
#             if requestAction == 'fak_nollie':
#                 actionState = 'fak_nollie'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('t_reg_ollie', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
                                
#             if requestAction == 'reg_kickflip':
#                 actionState = 'reg_kickflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_kickflip':
#                 actionState = 'fak_kickflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
                  
#             if requestAction == 'reg_varial_kickflip':
#                 actionState = 'reg_varial_kickflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_varial_kickflip':
#                 actionState = 'fak_varial_kickflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_nollie_varial_kickflip':
#                 actionState = 'reg_nollie_varial_kickflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_varial_kickflip':
#                 actionState = 'fak_nollie_varial_kickflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_nollie_varial_heelflip':
#                 actionState = 'reg_nollie_varial_heelflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_varial_heelflip':
#                 actionState = 'fak_nollie_varial_heelflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_varialkickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_varial_heelflip':
#                 actionState = 'reg_varial_heelflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_varialheelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_varialheelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_varialheelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_varial_heelflip':
#                 actionState = 'fak_nollie_varial_heelflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_varialheelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_varialheelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_varialheelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_nollie_kickflip':
#                 actionState = 'reg_nollie_kickflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_kickflip':
#                 actionState = 'fak_nollie_kickflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_kickflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_heelflip':
#                 actionState = 'reg_heelflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_heelflip':
#                 actionState = 'fak_heelflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
                   
#             if requestAction == 'reg_nollie_heelflip':
#                 actionState = 'reg_nollie_heelflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_heelflip':
#                 actionState = 'fak_nollie_heelflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
                
#             if requestAction == 'reg_shuvit':
#                 actionState = 'reg_shuvit'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_shuvit':
#                 actionState = 'fak_shuvit'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
              
#             if requestAction == 'reg_shuvit360':
#                 actionState = 'reg_shuvit360'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_shuvit360':
#                 actionState = 'fak_shuvit360'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 

#             if requestAction == 'reg_fsshuvit360':
#                 actionState = 'reg_fsshuvit360'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_fsshuvit360':
#                 actionState = 'fak_fsshuvit360'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                                           
#             if requestAction == 'reg_nollie_shuvit':
#                 actionState = 'reg_nollie_shuvit'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)  
                                   
#             if requestAction == 'fak_nollie_shuvit':
#                 actionState = 'fak_nollie_shuvit'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
                
#             if requestAction == 'reg_fsshuvit':
#                 actionState = 'reg_fsshuvit'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_fsshuvit':
#                 actionState = 'fak_fsshuvit'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
# #             
#             if requestAction == 'reg_nollie_fsshuvit':
#                 actionState = 'reg_nollie_fsshuvit'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)    
# #                                 
#             if requestAction == 'fak_nollie_fsshuvit':
#                 actionState = 'fak_nollie_fsshuvit'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 






#             if requestAction == 'reg_nollie_fsshuvit_360':
#                 actionState = 'reg_nollie_fsshuvit_360'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_fsshuvit_360':
#                 actionState = 'fak_nollie_fsshuvit_360'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
             


#             if requestAction == 'reg_nollie_shuvit360':
#                 actionState = 'reg_nollie_shuvit360'
#                 own['actionTimer'] = 38            
#                 skater.playAction('nollie_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_360fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_shuvit360':
#                 actionState = 'fak_nollie_shuvit360'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_fsshuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_360shuvit', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
             
#             if requestAction == 'reg_inward_heelflip':
#                 actionState = 'reg_inward_heelflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_inward_heelflip':
#                 actionState = 'fak_inward_heelflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)   

#             if requestAction == 'reg_hardflip':
#                 actionState = 'reg_hardflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_hardflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_hardflip.001', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_hardflip.001', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_hardflip':
#                 actionState = 'fak_hardflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_hardflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_hardflip.001', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_hardflip.001', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5) 
                               
#             if requestAction == 'reg_nollie_inward_heelflip':
#                 actionState = 'reg_nollie_inward_heelflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_nollie_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_inward_heelflip':
#                 actionState = 'fak_nollie_inward_heelflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_inward_heelflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)   

#             if requestAction == 'reg_nollie_hardflip':
#                 actionState = 'reg_nollie_hardflip'
#                 own['actionTimer'] = 38            
#                 skater.playAction('reg_hardflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_hardflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_hardflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)                     
#             if requestAction == 'fak_nollie_hardflip':
#                 actionState = 'fak_nollie_hardflip'
#                 own['actionTimer'] = 38  
#                 skater.playAction('fak_nollie_hardflip', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_hardflip.001', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_hardflip.001', flip_start_frame,20, layer=flip_layer, play_mode=0, speed=.5)              
                     
#             #reg_jump
#             if requestAction == 'reg_jump':
#                 actionState = 'reg_jump'
#                 own['actionTimer'] = 19                
#                 skater.playAction('reg_jump', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_jump', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_jump2', 1,10, layer=trans_layer, play_mode=0, speed=.5)

#             #fak_jump    
#             if requestAction == 'fak_jump':
#                 actionState = 'fak_jump'
#                 own['actionTimer'] = 19                
#                 skater.playAction('fak_jump', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_jump', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_jump', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#             #reg_onboard            
#             if requestAction == 'reg_onboard':
#                 actionState = 'reg_onboard'
#                 skater.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer) 
#                 skater.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)
#                 own['actionTimer'] = 40
#                 skater.playAction('reg_noffboard', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_offboard', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_noffboard', 20,1, layer=trans_layer, play_mode=0, speed=1)

#             if requestAction == 'fak_onboard':
#                 actionState = 'fak_onboard'
#                 skater.stopAction(loop_layer)
#                 deck.stopAction(loop_layer)
#                 trucks.stopAction(loop_layer) 
#                 skater.stopAction(trans_layer)
#                 deck.stopAction(trans_layer)
#                 trucks.stopAction(trans_layer)                
#                 own['actionTimer'] = 40
#                 skater.playAction('fak_noffboard', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_offboard', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_noffboard', 10,1, layer=trans_layer, play_mode=0, speed=.5)
                
#             if requestAction == 'reg_offboard' and l_actionState != 'reg_offboard':
#                 actionState = 'reg_offboard'
#                 own['actionTimer'] = 60
#                 skater.playAction('reg_noffboard', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_offboard', 1,40, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_noffboard', 1,40, layer=trans_layer, play_mode=0, speed=1)

#             if requestAction == 'fak_offboard' and l_actionState != 'fak_offboard':
#                 actionState = 'fak_offboard'                
#                 own['actionTimer'] = 60
#                 skater.playAction('fak_noffboard', 1,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_fak_offboard', 1,30, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_fak_noffboard', 1,30, layer=trans_layer, play_mode=0, speed=.5)                
                              
#             if requestAction == 'reg_powerslide_out':
#                 actionState = 'reg_powerslide_out'
#                 own['actionTimer'] = 40  
#                 skater.playAction('nreg_powerslide2', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 deck.playAction('a_reg_powerslide2_d', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 trucks.playAction('a_reg_powerslide2_t', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#             if requestAction == 'fak_powerslide_out':
#                 actionState = 'fak_powerslide_out'
#                 own['actionTimer'] = 40  
#                 skater.playAction('nfak_powerslide2', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 deck.playAction('a_fak_powerslide2_d', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 trucks.playAction('a_fak_powerslide2_t', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#             if requestAction == 'reg_fs_powerslide_out':
#                 actionState = 'reg_fs_powerslide_out'
#                 own['actionTimer'] = 40  
#                 skater.playAction('nreg_powerslide', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 deck.playAction('a_reg_powerslide', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 trucks.playAction('a_reg_powerslide', 20,0, layer=trans_layer, play_mode=0, speed=1.5)        
#             if requestAction == 'fak_fs_powerslide_out':
#                 actionState = 'fak_fs_powerslide_out'
#                 own['actionTimer'] = 40  
#                 skater.playAction('nfak_powerslide', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 deck.playAction('a_fak_powerslide_d', 20,0, layer=trans_layer, play_mode=0, speed=1.5)
#                 trucks.playAction('a_fak_powerslide_t', 20,0, layer=trans_layer, play_mode=0, speed=1.5)             
                
#             if requestAction == 'fak-reg_5050':
#                 actionState = 'fak-reg_5050'
#                 own['actionTimer'] = 10  
#                 skater.playAction('reg_5050', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 20,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 20,0, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'reg-fak_5050':
#                 actionState = 'fak-reg_5050'
#                 own['actionTimer'] = 10  
#                 skater.playAction('reg_5050', 30,40, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 20,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 20,0, layer=trans_layer, play_mode=0, speed=1)                                  
                                                    
#             if requestAction == 'fak-reg_roll':
#                 actionState = 'fak-reg_roll'
#                 own['actionTimer'] = 10  
#                 skater.playAction('nreg', 70,60, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 10,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 10,0, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'reg-fak_roll':
#                 actionState = 'fak-reg_roll'
#                 own['actionTimer'] = 10  
#                 skater.playAction('nreg', 60,70, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg', 10,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 10,0, layer=trans_layer, play_mode=0, speed=1)

# #333333333333333333333333


#             if requestAction == 'fak-reg_noseslide':
#                 actionState = 'fak-reg_noseslide'
#                 own['actionTimer'] = 20
#                 skater.playAction('reg_noses', 45,55, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_noses', 45,55, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_noses', 45,55, layer=trans_layer, play_mode=0, speed=.5)        
#             if requestAction == 'reg-fak_tailslide':
#                 actionState = 'reg-fak_tailslide'
#                 own['actionTimer'] = 20
#                 skater.playAction('reg_noses', 55,45, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_noses', 55,45, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_noses', 55,45, layer=trans_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak-reg_tailslide':
#                 actionState = 'fak-reg_tailslide'
#                 own['actionTimer'] = 20
#                 skater.playAction('reg_tails', 45,55, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_tails', 45,55, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_tails', 45,55, layer=trans_layer, play_mode=0, speed=.5)        
#             if requestAction == 'reg-fak_noseslide':
#                 actionState = 'reg-fak_noseslide'
#                 own['actionTimer'] = 20
#                 skater.playAction('reg_tails', 55,45, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg_tails', 55,45, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg_tails', 55,45, layer=trans_layer, play_mode=0, speed=.5)                        
        
        



# #333333333333333333333333333333
#             if requestAction == 'reg_hippy':
#                 actionState = 'reg_hippy'
#                 own['actionTimer'] = 60  
#                 skater.playAction('reg_hippy', 10,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)

#             if requestAction == 'fak_hippy':
#                 actionState = 'fak_hippy'
#                 own['actionTimer'] = 60  
#                 skater.playAction('fak_hippy', 10,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)                  

#             if requestAction == 'reg_hippy_ncw':
#                 actionState = 'reg_hippy_ncw'
#                 own['actionTimer'] = 60  
#                 skater.playAction('reg_hippy_ncw', 10,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#             if requestAction == 'reg_hippy_nccw':
#                 actionState = 'reg_hippy_nccw'
#                 own['actionTimer'] = 60  
#                 skater.playAction('reg_hippy_nccw', 10,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 30,0, layer=trans_layer, play_mode=0, speed=1) 
#             if requestAction == 'fak_hippy_ncw':
#                 actionState = 'fak_hippy_ncw'
#                 own['actionTimer'] = 60  
#                 skater.playAction('fak_hippy_ncw', 10,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)                               

#             if requestAction == 'fak_hippy_nccw':
#                 actionState = 'fak_hippy_nccw'
#                 own['actionTimer'] = 60  
#                 skater.playAction('fak_hippy_nccw', 10,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg', 30,0, layer=trans_layer, play_mode=0, speed=1)                               

#             if requestAction == 'reg_air-roll':
#                 actionState = 'reg_air-roll'
#                 own['actionTimer'] = 10
#                 skater.playAction('reg_air', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,40, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 30,40, layer=trans_layer, play_mode=1, speed=.5)  
#             if requestAction == 'fak_air-roll':
#                 actionState = 'fak_air-roll'
#                 own['actionTimer'] = 10
#                 skater.playAction('fak_air', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg', 30,40, layer=trans_layer, play_mode=0, speed=.5)

#             if requestAction == 'reg_roll-air':
#                 actionState = 'reg_roll-air'
#                 own['actionTimer'] = 10
#                 skater.playAction('reg_air', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 40,30, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 40,30, layer=trans_layer, play_mode=1, speed=.5)  
#             if requestAction == 'fak_roll-air':
#                 actionState = 'fak_roll-air'
#                 own['actionTimer'] = 10
#                 skater.playAction('fak_air', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 40,30, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg', 40,30, layer=trans_layer, play_mode=0, speed=.5)                

#             if requestAction == 'reg_5050-roll':
#                 actionState = 'reg_5050-roll'
#                 own['actionTimer'] = 10 
#                 skater.playAction('reg_air', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,40, layer=trans_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 30,40, layer=trans_layer, play_mode=1, speed=.5)  
            
#             if requestAction == 'fak_5050-roll':
#                 actionState = 'fak_5050-roll'
#                 own['actionTimer'] = 10
#                 skater.playAction('fak_air', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                 bdeck.playAction('b_reg', 30,40, layer=trans_layer, play_mode=0, speed=.5)
#                 #trucks.playAction('a_reg', 30,40, layer=trans_layer, play_mode=0, speed=.5) 
                     
#             if requestAction == 'reg_back_invert_out':
#                 own['actionTimer'] = 30
#                 actionState = 'reg_back_invert_out'
#                 skater.playAction('reg_back_invert_in.001', 30,1, layer=trans_layer, play_mode=0, speed=1)
#                 deck.playAction('a_reg_back_invert_in', 30,1, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_back_invert_in', 30,1, layer=trans_layer, play_mode=0, speed=1)            
#             if requestAction == 'fak_fr_invert_out':  
#                 own['actionTimer'] = 30
#                 actionState = 'fak_fr_invert_out'
#                 skater.playAction('fak_fr_invert', 30,1, layer=trans_layer, play_mode=0, speed=1)
#                 deck.playAction('a_fak_fr_invert', 30,1, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_fak_fr_invert', 30,1, layer=trans_layer, play_mode=0, speed=1) 
                
#             if requestAction == 'reg_throw':
#                 own['actionTimer'] = 40
#                 actionState = 'reg_throw'
#                 skater.playAction('reg_throw', 10,30, layer=throw_layer, priority=0, play_mode=0, speed=.5)      
                
#             if requestAction == 'fak_throw':
#                 own['actionTimer'] = 40
#                 actionState = 'fak_throw'                
#                 skater.playAction('fak_throw', 10,30, layer=throw_layer, priority=0, play_mode=0, speed=.5)
#             if requestAction == 'reg_fall1':
#                 own['actionTimer'] = 50
#                 actionState = 'reg_fall1'                
#                 skater.playAction('reg_fall1', 1,50, layer=trans_layer, priority=0, play_mode=0, speed=1)                
                
#             if requestAction == 'reg_air_tail to manual':
#                 own['actionTimer'] = 20
#                 actionState = 'reg_air_tail to manual'
#                 skater.playAction('reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                 bdeck.playAction('b_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1) 
                
#             if l_requestAction == 'fak_air_tail to manual':
#                 own['actionTimer'] = 20
#                 actionState = 'fak_air_tail to manual'                    
#                 skater.playAction('fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                 bdeck.playAction('b_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1) 
#             if requestAction == 'reg_air_nose to nmanual':
#                 own['actionTimer'] = 20
#                 actionState = 'reg_air_nose to nmanual'
#                 skater.playAction('reg_nmanual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                 bdeck.playAction('b_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1) 
                
#             if l_requestAction == 'fak_air_nose to nmanual':
#                 own['actionTimer'] = 20
#                 actionState = 'fak_air_nose to nmanual'                    
#                 skater.playAction('fak_nmanual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                 bdeck.playAction('b_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                                                                 
#             if requestAction == 'reg_sit_out':
#                 own['actionTimer'] = 28
#                 actionState = 'reg_sit_out'                     
#                 own['actionState'] = 'reg_sit_out'                     
#                 skater.playAction('reg_sit', 65, 30, layer=trans_layer, play_mode=0, speed=1)
#                 deck.playAction('a_reg_sit', 65,30, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_reg_sit', 65,30, layer=trans_layer, play_mode=0, speed=1)   
#             if requestAction == 'fak_sit_out':
#                 own['actionTimer'] = 28
#                 own['actionState'] = 'fak_sit_out'                     
#                 actionState = 'fak_sit_out'                     
#                 skater.playAction('fak_sit', 65, 30, layer=trans_layer, play_mode=0, speed=1)
#                 deck.playAction('a_fak_sit', 65,30, layer=trans_layer, play_mode=0, speed=1)
#                 trucks.playAction('a_fak_sit', 65,30, layer=trans_layer, play_mode=0, speed=1) 
                 
#             if requestAction == 'reg_ollie_north_out':
#                 actionState = 'reg_ollie_north_out'
#                 own['actionState'] = 'reg_ollie_north_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_ollie_north', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_ollie_north', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5) 
                    
#             if requestAction == 'reg_ollie_south_out':
#                 actionState = 'reg_ollie_south_out'
#                 own['actionState'] = 'reg_ollie_south_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_ollie_south', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_ollie_south', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)                       
                
#             if requestAction == 'fak_ollie_north_out':
#                 actionState = 'fak_ollie_north_out'
#                 own['actionState'] = 'fak_ollie_north_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_ollie_north', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_ollie_north', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)                                                           
#             if requestAction == 'fak_ollie_south_out':
#                 actionState = 'fak_ollie_south_out'
#                 own['actionState'] = 'fak_ollie_south_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_ollie_south', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_ollie_south', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)                   

#             if requestAction == 'frontside_grab_out':
#                 actionState = 'frontside_grab_out'
#                 own['actionState'] = 'frontside_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_fg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_fg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)

#             if requestAction == 'backside_grab_out':
#                 actionState = 'backside_grab_out'
#                 own['actionState'] = 'backside_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_bsg2', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_bsg2', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#             if requestAction == 'fak_frontside_grab_out':
#                 actionState = 'fak_frontside_grab_out'
#                 own['actionState'] = 'fak_frontside_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_fg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_fg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#             if requestAction == 'fak_backside_grab_out':
#                 actionState = 'fak_backside_grab_out'
#                 own['actionState'] = 'fak_backside_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_bg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_bg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)                                           
#             if requestAction == 'frontside_nose_grab_out':
#                 actionState = 'frontside_nose_grab_out'
#                 own['actionState'] = 'frontside_nose_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('frontside_nose_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('frontside_nose_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#             if requestAction == 'backside_nose_grab_out':
#                 actionState = 'backside_nose_grab_out'
#                 own['actionState'] = 'backside_nose_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('backside_nose_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('backside_nose_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)                                            
#             if requestAction == 'fak_frontside_nose_out':
#                 actionState = 'fak_frontside_nose_out'
#                 own['actionState'] = 'fak_frontside_nose_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_frontside_nose_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_frontside_nose_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5) 

#             if requestAction == 'fak_backside_nose_grab_out':
#                 actionState = 'fak_backside_nose_grab_out'
#                 own['actionState'] = 'fak_backside_nose_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_backside_nose_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_backside_nose_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)                                            
#             if requestAction == 'frontside_tail_grab_out':
#                 actionState = 'frontside_tail_grab_out'
#                 own['actionState'] = 'frontside_tail_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('frontside_tail_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('frontside_tail_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)

#             if requestAction == 'backside_tail_grab_out':
#                 actionState = 'backside_tail_grab_out'
#                 own['actionState'] = 'backside_tail_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('backside_tail_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('backside_tail_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#             if requestAction == 'fak_frontside_tail_grab_out':
#                 actionState = 'fak_frontside_tail_grab_out'
#                 own['actionState'] = 'fak_frontside_tail_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_frontside_tail_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_frontside_tail_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)

#             if requestAction == 'fak_backside_tail_grab_out':
#                 actionState = 'fak_backside_tail_grab_out'
#                 own['actionState'] = 'fak_backside_tail_grab_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_backside_tail_grab', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_backside_tail_grab', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1.5) 
                    
#             if requestAction == 'reg_judo_out':
#                 actionState = 'reg_judo_out'
#                 own['actionState'] = 'reg_judo_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_judo', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_judo', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1) 
#             if requestAction == 'fak_judo_out':
#                 actionState = 'fak_judo_out'
#                 own['actionState'] = 'fak_judo_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_judo', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_judo', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)  
# #---
#             if requestAction == 'reg_frigid_out':
#                 actionState = 'reg_frigid_out'
#                 own['actionState'] = 'reg_frigid_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_frigid', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_frigid', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1) 
#             if requestAction == 'fak_frigid_out':
#                 actionState = 'fak_frigid_out'
#                 own['actionState'] = 'fak_frigid_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_frigid', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_frigid', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)  
                    
#             if requestAction == 'reg_fsonefoot_out':
#                 actionState = 'reg_fsonefoot_out'
#                 own['actionState'] = 'reg_fsonefoot_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_fsonefoot', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_fsonefoot', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1) 
#             if requestAction == 'fak_fsonefoot_out':
#                 actionState = 'fak_fsonefoot_out'
#                 own['actionState'] = 'fak_fsonefoot_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_fsonefoot', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_fsonefoot', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)                      
#             if requestAction == 'reg_onefoot_out':
#                 actionState = 'reg_onefoot_out'
#                 own['actionState'] = 'reg_onefoot_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_onefoot', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_onefoot', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1) 
#             if requestAction == 'fak_onefoot_out':
#                 actionState = 'fak_onefoot_out'
#                 own['actionState'] = 'fak_onefoot_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_onefoot', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_onefoot', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=1)                      

#             if requestAction == 'reg_airwalk_out':
#                 actionState = 'reg_airwalk_out'
#                 own['actionState'] = 'reg_airwalk_out'
#                 own['actionTimer'] = 7
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_airwalk', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_airwalk', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_airwalk', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_airwalk', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_airwalk', 10,1, layer=trans_layer, play_mode=0, speed=1) 

# #--                    
          
                                       

#             if requestAction == 'reg_walk_air_out':
#                 actionState = 'reg_walk_air_out'
#                 own['actionState'] = 'reg_walk_air_out'
#                 own['actionTimer'] = 24
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_walk_air', cur_frame,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_reg_walk_air', cur_frame,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_reg_walk_air', cur_frame,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_walk_air', 10,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_reg_walk_air', 10,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_reg_walk_air', 10,40, layer=trans_layer, play_mode=0, speed=1.5)            
                    
#             if requestAction == 'fak_walk_air_out':
#                 actionState = 'fak_walk_air_out'
#                 own['actionState'] = 'fak_walk_air_out'
#                 own['actionTimer'] = 24
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_walk_air', cur_frame,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_fak_walk_air', cur_frame,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_fak_walk_air', cur_frame,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_walk_air', 10,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_fak_walk_air', 10,40, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_fak_walk_air', 10,40, layer=trans_layer, play_mode=0, speed=1.5)                     
                    
#             if requestAction == 'reg_air-walk_air':
#                 actionState = 'reg_air-walk_air'
#                 own['actionState'] = 'reg_air-walk_air'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('reg_walk_air', cur_frame,60, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_walk_air', cur_frame,60, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_walk_air', cur_frame,60, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('reg_walk_air', 50,60, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_walk_air', 50,60, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_walk_air', 50,60, layer=trans_layer, play_mode=0, speed=1)         

#             if requestAction == 'fak_air-walk_air':
#                 actionState = 'fak_air-walk_air'
#                 own['actionState'] = 'fak_air-walk_air'
#                 own['actionTimer'] = 9
#                 trans_playing = skater.isPlayingAction(trans_layer)
#                 if trans_playing:
#                     cur_frame = skater.getActionFrame(trans_layer)                   
#                 if trans_playing and cur_frame > 1:                
#                     skater.playAction('fak_walk_air', cur_frame,60, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_fak_walk_air', cur_frame,60, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_fak_walk_air', cur_frame,60, layer=trans_layer, play_mode=0, speed=1)
#                     own['actionTimer'] = cur_frame
#                 else:                                          
#                     skater.playAction('fak_walk_air', 50,60, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_fak_walk_air', 50,60, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_fak_walk_air', 50,60, layer=trans_layer, play_mode=0, speed=1)             
                    
                    
#             if requestAction == 'reg_fak_noseg':
#                 #stopActs()
#                 own['actionState'] = 'reg_fak_noseg'
#                 actionState = 'reg_fak_noseg'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_noseg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_noseg', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_noseg.002', 10,10, layer=trans_layer, play_mode=0, speed=1)   
                
#             if requestAction == 'fak_reg_tailg':
#                 own['actionState'] = 'fak_reg_tailg'
#                 own['actionTimer'] = 11               
#                 skater.playAction('reg_fak_noseg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_noseg', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_noseg.002', 10,10, layer=trans_layer, play_mode=0, speed=1)        
                
#             if requestAction == 'fak_reg_noseg':
#                 #stopActs()
#                 own['actionState'] = 'fak_reg_noseg'
#                 actionState = 'fak_reg_noseg'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_tailg', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_tailg', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_tailg.001', 10,10, layer=trans_layer, play_mode=0, speed=1)   
                
#             if requestAction == 'reg_fak_tailg':
#                 own['actionState'] = 'reg_fak_tailg'
#                 actionState = 'reg_fak_tailg'
#                 own['actionTimer'] = 11                
#                 skater.playAction('reg_fak_tailg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_tailg', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_tailg.001', 10,10, layer=trans_layer, play_mode=0, speed=1)   
                
                
                                          
#             if requestAction == 'reg_fak_nosegl':
#                 #stopActs()
#                 own['actionState'] = 'reg_fak_nosegl'
#                 actionState = 'reg_fak_nosegl'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_nosegl', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_nosegL', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_nosegL', 10,10, layer=trans_layer, play_mode=0, speed=1)  
#             if requestAction == 'reg_fak_nosegr':
#                 #stopActs()
#                 own['actionState'] = 'reg_fak_nosegr'
#                 actionState = 'reg_fak_nosegr'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_nosegr', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_nosegR', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_nosegR', 10,10, layer=trans_layer, play_mode=0, speed=1)  
                
#             if requestAction == 'reg_fak_tailgl':
#                 #stopActs()
#                 own['actionState'] = 'reg_fak_tailgl'
#                 actionState = 'reg_fak_tailgl'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_tailgl', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_tailgL', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_tailgL', 10,10, layer=trans_layer, play_mode=0, speed=1)                                  
                
#             if requestAction == 'reg_fak_tailgr':
#                 own['actionState'] = 'reg_fak_tailgr'
#                 own['actionTimer'] = 11               
#                 skater.playAction('reg_fak_tailgr', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_tailgR', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_tailgR', 10,10, layer=trans_layer, play_mode=0, speed=1)  
                
                
#             if requestAction == 'fak_reg_nosegl':
#                 #stopActs()
#                 own['actionState'] = 'fak_reg_nosegl'
#                 actionState = 'fak_reg_nosegl'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_tailgl', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_tailgL', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_tailgL', 10,10, layer=trans_layer, play_mode=0, speed=1)   
#             if requestAction == 'fak_reg_nosegr':
#                 #stopActs()
#                 own['actionState'] = 'fak_reg_nosegr'
#                 actionState = 'fak_reg_nosegr'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_tailgr', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_tailgR', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_tailgR', 10,10, layer=trans_layer, play_mode=0, speed=1)    
                
                
#             if requestAction == 'fak_reg_tailgl':
#                 #stopActs()
#                 own['actionState'] = 'fak_reg_tailgl'
#                 actionState = 'fak_reg_tailgl'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_nosegl', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_nosegL', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_nosegL', 10,10, layer=trans_layer, play_mode=0, speed=1)   
#             if requestAction == 'fak_reg_tailgr':
#                 #stopActs()
#                 own['actionState'] = 'fak_reg_tailgr'
#                 actionState = 'fak_reg_tailgr'
#                 own['actionTimer'] = 11
#                 #actionTimer = 9            
#                 skater.playAction('reg_fak_nosegr', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                 bdeck.playAction('b_reg_nosegR', 10,10, layer=trans_layer, play_mode=0, speed=1)
#                 #trucks.playAction('a_reg_nosegR', 10,10, layer=trans_layer, play_mode=0, speed=1)                                                
#         own['actionState'] = actionState
   
# #######################################        
#         #loop actions 
# ######################################        
#         #check if request is possible    
#         #if requestAction in requestActions_list:
#         if requestAction in requestActions_list and own['actionTimer'] == 0:    
#             #print(requestAction)
#             #reg_turnLeft
#             if requestAction == 'reg_turnLeft':
#                 actionState = 'reg_turnLeft'
#                 #in
#                 if l_actionState not in ['reg_turnLeft', 'reg_pump_left-leftb']:
#                     skater.playAction('nreg_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     ##trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nreg_left', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_left', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     ##trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#             #reg_turnRight        
#             if requestAction == 'reg_turnRight':
#                 actionState = 'reg_turnRight'
#                 #in
#                 if l_actionState not in ['reg_turnRight', 'reg_pump_right-rightb']:
#                     skater.playAction('nreg_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nreg_right', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_right', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)       
#             #fak_turnLeft
#             if requestAction == 'fak_turnLeft':
#                 actionState = 'fak_turnLeft'
#                 #in
#                 if l_actionState not in ['fak_turnLeft', 'fak_pump_left-leftb']:
#                     skater.playAction('nfak_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nfak_left', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_right', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#             #fak_turnRight        
#             if requestAction == 'fak_turnRight':
#                 actionState = 'fak_turnRight'
#                 #in
#                 if l_actionState not in ['fak_turnRight', 'fak_pump_right-rightb']:
#                     skater.playAction('nfak_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nfak_right', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_left', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
                    
# #nfak_pump_right                    
                    
#             if requestAction == 'reg_pump_left':
#                 actionState = 'reg_pump_left'
#                 #in
#                 if l_actionState not in ['reg_pump_left', 'reg_pump_left-left', 'reg_left-pump']:
#                     own['actionTimer'] = 18
#                     skater.playAction('nreg_pump_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     ##trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nreg_pump_left', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_left', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     ##trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#             #reg_pump_right     
#             if requestAction == 'reg_pump_right':
#                 actionState = 'reg_pump_right'
#                 #in
#                 if l_actionState not in ['reg_pump_right', 'reg_pump_right-right', 'reg_right-pump']:
#                     own['actionTimer'] = 18
#                     skater.playAction('nreg_pump_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nreg_pump_right', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_right', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
                    
#             if requestAction == 'fak_pump_left':
#                 actionState = 'fak_pump_left'
#                 #in
#                 if l_actionState not in ['fak_pump_left', 'fak_pump_left-left', 'fak_left-pump']:
#                     own['actionTimer'] = 18
#                     skater.playAction('nfak_pump_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nfak_pump_left', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_right', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#             #fak_turnRight        
#             if requestAction == 'fak_pump_right':
#                 actionState = 'fak_pump_right'
#                 #in
#                 if l_actionState not in ['fak_pump_right', 'fak_pump_right-right', 'fak_right-pump']:
#                     own['actionTimer'] = 18
#                     skater.playAction('nfak_pump_right', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg_left', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                 #loop
#                 else:
#                     skater.playAction('nfak_pump_right', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg_left', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)                     
                    
                    
                                        
                    
                    
#             #reg_fs_powerslide
#             if requestAction == 'reg_fs_powerslide':
#                 actionState = 'reg_fs_powerslide'
#                 if l_actionState != 'reg_fs_powerslide':
#                     skater.playAction('nreg_powerslide', 0,20, layer=trans_layer, priority=8, play_mode=0, speed=1.5)
#                     deck.playAction('a_reg_powerslide', 0,20, layer=trans_layer, priority=1, play_mode=0, speed=1.5)
#                     trucks.playAction('a_reg_powerslide', 0,20, layer=trans_layer, priority=0, play_mode=0, speed=1.5)        
#                 else:                           
#                     skater.playAction('nreg_powerslide', 20,80, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_powerslide', 20,80, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_powerslide', 20,80, layer=loop_layer, play_mode=1, speed=.5) 
#             #reg_powerslide        
#             if requestAction == 'reg_powerslide':
#                 actionState = 'reg_powerslide'
#                 if l_actionState != 'reg_powerslide':
#                     skater.playAction('nreg_powerslide2', 0,20, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_reg_powerslide2_d', 0,20, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_reg_powerslide2_t', 0,20, layer=trans_layer, priority=0, play_mode=0, speed=1.5)        
#                 else:                           
#                     skater.playAction('nreg_powerslide2', 20,80, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_powerslide2_d', 20,80, layer=loop_layer, speed=.5)
#                     trucks.playAction('a_reg_powerslide2_t', 20,80, layer=loop_layer, play_mode=1, speed=.5) 
#             #fak_fs_powerslide        
#             if requestAction == 'fak_fs_powerslide':
#                 actionState = 'fak_fs_powerslide'
#                 if l_actionState != 'fak_fs_powerslide':
#                     skater.playAction('nfak_powerslide', 0,20, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_fak_powerslide_d', 0,20, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_fak_powerslide_t', 0,20, layer=trans_layer, priority=0, play_mode=0, speed=1.5)        
#                 else:                           
#                     skater.playAction('nfak_powerslide', 20,80, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_fak_powerslide_d', 20,80, layer=loop_layer, speed=.5)
#                     trucks.playAction('a_fak_powerslide_t', 20,80, layer=loop_layer, play_mode=1, speed=.5) 
#             #fak_powerslide        
#             if requestAction == 'fak_powerslide':
#                 actionState = 'fak_powerslide'
#                 if l_actionState != 'fak_powerslide':
#                     skater.playAction('nfak_powerslide2', 0,20, layer=trans_layer, play_mode=0, speed=1.5)
#                     deck.playAction('a_fak_powerslide2_d', 0,20, layer=trans_layer, play_mode=0, speed=1.5)
#                     trucks.playAction('a_fak_powerslide2_t', 0,20, layer=trans_layer, priority=0, play_mode=0, speed=1.5)        
#                 else:                           
#                     skater.playAction('nfak_powerslide2', 20,80, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_fak_powerslide2_d', 20,80, layer=loop_layer, speed=.5)
#                     trucks.playAction('a_fak_powerslide2_t', 20,80, layer=loop_layer, play_mode=1, speed=.5)
                    
#             #reg_roll    
#             if requestAction == 'reg_roll':
#                 if l_actionState == 'reg_pump':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         #cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:
#                         skater.playAction('nreg_pump_in', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #print(cur_frame)
#                     else:   
#                         own['actionTimer'] = 20 
#                         skater.playAction('nreg_pump_in', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                 if l_actionState == 'reg_opos':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                     if trans_playing and cur_frame > 1:
#                         skater.playAction('noposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:   
#                         own['actionTimer'] = 20 
#                         skater.playAction('noposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=1) 
#                 if l_actionState == 'reg_nopos':
#                     flipping = skater.isPlayingAction(flip_layer)
#                     if flipping == 0:
#                         trans_playing = skater.isPlayingAction(trans_layer)
#                         if trans_playing:
#                             cur_frame = skater.getActionFrame(trans_layer)                
#                         if trans_playing and cur_frame > 1:
#                             skater.playAction('nnoposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                             bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                             #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         else: 
#                             own['actionTimer'] = 20   
#                             skater.playAction('nnoposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                             bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                             #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
                
#                 if l_actionState == 'reg_manual':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('reg_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_reg_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         trucks.playAction('a_reg_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:
#                         skater.playAction('reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                         trucks.playAction('a_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
                        
#                 if l_actionState == 'reg_nmanual':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('reg_nmanual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_fak_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_fak_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:
#                         skater.playAction('reg_nmanual', 10,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)

#                 if l_actionState == 'reg_turnLeft':
#                     trans_playing = skater.isPlayingAction(trans_layer)
                    
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)             
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('nreg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         ##trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5) 
#                     else:
#                         own['actionTimer'] = 20
#                         requestAction = 'reg_turnLeft_out'
#                         actionState = 'reg_turnLeft_out'
#                         skater.playAction('nreg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         ##trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5) 
#                 if l_actionState == 'reg_turnRight':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)                   
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('nreg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5) 
#                     else:
#                         own['actionTimer'] = 20
#                         requestAction = 'reg_turnRight_out'
#                         actionState = 'reg_turnRight_out'                        
#                         skater.playAction('nreg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5) 
                        
                        
#                 if requestAction == 'reg_roll':
#                     actionState = 'reg_roll' 
#                     skater.playAction('nreg', 1,60, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 1,40, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=loop_layer, play_mode=1, speed=.5)                           

                                                                                                                              
#             #fak_roll    
#             if requestAction == 'fak_roll':
#                 if l_actionState == 'fak_pump':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)    
#                     if trans_playing and cur_frame > 1:
#                         skater.playAction('nfak_pump_in', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:   
#                         own['actionTimer'] = 20 
#                         skater.playAction('nfak_pump_in', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                 if l_actionState == 'fak_opos':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)    
#                     if trans_playing and cur_frame > 1:
#                         skater.playAction('fak_oposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)

#                     else:
#                         own['actionTimer'] = 20    
#                         skater.playAction('fak_oposin', 20,1, layer=trans_layer, play_mode=0, speed=1) 
#                         bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                 if l_actionState == 'fak_nopos':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)    
#                     if trans_playing and cur_frame > 1:
#                         skater.playAction('fak_noposin', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:    
#                         own['actionTimer'] = 20
#                         skater.playAction('fak_noposin', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         bdeck.playAction('b_reg', 20,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=1) 
#                 if l_actionState == 'fak_manual':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         #cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('fak_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_fak_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_fak_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:
#                         skater.playAction('fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                         #trucks.playAction('a_fak_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
                        
#                 if l_actionState == 'fak_nmanual':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         #cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('fak_nmanual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_reg_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                         trucks.playAction('a_reg_manual', cur_frame,1, layer=trans_layer, play_mode=0, speed=1)
#                     else:
#                         skater.playAction('fak_nmanual', 10,1, layer=trans_layer, play_mode=0, speed=1)                
#                         bdeck.playAction('b_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)
#                         trucks.playAction('a_reg_manual', 10,1, layer=trans_layer, play_mode=0, speed=1)  
                        
#                 if l_actionState == 'fak_turnLeft':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         #cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('nfak_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     else:
#                         skater.playAction('nfak_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 20,1, layer=trans_layer, play_mode=0, speed=.5) 
#                 if l_actionState == 'fak_turnRight':
#                     trans_playing = skater.isPlayingAction(trans_layer)
#                     if trans_playing:
#                         cur_frame = skater.getActionFrame(trans_layer)
#                         #cur_frame -= 2                    
#                     if trans_playing and cur_frame > 1:                
#                         skater.playAction('nfak_right', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', cur_frame,1, layer=trans_layer, play_mode=0, speed=.5)
#                     else:
#                         skater.playAction('nfak_right', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         bdeck.playAction('b_reg_left', 10,1, layer=trans_layer, play_mode=0, speed=.5)
#                         #trucks.playAction('a_reg', 10,1, layer=trans_layer, play_mode=0, speed=.5)                                 
                
#                 if requestAction == 'fak_roll':
#                     actionState = 'fak_roll' 
#                     skater.playAction('nfak', 1,60, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 1,40, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,40, layer=loop_layer, play_mode=1, speed=.5)                                                                                                                          

#             #reg_opos
#             if requestAction == 'reg_opos' and own['actionTimer'] == 0:
#                 actionState = 'reg_opos'
#                 #in
#                 if l_actionState != 'reg_opos':
#                     skater.playAction('noposin', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                 #loop
#                 else:
                  
#                     skater.playAction('nopos', 1,40, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 1,40, layer=loop_layer, play_mode=1, speed=1)
            
#             #fak_opos
#             if requestAction == 'fak_opos':
#                 actionState = 'fak_opos'
#                 #in
#                 if l_actionState != 'fak_opos':
#                     skater.playAction('fak_oposin', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                 #loop
#                 else:
#                     skater.playAction('fak_opos', 1,40, layer=loop_layer, play_mode=1, speed=1) 
#                     bdeck.playAction('b_reg', 1,40, layer=loop_layer, play_mode=1, speed=1) 
#                     #trucks.playAction('a_reg', 1,40, layer=loop_layer, play_mode=1, speed=1) 
            
#             #reg_nopos
#             if requestAction == 'reg_nopos':
#                 actionState = 'reg_nopos'
#                 #in
#                 if l_actionState != 'reg_nopos':
#                     skater.playAction('nnoposin', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                 #loop
#                 else:
#                     skater.playAction('nnopos', 1,40, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=loop_layer, play_mode=1, speed=1) 
#                     #trucks.playAction('a_reg', 1,40, layer=loop_layer, play_mode=1, speed=1)
            
#             #fak_nopos
#             if requestAction == 'fak_nopos':
#                 actionState = 'fak_nopos'
#                 #in
#                 if l_actionState != 'fak_nopos':
#                     skater.playAction('fak_noposin', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                 #loop
#                 else:
#                     skater.playAction('fak_nopos', 1,40, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 1,40, layer=loop_layer, play_mode=1, speed=1) 
#                     #trucks.playAction('a_reg', 1,40, layer=loop_layer, play_mode=1, speed=1)                            
            
#             #reg_pump
#             if requestAction == 'reg_pump':
#                 actionState = 'reg_pump'
#                 if l_actionState not in ['reg_pump', 'reg_pump_left', 'reg_pump_right', 'reg_pump_left_out', 'reg_pump_right_out', 'reg_pump_right-rightb', 'reg_pump_left-leftb']:
#                     skater.playAction('nreg_pump_in', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #skater.playAction('noposin', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     own['actionTimer'] = 0
#                     skater.playAction('nreg_pump', 1,60, layer=loop_layer, play_mode=1, speed=.5)
#                     #skater.playAction('nopos', 1,40, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 1,60, layer=loop_layer, play_mode=1, speed=1) 
#                     #trucks.playAction('a_reg', 1,60, layer=loop_layer, play_mode=1, speed=1)        

#             #fak_pump
#             if requestAction == 'fak_pump':
#                 actionState = 'fak_pump'
#                 if l_actionState not in ['fak_pump', 'fak_pump_left', 'fak_pump_right', 'fak_pump_left_out', 'fak_pump_right_out', 'fak_pump_right-rightb', 'fak_pump_left-leftb']:
                    
#                     skater.playAction('nfak_pump_in', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,20, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     own['actionTimer'] = 0
#                     skater.playAction('nfak_pump.001', 1,60, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 1,60, layer=loop_layer, play_mode=1, speed=1) 
#                     #trucks.playAction('a_reg', 1,60, layer=loop_layer, play_mode=1, speed=1) 
                    
#             #reg_manual
#             if requestAction == 'reg_manual':
#                 actionState = 'reg_manual'
#                 #in
#                 if l_requestAction != 'reg_manual' and l_actionState != ('reg_air_tail', 'reg_manual_out'):
#                     skater.playAction('reg_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_reg_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 if l_requestAction != 'reg_manual' and l_actionState == 'reg_air_tail':
#                     own['actionTimer'] = 20
#                     actionState = 'reg_air_tail to manual'
#                     skater.playAction('reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                    
#                 #loop
#                 else:
#                     skater.playAction('reg_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)                
#                     bdeck.playAction('b_reg_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
#             #fak_manual
#             if requestAction == 'fak_manual':
#                 actionState = 'fak_manual'
#                 #in
#                 if l_requestAction != 'fak_manual' and l_actionState not in ('fak_air_tail', 'reg_manual_revert_ccw', 'fak_manual_out'):
#                     skater.playAction('fak_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_fak_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 if l_requestAction != 'fak_manual' and l_actionState == 'fak_air_tail':
#                     own['actionTimer'] = 20
#                     actionState = 'fak_air_tail to manual'                    
#                     skater.playAction('fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                    
#                 #loop
#                 else:
#                     skater.playAction('fak_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)                
#                     bdeck.playAction('b_fak_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
                    
#             #reg_nmanual
#             if requestAction == 'reg_nmanual':
#                 actionState = 'reg_nmanual'
#                 #in
#                 if l_requestAction != 'reg_nmanual' and l_actionState != 'reg_air_nose':
#                     skater.playAction('reg_nmanual', 1,10, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_fak_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 if l_requestAction != 'reg_nmanual' and l_actionState not in ('fak_manual_revert_ccw', 'reg_nmanual_out'):
#                     own['actionTimer'] = 20
#                     actionState = 'reg_air_nose to nmanual'                     
#                     skater.playAction('reg_nmanual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                    
#                 #loop
#                 else:
#                     skater.playAction('reg_nmanual', 10,70, layer=loop_layer, play_mode=1, speed=.5)                
#                     bdeck.playAction('b_fak_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
#             #fak_nmanual
#             if requestAction == 'fak_nmanual':
#                 actionState = 'fak_nmanual'
#                 #in
#                 if l_requestAction != 'fak_nmanual' and l_actionState not in ('fak_air_nose', 'reg_manual_revert_ccw', 'fak_nmanual_out'):
#                     skater.playAction('fak_nmanual', 1,10, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_reg_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_manual', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                 if l_requestAction != 'fak_nmanual' and l_actionState == 'fak_air_nose':
#                     own['actionTimer'] = 20
#                     actionState = 'fak_air_nose to nmanual'                      
#                     skater.playAction('fak_nmanual', 90,110, layer=trans_layer, play_mode=0, speed=1)                
#                     bdeck.playAction('b_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_manual', 90,110, layer=trans_layer, play_mode=0, speed=1)                    
#                 #loop
#                 else:
#                     skater.playAction('fak_nmanual', 10,70, layer=loop_layer, play_mode=1, speed=.5)                
#                     bdeck.playAction('b_reg_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_manual', 10,70, layer=loop_layer, play_mode=1, speed=.5) 
                 
#             if requestAction == 'reg_air':
#                 actionState = 'reg_air'
#                 skater.playAction('reg_air', 1,60, layer=loop_layer, play_mode=1, speed=.5)
#                 bdeck.playAction('b_reg', 1,2, layer=loop_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 1,2, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_air':
#                 actionState = 'fak_air'
#                 skater.playAction('fak_air', 1,60, layer=loop_layer, play_mode=1, speed=.5)        
#                 bdeck.playAction('b_reg', 1,2, layer=loop_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg', 1,2, layer=loop_layer, play_mode=1, speed=.5)                  
#             if requestAction == 'frontside_grab':
#                 actionState = 'frontside_grab'
#                 if l_actionState != 'frontside_grab':
#                     skater.playAction('reg_fg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('reg_fg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'backside_grab':
#                 actionState = 'backside_grab'
#                 if l_actionState != 'backside_grab':
#                     skater.playAction('reg_bsg2', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('reg_bsg2', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)        
                    
#             if requestAction == 'fak_frontside_grab':
#                 actionState = 'fak_frontside_grab'
#                 if l_actionState != 'fak_frontside_grab':
#                     skater.playAction('fak_fg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_fg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_backside_grab':
#                 actionState = 'fak_backside_grab'
#                 if l_actionState != 'fak_backside_grab':
#                     skater.playAction('fak_bg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_bg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)                                                                                                     
#             if requestAction == 'frontside_nose_grab':
#                 actionState = 'frontside_nose_grab'
#                 if l_actionState != 'frontside_nose_grab':
#                     skater.playAction('frontside_nose_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('frontside_nose_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)   
#             if requestAction == 'backside_nose_grab':
#                 actionState = 'backside_nose_grab'
#                 if l_actionState != 'backside_nose_grab':
#                     skater.playAction('backside_nose_grab', 1,10, layer=trans_layer, play_mode=0, speed=1) 
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)   
#                 else:
#                     skater.playAction('backside_nose_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#             if requestAction == 'fak_frontside_nose_grab':
#                 actionState = 'fak_frontside_nose_grab'
#                 if l_actionState != 'fak_frontside_nose_grab':
#                     skater.playAction('fak_frontside_nose_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_frontside_nose_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)   
#             if requestAction == 'fak_backside_nose_grab':
#                 actionState = 'fak_backside_nose_grab'
#                 if l_actionState != 'fak_backside_nose_grab':
#                     skater.playAction('fak_backside_nose_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_backside_nose_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)  

#             if requestAction == 'frontside_tail_grab':
#                 actionState = 'frontside_tail_grab'
#                 if l_actionState != 'frontside_tail_grab':
#                     skater.playAction('frontside_tail_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('frontside_tail_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)   
#             if requestAction == 'backside_tail_grab':
#                 actionState = 'backside_tail_grab'
#                 if l_actionState != 'backside_tail_grab':
#                     skater.playAction('backside_tail_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('backside_tail_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_frontside_tail_grab':
#                 actionState = 'fak_frontside_tail_grab'
#                 if l_actionState != 'fak_frontside_tail_grab':
#                     skater.playAction('fak_frontside_tail_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_frontside_tail_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)   
#             if requestAction == 'fak_backside_tail_grab':
#                 actionState = 'fak_backside_tail_grab'
#                 if l_actionState != 'fak_backside_tail_grab':
#                     skater.playAction('fak_backside_tail_grab', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_backside_tail_grab', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=.5) 
                     
#             if requestAction == 'reg_judo':
#                 actionState = 'reg_judo'
#                 if l_actionState != 'reg_judo':
#                     skater.playAction('reg_judo', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('reg_judo', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)    
#             if requestAction == 'fak_judo':
#                 actionState = 'fak_judo'
#                 if l_actionState != 'fak_judo':
#                     skater.playAction('fak_judo', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('fak_judo', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)                                         


#             if requestAction == 'reg_frigid':
#                 actionState = 'reg_frigid'
#                 if l_actionState != 'reg_frigid':
#                     skater.playAction('reg_frigid', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('reg_frigid', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)    
#             if requestAction == 'fak_frigid':
#                 actionState = 'fak_frigid'
#                 if l_actionState != 'fak_frigid':
#                     skater.playAction('fak_frigid', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('fak_frigid', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)                                         
#             if requestAction == 'reg_fsonefoot':
#                 actionState = 'reg_fsonefoot'
#                 if l_actionState != 'reg_fsonefoot':
#                     skater.playAction('reg_fsonefoot', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('reg_fsonefoot', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)    
#             if requestAction == 'fak_fsonefoot':
#                 actionState = 'fak_fsonefoot'
#                 if l_actionState != 'fak_fsonefoot':
#                     skater.playAction('fak_fsonefoot', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('fak_fsonefoot', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)  
#             if requestAction == 'reg_onefoot':
#                 actionState = 'reg_onefoot'
#                 if l_actionState != 'reg_onefoot':
#                     skater.playAction('reg_onefoot', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('reg_onefoot', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)    
#             if requestAction == 'fak_onefoot':
#                 actionState = 'fak_onefoot'
#                 if l_actionState != 'fak_onefoot':
#                     skater.playAction('fak_onefoot', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('fak_onefoot', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg', 10,30, layer=loop_layer, play_mode=1, speed=1)                                                            
#             if requestAction == 'reg_airwalk':
#                 actionState = 'reg_airwalk'
#                 if l_actionState != 'reg_airwalk':
#                     skater.playAction('reg_airwalk', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     deck.playAction('a_reg_airwalk', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_airwalk', 1,10, layer=trans_layer, play_mode=0, speed=.5)    
#                 else:
#                     skater.playAction('reg_airwalk', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     deck.playAction('a_reg_airwalk', 10,30, layer=loop_layer, play_mode=1, speed=1)
#                     trucks.playAction('a_reg_airwalk', 10,30, layer=loop_layer, play_mode=1, speed=1) 




# #'reg_frigid', 'reg_fsonefoot', 'reg_onefoot', 'fak_frigid', 'fak_fsonefoot', 'fak_onefoot'                              
                    
                    
                    
#             if requestAction == 'reg_ollie_north':
#                 actionState = 'reg_ollie_north'
#                 if l_actionState != 'reg_ollie_north':
#                     skater.playAction('reg_ollie_north', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('reg_ollie_north', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)                     
#             if requestAction == 'fak_ollie_north':
#                 actionState = 'fak_ollie_north'
#                 if l_actionState != 'fak_ollie_north':
#                     skater.playAction('fak_ollie_north', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_ollie_north', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5) 
#             if requestAction == 'reg_ollie_south':
#                 actionState = 'reg_ollie_south'
#                 if l_actionState != 'reg_ollie_south':
#                     skater.playAction('reg_ollie_south', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('reg_ollie_south', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)                     
#             if requestAction == 'fak_ollie_south':
#                 actionState = 'fak_ollie_south'
#                 if l_actionState != 'fak_ollie_south':
#                     skater.playAction('fak_ollie_south', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=1)    
#                 else:
#                     skater.playAction('fak_ollie_south', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)                                           
#             if requestAction == 'reg_noseg':
#                 actionState = 'reg_noseg'
#                 if l_actionState not in ['reg_noseg', 'reg_air_nose', 'fak_reg_tailg', 'reg_noseg_out']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)
#                     skater.playAction('reg_noseg.002', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_noseg', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_noseg.002', 1,10, layer=trans_layer, play_mode=0, speed=1)                           
#                 else:
#                     skater.playAction('reg_noseg.002', 11,40, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_noseg', 11,40, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_noseg.002', 11,40, layer=loop_layer, play_mode=1, speed=.5)                   

#             if requestAction == 'fak_noseg':
#                 actionState = 'fak_noseg'
#                 if l_actionState not in ['fak_noseg', 'fak_air_nose', 'reg_fak_tailg', 'fak_noseg_out']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_noseg', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_noseg', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_noseg', 40,30, layer=trans_layer, play_mode=0, speed=1)                            
#                 else:
#                     skater.playAction('fak_noseg', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_noseg', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_noseg', 30,1, layer=loop_layer, play_mode=1, speed=.5)                    
            
#             if requestAction == 'reg_tailg':
#                 actionState = 'reg_tailg'
#                 if l_actionState not in ['reg_tailg', 'reg_air_tail', 'fak_reg_noseg', 'fak_tailg_out']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_tailg.001', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailg', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailg.001', 40,30, layer=trans_layer, play_mode=0, speed=1)
                            
#                 else:
#                     skater.playAction('reg_tailg.001', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_tailg', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_tailg.001', 30,1, layer=loop_layer, play_mode=1, speed=.5)                   

#             if requestAction == 'fak_tailg':
#                 actionState = 'fak_tailg'
#                 if l_actionState not in ['fak_tailg', 'fak_air_tail', 'reg_fak_noseg', 'reg_tailg_out']:
#                     #print('fak_tailg in')
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_tailg', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailg', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailg', 40,30, layer=trans_layer, play_mode=0, speed=1)                            
#                 else:
#                     #print('tailging')
#                     skater.playAction('fak_tailg', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_tailg', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_tailg', 30,1, layer=loop_layer, play_mode=1, speed=.5)
                                                              
#             if requestAction == 'reg_tailgr':
#                 actionState = 'reg_tailgr'
#                 if l_actionState not in ['reg_tailgr', 'reg_air_tail', 'reg_tailgr_out', 'fak_reg_nosegr']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_tailgR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailgR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailgR', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('reg_tailgR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_tailgR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_tailgR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_tailgr':
#                 actionState = 'fak_tailgr'
#                 if l_actionState not in ['fak_tailgr', 'fak_air_tail', 'fak_tailgr_out', 'reg_fak_nosegr']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_tailgR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailgR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailgR', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_tailgR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_tailgR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_tailgR', 30,1, layer=loop_layer, play_mode=1, speed=.5)                    
#             if requestAction == 'reg_tailgl':
#                 actionState = 'reg_tailgl'
#                 if l_actionState not in ['reg_tailgl', 'reg_air_tail', 'reg_tailgl_out', 'fak_reg_nosegl']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_tailgL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailgL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailgL', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('reg_tailgL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_tailgL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_tailgL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_tailgl':
#                 actionState = 'fak_tailgl'
#                 if l_actionState not in ['fak_tailgl', 'fak_air_tail', 'fak_tailgl_out', 'reg_fak_nosegl']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_tailgL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailgL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailgL', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_tailgL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_tailgL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_tailgL', 30,1, layer=loop_layer, play_mode=1, speed=.5)



#             if requestAction == 'reg_nosegr':
#                 actionState = 'reg_nosegr'
#                 if l_actionState not in ['reg_nosegr', 'reg_air_tail', 'reg_nosegr_out', 'fak_reg_tailgr']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_nosegR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_nosegR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_nosegR', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('reg_nosegR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_nosegR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_nosegR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_nosegr':
#                 actionState = 'fak_nosegr'
#                 if l_actionState not in ['fak_nosegr', 'fak_air_nose', 'fak_nosegr_out', 'reg_fak_tailgr']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_nosegR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_nosegR', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_nosegR', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_nosegR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_nosegR', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_nosegR', 30,1, layer=loop_layer, play_mode=1, speed=.5)                    
#             if requestAction == 'reg_nosegl':
#                 actionState = 'reg_nosegl'
#                 if l_actionState not in ['reg_nosegl', 'reg_air_nose', 'reg_nosegl_out', 'fak_reg_tailgl']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_nosegL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_nosegL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_nosegL', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('reg_nosegL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_nosegL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_nosegL', 30,1, layer=loop_layer, play_mode=1, speed=.5)       
#             if requestAction == 'fak_nosegl':
#                 actionState = 'fak_nosegl'
#                 if l_actionState not in ['fak_nosegl', 'fak_air_nose', 'fak_nosegl_out', 'reg_fak_tailgl']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_nosegL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_nosegL', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_nosegL', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_nosegL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_nosegL', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_nosegL', 30,1, layer=loop_layer, play_mode=1, speed=.5)                    



#             if requestAction == 'reg_wall_r':
#                 actionState = 'reg_wall_r'
#                 if l_actionState != 'reg_wall_r':
#                     skater.playAction('reg_wall_r', 10,10, layer=trans_layer, play_mode=0, speed=.5)                    
#                     deck.playAction('a_reg_wall_r', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_wall_r', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 else:
#                     skater.playAction('reg_wall_r', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_wall_r', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_wall_r', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'reg_wall_l':
#                 actionState = 'reg_wall_l'
#                 if l_actionState != 'reg_wall_l':
#                     skater.playAction('reg_wall_l', 10,10, layer=trans_layer, play_mode=0, speed=.5)                    
#                     deck.playAction('a_reg_wall_l2', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_wall_l2', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 else:
#                     skater.playAction('reg_wall_l', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_wall_l2', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_wall_l2', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_wall_r':
#                 actionState = 'fak_wall_r'
#                 if l_actionState != 'fak_wall_r':
#                     skater.playAction('fak_wall_r', 10,10, layer=trans_layer, play_mode=0, speed=.5)                    
#                     deck.playAction('a_reg_wall_l2', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_wall_l2', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 else:
#                     skater.playAction('fak_wall_r', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_wall_l2', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_wall_l2', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_wall_l':
#                 actionState = 'fak_wall_l'
#                 if l_actionState != 'reg_wall_r':
#                     skater.playAction('fak_wall_l', 10,10, layer=trans_layer, play_mode=0, speed=.5)                    
#                     deck.playAction('a_reg_wall_r', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_wall_r', 10,10, layer=trans_layer, play_mode=0, speed=.5)
#                 else:
#                     skater.playAction('fak_wall_l', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_wall_r', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_wall_r', 10,10, layer=loop_layer, play_mode=1, speed=.5)                        

# ##############################

#             if requestAction == 'reg_tailslide':
#                 actionState = 'reg_tailslide'
#                 if l_actionState not in ['reg_tailslide', 'reg_air_tail', 'reg_tailslide_out', 'fak_noseslide', 'fak-reg_tailslide']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_noses', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_noses', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_noses', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_noses', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_noses', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_noses', 30,1, layer=loop_layer, play_mode=1, speed=.5)                    
#             if requestAction == 'fak_tailslide':
#                 actionState = 'fak_tailslide'
#                 if l_actionState not in ['fak_tailslide', 'fak_air_tail', 'reg_tailslide_out', 'reg_noseslide', 'reg-fak_tailslide']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_noses', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_noses', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_noses', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('reg_noses', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_noses', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_noses', 30,1, layer=loop_layer, play_mode=1, speed=.5)  


#             if requestAction == 'reg_noseslide':
#                 actionState = 'reg_noseslide'
#                 if l_actionState not in ['reg_noseslide', 'reg_air_nose', 'reg_tailslide_out', 'fak_tailslide', 'fak-reg_noseslide']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_tails', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tails', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tails', 40,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_tails', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_tails', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_tails', 30,1, layer=loop_layer, play_mode=1, speed=.5)    



#             if requestAction == 'fak_noseslide':
#                 actionState = 'fak_noseslide'
#                 if l_actionState not in ['fak_noseslide', 'fak_air_nose',  'reg_tailslide_out', 'reg_tailslide', 'reg-fak_noseslide']:
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_tails', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tails', 40,30, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tails', 40,30, layer=trans_layer, play_mode=0, speed=1)    
#                     print('fak nose in')    
#                 else:
#                     skater.playAction('reg_tails', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_tails', 30,1, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_tails', 30,1, layer=loop_layer, play_mode=1, speed=.5)                    

#             if requestAction == 'reg_5050':
#                 actionState = 'reg_5050'
#                 #own['actionTimer'] = 0
#                 #if l_actionState == 'reg_roll':
#                 if l_actionState != 'reg_5050' and l_actionState != 'fak-reg_5050':    
#                     skater.playAction('reg_5050', 50,70, layer=trans_layer, play_mode=0, speed=.5)
#                 else:
#                     skater.playAction('reg_5050', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)                                     
#             if requestAction == 'fak_5050':
#                 actionState = 'fak_5050'
#                 if l_actionState != 'fak_5050' and l_actionState != 'fak-reg_5050':    
#                     skater.playAction('fak_air', 40,34, layer=trans_layer, play_mode=0, speed=.5)
#                 #own['actionTimer'] = 0
#                 else:
#                     skater.playAction('fak_5050', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)

#             if requestAction == 'reg_bsboard':
#                 actionState = 'reg_bsboard'
#                 skater.playAction('reg_BS_Board2', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                 deck.playAction('b_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg_BS_Board2', 1,30, layer=loop_layer, play_mode=1, speed=.5)                                     
#             if requestAction == 'fak_bsboard':
#                 actionState = 'fak_bsboard'
#                 skater.playAction('fak_BS_Board2', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                 deck.playAction('b_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_fak_BS_Board2', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'reg_fsboard':
#                 actionState = 'reg_fsboard'
#                 skater.playAction('reg_FS_Board', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                 deck.playAction('b_reg', 1,30, layer=loop_layer, play_mode=1, speed=.5)
#                 #trucks.playAction('a_reg_FS_Board', 1,30, layer=loop_layer, play_mode=1, speed=.5)

#             if requestAction == 'reg_air_nose':
#                 actionState = 'reg_air_nose'
#                 if l_actionState != 'reg_air_nose':
#                     skater.stopAction(loop_layer)
#                     deck.stopAction(loop_layer)
#                     trucks.stopAction(loop_layer)
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                           
#                     skater.playAction('reg_noseg.002', 0,5, layer=trans_layer, play_mode=0, speed=.5)                
#                     bdeck.playAction('b_reg_noseg', 0,5, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg_noseg.002', 0,5, layer=trans_layer, play_mode=0, speed=.5)                        
#                 else:
#                     skater.playAction('reg_noseg.002', 5,5, layer=loop_layer, play_mode=1, speed=.5)                
#                     bdeck.playAction('b_reg_noseg', 5,5, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_noseg.002', 5,5, layer=loop_layer, play_mode=1, speed=.5)                     
#             if requestAction == 'fak_air_nose':
#                 actionState = 'fak_air_nose'
#                 if l_actionState != 'fak_air_nose':
#                     skater.stopAction(loop_layer)
#                     deck.stopAction(loop_layer)
#                     trucks.stopAction(loop_layer)
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_noseg', 40,35, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_noseg', 40,35, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_noseg', 40,35, layer=trans_layer, play_mode=0, speed=1)                           
#                 else:
#                     skater.playAction('fak_noseg', 35,35, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_noseg', 35,35, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_noseg', 35,35, layer=loop_layer, play_mode=1, speed=.5)                  
                    
#             if requestAction == 'reg_air_tail':
#                 actionState = 'reg_air_tail'
#                 if l_actionState != 'reg_air_tail':
#                     skater.stopAction(loop_layer)
#                     deck.stopAction(loop_layer)
#                     trucks.stopAction(loop_layer)
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('reg_tailg.001', 40,35, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_reg_tailg', 40,35, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_reg_tailg.001', 40,35, layer=trans_layer, play_mode=0, speed=1)                            
#                 else:
#                     #pass
#                     skater.playAction('reg_tailg.001', 35,35, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_tailg', 35,35, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_tailg.001', 35,35, layer=loop_layer, play_mode=1, speed=.5)                   
#             if requestAction == 'fak_air_tail':
#                 actionState = 'fak_air_tail'
#                 if l_actionState != 'fak_air_tail':
#                     skater.stopAction(loop_layer)
#                     deck.stopAction(loop_layer)
#                     trucks.stopAction(loop_layer)
#                     skater.stopAction(flip_layer)
#                     deck.stopAction(flip_layer)
#                     trucks.stopAction(flip_layer)                    
#                     skater.playAction('fak_tailg', 40,35, layer=trans_layer, play_mode=0, speed=1)
#                     bdeck.playAction('b_fak_tailg', 40,35, layer=trans_layer, play_mode=0, speed=1)
#                     #trucks.playAction('a_fak_tailg', 40,35, layer=trans_layer, play_mode=0, speed=1)                          
#                 else:
#                     skater.playAction('fak_tailg', 35,35, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_fak_tailg', 35,35, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_tailg', 35,35, layer=loop_layer, play_mode=1, speed=.5)
            
#             if requestAction == 'reg_stop':
#                 actionState = 'reg_stop'
#                 if l_actionState != 'reg_stop':
#                     skater.playAction('reg_stopin', 1,15, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,15, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,15, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('reg_stop', 30,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 30,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 30,30, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_stop':
#                 actionState = 'fak_stop'
#                 if l_actionState != 'fak_stop':
#                     skater.playAction('fak_stopin', 1,15, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,15, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,15, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('fak_stop', 30,30, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 30,30, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 30,30, layer=loop_layer, play_mode=1, speed=.5)  

#             if requestAction == 'reg_fp_rback':
#                 actionState = 'reg_fp_rback'
#                 if l_actionState != 'reg_fp_rback':
#                     skater.playAction('reg_fp_rback', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     deck.playAction('a_reg_fp_rback', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_fp_rback', 1,10, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('reg_fp_rback', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_reg_fp_rback', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_reg_fp_rback', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#             if requestAction == 'fak_fp_rback':
#                 actionState = 'fak_fp_rback'
#                 if l_actionState != 'fak_fp_rback':
#                     skater.playAction('fak_fp_rback', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     deck.playAction('a_fak_fp_rback', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_fak_fp_rback', 1,10, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('fak_fp_rback', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     deck.playAction('a_fak_fp_rback', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     trucks.playAction('a_fak_fp_rback', 10,10, layer=loop_layer, play_mode=1, speed=.5)         

#             if requestAction == 'reg_back_invert':
#                 actionState = 'reg_back_invert'
#                 if l_actionState != 'reg_back_invert':
#                     skater.playAction('reg_back_invert_in.001', 1,30, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_back_invert_in', 1,30, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_back_invert_in', 1,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('reg_back_invert_in.001', 30,30, layer=loop_layer, play_mode=1, speed=1)
#                     deck.playAction('a_reg_back_invert_in', 30,30, layer=loop_layer, play_mode=1, speed=1)
#                     trucks.playAction('a_reg_back_invert_in', 30,30, layer=loop_layer, play_mode=1, speed=1)
#             if requestAction == 'fak_fr_invert':
#                 actionState = 'fak_fr_invert'
#                 if l_actionState != 'fak_fr_invert':
#                     skater.playAction('fak_fr_invert', 1,30, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_fak_fr_invert', 1,30, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_fak_fr_invert', 1,30, layer=trans_layer, play_mode=0, speed=1)        
#                 else:
#                     skater.playAction('fak_fr_invert', 30,30, layer=loop_layer, play_mode=1, speed=1)
#                     deck.playAction('a_fak_fr_invert', 30,30, layer=loop_layer, play_mode=1, speed=1)
#                     trucks.playAction('a_fak_fr_invert', 30,30, layer=loop_layer, play_mode=1, speed=1)           
                    
#             if requestAction == 'reg_hippy_in':
#                 actionState = 'reg_hippy_in'
#                 if l_actionState != 'reg_hippy_in':
#                     skater.playAction('reg_hippy', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('reg_hippy', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)   
#             if requestAction == 'fak_hippy_in':
#                 actionState = 'fak_hippy_in'
#                 if l_actionState != 'fak_hippy_in':
#                     skater.playAction('fak_hippy', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     bdeck.playAction('b_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)
#                     #trucks.playAction('a_reg', 1,10, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('fak_hippy', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg', 10,10, layer=loop_layer, play_mode=1, speed=.5)

#     #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#     #walk anims            
#             #reg_idle    
#             if requestAction == 'reg_idle' and own['actionTimer'] == 0:
#                 actionState = 'reg_idle'
#                 if l_requestAction != 'reg_idle':
#                     if l_requestAction == 'reg_walk' or l_requestAction == 'reg_walkFast':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.stopAction(loop_layer)
#                         deck.stopAction(loop_layer)
#                         trucks.stopAction(loop_layer)
#                         if cur_frame > 11:#11:
#                             cur_frame += 1
#                             skater.playAction('reg_nwalk', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)
#                             bdeck.playAction('b_reg_walk', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)
#                             #trucks.playAction('a_reg_nwalk', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)  
#                         else:
#                             cur_frame -= 1
#                             skater.playAction('reg_nwalk', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                             bdeck.playAction('b_reg_walk', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                             #trucks.playAction('a_reg_nwalk', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                     else:
#                         trans_playing = skater.isPlayingAction(trans_layer)
#                 else:        
#                     skater.playAction('reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                     deck.playAction('a_reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)                            
#             #reg_idle_nb     
#             if requestAction == 'reg_idle_nb' and own['actionTimer'] == 0:
#                 actionState = 'reg_idle_nb'
#                 if l_requestAction != 'reg_idle_nb':
#                     if l_requestAction == 'reg_walk_nb' or l_requestAction == 'reg_walkFast_nb':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.stopAction(loop_layer)
#                         deck.stopAction(loop_layer)
#                         trucks.stopAction(loop_layer)
#                         if cur_frame > 11:
#                             cur_frame += 1
#                             skater.playAction('reg_nwalk_nb.005', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)
#                         else:
#                             cur_frame -= 1
#                             skater.playAction('reg_nwalk_nb.005', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                     else:
#                         trans_playing = skater.isPlayingAction(trans_layer)
#                         if trans_playing == 1:
#                             skater.playAction('reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 else:
#                     skater.playAction('reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)            
                                
#             #fak_idle                
#             if requestAction == 'fak_idle' and own['actionTimer'] == 0:
#                 actionState = 'fak_idle'
#                 if l_requestAction != 'fak_idle':
#                     if l_requestAction == 'fak_walk' or l_requestAction == 'fak_walkFast':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.stopAction(loop_layer)
#                         deck.stopAction(loop_layer)
#                         trucks.stopAction(loop_layer)                    
#                         if cur_frame > 11:
#                             cur_frame += 1
#                             skater.playAction('fak_nwalk', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)
#                             bdeck.playAction('b_fak_walk', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)
#                             #trucks.playAction('a_fak_nwalk', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)  
#                         else:
#                             cur_frame -= 1
#                             skater.playAction('fak_nwalk', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                             bdeck.playAction('b_fak_walk', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                             #trucks.playAction('a_fak_nwalk', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)                  
#                     else:
#                         trans_playing = skater.isPlayingAction(trans_layer)
#                 else:        
#                     skater.playAction('fak_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)     
                                     

#             #fak_idle_nb                
#             if requestAction == 'fak_idle_nb' and own['actionTimer'] == 0:
#                 actionState = 'fak_idle_nb'
#                 if l_requestAction != 'fak_idle_nb':
#                     if l_requestAction == 'fak_walk_nb' or l_requestAction == 'fak_walkFast_nb':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.stopAction(loop_layer)
#                         deck.stopAction(loop_layer)
#                         trucks.stopAction(loop_layer)                    
#                         if cur_frame > 11:
#                             cur_frame += 1
#                             skater.playAction('fak_nwalk_nb.005', cur_frame,35, layer=loop_layer, play_mode=0, speed=.5)
#                         else:
#                             cur_frame -= 1
#                             skater.playAction('fak_nwalk_nb.005', cur_frame,0, layer=loop_layer, play_mode=0, speed=.5)
#                     else:
#                         trans_playing = skater.isPlayingAction(trans_layer)
#                         if trans_playing == 1:
#                             #pass
#                             skater.playAction('fak_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
                            
#                 else:
#                     skater.playAction('fak_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
            
#             if requestAction == 'reg_idle1':
#                 actionState = 'reg_idle1'
#                 skater.playAction('reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#             if requestAction == 'reg_idle2':
#                 actionState = 'reg_idle2'
#                 skater.playAction('reg_idle2_', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle2', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle2', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'reg_idle3':
#                 actionState = 'reg_idle3'
#                 skater.playAction('reg_idle3', 1,240, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle3', 1,240, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle3', 1,240, layer=loop_layer, play_mode=0, speed=.5)          
#             if requestAction == 'reg_idle4':
#                 actionState = 'reg_idle4'        
#                 skater.playAction('reg_idle4', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle4', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle4', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'reg_idle5':
#                 actionState = 'reg_idle5'        
#                 skater.playAction('reg_idle5', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle5', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle5', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'reg_idle6':
#                 actionState = 'reg_idle6'        
#                 skater.playAction('reg_idle6', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle6', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle6', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'reg_idle7':
#                 actionState = 'reg_idle7'        
#                 skater.playAction('reg_idle7', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_reg_idle7', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_reg_idle7', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak_idle1':
#                 actionState = 'fak_idle1'
#                 skater.playAction('fak_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle1', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#             if requestAction == 'fak_idle2':
#                 actionState = 'fak_idle2'
#                 skater.playAction('fak_idle2_', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle2', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle2', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak_idle3':
#                 actionState = 'fak_idle3'
#                 skater.playAction('fak_idle3', 1,240, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle3', 1,240, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle3', 1,240, layer=loop_layer, play_mode=0, speed=.5)          
#             if requestAction == 'fak_idle4':
#                 actionState = 'fak_idle4'        
#                 skater.playAction('fak_idle4', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle4', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle4', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak_idle5':
#                 actionState = 'fak_idle5'        
#                 skater.playAction('fak_idle5', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle5', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle5', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak_idle6':
#                 actionState = 'fak_idle6'        
#                 skater.playAction('fak_idle6', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle6', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle6', 1,120, layer=loop_layer, play_mode=0, speed=.5) 
#             if requestAction == 'fak_idle7':
#                 actionState = 'fak_idle7'        
#                 skater.playAction('fak_idle7', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 deck.playAction('a_fak_idle7', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#                 trucks.playAction('a_fak_idle7', 1,120, layer=loop_layer, play_mode=0, speed=.5)
#             if requestAction == 'reg_idle2_nb':
#                 skater.playAction('reg_idle2_nb', 1,120, layer=loop_layer, play_mode=0, speed=.5)                                                    

#             #reg_walk    
#             if requestAction == 'reg_walk':
#                 actionState = 'reg_walk'
#                 #in
#                 if l_requestAction != 'reg_walk':
#                     if l_requestAction == 'reg_walkFast':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
#                         bdeck.playAction('b_reg_walk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
#                         #trucks.playAction('a_reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5) 
#                         skater.setActionFrame(cur_frame, loop_layer)
#                         deck.setActionFrame(cur_frame, loop_layer)
#                         trucks.setActionFrame(cur_frame, loop_layer)                                       
#                 #loop
#                 else:
#                     skater.playAction('reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
#                     bdeck.playAction('b_reg_walk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
            
#             #reg_walk_nb    
#             if requestAction == 'reg_walk_nb':
#                 actionState = 'reg_walk_nb'
#                 #in
#                 if l_requestAction != 'reg_walk_nb':
#                     if l_requestAction == 'reg_walkFast_nb':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('reg_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=.5) 
#                         skater.setActionFrame(cur_frame, loop_layer)
                                          
#                 #loop
#                 else:
#                     skater.playAction('reg_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=.5)               

#             #reg_walkFast    
#             if requestAction == 'reg_walkFast':
#                 actionState = 'reg_walkFast'
#                 #in
#                 if l_requestAction != 'reg_walkFast':
#                     if l_requestAction == 'reg_walk':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1)
#                         bdeck.playAction('b_reg_walk', 0,35, layer=loop_layer, play_mode=1, speed=1)
#                         #trucks.playAction('a_reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1) 
#                         skater.setActionFrame(cur_frame, loop_layer)
#                         deck.setActionFrame(cur_frame, loop_layer)
#                         trucks.setActionFrame(cur_frame, loop_layer)
#                         #print('set last walk frame', cur_frame)                                       
#                 #loop
#                 else:
#                     skater.playAction('reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1)
#                     bdeck.playAction('b_reg_walk', 0,35, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_reg_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1)          
#             #reg_walkFast_nb    
#             if requestAction == 'reg_walkFast_nb':
#                 actionState = 'reg_walkFast_nb'
#                 #in
#                 if l_requestAction != 'reg_walkFast_nb':
#                     if l_requestAction == 'reg_walk_nb':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('reg_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=1) 
#                         skater.setActionFrame(cur_frame, loop_layer)
                                          
#                 #loop
#                 else:
#                     skater.playAction('reg_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=1)                   
            
#             #fak_walk
#             if requestAction == 'fak_walk':
#                 #in
#                 actionState = 'fak_walk'
#                 if l_requestAction != 'fak_walk':
#                     if l_requestAction == 'fak_walkFast':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         #actionState = 'fak_walk'
#                         skater.playAction('fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)    
#                         bdeck.playAction('b_fak_walk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
#                         #trucks.playAction('a_fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)      
#                         skater.setActionFrame(cur_frame, loop_layer)
#                         deck.setActionFrame(cur_frame, loop_layer)
#                         trucks.setActionFrame(cur_frame, loop_layer)
#                     #pass
#                 #loop
#                 else:
#                     skater.playAction('fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)    
#                     bdeck.playAction('b_fak_walk', 0,35, layer=loop_layer, play_mode=1, speed=.5)
#                     #trucks.playAction('a_fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=.5)

#             #fak_walk_nb    
#             if requestAction == 'fak_walk_nb':
#                 actionState = 'fak_walk_nb'
#                 #in
#                 if l_requestAction != 'fak_walk_nb':
#                     if l_requestAction == 'fak_walkFast_nb':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('fak_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=.5) 
#                         skater.setActionFrame(cur_frame, loop_layer)
                                          
#                 #loop
#                 else:
#                     skater.playAction('fak_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=.5)
            
#             #fak_walkFast            
#             if requestAction == 'fak_walkFast':
#                 actionState = 'fak_walkFast'
#                 #in
#                 if l_requestAction != 'fak_walkFast':
#                     if l_requestAction == 'fak_walk':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1)         
#                         bdeck.playAction('b_fak_walk', 0,35, layer=loop_layer, play_mode=1, speed=1)
#                         #trucks.playAction('a_fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1)       
#                         skater.setActionFrame(cur_frame, loop_layer)
#                         deck.setActionFrame(cur_frame, loop_layer)
#                         trucks.setActionFrame(cur_frame, loop_layer)                    
#                 #loop
#                 else:
#                     skater.playAction('fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1)             
#                     bdeck.playAction('b_fak_walk', 0,35, layer=loop_layer, play_mode=1, speed=1)
#                     #trucks.playAction('a_fak_nwalk', 0,35, layer=loop_layer, play_mode=1, speed=1) 
            
#             #fak_walkFast_nb    
#             if requestAction == 'fak_walkFast_nb':
#                 actionState = 'fak_walkFast_nb'
#                 #in
#                 if l_requestAction != 'fak_walkFast_nb':
#                     if l_requestAction == 'fak_walk_nb':
#                         cur_frame = skater.getActionFrame(loop_layer)
#                         skater.playAction('fak_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=1) 
#                         skater.setActionFrame(cur_frame, loop_layer)
                                          
#                 #loop
#                 else:
#                     skater.playAction('fak_nwalk_nb.005', 0,35, layer=loop_layer, play_mode=1, speed=1)  

#             if requestAction == 'reg_dropin_pos':
#                 actionState = 'reg_dropin_pos'
#                 if l_actionState != 'reg_dropin_pos':
#                     skater.playAction('reg_dropin3', 30,50, layer=trans_layer, play_mode=0, speed=.5)
#                     deck.playAction('a_reg_dropin3', 30,50, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_reg_dropin3', 30,50, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('reg_dropin3', 50,60, layer=loop_layer, play_mode=1, speed=.75)
#                     deck.playAction('a_reg_dropin3', 50,60, layer=loop_layer, play_mode=1, speed=.75)
#                     trucks.playAction('a_reg_dropin3', 50,60, layer=loop_layer, play_mode=1, speed=.75) 
                    
                    
#             if requestAction == 'fak_dropin_pos':
#                 actionState = 'fak_dropin_pos'
#                 if l_actionState != 'fak_dropin_pos':
#                     skater.playAction('nfak_dropin', 30,50, layer=trans_layer, play_mode=0, speed=.5)
#                     deck.playAction('a_fak_dropin', 30,50, layer=trans_layer, play_mode=0, speed=.5)
#                     trucks.playAction('a_fak_dropin', 30,50, layer=trans_layer, play_mode=0, speed=.5)        
#                 else:
#                     skater.playAction('nfak_dropin', 50,60, layer=loop_layer, play_mode=1, speed=.75)
#                     deck.playAction('a_fak_dropin', 50,60, layer=loop_layer, play_mode=1, speed=.75)
#                     trucks.playAction('a_fak_dropin', 50,60, layer=loop_layer, play_mode=1, speed=.75) 

#             if requestAction == 'reg_sit':
#                 actionState = 'reg_sit'
#                 if l_actionState != 'reg_sit':      
#                     skater.playAction('reg_sit', 1,65, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_sit', 1,65, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_sit', 1,65, layer=trans_layer, play_mode=0, speed=1)                    
#                 else:
#                     skater.playAction('reg_sit', 65,65, layer=loop_layer, play_mode=1, speed=0)
#                     deck.playAction('a_reg_sit', 65,65, layer=loop_layer, play_mode=1, speed=0)
#                     trucks.playAction('a_reg_sit', 65,65, layer=loop_layer, play_mode=1, speed=0)

#             if requestAction == 'fak_sit':
#                 actionState = 'fak_sit'
#                 if l_actionState != 'fak_sit':      
#                     skater.playAction('fak_sit', 1,65, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_fak_sit', 1,65, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_fak_sit', 1,65, layer=trans_layer, play_mode=0, speed=1)                    
#                 else:
#                     skater.playAction('fak_sit', 65,65, layer=loop_layer, play_mode=1, speed=0)
#                     deck.playAction('a_fak_sit', 65,65, layer=loop_layer, play_mode=1, speed=0)
#                     trucks.playAction('a_fak_sit', 65,65, layer=loop_layer, play_mode=1, speed=0)   
                    
#             if requestAction == 'reg_walk_air':
#                 actionState = 'reg_walk_air'
#                 if l_actionState not in ('reg_walk_air', 'reg_jump', 'reg_air-walk_air'):      
#                     skater.playAction('reg_walk_air', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_reg_walk_air', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_reg_walk_air', 1,10, layer=trans_layer, play_mode=0, speed=1)                    
#                 else:
#                     skater.playAction('reg_walk_air', 10,10, layer=loop_layer, play_mode=1, speed=0)
#                     deck.playAction('a_reg_walk_air', 10,10, layer=loop_layer, play_mode=1, speed=0)
#                     trucks.playAction('a_reg_walk_air', 10,10, layer=loop_layer, play_mode=1, speed=0)                                        
#             if requestAction == 'fak_walk_air':
#                 actionState = 'fak_walk_air'
#                 if l_actionState not in ('fak_walk_air', 'fak_jump', 'fak_air-walk_air'):      
#                     skater.playAction('fak_walk_air', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     deck.playAction('a_fak_walk_air', 1,10, layer=trans_layer, play_mode=0, speed=1)
#                     trucks.playAction('a_fak_walk_air', 1,10, layer=trans_layer, play_mode=0, speed=1)                    
#                 else:
#                     skater.playAction('fak_walk_air', 10,10, layer=loop_layer, play_mode=1, speed=0)
#                     deck.playAction('a_fak_walk_air', 10,10, layer=loop_layer, play_mode=1, speed=0)
#                     trucks.playAction('a_fak_walk_air', 10,10, layer=loop_layer, play_mode=1, speed=0)                            
#             own['actionState'] = actionState
#         else:
#             pass
#         own['actionState'] = actionState
#         return actionState
            
#     l_playing = skater.isPlayingAction(loop_layer)
#     t_playing = skater.isPlayingAction(trans_layer)        
#     if l_playing == 1 and t_playing == 1:
#         skater.stopAction(loop_layer)
#         trucks.stopAction(loop_layer)
#         deck.stopAction(loop_layer)
    
#     ###
#     if own['actionTimer'] > 0:
#         own['actionTimer'] -= 1
    
# ##############
#     #turn off land for manuals
# #    if (requestAction == 'reg_manual' or requestAction == 'fak_manual' or requestAction == 'reg_nmanual' or requestAction == 'fak_nmanual') and (actionState == 'reg_land' or actionState == 'fak_land'):
# #        own['actionTimer'] = 0
                  
#     if (own['actionTimer'] == 0 or requestAction == 'reg_land' or requestAction == 'fak_land') or requestAction in jump_overrideList:
        
#         actionState = updateAction(requestAction, actionState)
        
# #    if (own['actionState'] == 'reg_land' or own['actionState'] == 'reg_land') and own['actionTimer'] > 0:
# #        own['clear_request'] = 1    
  
# #blend test ===============-----------------------=========================
#     # if own['inc'] == 1 and own['actionState'] == 'reg_roll' or own['actionState'] == 'fak-reg_roll':
#     #     print('incing')
#     #     if own['blending_val'] > 0.5:
#     #         own['blending_val'] -= .5
#     #         print(own['blending_val'])
#     #     skater.stopAction(5)     
#     #     skater.playAction('blend_test', 10,10, layer=5, play_mode=1, speed=1, layer_weight=own['blending_val'])
    

#     # elif own['inc'] == 1 and own['actionState'] == 'fak_roll' or own['actionState'] == 'reg-fak_roll' :
#     #     print('incing')
#     #     if own['blending_val'] > 0.5:
#     #         own['blending_val'] -= .1
#     #         print(own['blending_val'])
#     #     skater.stopAction(5)     
#     #     skater.playAction('nfak_pump_in', 30,40, layer=5, play_mode=1, speed=1, layer_weight=own['blending_val'])
#     # else:
#     #     pass
#         # if 'blending_val' in own:
#         #     if own['blending_val'] < .95:
#         #         own['blending_val'] += .05
#         #         skater.stopAction(5)     
#         #         skater.playAction('nfak_pump_in', 30,40, layer=5, play_mode=1, speed=1, layer_weight=own['blending_val'])                
#         #     else: 
#         #         own['blending_val'] = 1.0
#         #         skater.stopAction(5)     
#         # else:
#         #     own['blending_val'] = 0         


# ###################  
    
#     #debug
    
#     cur_frame = -1
#     trans_playing = skater.isPlayingAction(trans_layer)
#     if trans_playing:
#         cur_frame = skater.getActionFrame(trans_layer)  
#         cur_frame = round(cur_frame, 2)     
#     #print('oG: ', og_request, 'rA:', requestAction, '|aS:', own['actionState'], 'q', queueAction, own['actionTimer'], 'cf', cur_frame)
#     #print('rA:', requestAction, '|aS:', own['actionState'], own['actionTimer'], 'cf', cur_frame)
#     cur_frame = skater.getActionFrame(trans_layer)
#     #print(cur_frame)
#     def printplaying():
#         splaying_layers = 'S: '
#         playing_layers = 'D: '
#         tplaying_layers = 'T: '
#         for x in range(9900):
#             if skater.isPlayingAction(x):
#             #if trucks.isPlayingAction(x):
#             #if skater.isPlayingAction(x):                        
#                 splaying_layers += str(x)
#                 splaying_layers += ' '        
#             if deck.isPlayingAction(x):
#             #if trucks.isPlayingAction(x):
#             #if skater.isPlayingAction(x):                        
#                 playing_layers += str(x)
#                 playing_layers += ' '
#             if trucks.isPlayingAction(x):
#             #if trucks.isPlayingAction(x):
#             #if skater.isPlayingAction(x):                        
#                 tplaying_layers += str(x)
#                 tplaying_layers += ' '            
#         print(splaying_layers, playing_layers, tplaying_layers)
#     #printplaying()     

#     if actionState == 'empty':
#         #print('EMPTY ACTION!!!')
#         skater.stopAction(loop_layer)
#         deck.stopAction(loop_layer)
#         trucks.stopAction(loop_layer)    
#     #set last variables
#     own['l_actionState'] = actionState
#     own['l_requestAction'] = requestAction
#     own['queueAction'] = queueAction

    #actionsFSM.main(cont)
    actionPlayer.main(cont)
    
main()
