import bge
dict = bge.logic.globalDict
import sound_man
#====================================     

State = type("State", (object,), {})
#====================================     
class State(object):
	def __init__(self, FSM):
		self.FSM = FSM
		self.timer = 0
		self.startTime = 0
	def Enter(self):
		self.timer = 0
		self.startTime = 0
	def Execute(self):
		print('Executing')
	def Exit(self):
		print('Exiting')

#==================================== 
			
class Example(State):
	def __init__(self,FSM):
		super(Example, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		super(Example, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		#self.FSM.ToTransition('toExample')
		
	def Exit(self):
		pass

#==================================== 

class Startup(State):
	def __init__(self,FSM):
		super(Startup, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		super(Startup, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1

		if self.FSM.stateLife == 5:
			self.FSM.ToTransition('toWalk')
		print('player FSM')
		
	def Exit(self):
		pass

#==================================== 
			
class Walk(State):
	def __init__(self,FSM):
		super(Walk, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		o = self.FSM.owner.obj
		c = self.FSM.owner
		o['getoffboard'] = False
		o['getonboard'] = False

		if c.arm == None:
			
			c.arm = o.childrenRecursive['Char4']
			c.deck_arm = o.childrenRecursive['deck_arm']
		self.walk_weight = 1
		self.run_weight = 1
		self.run_speed = 0.0
		self.turn_weight = 0
		self.FSM.owner.walking = True
		dict['walking'] = True
		dict['walk'] = 1
		print('fsm enter walk')

		super(Walk, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		o = self.FSM.owner.obj
		c = self.FSM.owner
		#print(o.worldPosition.z)
		#print(o['getonboard'], 'dict onboard')
		#print(dict['walk'], 'fsm walk walk')
		# if self.FSM.stateLife == 2:
		#     if o['stance']:
		#         o.applyRotation([0,0,3.14], True)
		#         o['stance'] = False
		#         o['requestAction'] = 'fak_offboard' 
		#         print('request fak offboard')            
		#     else:
		#         o['requestAction'] = 'reg_offboard'                 
		#         print('request reg offboard') 
		# if self.FSM.stateLife > 5:
		#     o['requestAction'] = 'reg_idle'                 
		c.align_walk_z()
		ground_rays = c.get_ground_ray()
		#77777777777777777777777777777777777777777777777777777777777777777777777777777777777
		c.get_hang_align_ray()
		c.move_walk_cam()
		c.check_reset_point()       
		c.check_throw()
		c.check_pause()
		c.check_idle(False)
		c.idle_anim()

		if ground_rays[0][0] != None:
			dist = c.get_ground_dist(ground_rays)
			if dist > .7:
				self.FSM.ToTransition('toWalkAir')
				#print('---falling')
			else:
				c.set_walk_z(dist)
			#print(dist, 'setting height')

		if self.FSM.stateLife > 4:
			self.check_onboard()
			self.check_jump()

			moving = c.walk_movement()
			
			self.get_walk_weight()
			self.idle_blend()
		else:
			dict['walk'] = 1


	def check_onboard(self):

		o = self.FSM.owner.obj
		#print(o['getonboard'], 'getonboard')

		
		if dict['walk'] == 0:
			o['getonboard'] = True
			#self.FSM.ToTransition('toRoll')
			self.FSM.ToTransition('toWalkOnboard')
			#print('onboard', dict['walk'], o['getonboard'])
		if dict['yBut'] == 1 and dict['last_yBut'] == 0:
		
			
			dropin = self.check_dropin()
			print('-----dropin', dropin)
			if dropin == None:
				o['getonboard'] = True

				self.FSM.ToTransition('toWalkOnboard')
			else:
				self.FSM.owner.dropin_obj = dropin
				self.FSM.ToTransition('toDropin')



	def check_dropin(self):
		dr = None
		ground_rays = self.FSM.owner.get_dropin_rays()
		for x in ground_rays:
			if x[0] != None:
				#print(x[0], '---checked')
				if 'coping' in x[0]:
					print('dropin collided')
					dr = x
		return dr

	def check_jump(self):
		if dict['xBut'] == True or dict['kb_space'] == 1: 
			if dict['last_xBut'] == 0:
				self.FSM.ToTransition('toWalkJump')
				

	def idle_blend(self):
		arm = self.FSM.owner.arm
		deck = self.FSM.owner.deck_arm
		arm.playAction('reg_idle1', 1,120, layer=1, play_mode=1, speed=.5, blendin=10)
		deck.playAction('b_reg_idle1', 1,120, layer=1, play_mode=1, speed=1, blendin=10)
		if self.FSM.stateLife > 20:
			frame = arm.getActionFrame(2) + .5 * self.run_speed
			if frame > 30:
				frame = 0
			arm.stopAction(2)
			deck.stopAction(2)
			arm.playAction('reg_nwalk3', 0,30, layer=2, play_mode=1, speed=self.run_speed, blendin=0, layer_weight=self.walk_weight)
			deck.playAction('b_reg_walk', 0,30, layer=2, play_mode=1, speed=self.run_speed, blendin=0, layer_weight=self.walk_weight)
			#b_reg_walk
			arm.setActionFrame(frame, 2)
			deck.setActionFrame(frame, 2)
			#print(self.walk_weight, frame)

			

			frame = arm.getActionFrame(3) + .5 * self.run_speed
			if frame > 30:
				frame = 0
			arm.stopAction(3)
			deck.stopAction(3)
			arm.playAction('reg_run.003', 0,30, layer=3, play_mode=1, speed=self.run_speed, blendin=0, layer_weight=self.run_weight)
			deck.playAction('b_reg_run', 0,30, layer=3, play_mode=1, speed=self.run_speed, blendin=0, layer_weight=self.run_weight)
			
			arm.setActionFrame(frame, 3)
			deck.setActionFrame(frame, 3)
			#print(frame, 'frame')
			#print(self.run_weight, frame)
			
		# if self.turn_weight > 0:
		#     tw = abs(self.turn_weight - 1)
		#     #tw = self.turn_weight
		#     arm.stopAction(4)
		#     arm.playAction('bwalk_right', 1,60, layer=4, play_mode=1, speed=self.run_speed, blendin=0, layer_weight=tw)
		# elif self.turn_weight < 0:
		#     tw = abs(abs(self.turn_weight) - 1)
		#     #tw = self.turn_weight
		#     arm.stopAction(4)
		#     arm.playAction('bwalk_left', 1,60, layer=4, play_mode=1, speed=self.run_speed, blendin=0, layer_weight=tw)
			#print('walk_weight', self.walk_weight)
			if self.walk_weight < .8:
				if frame > 15:
					if self.FSM.owner.step == False:
						self.FSM.owner.step = True
						
						if frame < 23:
							#print('step left')
							dict['camera']['sndmgr'].queue_sound(['s_l_1', self.FSM.owner.obj, dict['camera']])
				else:
					if self.FSM.owner.step == True:
						self.FSM.owner.step = False
						if frame < 8:
							dict['camera']['sndmgr'].queue_sound(['s_r_1', self.FSM.owner.obj, dict['camera']])
							#print('step right')
					
			# if frame > 15 and self.FSM.owner.step:
			#     self.FSM.owner.step = True
			#     print('step left')
			# if frame < 15 and not self.FSM.owner.step:
			#     self.FSM.owner.step = False
			#     print('step right')
			#print('frame', frame)

	def get_walk_weight(self):
		#print(self.FSM.owner.linearVelocity.y) 


		o = self.FSM.owner.obj
		w = abs(o.linearVelocity.x)
		#wt = 3.5
		#yt = 8
		wt = 1.7
		yt = 3
		out2 = 0
		if w < wt:
			out = w / wt
		else:
			out = 1
			#print('running', w)
			out2 = w / yt
		out = abs(1 - out)
		out2 = abs(1 - out2)
		tgww = round(out, 3)
		tgrw = round(out2, 3)
		incer = .05
		if self.walk_weight < tgww:
			self.walk_weight += incer
		if self.walk_weight > tgww:
			self.walk_weight -= incer
		if self.run_weight <= tgrw:
			self.run_weight += incer
		if self.run_weight > tgrw:
			self.run_weight -= incer
		if self.walk_weight <= 0:
			self.walk_weight = 0
		if self.walk_weight > .95:            
			self.walk_weight = 1        
		if self.run_weight <= 0:
			self.run_weight = 0
		if self.run_weight > .95:
			self.run_weight = 1


		if dict['kb_lsh'] == 2 or dict['aBut'] == 1:
			self.run_speed = 1.3
		else:
			self.run_speed = .8
		#print(self.run_speed, '---', self.walk_weight, 'walk weight', self.run_weight, 'run weight')

	def Exit(self):
		self.FSM.owner.arm.stopAction(0)
		self.FSM.owner.arm.stopAction(1)
		self.FSM.owner.arm.stopAction(2)
		self.FSM.owner.arm.stopAction(3)
		self.FSM.owner.deck_arm.stopAction(0)
		self.FSM.owner.deck_arm.stopAction(1)
		self.FSM.owner.deck_arm.stopAction(2)
		self.FSM.owner.deck_arm.stopAction(3)
		dict['walk'] = 0


#==================================== 
			
class WalkAir(State):
	def __init__(self,FSM):
		super(WalkAir, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		super(WalkAir, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		self.FSM.owner.check_reset_point()  
		arm = self.FSM.owner.arm
		deck_arm = self.FSM.owner.deck_arm
		#arm.playAction('a_jump_t', 23, 23, layer=3, play_mode=1, speed=1, blendin=5)
		arm.playAction('c_jump_up', 23, 23, layer=3, play_mode=1, speed=1, blendin=15)
		deck_arm.playAction('b_walk_jump', 23, 23, layer=3, play_mode=1, speed=1, blendin=15)

		if self.FSM.owner.obj.linearVelocity.z < -10:
			self.FSM.owner.obj.linearVelocity.z = -10
		moving = self.FSM.owner.walk_movement() 
		ground_rays = self.FSM.owner.get_ground_ray() 

		self.FSM.owner.check_throw()
		self.check_onboard()
		self.FSM.owner.align_walk_z()
		self.FSM.owner.check_pause()
		if ground_rays[0][0] != None:
			dist = self.FSM.owner.get_ground_dist(ground_rays)
			if dist < .4:
					self.FSM.ToTransition('toWalkLand')  



			if dist > .5 and self.FSM.owner.obj.worldPosition.z < self.FSM.owner.last_pos.z: 
				hang_ray = self.FSM.owner.get_hang_ray()           
				hang_align_ray = self.FSM.owner.get_hang_align_ray()

				if hang_ray[0] != None and hang_align_ray[0] != None and self.FSM.prevState != 'WalkHang' and dict['lUD'] < .04:
					hr_dist = self.FSM.owner.get_hang_dist(hang_ray)
					if hr_dist < .1:
						self.FSM.owner.obj.linearVelocity = [0,0,0]
						self.FSM.owner.obj.worldPosition.z = hang_ray[1].z - 1
						self.FSM.ToTransition('toWalkHang')
		
	def check_onboard(self):
		o = self.FSM.owner.obj
		# if dict['walk'] == 0:
		#     o['getonboard'] = True
		#     self.FSM.ToTransition('toWalkOnboard')
		if dict['yBut'] == 1 and dict['last_yBut'] == 0:
			o['getonboard'] = True
			#self.FSM.ToTransition('toWalkOnboard')
			self.FSM.ToTransition('toAirOnboard')

	def Exit(self):
		self.FSM.owner.arm.stopAction(0)
		self.FSM.owner.arm.stopAction(1)
		self.FSM.owner.arm.stopAction(2)
		self.FSM.owner.arm.stopAction(3)
		self.FSM.owner.deck_arm.stopAction(0)
		self.FSM.owner.deck_arm.stopAction(1)
		self.FSM.owner.deck_arm.stopAction(2)
		self.FSM.owner.deck_arm.stopAction(3)

#==================================== 
			
class WalkJump(State):
	def __init__(self,FSM):
		super(WalkJump, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		dict['camera']['sndmgr'].queue_sound(['walk_grunt', self.FSM.owner.obj, dict['camera']])
		arm = self.FSM.owner.arm
		deck = self.FSM.owner.deck_arm
		#arm.playAction('a_jump_t', 1, 23, layer=3, play_mode=0, speed=1, blendin=5)
		arm.playAction('c_jump_up', 1, 23, layer=3, play_mode=0, speed=1, blendin=5)
		deck.playAction('b_walk_jump', 1, 23, layer=3, play_mode=0, speed=1, blendin=5)
			

		super(WalkJump, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		arm = self.FSM.owner.arm
		o = self.FSM.owner.obj

		moving = self.FSM.owner.walk_movement()              
		self.check_onboard()
		self.FSM.owner.check_pause()
		if self.FSM.stateLife == 10:
				force = [ 0.0, 0.0, dict['walk_jump_force']]
				if o.linearVelocity.z < 10:
					 o.applyForce(force, True)

		if self.FSM.stateLife > 27:
			self.FSM.ToTransition('toWalkAir')

	def check_onboard(self):
		o = self.FSM.owner.obj
		# if dict['walk'] == 0:
		#     o['getonboard'] = True
		#     self.FSM.ToTransition('toWalkOnboard')
		if dict['yBut'] == 1 and dict['last_yBut'] == 0:
			o['getonboard'] = True
			self.FSM.ToTransition('toAirOnboard')            
		
	def Exit(self):
		pass

#==================================== 
			
class WalkLand(State):
	def __init__(self,FSM):
		super(WalkLand, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		#self.FSM.owner.arm.playAction('a_jump_t', 23, 50, layer=5, play_mode=0, speed=1, blendin=5)
		self.FSM.owner.arm.playAction('c_land', 0, 50, layer=1, play_mode=0, speed=1, blendin=5)
		self.FSM.owner.deck_arm.playAction('b_walk_land', 0, 50, layer=1, play_mode=0, speed=1, blendin=5)
		dict['camera']['sndmgr'].queue_sound(['walk_land', self.FSM.owner.obj, dict['camera']])
		#c_land
		super(WalkLand, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		self.FSM.owner.obj.linearVelocity.x *= .8
		self.FSM.owner.obj.linearVelocity.y *= .8
		# arm = self.FSM.owner.arm
		
		# print('af', arm.getActionFrame(5))
		# if arm.getActionFrame(5) > 30:
		#     print('stopping land')
		#     arm.stopAction(5)
		#     arm.playAction('reg_idle1', 1,120, layer=1, play_mode=1, speed=1, blendin=2)
		if self.FSM.stateLife > 30:
			self.FSM.ToTransition('toWalk')

		self.FSM.owner.check_pause()
		ground_rays = self.FSM.owner.get_ground_ray() 
		if ground_rays[0][0] != None:
			dist = self.FSM.owner.get_ground_dist(ground_rays)
			self.FSM.owner.set_walk_z(dist)

	def Exit(self):
		pass

#==================================== 
			
class WalkHang(State):
	def __init__(self,FSM):
		super(WalkHang, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		self.FSM.owner.arm.playAction('c_braced_hang', 0, 36, layer=2, play_mode=0, speed=1, blendin=10)
		self.moveable = False
		dict['camera']['sndmgr'].queue_sound(['walk_hang', self.FSM.owner.obj, dict['camera']])
		self.FSM.owner.drop_deck()
		#self.FSM.owner.arm.playAction('c_braced_hang', 36, 50, layer=1, play_mode=1, speed=1, blendin=10)
		super(WalkHang, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		self.FSM.owner.obj.linearVelocity = [0,0,0]
		#self.FSM.owner.arm.playAction('c_braced_hang', 0, 36, layer=2, play_mode=0, speed=1, blendin=10)
		self.FSM.owner.arm.playAction('c_braced_hang', 36, 50, layer=1, play_mode=1, speed=1, blendin=10)
		if dict['lUD'] > .04:
			self.FSM.ToTransition('toWalkAir')
		   
		
		self.FSM.owner.move_walk_cam()
		self.FSM.owner.check_pause()
		if self.FSM.stateLife > 25:
			self.moveable = True
		if self.moveable:
			if dict['lUD'] < -.04:
				self.FSM.ToTransition('toWalkClimb') 
			self.FSM.owner.hang_move()
		ground_rays = self.FSM.owner.get_ground_ray() 
		if ground_rays[0][0] != None:
			dist = self.FSM.owner.get_ground_dist(ground_rays)
			if dist < .4:
				self.FSM.ToTransition('toWalkLand') 

		hang_ray = self.FSM.owner.get_hang_ray()           
		hang_align_ray = self.FSM.owner.get_hang_align_ray()
		#print(hang_align_ray, 'hang_align_ray')
		#self.FSM.owner.hang_move()



		if hang_ray[0] != None:
			hr_dist = self.FSM.owner.get_hang_dist(hang_ray)
			#print(hr_dist, 'hang dist')
			if hr_dist < .5:
				self.FSM.owner.obj.linearVelocity = [0,0,0]
				self.FSM.owner.obj.worldPosition.z = hang_ray[1].z  - 1
			if hang_align_ray[0] != None:
				#print(hang_align_ray[0])
				v = hang_align_ray[2]
				self.FSM.owner.obj.alignAxisToVect(v, 0, .5)    
		else:
			self.FSM.ToTransition('toWalkAir')
		if hang_align_ray[0] == None:
			self.FSM.ToTransition('toWalkAir')

		self.FSM.owner.align_walk_z()
	def Exit(self):
		pass

#==================================== 
			
class WalkClimb(State):
	def __init__(self,FSM):
		super(WalkClimb, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		self.FSM.owner.arm.playAction('c_wallclimb', 1, 50, layer=3, play_mode=0, speed=1, blendin=10)
		dict['camera']['sndmgr'].queue_sound(['walk_climb', self.FSM.owner.obj, dict['camera']])
		super(WalkClimb, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		#self.FSM.ToTransition('toLand')
		self.FSM.owner.check_pause()
		if self.FSM.stateLife > 25:
			self.FSM.owner.obj.applyForce([-300, 0, 80], True)
		else:
			self.FSM.owner.obj.applyForce([-20, 0, 80], True)    
		if self.FSM.stateLife > 35:
			self.FSM.ToTransition('toWalkLand')
		
	def Exit(self):
		pass                                

#==================================== 
			
class WalkHurdle(State):
	def __init__(self,FSM):
		super(WalkHurdle, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		super(WalkHurdle, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		#self.FSM.ToTransition('toLand')
		
	def Exit(self):
		pass                                


#==================================== 

			
class WalkOnboard(State):
	def __init__(self,FSM):
		super(WalkOnboard, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		self.FSM.owner.walking = False
		self.FSM.owner.obj['getonboard'] = False
		if self.FSM.owner.throw_deck != None:
			self.FSM.owner.throw_deck.endObject()
			self.FSM.owner.throw_deck = None
			self.FSM.owner.show_deck()
		dict['walking'] = False
		self.FSM.owner.obj['walking'] = False
		self.FSM.owner.obj['requestAction'] = 'reg_onboard'
		self.FSM.owner.obj['getoffboard'] == False
		self.FSM.owner.obj.applyForce([-300, 0, 0], True)
		print('walkonboard entered')
		super(WalkOnboard, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		self.FSM.ToTransition('toRoll')
		
	def Exit(self):
		pass

#==================================== 


			
class AirOnboard(State):
	def __init__(self,FSM):
		super(AirOnboard, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		self.FSM.owner.walking = False
		self.FSM.owner.obj['getonboard'] = False
		if self.FSM.owner.throw_deck != None:
			self.FSM.owner.throw_deck.endObject()
			self.FSM.owner.throw_deck = None
			self.FSM.owner.show_deck()
		dict['walking'] = False
		self.FSM.owner.obj['walking'] = False
		#self.FSM.owner.obj['requestAction'] = 'reg_onboard'

		self.FSM.owner.arm.playAction('c_land', 0, 50, layer=1, play_mode=0, speed=1, blendin=5)
		self.FSM.owner.deck_arm.playAction('b_walk_land', 0, 50, layer=1, play_mode=0, speed=1, blendin=5)

		self.FSM.owner.obj['getoffboard'] == False
		self.FSM.owner.obj.applyForce([-300, 0, 0], True)
		print('air entered')
		super(AirOnboard, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		self.FSM.ToTransition('toRoll')
		
	def Exit(self):
		pass

#==================================== 
			
class Roll(State):
	def __init__(self,FSM):
		super(Roll, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		# self.FSM.owner.obj['getoffboard'] == False
		# dict['walk'] = 0
		print('roll entered')
		super(Roll, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		if dict['walk'] == 1 or self.FSM.owner.obj['getoffboard'] == True:
			dict['walk'] = 1
			self.FSM.owner.obj['getoffboard'] = True
			#self.FSM.ToTransition('toWalk')
			self.FSM.ToTransition('toOffboard')
			#print('fsm to walk', dict['walk'], self.FSM.owner.obj['getoffboard'])
		if self.FSM.owner.obj['fall'] == True:
			self.FSM.ToTransition('toRagdoll')    
		#print('rolling')
		#self.FSM.ToTransition('toLand')
		
	def Exit(self):
		pass

#==================================== 

class Offboard(State):
	def __init__(self,FSM):
		super(Offboard, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		
		o = self.FSM.owner.obj
		if o['stance']:
			o.applyRotation([0,0,3.14], True)
			o['stance'] = False
			self.FSM.owner.arm.playAction('fak_noffboard', 0,24, layer=2, play_mode=0, speed=1, blendin=0)
			self.FSM.owner.deck_arm.playAction('b_reg_offboard', 0,30, layer=2, play_mode=0, speed=1, blendin=0)
		else:
			self.FSM.owner.arm.playAction('reg_noffboard', 0,40, layer=2, play_mode=0, speed=1, blendin=0)
			self.FSM.owner.deck_arm.playAction('b_reg_offboard', 0,40, layer=2, play_mode=0, speed=1, blendin=0)
		print('fsm getting off board')
		super(Offboard, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		
		self.FSM.ToTransition('toWalk')
		
	def Exit(self):
		pass
#==================================== 

class Ragdoll(State):
	def __init__(self,FSM):
		super(Ragdoll, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		self.FSM.owner.obj['ragdoll_active'] = True
		self.FSM.owner.drop_deck()
		print('ragdoll_player_fsm entered')
		super(Ragdoll, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		#print('ragdolling')
		if dict['yBut'] == 1:
			self.FSM.owner.obj['fall'] = False
			self.FSM.owner.cont.activate(self.FSM.owner.cont.actuators['walk'])
			#self.FSM.ToTransition('toWalk')
			self.FSM.ToTransition('toOffboard')
		self.FSM.owner.move_walk_cam()
		self.FSM.owner.check_pause()
	def Exit(self):
		print('ragdoll_player_fsm exited')
		self.FSM.owner.obj['ragdoll_active'] = False
		#pass                                

#==================================== 
			
class Dropin(State):
	def __init__(self,FSM):
		super(Dropin, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		if self.FSM.owner.throw_deck != None:
			self.FSM.owner.throw_deck.endObject()
			self.FSM.owner.throw_deck = None
			self.FSM.owner.show_deck()
		self.FSM.owner.arm.playAction('reg_dropin3', 30, 50, layer=2, play_mode=0, speed=1, blendin=5)
		self.FSM.owner.deck_arm.playAction('b_reg_dropin', 30, 50, layer=2, play_mode=0, speed=1, blendin=5)
		self.out_triggered = False
		# #player armature action name, start, end frames
		# 'reg_dropin3', 30, 50,
		# #deck action name, start, end frames  
		# 'b_reg_dropin', 30, 50,
		# #layer, speed, mode (0 = play, 1 = loop), blendin
		# 1, .5, 0, 15,
		# #intro, length

		super(Dropin, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		#print('dropin')
		eu = self.FSM.owner.get_vert_rot(self.FSM.owner.obj, self.FSM.owner.dropin_obj[0])
		#print(eu, 'eu')
		
		self.FSM.owner.arm.playAction('reg_dropin3', 50, 50, layer=1, play_mode=1, speed=1, blendin=5)
		self.FSM.owner.deck_arm.playAction('b_reg_dropin', 50, 50, layer=1, play_mode=1, speed=1, blendin=5)
		
		if dict['last_yBut'] == True and dict['yBut'] == False:
			self.out_triggered = True
		
		if self.out_triggered:
			self.FSM.owner.arm.playAction('reg_dropin3', 60, 80, layer=2, play_mode=0, speed=1, blendin=5)
			self.FSM.owner.deck_arm.playAction('b_reg_dropin', 60, 80, layer=2, play_mode=0, speed=1, blendin=5)
			self.FSM.owner.obj.applyForce([-15, 0, 0], True)
			self.FSM.owner.obj.applyRotation([0, -.01, 0], True)

		else:
			self.FSM.owner.move_to_te()

		if self.FSM.owner.arm.getActionFrame(2)> 78:
			
			self.FSM.ToTransition('toRoll')

		self.FSM.owner.move_walk_cam()
		self.FSM.owner.check_pause()

		
		


	def Exit(self):
		self.FSM.owner.obj['getonboard'] = True
		self.FSM.owner.walking = False
		#self.FSM.owner.obj['getonboard'] = False
		dict['walking'] = False
		self.FSM.owner.obj['walking'] = False
		self.FSM.owner.obj['requestAction'] = 'reg_air'
		self.FSM.owner.obj['getoffboard'] == False



#==================================== 

#==================================== 
			
class Pause(State):
	def __init__(self,FSM):
		super(Pause, self).__init__(FSM)    
		
	def Enter(self):
		self.FSM.stateLife = 1
		super(Pause, self).Enter()        
		
	def Execute(self):
		self.FSM.stateLife += 1
		#print('******paused')
		self.FSM.owner.obj['walk'] = True
		if dict['npause'] == False:
			n = self.FSM.prevState.__class__.__name__
			n = 'to' + n
			#self.FSM.ToTransition('toWalk')
			self.FSM.ToTransition(n)
		
	def Exit(self):
		pass

#==================================== 