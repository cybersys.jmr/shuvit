import bge
import ast
import glob
import random
import os

def read_files(own):

	file = bge.logic.expandPath('//assets/bsp/bsp_00-00.gob')



	directory = bge.logic.expandPath("//assets/bsp/")
	file_name = directory + "*.gob" # Test directory "Test\\Music\\*.mp3"
	file_list = glob.glob(file_name)
	selected = random.choice(file_list)
	full_path = os.path.join(directory, selected)
	print('glob got', file_list)



	for f in file_list:
		load = open(file, 'r')
		lines = load.readlines()
		load.close()
		blist = []
		for x in lines:
			x = str(x)
			x = x.replace(","," ")
			ostr = str(x.split()[0])
			pX = float(x.split()[1])
			pY = float(x.split()[2])
			pZ = float(x.split()[3])
			pR = float(x.split()[4])
			pS = float(x.split()[5])		
			#x = x.replace("'", "")
			#x = ast.literal_eval(x)
			#print(x, pX, pY, pZ)
			blist.append([ostr, pX, pY, pZ, pR, pS])

		bn = os.path.basename(f)
		bn = bn[:-4]
		print(bn, 'basename')
		own[bn] = blist
		bna = bn + '_active'		
		own[bna] = True
		bno = bn + '_obj'
		own[bno] = []



# 	load = open(file, 'r')
# 	lines = load.readlines()
# 	load.close()
# 	#line2 = lines[0]
# 	#line = line2.split()
# 	#lines = ast.literal_eval(lines)
# 	#l2 = lines.split()
# 	blist = []
# 	for x in lines:
# 		x = str(x)
# 		x = x.replace(","," ")
# 		ostr = str(x.split()[0])
# 		pX = float(x.split()[1])
# 		pY = float(x.split()[2])
# 		pZ = float(x.split()[3])
# 		pR = float(x.split()[4])
# 		pS = float(x.split()[5])		
# 		#x = x.replace("'", "")
# 		#x = ast.literal_eval(x)
# 		#print(x, pX, pY, pZ)
# 		blist.append([ostr, pX, pY, pZ, pR, pS])

# 	own['bsp_00-00'] = blist
# 	own['bsp_00-00_active'] = True
# 	own['bsp_00-00_obj'] = []
# #-----------------------
# 	file = bge.logic.expandPath('//assets/bsp/bsp_00-01.gob')

# 	load = open(file, 'r')
# 	lines = load.readlines()
# 	load.close()
# 	#line2 = lines[0]
# 	#line = line2.split()
# 	#lines = ast.literal_eval(lines)
# 	#l2 = lines.split()
# 	blist = []
# 	for x in lines:
# 		x = str(x)
# 		x = x.replace(","," ")
# 		ostr = str(x.split()[0])
# 		pX = float(x.split()[1])
# 		pY = float(x.split()[2])
# 		pZ = float(x.split()[3])
# 		pR = float(x.split()[4])
# 		pS = float(x.split()[5])
# 		#x = x.replace("'", "")
# 		#x = ast.literal_eval(x)
# 		#print(x, pX, pY, pZ)
# 		blist.append([ostr, pX, pY, pZ, pR, pS])

# 	own['bsp_00-01'] = blist	
# 	own['bsp_00-01_active'] = False	
# 	own['bsp_00-01_obj'] = []

def draw(own, tile):
	scene = bge.logic.getCurrentScene()
	de = scene.objects['dot_empty']
	de.applyRotation((0, 0, 0), False) 
	de.scaling = [1, 1, 1]
	yMult = tile[4:6]
	xMult = tile[7:9]

	print(yMult, xMult, 'multers')

	xOffset = 128 * int(xMult)
	yOffset = 128 * int(yMult)

	# if 'bsp_00-03' in tile:
	# 	xOffset = 128
	# 	yOffset = 0
	# 	de.applyRotation((0, 0, 0), False) 
	for x in own[tile]:
		objer = tile + '_obj'
		obna = scene.objectsInactive[x[0]]
		#if x[1] != 0 and x[2] != 0:
		de.scaling = [x[5], x[5], x[5]]
		de.applyRotation((0, 0, x[4]), False) 
		de.worldPosition.x = x[1] + xOffset
		de.worldPosition.y = x[2] + yOffset
		de.worldPosition.z = x[3]
		#de.applyRotation((0, 0, x[4]), False) 
		
		#de.rotation = [x[4], x[4], x[4]]
		obj = scene.addObject(obna, de, 0)
		#obj.applyRotation((x[4], x[4], x[4]), True) 
		#obj.scaling = [x[5], x[5], x[5]]
		de.applyRotation((0, 0, -x[4]), False) 
		own[objer].append(obj)

def remove_tile(own, tile):
	nm = tile + '_obj'
	for x in own[nm]:
		x.endObject()
	own[nm] = []	

def update_position(own):
	pX = own.worldPosition.x
	pY = own.worldPosition.y
	gsize = 128
	size = gsize / 2
	mX = int(pX / size)
	mY = int(pY / size)
	if mX == 1:
		if 'bsp_00-01' in own['active_tiles']:
			pass
		else:
			draw(own, 'bsp_00-01')
			own['bsp_00-01_active'] = True
			own['active_tiles'].append('bsp_00-01')
			print('drawing')
	if mX == 0:
		if 'bsp_00-01' in own['active_tiles']:
			own['bsp_00-01_active'] = False
			own['active_tiles'].remove('bsp_00-01')
			remove_tile(own, 'bsp_00-01')
			print('removing')		
	#print('uPos', mX, mY)

	sX = round(own .worldPosition.x / gsize ,1)
	sY = round(own.worldPosition.y / gsize, 1)
	own['sX'] = sX
	own['sY'] = sY
	print('sx, sy', sX, sY)
	ssx = str(int(sX))
	if len(ssx) < 2:
		ssx = '0' + ssx
	ssy = str(int(sY))
	if len(ssy) < 2:
		ssy = '0' + ssy	
	ct = 'bsp_' + ssx + '-' + ssy
	#print(ct) 
	own['current_tile'] = ct

	ssxp1 = str(int(sX) + 1)	
	if len(ssxp1) < 2: ssxp1 = '0' + ssxp1
	ssxm1 = str(int(sX) - 1)
	if len(ssxm1) < 2: ssxm1 = '0' + ssxm1


	ssyp1 = str(int(sY) + 1)
	if len(ssyp1) < 2: ssyp1 = '0' + ssyp1
	ssym1 = str(int(sY) - 1)
	if len(ssym1) < 2: ssym1 = '0' + symp1
	nname = 'bsp_' + ssy +'-' + ssxp1
	print(nname)


def main(cont):
	own = cont.owner
	if 'draw_inited' not in own:
		own['draw_inited'] = True
		own['active_tiles'] = []
		read_files(own)

		draw(own, 'bsp_00-00')
		own['active_tiles'].append('bsp_00-00')
	update_position(own)	
	