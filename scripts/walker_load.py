import bge

def main(cont):
    own = cont.owner
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
    objList = scene.objects
    mainDir = bge.logic.expandPath("//")
    skater = own['chr']
    
    fileName = mainDir + "npc_walkers/" + str(skater) + '.blend'    
    
    path = bge.logic.expandPath(fileName)
    try:
        bge.logic.LibLoad(path, 'Scene',  load_actions=True) 
        print(skater, 'loaded')
    except:
        print('trying to load', fileName, 'twice')    
    