#joytimers.py
import bge
countdown = 20

def main(own):
	
	class stick:
		def __init__(self, ud, lr):
			self.q1 = 0
			self.q2 = 0
			self.q3 = 0
			self.q4 = 0
			self.q5 = 0
			self.q6 = 0
			self.q7 = 0
			self.q8 = 0
			self.ud = ud
			self.lr = lr
			self.qs = ['q6', 'q8', 'q2', 'q4', 'q5', 'q1', 'q3', 'q7']

		def reset(self):
			self.q1 = 0
			self.q2 = 0
			self.q3 = 0
			self.q4 = 0
			self.q5 = 0
			self.q6 = 0
			self.q7 = 0
			self.q8 = 0			
	
		def update(self):
			dict = bge.logic.globalDict
	
			def q6():
				if dict[self.ud] > .04 and dict[self.lr] < -0.04:
					return countdown
				
			def q8():
				if dict[self.ud] < -.04 and dict[self.lr] < -0.04:
					return countdown
								
			def q2():
				if dict[self.ud] < -.04 and dict[self.lr] > 0.04:
					return countdown

			def q4():
				if dict[self.ud] > .04 and dict[self.lr] > 0.04:
					return countdown		

			def q5():
				if dict[self.ud] > .07 and self.q4 is not countdown and self.q6 is not countdown:
					return countdown		

			def q1():
				if dict[self.ud] < -.07 and self.q8 is not countdown and self.q2 is not countdown:
					return countdown		

			def q3():
				if dict[self.lr] > .07 and self.q2 is not countdown and self.q4 is not countdown:
					return countdown				

			def q7():
				if dict[self.lr] < -.07 and self.q6 is not countdown and self.q8 is not countdown:
					return countdown		

			for cur_q in self.qs:
				val = getattr(self, cur_q)
				v2 = locals()[cur_q]()
				if v2 is not None:
					setattr(self, cur_q, v2) 
				elif val > 0:
					setattr(self, cur_q, val - 1) 

	if 'jt_inited' not in own:
		own['jt_inited'] = True
		own['jsl'] = stick('lUD', 'lLR')
		own['jsr'] = stick('rUD', 'rLR')

	own['jsl'].update()	
	own['jsr'].update()	
