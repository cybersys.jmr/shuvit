import bge
from mathutils import Vector

dict = bge.logic.globalDict
dict['replay_record_length']

bone_list = ["foot.ik.R", "foot.ik.L", "knee.pt.ik.R", "knee.pt.ik.L", "hips", "root", "spine", "spine-1", "chest", "chest-1", "clavicle.L", "clavicle.R", "deltoid.L", "deltoid.R", "upper_arm.fk.L", "upper_arm.fk.R",  "forearm.fk.L", "forearm.fk.R", "hand.fk.L", "hand.fk.R", "neck", "head", "master", 
    'foot.rev.R', 'foot.rev.L', 'shin.ik.R', 'shin.ik.L', 'toe.rev.R', 'toe.rev.L']

deck_list = ['main', 'turn']

#bone_list = ['root', 'head']

def Record():   
    #print('bone recording') 
    scene = bge.logic.getCurrentScene()
    #own = cont.owner
    master = scene.objects['control_cube.002']
    cam = scene.objects['Camera.003']
    armature = scene.objects['Char4']
    deck = scene.objects['deck_arm']
    if 'dataList' not in master:
        for x in bone_list:
            master['dataList'] = True
            master[str(x) + '-pos'] = []
            master[str(x) + '-rot'] = []
        for x in deck_list:
            master['dataList'] = True
            master[str(x) + '-pos'] = []
            master[str(x) + '-rot'] = []            

    for y in bone_list:
        x = armature.channels[y]
        pos = x.location
        if x.rotation_mode == 0:
            rot = x.rotation_quaternion
        else:
            rot = Vector(x.channel_matrix.to_euler()) 

        if len(master[str(y) + '-pos']) > dict['replay_record_length']:
            master[str(y) + '-pos'].pop(0)
            master[str(y) + '-rot'].pop(0)            

            master[str(y) + '-pos'].pop(0)
            master[str(y) + '-rot'].pop(0)            

        master[str(y) + '-pos'].append(pos)
        master[str(y) + '-rot'].append(rot)

        master[str(y) + '-pos'].append(pos)
        master[str(y) + '-rot'].append(rot)

    # pl = master['foot.ik.R-pos'][len(master['foot.ik.R-pos']) - 1]

    # if dict['player_class'].obj['ragdoll_active']:
    #     rdfp = armature.worldPosition - scene.objects['npc_ed_arm_phys_foot.ik.R'].worldPosition
    #     print('rd foot pos', rdfp)
    #     #print(dict['player_class'].obj['ragdoll_active'], 'is ragdolling')
    # else:
    #     print('foot pos', pl)

    #print(len(master['foot.ik.R-pos']), 'br length---')



    for y in deck_list:
        x = deck.channels[y]
        pos = x.location   
        rot = x.rotation_quaternion

            
        if len(master[str(y) + '-pos']) > dict['replay_record_length']:
            master[str(y) + '-pos'].pop(0)
            master[str(y) + '-rot'].pop(0)            

            master[str(y) + '-pos'].pop(0)
            master[str(y) + '-rot'].pop(0)            

        master[str(y) + '-pos'].append(pos)
        master[str(y) + '-rot'].append(rot)

        master[str(y) + '-pos'].append(pos)
        master[str(y) + '-rot'].append(rot) 


def Playback(valueIndex):
    scene = bge.logic.getCurrentScene()
    #own = cont.owner
    master = scene.objects['control_cube.002']
    #cam = scene.objects['Camera.003']
    armature = scene.objects['Char4']
    deck = scene.objects['deck_arm']

    for y in bone_list:
        x = armature.channels[y]
        pl = master[str(y) + '-pos'] 
        rl = master[str(y) + '-rot'] 
        try:
            if x.rotation_mode == 0:
                x.rotation_quaternion = rl[valueIndex]
            else:    
                x.rotation_euler = rl[valueIndex]
            x.location = pl[valueIndex]    
            #print(x.rotation_mode, pl[valueIndex],  'mode', valueIndex)
        except:
            print('index out of range')

    for z in deck_list:
        x = deck.channels[z]
        pl = master[str(z) + '-pos'] 
        rl = master[str(z) + '-rot'] 
        try:
            x.rotation_quaternion = rl[valueIndex]
            x.location = pl[valueIndex]    

            #print(x.rotation_mode, pl[valueIndex],  'mode', valueIndex, x.name)
        except:
            print('index out of range')
    if armature.isPlayingAction(0):
        armature.stopAction(0)
    if armature.isPlayingAction(1):
        armature.stopAction(1)        
    if armature.isPlayingAction(2):
        armature.stopAction(2)
    if armature.isPlayingAction(3):
        armature.stopAction(3)
    if armature.isPlayingAction(4):
        armature.stopAction(4)
    if armature.isPlayingAction(5):
        armature.stopAction(5)
    if armature.isPlayingAction(6):
        armature.stopAction(6)

    armature.update()        
    deck.update()        