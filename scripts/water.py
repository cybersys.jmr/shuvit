import bge


def update(cont):
    cont = bge.logic.getCurrentController()
    own = cont.owner
    tex = own.meshes[0].materials[0].textures[1]
    tex2 = own.meshes[0].materials[0].textures[0]    
    tex3 = own.meshes[0].materials[0].textures[2]        
    tex.uvOffset.x += .0001
    tex2.uvOffset.x -= .0002
    tex3.uvOffset.x -= .00015
