import bge
import actionsFSMlist
import mathutils
import FSM

dict = bge.logic.globalDict
zero_blend = False

state_a = ['reg_idle',
'reg_idle2',
'reg_idle3',
'reg_idle4',
'reg_idle5',
'reg_idle6',
'reg_idle7',
'reg_roll',
'reg_turnLeft',
'reg_turnRight',
'reg_manual_left',
'reg_manual_right',
'reg_nmanual_left',
'reg_nmanual_right',
'reg_opos',
'reg_nopos',
'reg_pump',
'reg_pump_left',
'reg_pump_right',
'reg_manual',
'reg_nmanual',
'reg_air',
'reg_air_nb',
'reg_air_nose',
'reg_air_tail',
'reg_5050',
'reg_bsboard',
'reg_fsboard',
'reg_tailg',
'reg_tailgr',
'reg_tailgl',
'reg_noseg',
'reg_nosegr',
'reg_nosegl',
'reg_tailslide',
'reg_noseslide',
'fak_idle',
'fak_idle2',
'fak_idle3',
'fak_idle4',
'fak_idle5',
'fak_idle6',
'fak_idle7',
'fak_roll',
'fak_turnLeft',
'fak_turnRight',
'fak_manual_left',
'fak_manual_right',
'fak_nmanual_left',
'fak_nmanual_right',
'fak_opos',
'fak_nopos',
'fak_pump',
'fak_pump_left',
'fak_pump_right',
'fak_manual',
'fak_nmanual',
'fak_air',
'fak_air_nb',
'fak_air_nose',
'fak_air_tail',
'fak_5050',
'fak_bsboard',
'fak_fsboard',
'fak_tailg',
'fak_tailgr',
'fak_tailgl',
'fak_noseg',
'fak_nosegr',
'fak_nosegl',
'fak_tailslide',
'fak_noseslide',
'reg_ollie',
'reg_kickflip',
'reg_heelflip',
'reg_shuvit',
'reg_shuvit360',
'reg_fsshuvit',
'reg_fsshuvit360',
'reg_nollie',
'reg_nollie_kickflip',
'reg_nollie_heelflip',
'reg_nollie_shuvit',
'reg_nollie_fsshuvit',
'reg_nollie_shuvit360',
'reg_nollie_fsshuvit_360',
'reg_hardflip',
'reg_inward_heelflip',
'reg_varial_kickflip',
'reg_varial_heelflip',
'fak_ollie',
'fak_kickflip',
'fak_heelflip',
'fak_shuvit',
'fak_shuvit360',
'fak_fsshuvit',
'fak_fsshuvit360',
'fak_nollie',
'fak_nollie_kickflip',
'fak_nollie_heelflip',
'fak_nollie_shuvit',
'fak_nollie_fsshuvit',
'fak_nollie_shuvit360',
'fak_nollie_fsshuvit_360',
'fak_hardflip',
'fak_inward_heelflip',
'fak_varial_kickflip',
'fak_varial_heelflip',
'frontside_grab',
'backside_grab',
'frontside_nose_grab',
'backside_nose_grab',
'frontside_tail_grab',
'backside_tail_grab',
'reg_ollie_north',
'reg_ollie_south',
'reg_judo',
'reg_frigid',
'reg_fsonefoot',
'reg_onefoot',
'reg_airwalk',
'reg_wall_r',
'reg_wall_l',
'fak_wall_r',
'fak_wall_l',
'fak_frontside_grab',
'fak_backside_grab',
'fak_frontside_nose_grab',
'fak_backside_nose_grab',
'fak_frontside_tail_grab',
'fak_backside_tail_grab',
'fak_ollie_north',
'fak_ollie_south',
'fak_judo',
'fak_frigid',
'fak_fsonefoot',
'fak_onefoot',
'reg_back_invert',
'reg_onboard', 
'reg_offboard',
'fak_onboard', 
'fak_offboard',
'reg_dropin',
'fak_dropin', 
'reg_stop']

state_b = ['reg_jump',
'reg_walk_air',
'reg_walk_air_out',
#'reg_onboard',
#'reg_offboard',
'reg_brfoot',
'reg_frfoot',
'reg_blfoot',
'reg_flfoot',
'reg_land',
'reg_landL',
'reg_landLb',
'reg_landR',
'reg_landRb',
'revert1',
'revert2',
'fak_revert1',
'fak_revert2',
'revert3',
'revert4',
'fak_jump',
'fak_walk_air',
'fak_walk_air_out',
'fak_onboard',
#'fak_offboard',
'fak_brfoot',
'fak_frfoot',
'fak_blfoot',
'fak_flfoot',
'fak_land',
'fak_landL',
'fak_landLb',
'fak_landR',
'fak_landRb']


def actionPlayer(ac):
	scene = bge.logic.getCurrentScene()
	cont = bge.logic.getCurrentController()
	own = cont.owner
	skater = dict['p1']
	deck = dict['p1d']
	a = getattr(actionsFSMlist.reg_walk, 'name')
	name = getattr(ac, 'name')
	dname = getattr(ac, 'dname')
	start = getattr(ac, 'start')
	end = getattr(ac, 'end')
	dstart = getattr(ac, 'dstart')
	dend = getattr(ac, 'dend')
	layer = getattr(ac, 'layer')
	mode = getattr(ac, 'mode')
	speed = getattr(ac, 'speed')
	blendin = getattr(ac, 'blendin')
	#--------------------------------
	#if zero_blend:
	# if own['l_actionState'] != None and own['l_actionState'] in 'revert1':	
	# 	blendin = 0
	# 	print('zeroing blend')
	skater.playAction(name, start,end, layer=layer, play_mode=mode, speed=speed, blendin=blendin)
	deck.playAction(dname, dstart,dend, layer=layer, play_mode=mode, speed=speed, blendin=blendin)
	#print('playing', name, get_frame(ac))
	#if skater.isPlayingAction(3):
		#print(skater.getActionFrame(3))

def get_frame(ac):
	skater = dict['p1']
	layer = getattr(ac, 'layer')
	#print('getting frame', skater.getActionFrame(layer))
	return skater.getActionFrame(layer)

def set_frame(ac, frame):
	skater = dict['p1']
	deck = dict['p1d']
	layer = getattr(ac, 'layer')
	skater.setActionFrame(frame, layer)
	deck.setActionFrame(frame, layer)
	#print('setting frame', frame)

def check_exit(aState, rA, timer, ac):
	exits = getattr(ac, 'exits')
	force = getattr(ac, 'force_exit')
	fe = getattr(ac, 'fef')
	rv = ''
	
	#print('rA, aState', rA, aState, force, fe, timer)
	if rA in exits:
		rv = rA
	#

	elif ('fak' in rA and 'reg' in aState) or ('reg' in rA and 'fak' in aState):
		#print('change stance')
		if getattr(ac, 'opposite') != None:
			rv = getattr(ac, 'opposite')
	else:
		rv = aState

	if force != None and fe >= timer:
		print('doing exit timer', force)
		rv = force

	return rv

def state_timer(own):
	if own['aState'] == own['l_aState']:
		own['stateTimer'] += 1
	else:
		own['stateTimer'] = 0

def check_land(rA, aState, own):

	#check if manualling
	if rA in actionsFSMlist.reg_manuallist or rA in actionsFSMlist.reg_grindlist or rA in actionsFSMlist.fak_grindlist:
	#if rA in actionsFSMlist.reg_manuallist or rA in actionsFSMlist.fak_grindlist:	
		aState = rA
		#own['stateTimer'] = 0
		own['aState'] = rA
	#otherwise, force land, offboard or fliptrick
	elif 'land' in rA or 'onboard' in rA or 'offboard' in rA or rA in actionsFSMlist.reg_fliplist or rA in actionsFSMlist.fak_fliplist or rA in actionsFSMlist.revertlist:
		#print('landing________________________', rA)
		aState = rA
		own['stateTimer'] = 0
		own['aState'] = rA
	#print(own['stance'], '-----stance', rA)
	return aState	

def main(cont):
	dict = bge.logic.globalDict
	if not dict['walking']:
		own = cont.owner
		


		#initial state
		if 'aState' not in own:
			own['aState'] = 'reg_idle'
			own['l_aState'] = None
			own['stateTimer'] = 0
			own['eaState'] = FSM.EaFSM(own)
			own['EaRequest'] = None
		
		aState = own['aState']
		rA = own['requestAction']
		timer = own['stateTimer']
		newState = own['aState']
		aState = check_land(rA, aState, own)
		newState = aState
		og_state = own['aState']
		#print(aState)
		if aState in state_a:

			action = getattr(actionsFSMlist, aState)
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				intro = getattr(action, 'intro')
				actionPlayer(getattr(actionsFSMlist, intro))	
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, action)
				

		elif aState in state_b:
			action = getattr(actionsFSMlist, aState)
			actionPlayer(action)
			if own['stateTimer'] > getattr(action, 'fef'):
				newState = check_exit(aState, rA, timer, action)


	###################################

		check_state = 'reg_sit'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			#if own['stateTimer'] < 19:
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				actionPlayer(getattr(actionsFSMlist, 'intro'))
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, actionsFSMlist.reg_sit)

		#-----------------------
		check_state = 'reg_walk'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			if own['l_aState'] == 'reg_walkFast':
				frame = get_frame(actionsFSMlist.reg_walkFast)
				actionPlayer(action)
				set_frame(actionsFSMlist.reg_walk, frame)
			else:	
				frame = actionPlayer(action)
			
			newState = check_exit(aState, rA, timer, action)

		#-----------------------
		check_state = 'reg_walkFast'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			newState = check_exit(aState, rA, timer, actionsFSMlist.reg_walkFast)

			if own['l_aState'] == 'reg_walk':
				frame = get_frame(actionsFSMlist.reg_walk)
				actionPlayer(action)
				set_frame(actionsFSMlist.reg_walkFast, frame)

			else:
				actionPlayer(action)

		check_state = 'reg_push'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			actionPlayer(action)
			if own['stateTimer'] > 70:
				newState = 'reg_roll'

		check_state = 'reg_push_goof'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			actionPlayer(action)
			if own['stateTimer'] > 70:
				newState = 'reg_roll'			
		
		check_state = 'reg_powerslide'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			# if own['stateTimer'] < 20:
			# 	actionPlayer(actionsFSMlist.reg_powerslide_in)
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				intro = getattr(action, 'intro')
				actionPlayer(getattr(actionsFSMlist, intro))	
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, actionsFSMlist.reg_powerslide)

		check_state = 'reg_fs_powerslide'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			# if own['stateTimer'] < 20:
			# 	actionPlayer(actionsFSMlist.reg_fs_powerslide_in)
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				intro = getattr(action, 'intro')
				actionPlayer(getattr(actionsFSMlist, intro))	
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, actionsFSMlist.reg_fs_powerslide)				



		check_state = 'fak_sit'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			#if own['stateTimer'] < 19:
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				actionPlayer(getattr(actionsFSMlist, 'intro'))
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, actionsFSMlist.fak_sit)

		#-----------------------
		check_state = 'fak_walk'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			if own['l_aState'] == 'fak_walkFast':
				frame = get_frame(actionsFSMlist.fak_walkFast)
				actionPlayer(action)
				set_frame(actionsFSMlist.fak_walk, frame)
			else:	
				frame = actionPlayer(action)
			
			newState = check_exit(aState, rA, timer, action)

		#-----------------------
		check_state = 'fak_walkFast'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			newState = check_exit(aState, rA, timer, actionsFSMlist.fak_walkFast)

			if own['l_aState'] == 'fak_walk':
				frame = get_frame(actionsFSMlist.fak_walk)
				actionPlayer(action)
				set_frame(actionsFSMlist.fak_walkFast, frame)

			else:
				actionPlayer(action)


		check_state = 'fak_push'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			actionPlayer(action)
			if own['stateTimer'] > 70:
				newState = 'fak_roll'

		check_state = 'fak_push_goof'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			actionPlayer(action)
			if own['stateTimer'] > 70:
				newState = 'fak_roll'			
		
		check_state = 'fak_powerslide'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			# if own['stateTimer'] < 20:
			# 	actionPlayer(actionsFSMlist.fak_powerslide_in)
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				intro = getattr(action, 'intro')
				actionPlayer(getattr(actionsFSMlist, intro))	
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, actionsFSMlist.fak_powerslide)

		check_state = 'fak_fs_powerslide'
		if aState == check_state:
			action = getattr(actionsFSMlist, check_state)
			# if own['stateTimer'] < 20:
			# 	actionPlayer(actionsFSMlist.fak_fs_powerslide_in)
			if own['stateTimer'] < getattr(action, 'intro_frames'):	
				intro = getattr(action, 'intro')
				actionPlayer(getattr(actionsFSMlist, intro))	
			else:
				actionPlayer(action)
			newState = check_exit(aState, rA, timer, actionsFSMlist.fak_fs_powerslide)				


		#-----------------------
		#print(own['aState'], newState, rA, own['l_aState'])
		#print()
		if newState != '':
			own['aState'] = newState 
		own['l_aState'] = og_state
		own['l_actionState'] = og_state
		state_timer(own)
		skater = dict['p1']
		
		if own['requestAction'] == 'reg_roll' and own['rotz'] < .985:
			frame = int(own['rotz'] * 70) 
		else:
			skater.stopAction(5)	

		#own['eaState'].Execute()
		#print(dict['walking'], 'walk')