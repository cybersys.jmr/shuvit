import bge

import StatesGame
import StatesPlayer
import StatesWalker
import StatesCamera
import StatesCar
import StatesEa
import StatesTrain
   
#====================================
class Transition(object):
    def __init__(self, toState):
        self.toState = toState
    def Execute(self):
        pass
                        
#===================================
                        
class FSM(object):
    def __init__ (self, character, owner):
        self.char = character
        self.states = {}
        self.transitions = {}
        self.curState = None
        self.prevState = None
        self.trans = None
        self.stateLife = 0
        self.owner = owner
        self.name = None
        self.last_state_notes = None
        
    def AddTransition(self, transName, transition):
        self.transitions[transName] = transition
    
    def AddState(self, stateName, state):
        self.states[stateName] = state
        self.states[stateName].name = state        
        
    def SetState(self, stateName):
        self.prevState = self.curState
        self.curState = self.states[stateName]
        
    def ToTransition(self, toTrans):
        self.trans = self.transitions[toTrans]
        
    def Execute(self, owner):
        if (self.trans):
            self.owner = owner
            self.curState.Exit()
            self.trans.Execute()
            self.SetState(self.trans.toState)
            self.curState.Enter()
            self.trans = None        
            
        self.curState.Execute()    
                
#====================================

Char = type("Char",(object,),{})

#===================================
          

#=================================== 

class GameFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner
        
        state_list = [
        'Example',
        'Startup',
        'Initer',
        'LoadLevel',
        'GameOn',
        'Reload']
        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesGame, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Startup')
    
    def Execute(self):
        self.FSM.Execute(self.owner)    

#=================================== 
        
class PlayerFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner
        
        state_list = [
        'Example',
        'Startup',
        'Walk',
        'WalkJump',
        'WalkAir',
        'WalkLand',
        'WalkHang',
        'WalkClimb',
        'WalkHurdle',
        'WalkOnboard',
        'AirOnboard',
        'Offboard', 
        'Dropin',
        'Roll',
        'Ragdoll',
        'Pause']
        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesPlayer, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Startup')
    
    def Execute(self):
        self.FSM.Execute(self.owner)    

#=================================== 
        
#=================================== 

class CameraFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner
        
        state_list = [
        'Example']
        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesCamera, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Example')
    
    def Execute(self):
        self.FSM.Execute(self.owner)    

#=================================== 

class CarFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner

        
        state_list = [
        'Example',
        'Activate',
        'ExitParallelPark',
        'EnterParallelPark',
        'NavigateToTarget', 
        'RequestPath',
        'EnterPerpPark', 
        'ExitPerpPark']

        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesCar, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Example')
    
    def Execute(self):
        self.FSM.Execute(self.owner)    

#=================================== 

class WalkerFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner

        
        state_list = [
        'Example',
        'Activate',
        'ExitParallelPark',
        'EnterParallelPark',
        'NavigateToTarget', 
        'RequestPath',
        'HitBySkater',
        'WalkingHitBySkater',
        'Dance1',
        'GoToSleep']

        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesWalker, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Example')
    
    def Execute(self):
        self.FSM.Execute(self.owner)   

#=================================== 

class TrainFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner

        
        state_list = [
        'Example']
        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesTrain, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Example')
    
    def Execute(self):
        self.FSM.Execute(self.owner)           


#=================================== 

class EaFSM(Char):
    def __init__(self, owner):
        self.FSM = FSM(self, owner)
        self.owner = owner

        
        state_list = [
        'Example',
        'Land']

        
        for s in state_list:
            self.FSM.AddState(s, getattr(StatesEa, s)(self.FSM))
            t = 'to' + s
            self.FSM.AddTransition(t, Transition(s))
        
        if self.FSM.curState == None:
            self.FSM.SetState('Example')
    
    def Execute(self):
        self.FSM.Execute(self.owner)           