import bge
import colors
keyboard = bge.logic.keyboard
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED

player_list = ['annie', 'billy', 'fred', 'becky', 'kevin', 'ralph', 'ed']               

#exit functions
#=======
def endpoint(funct, motion, dict, cont):
    #print('endpoint', funct,motion)
    scene = bge.logic.getCurrentScene()
    
    scenes = bge.logic.getSceneList()
    main_scene = [scene for scene in scenes if scene.name=="main"][0]
    main_empty = main_scene.objects['Empty']    
    own = cont.owner
    import Settings
    load_timer = 100
    
    shirt = scene.objects["Char4:Zshirtt1"]  
    shoeR = scene.objects['Char4:Shoes02.R']
    shoeL = scene.objects['Char4:Shoes02.L']
    deck = scene.objects["b_deck"]
    trucks = scene.objects["b_trucks"]
    wheel1 = scene.objects["wheel1"]
    wheel2 = scene.objects["wheel2"]
    wheel3 = scene.objects["wheel3"]
    wheel4 = scene.objects["wheel4"]
    throw_deck = scene.objectsInactive["throw_deck"] 
    cam = scene.objects['Camera.003']
    cam2 = scene.objects['freecam']
    cam3 = scene.objects['followcam']       
    c_move = .025 #color move amount  
    if funct == 'exit':
        #print("exit function", motion)
        bge.logic.endGame()
    if funct == 'restart':
        #print("restart function", motion)  
        bge.logic.restartGame()      
    if funct == 'shirt color r':
        s_color = shirt.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        shirt.color = new_col
        dict['shirt_color_r'] = new_col[0]          
        
    if funct == 'shirt color g':
        s_color = shirt.color
        s_color_g = s_color[1]
        if motion == 'inc':
            if s_color_g < 1:
                s_color_g += c_move
                s_color_g = round(s_color_g, 2)
            else:
                s_color_rg = 1 
        if motion == 'dec':
            if s_color_g > c_move:
                s_color_g -= c_move
                s_color_g = round(s_color_g, 2)
            else:
                s_color_g = 0          
        new_col = [s_color[0], s_color_g, s_color[2], s_color[3]]
        shirt.color = new_col   
        dict['shirt_color_g'] = new_col[1]         
    if funct == 'shirt color b':
        s_color = shirt.color
        s_color_b = s_color[2]
        if motion == 'inc':
            if s_color_b < 1:
                s_color_b += c_move
                s_color_b = round(s_color_b, 2)
            else:
                s_color_b = 1
        if motion == 'dec':
            if s_color_b > c_move:
                s_color_b -= c_move
                s_color_b = round(s_color_b, 2)
            else:
                s_color_b = 0          
        new_col = [s_color[0], s_color[1], s_color_b, s_color[3]]
        shirt.color = new_col   
        #print(new_col)          
        dict['shirt_color_b'] = new_col[2]
        
        
        
        
    if funct == 'shoe color r':
        s_color = shoeL.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        shoeL.color = new_col
        shoeR.color = new_col
        dict['shoe_color_r'] = new_col[0]          
        
    if funct == 'shoe color g':
        s_color = shoeL.color
        s_color_g = s_color[1]
        if motion == 'inc':
            if s_color_g < 1:
                s_color_g += c_move
                s_color_g = round(s_color_g, 2)
            else:
                s_color_rg = 1 
        if motion == 'dec':
            if s_color_g > c_move:
                s_color_g -= c_move
                s_color_g = round(s_color_g, 2)
            else:
                s_color_g = 0          
        new_col = [s_color[0], s_color_g, s_color[2], s_color[3]]
        shoeL.color = new_col   
        shoeR.color = new_col
        dict['shoe_color_g'] = new_col[1]         
    if funct == 'shoe color b':
        s_color = shoeL.color
        s_color_b = s_color[2]
        if motion == 'inc':
            if s_color_b < 1:
                s_color_b += c_move
                s_color_b = round(s_color_b, 2)
            else:
                s_color_b = 1
        if motion == 'dec':
            if s_color_b > c_move:
                s_color_b -= c_move
                s_color_b = round(s_color_b, 2)
            else:
                s_color_b = 0          
        new_col = [s_color[0], s_color[1], s_color_b, s_color[3]]
        shoeL.color = new_col   
        shoeR.color = new_col
        #print(new_col)          
        dict['shoe_color_b'] = new_col[2]     
        
        
    if funct == 'deck color r':
        s_color = deck.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        deck.color = new_col
        throw_deck.color = new_col
        dict['deck_color_r'] = new_col[0]          
        
    if funct == 'deck color g':
        s_color = deck.color
        s_color_g = s_color[1]
        if motion == 'inc':
            if s_color_g < 1:
                s_color_g += c_move
                s_color_g = round(s_color_g, 2)
            else:
                s_color_rg = 1 
        if motion == 'dec':
            if s_color_g > c_move:
                s_color_g -= c_move
                s_color_g = round(s_color_g, 2)
            else:
                s_color_g = 0          
        new_col = [s_color[0], s_color_g, s_color[2], s_color[3]]
        deck.color = new_col   
        throw_deck.color = new_col
        dict['deck_color_g'] = new_col[1]         
    if funct == 'deck color b':
        s_color = deck.color
        s_color_b = s_color[2]
        if motion == 'inc':
            if s_color_b < 1:
                s_color_b += c_move
                s_color_b = round(s_color_b, 2)
            else:
                s_color_b = 1
        if motion == 'dec':
            if s_color_b > c_move:
                s_color_b -= c_move
                s_color_b = round(s_color_b, 2)
            else:
                s_color_b = 0          
        new_col = [s_color[0], s_color[1], s_color_b, s_color[3]]
        deck.color = new_col   
        throw_deck.color = new_col
        #print(new_col)          
        dict['deck_color_b'] = new_col[2]  
        
        
    if funct == 'trucks color r':
        s_color = trucks.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        trucks.color = new_col
        dict['trucks_r'] = new_col[0]   
        
        
    if funct == 'trucks color g':
        s_color = trucks.color
        s_color_r = s_color[1]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color_r, s_color[2], s_color[3]]
        trucks.color = new_col
        dict['trucks_g'] = new_col[1]    

    if funct == 'trucks color b':
        s_color = trucks.color
        s_color_r = s_color[2]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color[1], s_color_r, s_color[3]]
        trucks.color = new_col
        dict['trucks_b'] = new_col[2]                   



    #set all wheels to wheel color 1
    if funct == 'wheel color r':
        s_color = wheel1.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        wheel1.color = new_col
        wheel2.color = new_col
        wheel3.color = new_col
        wheel4.color = new_col
        dict['wheel1_r'] = new_col[0] 
        dict['wheel2_r'] = new_col[0] 
        dict['wheel3_r'] = new_col[0] 
        dict['wheel4_r'] = new_col[0]                 
    if funct == 'wheel color g':
        s_color = wheel1.color
        s_color_r = s_color[1]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color_r, s_color[2], s_color[3]]
        wheel1.color = new_col
        wheel2.color = new_col
        wheel3.color = new_col
        wheel4.color = new_col        
        dict['wheel1_g'] = new_col[1] 
        dict['wheel2_g'] = new_col[1] 
        dict['wheel3_g'] = new_col[1] 
        dict['wheel4_g'] = new_col[1]    
    if funct == 'wheel color b':
        s_color = wheel1.color
        s_color_r = s_color[2]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color[1], s_color_r, s_color[3]]
        wheel1.color = new_col
        wheel2.color = new_col
        wheel3.color = new_col
        wheel4.color = new_col        
        dict['wheel1_b'] = new_col[2] 
        dict['wheel2_b'] = new_col[2] 
        dict['wheel3_b'] = new_col[2] 
        dict['wheel4_b'] = new_col[2]                   








    if funct == 'wheel1 color r':
        s_color = wheel1.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        wheel1.color = new_col
        dict['wheel1_r'] = new_col[0]                 
    if funct == 'wheel1 color g':
        s_color = wheel1.color
        s_color_r = s_color[1]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color_r, s_color[2], s_color[3]]
        wheel1.color = new_col
        dict['wheel1_g'] = new_col[1]    
    if funct == 'wheel1 color b':
        s_color = wheel1.color
        s_color_r = s_color[2]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color[1], s_color_r, s_color[3]]
        wheel1.color = new_col
        dict['wheel1_b'] = new_col[2]                   


    if funct == 'wheel2 color r':
        s_color = wheel2.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        wheel2.color = new_col
        dict['wheel2_r'] = new_col[0]                 
    if funct == 'wheel2 color g':
        s_color = wheel2.color
        s_color_r = s_color[1]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color_r, s_color[2], s_color[3]]
        wheel2.color = new_col
        dict['wheel2_g'] = new_col[1]    
    if funct == 'wheel2 color b':
        s_color = wheel2.color
        s_color_r = s_color[2]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color[1], s_color_r, s_color[3]]
        wheel2.color = new_col
        dict['wheel2_b'] = new_col[2]                   
    if funct == 'wheel3 color r':
        s_color = wheel3.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        wheel3.color = new_col
        dict['wheel3_r'] = new_col[0]                 
    if funct == 'wheel3 color g':
        s_color = wheel3.color
        s_color_r = s_color[1]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color_r, s_color[2], s_color[3]]
        wheel3.color = new_col
        dict['wheel3_g'] = new_col[1]    
    if funct == 'wheel3 color b':
        s_color = wheel3.color
        s_color_r = s_color[2]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color[1], s_color_r, s_color[3]]
        wheel3.color = new_col
        dict['wheel3_b'] = new_col[2]                   
    if funct == 'wheel4 color r':
        s_color = wheel4.color
        s_color_r = s_color[0]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color_r, s_color[1], s_color[2], s_color[3]]
        wheel4.color = new_col
        dict['wheel4_r'] = new_col[0]                 
    if funct == 'wheel4 color g':
        s_color = wheel4.color
        s_color_r = s_color[1]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color_r, s_color[2], s_color[3]]
        wheel4.color = new_col
        dict['wheel4_g'] = new_col[1]    
    if funct == 'wheel4 color b':
        s_color = wheel4.color
        s_color_r = s_color[2]
        if motion == 'inc':
            if s_color_r < 1:
                s_color_r += c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 1 
        if motion == 'dec':
            if s_color_r > c_move:
                s_color_r -= c_move
                s_color_r = round(s_color_r, 2)
            else:
                s_color_r = 0                          
        new_col = [s_color[0], s_color[1], s_color_r, s_color[3]]
        wheel4.color = new_col
        dict['wheel4_b'] = new_col[2]                   


    if funct == 'deck graphic':
        if motion == 'inc':
            dict['deck_index'] = int(dict['deck_index'])
            if dict['deck_index'] < ((len(dict['deckList']) - 1)):
                dict['deck_index'] += 1
                colors.update_truck_tex() 
                
        if motion == 'dec':
            dict['deck_index'] = int(dict['deck_index'])
            if dict['deck_index'] > 0 :                
                dict['deck_index'] -= 1                
                colors.update_truck_tex() 
                
                
                
    if funct == 'shirt logo':
        if motion == 'inc':
            dict['shirt_logo'] = int(dict['shirt_logo'])
            if dict['shirt_logo'] < (len(dict['shirtList']) - 1):
                dict['shirt_logo'] += 1
                colors.update_shirt_tex() 
                
        if motion == 'dec':
            dict['shirt_logo'] = int(dict['shirt_logo'])
            if dict['shirt_logo'] > 0 :                
                dict['shirt_logo'] -= 1                
                colors.update_shirt_tex()                 
                
                
        
        
#    if funct == 'shirt logo':
#        logo = dict['shirt_logo']               
#        logo1 = shirt.meshes[0].materials[0].textures[6]
#        logo2 = shirt.meshes[0].materials[0].textures[7]
#        logo3 = shirt.meshes[0].materials[0].textures[5]        
#        if motion == 'inc':
#            if logo == 0:
#                logo = 1
#            elif logo == 1:
#                logo = 2   
#            elif logo == 2:
#                logo = 3
#            elif logo == 3:
#                logo = 0                                
#        if motion == 'dec':
#            if logo == 0:
#                logo = 3
#            elif logo == 1:
#                logo = 0   
#            elif logo == 2:
#                logo = 1 
#            elif logo == 3:
#                logo = 2                                               
#        if logo == 1:
#            logo1.diffuseIntensity = 1
#            logo2.diffuseIntensity = 0
#            logo1.diffuseFactor = 1
#            logo2.diffuseFactor = 0
#            logo3.diffuseIntensity = 0
#            logo3.diffuseFactor = 0
#        if logo == 2:
#            logo1.diffuseIntensity = 0
#            logo1.diffuseFactor = 0
#            logo2.diffuseIntensity = 1 
#            logo2.diffuseFactor = 1  
#            logo3.diffuseIntensity = 0
#            logo3.diffuseFactor = 0            
#        if logo == 3:
#            logo1.diffuseIntensity = 0
#            logo1.diffuseFactor = 0
#            logo2.diffuseIntensity = 0 
#            logo2.diffuseFactor = 0  
#            logo3.diffuseIntensity = 1
#            logo3.diffuseFactor = 1                                 
#        if logo == 0:
#            logo1.diffuseIntensity = 0
#            logo2.diffuseIntensity = 0            
#            logo1.diffuseFactor = 0
#            logo2.diffuseFactor = 0
#            logo3.diffuseIntensity = 0
#            logo3.diffuseFactor = 0            
#        dict['shirt_logo']  = logo
        
    if funct == 'brightness / contrast on':
        if motion == 'inc':
            cont.activate(cam.actuators["bc"])
            cam['bc'] = 1
            dict['bc'] = 1
        else:
            cont.activate(cam.actuators["bc_off"])            
            cam['bc'] = 0
            dict['bc'] = 0
    if funct == 'brightness value':
        b = cam['BC_BRIGHTNESS']
        if motion == 'inc':
            b += .01
        else:
            b -= .01    
        cam['BC_BRIGHTNESS'] = round(b,2)        
        dict['BC_BRIGHTNESS'] = round(b,2)        

    if funct == 'contrast value':
        b = cam['BC_CONTRAST']
        if motion == 'inc':
            b += .01
        else:
            b -= .01    
        cam['BC_CONTRAST'] = round(b,2)        
        dict['BC_CONTRAST'] = round(b,2)    
        
    if funct == 'hdr on':
        if motion == 'inc':
            cont.activate(cam.actuators["hdr"])
            cam['hdr'] = 1
            dict['hdr'] = 1
        else:
            cont.activate(cam.actuators["hdr_off"])            
            cam['hdr'] = 0
            dict['hdr'] = 0
    if funct == 'avgL':
        b = cam['avgL']
        if motion == 'inc':
            b += .01
        else:
            b -= .01    
        cam['avgL'] = round(b,2)        
        dict['avgL'] = round(b,2)        

    if funct == 'hdr strength':
        b = cam['HDRamount']
        if motion == 'inc':
            b += .01
        else:
            b -= .01    
        cam['HDRamount'] = round(b,2)        
        dict['HDRamount'] = round(b,2)              

    if funct == 'ao on':
        if motion == 'inc':
            cont.activate(cam.actuators["ao"])
            cam['ao'] = 1
            dict['ao'] = 1
        else:
            cont.activate(cam.actuators["ao_off"])            
            cam['ao'] = 0
            dict['ao'] = 0
    if funct == 'ao width':
        b = cam['aowidth']
        if motion == 'inc':
            b += .2
        else:
            b -= .2    
        cam['aowidth'] = round(b,2)        
        dict['aowidth'] = round(b,2)        

    if funct == 'ao radius':
        b = cam['aoradius']
        if motion == 'inc':
            b += .2
        else:
            b -= .2   
        cam['aoradius'] = round(b,2)        
        dict['aoradius'] = round(b,2)  
    if funct == 'ao only':
        b = cam['onlyAO']
        if motion == 'inc':
            b = 1
        else:
            b = 0    
        cam['onlyAO'] = b        
        dict['onlyAO'] = b  
        
        
    if funct == 'dof on':
        if motion == 'inc':
            cont.activate(cam.actuators["DOF"])
            cam['dof_on'] = 1
            dict['dof_on'] = 1
        else:
            cont.activate(cam.actuators["DOF_off"])            
            cam['dof_on'] = 0
            dict['dof_on'] = 0      
            
    if funct == 'bloom on':
        if motion == 'inc':
            cont.activate(cam.actuators["Bloom"])
            cam['bloom_on'] = 1
            dict['bloom_on'] = 1
        else:
            cont.activate(cam.actuators["bloom_off"])            
            cam['bloom_on'] = 0
            dict['bloom_on'] = 0  
            
#------------
    if funct == 'fxaa on':
        if motion == 'inc':
            cont.activate(cam.actuators["fxaa"])
            cam['fxaa'] = 1
            dict['fxaa'] = 1
        else:
            cont.activate(cam.actuators["fxaa_off"])            
            cam['fxaa'] = 0
            dict['fxaa'] = 0
    if funct == 'fxaa span max':
        b = cam['FXAA_SPAN_MAX']
        if motion == 'inc':
            b += 1
        else:
            b -= 1    
        cam['FXAA_SPAN_MAX'] = b        
        dict['FXAA_SPAN_MAX'] = b  
#---------            

    if funct == 'shadow on':
        if motion == 'inc':
            cam['shadow_on'] = 1
            dict['shadow_on'] = 1
            #bge.render.setGLSLMaterialSetting("shadows", 1)                
        else:
            #cont.activate(cam.actuators["bloom_off"])            
            cam['shadow_on'] = 0
            dict['shadow_on'] = 0  
            #bge.render.setGLSLMaterialSetting("shadows", 0)                

            
                     
        
    if funct == 'sun strength':
        b = cam['sun_strength']
        if motion == 'inc':
            b += .05
        else:
            b -= .05    

        cam['sun_strength'] = round(b,2)        
        dict['sun_strength'] = round(b,2)                                       

        for x in dict['sun_lights']:
            x.energy = dict['sun_strength']
    
    if funct == 'ambient strength':
        b = cam['ambient_strength']
        if motion == 'inc':
            b += .025
        else:
            b -= .025    
        cam['ambient_strength'] = round(b,2)        
        dict['ambient_strength'] = round(b,2)  

        for x in dict['ambient_lights']:
            if 'down' in x:
                x.energy = dict['ambient_strength']
            else:
                x.energy = dict['ambient_strength'] * .7         

    if funct == 'sun rot x':
        #b = cam['sun_rot_x']
        b=0
        if motion == 'inc':
            b += .05
        else:
            b -= .05        
        if 'camposEmpty' in scene.objects:
            
            em = scene.objects['camposEmpty']
            
            rot = [ b, 0.0, 0]
            em.applyRotation(rot,True)
        cam['sun_rot_x'] = round(b,2)
        dict['sun_rot_x'] = round(b,2)
    if funct == 'sun rot y':
        #b = cam['sun_rot_y']
        b=0
        if motion == 'inc':
            b += .05
        else:
            b -= .05         
        if 'camposEmpty' in scene.objects:
            
            em = scene.objects['camposEmpty']
            
            rot = [ 0, b, 0]
            em.applyRotation(rot,True)
        cam['sun_rot_y'] = round(b,2)
        dict['sun_rot_y'] = round(b,2) 
        
        
        
    if funct == 'cam height':
        b = dict['cam_height']
        if motion == 'inc':
            b += .1
        else:
            b -= .1    
        dict['cam_height'] = round(b,2)  
        
    if funct == 'focal length':
        b = dict['focal_length']
        if motion == 'inc':
            b += 2
        else:
            b -= 2    
        dict['focal_length'] = b                   
        
    if funct == 'min dist':
        b = dict['cam_min']
        if motion == 'inc':
            b += .1
        else:
            b -= .1    
        dict['cam_min'] = round(b,2) 
    if funct == 'max dist':
        b = dict['cam_max']
        if motion == 'inc':
            b += .1
        else:
            b -= .1    
        dict['cam_max'] = round(b,2) 
        
    if funct == 'target height':
        b = dict['cch_targetHeight']
        if motion == 'inc':
            b += .05
        else:
            b -= .05    
        dict['cch_targetHeight'] = round(b,3) 
            
        
#cch_targetHeight        
        
    if funct == 'recorder on':
        if motion == 'inc':
            cam['recorder_on'] = 1
            dict['recorder_on'] = 1
        else:         
            cam['recorder_on'] = 0
            dict['recorder_on'] = 0

 


    if funct == 'record length':
        b = dict['replay_record_length']
        if motion == 'inc':
            if b < 7300:    
                b += 60
        else:
            if b > 60:
                b -= 60    
        dict['replay_record_length'] = b  
        
    #levels
    print(funct)
    if funct in dict['maps']:
        print('menu - new funct in maps', funct)
        
        if motion == 'inc':
            level = dict['map_template']
            main_empty["level"] = dict['map_template']
            dict['levelBlend'] = dict['map_template']
            dict['map'] = funct
            #dict['level'] = level
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1              
            dict['gameFSM'].FSM.ToTransition('toReload')



    else:
        print('funct not in maps')

    if funct == 'Demo Scene':
        if motion == 'inc':
            level = "pats"
            main_empty["level"] = "pats"
            dict['levelBlend'] = 'mini_island'
            #dict['level'] = level
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1              
            
    if funct == 'Slavonski Brod (WIP)':
        if motion == 'inc':
            level = "j_scene"
            main_empty["level"] = "j_scene"
            #dict['level'] = level
            dict['levelBlend'] = 'j_scene'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1 
            
    if funct == 'Empty Lot':
        if motion == 'inc':
            level = "lot"
            main_empty["level"] = "lot"
            #dict['level'] = level
            dict['levelBlend'] = 'lot'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1   
            
    if funct == 'Training':
        if motion == 'inc':
            level = "train"
            main_empty["level"] = "train"
            #dict['level'] = level
            dict['levelBlend'] = 'train'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1 
            
    if funct == 'Spine':
        if motion == 'inc':
            level = "spine"
            main_empty["level"] = "spine"
            #dict['level'] = level
            dict['levelBlend'] = 'spine'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1 
            
    if funct == 'Warehouse':
        if motion == 'inc':
            level = "warehouse"
            main_empty["level"] = "warehouse"
            #dict['level'] = level
            dict['levelBlend'] = 'warehouse'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1 
            
    if funct == 'Shop':
        if motion == 'inc':
            level = "shop"
            main_empty["level"] = "shop"
            #dict['level'] = level
            dict['levelBlend'] = 'shop'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1 
            
    if funct == 'SC':
        if motion == 'inc':
            level = "sc"
            main_empty["level"] = "sc"
            #dict['level'] = level
            dict['levelBlend'] = 'sc'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1                          

    if funct == 'User1':
        if motion == 'inc':
            level = "user1"
            main_empty["level"] = "user1"
            #dict['level'] = level
            dict['levelBlend'] = 'user1'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1 
            
    if funct == 'User2':
        if motion == 'inc':
            level = "user2"
            main_empty["level"] = "user2"
            #dict['level'] = level
            dict['levelBlend'] = 'user2'
            Settings.writeSettings()
            Settings.readSettings()
            #cont.activate(own.actuators['restart']) 
            #Settings.loadlevel()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1         

                                         
    if funct in player_list:
        if motion == 'inc':
            dict['character'] = funct
            Settings.writeSettings()
            Settings.readSettings()
            dict['load_timer'] = load_timer
            dict['reload_timer'] = 250
            dict['overlay_fadein'] = 1  
            
#    if funct == 'billy':
#        if motion == 'inc':
#            dict['character'] = 'billy'
#            Settings.writeSettings()
#            Settings.readSettings()
#            dict['load_timer'] = load_timer
#            dict['reload_timer'] = 250
#            dict['overlay_fadein'] = 1                                                       
#'1920x1080', '1280x720', '1024x768', '800x600', 'fullscreen'                            
    if funct == '1920x1080':
        #pass
        if motion == 'inc':
            dict['resx'] = 1920
            dict['resy'] = 1080
            Settings.writeSettings()
            bge.render.setWindowSize(1920, 1080)
#            if dict['fullscreen_on'] == 1:
#                bge.render.setFullScreen(True)  
#            else:
#                bge.render.setFullScreen(False)              
    if funct == '1280x720':
        if motion == 'inc':
            dict['resx'] = 1280
            dict['resy'] = 720
            Settings.writeSettings()
            bge.render.setWindowSize(1280, 720)
#            if dict['fullscreen_on'] == 1:
#                bge.render.setFullScreen(True)  
#            else:
#                bge.render.setFullScreen(False)              
    if funct == '1024x768':
        if motion == 'inc':
            dict['resx'] = 1024
            dict['resy'] = 768
            Settings.writeSettings()
            bge.render.setWindowSize(1024, 768)
#            if dict['fullscreen_on'] == 1:
#                bge.render.setFullScreen(True)  
#            else:
#                bge.render.setFullScreen(False)              
    if funct == '800x600':
        if motion == 'inc':
            dict['resx'] = 800
            dict['resy'] = 600
  
            Settings.writeSettings()
            bge.render.setWindowSize(800, 600)
#            if dict['fullscreen_on'] == 1:
#                bge.render.setFullScreen(True)  
#            else:
#                bge.render.setFullScreen(False)                
    if funct == 'fullscreen':
        if motion == 'inc':            
            dict['fullscreen_on'] = 1
            bge.render.setFullScreen(True)
            Settings.writeSettings()
        else:
            dict['fullscreen_on'] = 0   
            bge.render.setFullScreen(False)
            Settings.writeSettings()
                            
                                                                                    

#=======
#end exit functions

def get_c_list(dict):
    #main menu
    if dict['mlevel'] == 0:
        dict['current_list'] = ['settings', 'level', 'replay', 'exit']
        
    #opt = dict['current_list'][dict['current_index']]
    
    if dict['mlevel'] == 1:
        if dict['lv0_opt'] == 'settings':
        #if opt == 'settings':
            dict['current_list'] = ['resolution', 'graphics', 'player', 'deck', 'level', 'camera', 'physics']
        if dict['lv0_opt'] == 'replay':
            dict['current_list'] = ['enter replay (press back button)', 'recorder on', 'record length', 'loop start', 'loop end'] 
        if dict['lv0_opt'] == 'level':
            dict['current_list'] = ['Demo Scene', 'Empty Lot', 'Training', 'Spine', 'Warehouse', 'Shop', 'SC', 'User1', 'User2'] 
            dict['current_list'] = dict['maps'] 
            
    if dict['mlevel'] == 2:
        if dict['lv1_opt'] == 'resolution':
        #if opt == 'settings':
            dict['current_list'] = ['fullscreen']               
    if dict['mlevel'] == 2:
        if dict['lv1_opt'] == 'graphics':
        #if opt == 'settings':
            dict['current_list'] = ['brightness / contrast', 'ao', 'hdr', 'dof', 'bloom', 'fxaa']
    if dict['mlevel'] == 2:
        if dict['lv1_opt'] == 'player':
        #if opt == 'settings':
            dict['current_list'] = ['character', 'shirt color r', 'shirt color g', 'shirt color b', 'shirt logo', 'shoe color r', 'shoe color g', 'shoe color b'] 
    if dict['mlevel'] == 2:
        if dict['lv1_opt'] == 'deck':
        #if opt == 'settings':
            dict['current_list'] = ['deck graphic', 'deck color r', 'deck color g', 'deck color b', 'trucks color r', 'trucks color g', 'trucks color b', 'wheel color r', 'wheel color g', 'wheel color b', 'wheel1 color r', 'wheel1 color g', 'wheel1 color b', 'wheel2 color r', 'wheel2 color g', 'wheel2 color b', 'wheel3 color r', 'wheel3 color g', 'wheel3 color b', 'wheel4 color r', 'wheel4 color g', 'wheel4 color b']                        

    if dict['mlevel'] == 2:
        if dict['lv1_opt'] == 'level':
        #if opt == 'settings':
            dict['current_list'] = ['sun strength', 'ambient strength', 'sun rot x', 'sun rot y', 'shadow on']
            
    if dict['mlevel'] == 2:
        if dict['lv1_opt'] == 'camera':
        #if opt == 'settings':
            dict['current_list'] = ['cam height', 'focal length', 'min dist', 'max dist', 'target height']            
            

    if dict['mlevel'] == 3:
        if dict['lv2_opt'] == 'ao':
            dict['current_list'] = ['ao on', 'ao radius', 'ao width', 'ao only'] 
        if dict['lv2_opt'] == 'brightness / contrast':
            dict['current_list'] = ['brightness / contrast on', 'brightness value', 'contrast value'] 
            
        if dict['lv2_opt'] == 'hdr':
            dict['current_list'] = ['hdr on', 'avgL', 'hdr strength']
        if dict['lv2_opt'] == 'dof':
            dict['current_list'] = ['dof on']  
        if dict['lv2_opt'] == 'bloom':
            dict['current_list'] = ['bloom on'] 
        if dict['lv2_opt'] == 'fxaa':
            dict['current_list'] = ['fxaa on', 'fxaa span max']     
            
        if dict['lv2_opt'] == 'character':
            dict['current_list'] = player_list   

def get_inputs(dict, cont):
    import aud
    obj = cont.owner
    #opt = dict['current_list'][dict['current_index']]
    #b
    
    obj['sound'] = aud.Factory(bge.logic.expandPath('//sounds/select.wav'))
    obj['sound_a'] = aud.Factory(bge.logic.expandPath('//sounds/a.wav'))
    obj['sound_b'] = aud.Factory(bge.logic.expandPath('//sounds/b.wav')) 

    obj['sound'] = obj['sound'].pitch(.8)
    obj['sound_a'] = obj['sound_a'].pitch(.8)
    obj['sound_b'] = obj['sound_b'].pitch(.8)   
    sound = aud.Factory(bge.logic.expandPath('//sounds/select.wav')) 
    sound = sound.pitch(.8)

    if dict['kb_q'] == JUST_ACTIVATED or (dict['bBut'] == False and dict['last_bBut'] == True):
        aud.device().play(obj['sound_b'])
        if dict['current_index'] not in dict['menu_end_points']:
            #print('lkey')
            if dict['mlevel'] == 2:
                dict['mlevel'] = 1
                dict['current_index'] = dict['lv1_index']
            elif dict['mlevel'] == 1:
                dict['mlevel'] = 0
                dict['current_index'] = dict['lv0_index'] 
            elif dict['mlevel'] == 3:
                dict['mlevel'] = 2
                dict['current_index'] = dict['lv2_index']
        else:
            pass
                              
        #a
    if dict['kb_w'] == JUST_ACTIVATED or (dict['aBut'] == False and dict['last_aBut'] == True):
        aud.device().play(obj['sound_a'])
        if dict['current_opt'] not in dict['menu_end_points']:
            if dict['mlevel'] == 0: 
                dict['lv0_opt'] = dict['current_list'][dict['current_index']]
                dict['lv0_index'] = dict['current_index']
                dict['mlevel'] = 1
                dict['lv1_index'] = 0   
                dict['current_index'] = 0 
            elif dict['mlevel'] == 1: 
                dict['lv1_opt'] = dict['current_list'][dict['current_index']]
                dict['lv1_index'] = dict['current_index']
                dict['mlevel'] = 2
                dict['lv2_index'] = 0   
                dict['current_index'] = 0 
            elif dict['mlevel'] == 2: 
                dict['lv2_opt'] = dict['current_list'][dict['current_index']]
                dict['lv2_index'] = dict['current_index']
                dict['mlevel'] = 3
                dict['lv3_index'] = 0   
                dict['current_index'] = 0  
            elif dict['mlevel'] == 3: 
                dict['lv3_opt'] = dict['current_list'][dict['current_index']]
                dict['lv3_index'] = dict['current_index']
                #dict['mlevel'] = 3
                #dict['lv3_index'] = 0   
                dict['current_index'] = 0                                      
        else:
            pass
            funct = dict['current_opt']
            motion = 'inc'
            endpoint(funct, motion, dict, cont)                          
    #down     
    if dict['kb_da'] == JUST_ACTIVATED or (dict['ddPad'] == False and dict['last_ddPad'] == True):
        aud.device().play(obj['sound'])
        if dict['current_index'] < (len(dict['current_list']) - 1):
            dict['current_index'] += 1  
            if dict['mlevel'] == 0:
                dict['lv0_index'] = dict['current_index']
            if dict['mlevel'] == 1:
                dict['lv1_index'] = dict['current_index']                
            if dict['mlevel'] == 2:
                dict['lv2_index'] = dict['current_index']  
            if dict['mlevel'] == 3:
                dict['lv3_index'] = dict['current_index']                                                
    #up    
    if dict['kb_ua'] == JUST_ACTIVATED or (dict['udPad'] == False and dict['last_udPad'] == True):
        aud.device().play(obj['sound'])
        if dict['current_index'] > 0:
            dict['current_index'] -= 1  
            if dict['mlevel'] == 0:
                dict['lv0_index'] = dict['current_index']
            if dict['mlevel'] == 1:
                dict['lv1_index'] = dict['current_index']                
            if dict['mlevel'] == 2:
                dict['lv2_index'] = dict['current_index'] 
            if dict['mlevel'] == 3:
                dict['lv3_index'] = dict['current_index']        
                
    #left
    if dict['kb_la'] == JUST_ACTIVATED or (dict['ldPad'] == True and dict['last_ldPad'] == False):
        aud.device().play(obj['sound_a'])
        print('left')
        if dict['current_opt'] in dict['menu_end_points']:
            funct = dict['current_opt']
            motion = 'dec'
            endpoint(funct, motion, dict, cont)         
            print('left2')
    
    #right
    if dict['kb_ra'] == JUST_ACTIVATED or (dict['rdPad'] == True and dict['last_rdPad'] == False):
        aud.device().play(obj['sound_b'])
        print('right')
        if dict['current_opt'] in dict['menu_end_points']:
            funct = dict['current_opt']
            motion = 'inc'
            endpoint(funct, motion, dict, cont)              
            print('right2')
    
    modder = 15                                  
    #left repeater
    if dict['ldPad'] == True:
        dict['lRepeater'] += 1
    else:
        dict['lRepeater'] = 0
    leftmod = dict['lRepeater'] % modder
    if leftmod == (modder - 1):
        aud.device().play(obj['sound_a'])
        funct = dict['current_opt']
        motion = 'dec'
        endpoint(funct, motion, dict, cont)  
    #right repeater
    if dict['rdPad'] == True:
        dict['rRepeater'] += 1
    else:
        dict['rRepeater'] = 0
    rightmod = dict['rRepeater'] % modder
    if rightmod == (modder - 1):
        aud.device().play(obj['sound_b'])
        funct = dict['current_opt']
        motion = 'inc'
        endpoint(funct, motion, dict, cont)                   
       
    
    
def init(own, dict):
    #print('initing')
    own['inited'] = True
    dict['lv0_opt'] = 0
    dict['lv0_index'] = 0
    dict['lv1_opt'] = 0
    dict['lv1_index'] = 0 
    dict['lv2_opt'] = 0
    dict['lv2_index'] = 0
    dict['lv3_opt'] = 0
    dict['lv3_index'] = 0        
    dict['current_list'] = ['']
    dict['current_index'] = 0
    dict['mlevel'] = 0
    dict['current_opt'] = ''
    dict['pause_menu_text'] = ''
    dict['menu_end_points'] = ['exit', 'restart', 'shirt color r', 'shirt color g', 'shirt color b', 'shoe color r', 'shoe color g', 'shoe color b','deck color r', 'deck color g', 'deck color b', 'shirt logo', 'brightness / contrast on', 'brightness value', 'contrast value', 'hdr on', 'avgL', 'hdr strength', 'ao on', 'ao radius', 'ao width', 'ao only', 'dof on', 'sun strength', 'sun rot x', 'sun rot y', 'shadow on', 'ambient strength', 'Demo Scene', 'Empty Lot', 'Park A', 'Training', 'Spine', 'Warehouse', 'Shop', '1920x1080', '1280x720', '1024x768', '800x600', 'fullscreen', 'bloom on', 'cam height', 'focal length', 'min dist', 'max dist', 'enter replay', 'recorder on', 'record length', 'enter replay (press back button)', 'recorder on', 'record length', 'loop start', 'loop end', 'physics', 'fxaa on', 'fxaa span max', 'target height', 'trucks color r', 'trucks color g', 'trucks color b', 'wheel1 color r', 'wheel1 color g', 'wheel1 color b', 'wheel2 color r', 'wheel2 color g', 'wheel2 color b', 'wheel3 color r', 'wheel3 color g', 'wheel3 color b', 'wheel4 color r', 'wheel4 color g', 'wheel4 color b', 'deck graphic', 'wheel color r', 'wheel color g', 'wheel color b', 'SC', 'User1', 'User2'] + player_list + dict['maps']
    
def output(dict):
    try:
        opt = dict['current_list'][dict['current_index']]
    except:
        pass
        #print('broken!!', dict['current_list'], dict['current_index'])    
    dict['current_opt'] = opt
    #print(dict['lv1_index'], 'level: ', dict['mlevel'], dict['lv0_opt'], dict['current_index'], dict['current_list'], dict['current_opt'])    
    outp = 'menu > '
    if dict['mlevel'] == 0:
        outp = outp + dict['current_opt']
    if dict['mlevel'] == 1:
        outp = outp + dict['lv0_opt'] +' > ' + dict['current_opt'] 
    if dict['mlevel'] == 2:
        outp = outp + dict['lv0_opt'] +' > ' + dict['lv1_opt'] + ' > ' + dict['current_opt']  
    if dict['mlevel'] == 3:
        outp = outp + dict['lv0_opt'] +' > ' + dict['lv1_opt'] + ' > '  + dict['lv2_opt'] + ' > ' + dict['current_opt']                       
    
    #print(outp) 
    #print(dict['current_opt'])
    if dict['current_opt'] in dict['menu_end_points']:
        #print('adding end point value')
        if dict['current_opt'] == 'shirt color r':
            outp = outp + ': ' + str(dict['shirt_color_r'])
            #print(outp + ': ' + str(dict['shirt_color_r']))
        if dict['current_opt'] == 'shirt color g':
            outp = outp + ': ' + str(dict['shirt_color_g'])
        if dict['current_opt'] == 'shirt color b':
            outp = outp + ': ' + str(dict['shirt_color_b']) 
            
        if dict['current_opt'] == 'shoe color r':
            outp = outp + ': ' + str(dict['shoe_color_r'])
            #print(outp + ': ' + str(dict['shoe_color_r']))
        if dict['current_opt'] == 'shoe color g':
            outp = outp + ': ' + str(dict['shoe_color_g'])
        if dict['current_opt'] == 'shoe color b':
            outp = outp + ': ' + str(dict['shoe_color_b'])      
        if dict['current_opt'] == 'deck color r':
            outp = outp + ': ' + str(dict['deck_color_r'])
            #print(outp + ': ' + str(dict['deck_color_r']))
        if dict['current_opt'] == 'deck color g':
            outp = outp + ': ' + str(dict['deck_color_g'])
        if dict['current_opt'] == 'deck color b':
            outp = outp + ': ' + str(dict['deck_color_b'])  
        if dict['current_opt'] == 'shirt logo':
            outp = outp + ': ' + str(dict['shirt_name'])   
        if dict['current_opt'] == 'deck graphic':
            outp = outp + ': ' + str(dict['deck_name'])               
            
        if dict['current_opt'] == 'trucks color r':
            outp = outp + ': ' + str(dict['trucks_r'])
        if dict['current_opt'] == 'trucks color g':
            outp = outp + ': ' + str(dict['trucks_g'])
        if dict['current_opt'] == 'trucks color b':
            outp = outp + ': ' + str(dict['trucks_b']) 
        
        if dict['current_opt'] == 'wheel1 color r':
            outp = outp + ': ' + str(dict['wheel1_r'])
        if dict['current_opt'] == 'wheel1 color g':
            outp = outp + ': ' + str(dict['wheel1_g'])
        if dict['current_opt'] == 'wheel1 color b':
            outp = outp + ': ' + str(dict['wheel1_b'])  
        
        if dict['current_opt'] == 'wheel2 color r':
            outp = outp + ': ' + str(dict['wheel2_r'])
        if dict['current_opt'] == 'wheel2 color g':
            outp = outp + ': ' + str(dict['wheel2_g'])
        if dict['current_opt'] == 'wheel2 color b':
            outp = outp + ': ' + str(dict['wheel2_b'])  
        
        if dict['current_opt'] == 'wheel3 color r':
            outp = outp + ': ' + str(dict['wheel3_r'])
        if dict['current_opt'] == 'wheel3 color g':
            outp = outp + ': ' + str(dict['wheel3_g'])
        if dict['current_opt'] == 'wheel3 color b':
            outp = outp + ': ' + str(dict['wheel3_b'])  
        
        if dict['current_opt'] == 'wheel4 color r':
            outp = outp + ': ' + str(dict['wheel4_r'])
        if dict['current_opt'] == 'wheel4 color g':
            outp = outp + ': ' + str(dict['wheel4_g'])
        if dict['current_opt'] == 'wheel4 color b':
            outp = outp + ': ' + str(dict['wheel4_b'])                                                 
        if dict['current_opt'] == 'wheel color r':
            outp = outp + ': ' + str(dict['wheel1_r'])
        if dict['current_opt'] == 'wheel color g':
            outp = outp + ': ' + str(dict['wheel1_g'])
        if dict['current_opt'] == 'wheel color b':
            outp = outp + ': ' + str(dict['wheel1_b'])                          
            
            
                                                                  
        if dict['current_opt'] ==  'brightness / contrast on':
            outp = outp + ': ' + str(dict['bc'])             
            
        if dict['current_opt'] ==  'brightness value':
            outp = outp + ': ' + str(dict['BC_BRIGHTNESS']) 
        if dict['current_opt'] ==  'contrast value':
            outp = outp + ': ' + str(dict['BC_CONTRAST'])   
            
        if dict['current_opt'] ==  'hdr on':
            outp = outp + ': ' + str(dict['hdr'])             
        if dict['current_opt'] ==  'avgL':
            outp = outp + ': ' + str(dict['avgL']) 
        if dict['current_opt'] ==  'hdr strength':
            outp = outp + ': ' + str(dict['HDRamount'])  
            
        if dict['current_opt'] ==  'ao on':
            outp = outp + ': ' + str(dict['ao'])             
        if dict['current_opt'] ==  'ao width':
            outp = outp + ': ' + str(dict['aowidth']) 
            
            
        if dict['current_opt'] ==  'ao radius':
            outp = outp + ': ' + str(dict['aoradius'])
            
            
        if dict['current_opt'] ==  'ao only':
            outp = outp + ': ' + str(dict['onlyAO'])   
            
            
        if dict['current_opt'] ==  'dof on':
            outp = outp + ': ' + str(dict['dof_on'])  
        if dict['current_opt'] ==  'sun strength':
            outp = outp + ': ' + str(dict['sun_strength'])                                   
        if dict['current_opt'] ==  'sun rot x':
            outp = outp + ': ' + str(dict['sun_rot_x'])                                      
        if dict['current_opt'] ==  'sun rot y':
            outp = outp + ': ' + str(dict['sun_rot_y'])                
        if dict['current_opt'] ==  'shadow on':
            #outp = outp + ': ' + str(dict['shadow_on']) 
            outp = outp + ': (not available)'  
        if dict['current_opt'] ==  'ambient strength':
            outp = outp + ': ' + str(dict['ambient_strength'])   
        if dict['current_opt'] ==  'fullscreen':
            outp = outp + ': ' + str(dict['fullscreen_on'])  
        if dict['current_opt'] ==  'bloom on':
            outp = outp + ': ' + str(dict['bloom_on'])  
        if dict['current_opt'] ==  'fxaa on':
            outp = outp + ': ' + str(dict['fxaa'])  
        if dict['current_opt'] ==  'fxaa span max':
            outp = outp + ': ' + str(dict['FXAA_SPAN_MAX'])              
            
            
        if dict['current_opt'] ==  'cam height':
            outp = outp + '(-0.4): ' + str(dict['cam_height']) 
        if dict['current_opt'] ==  'focal length':
            outp = outp + '(16): ' + str(dict['focal_length'])  
        
        if dict['current_opt'] ==  'min dist':
            outp = outp + '(1.6): ' + str(dict['cam_min'])  
        if dict['current_opt'] ==  'max dist':
            outp = outp + '(2.4): ' + str(dict['cam_max'])  
        if dict['current_opt'] ==  'recorder on':
            outp = outp + ': ' + str(dict['recorder_on']) 
        if dict['current_opt'] ==  'target height':
            outp = outp + ': ' + str(dict['cch_targetHeight']) 

        if dict['current_opt'] ==  'loop start':
            outp = outp + ': ' + str(dict['replay_loop_start'])   
        if dict['current_opt'] ==  'loop end':
            outp = outp + ': ' + str(dict['replay_loop_end'])               
        if dict['current_opt'] ==  'record length':
            outp = outp + ': ' + str(dict['replay_record_length'])               

#        if dict['current_opt'] ==  'character':
#            outp = outp + ': ' + str(dict['character'])             
                                
            
    dict['pause_menu_text'] = outp
def main(cont):
    
    own = cont.owner
    dict = bge.logic.globalDict
    if 'inited' not in own:
        init(own, dict)
    get_c_list(dict) 
    get_inputs(dict, cont) 
    get_c_list(dict)
    output(dict)  