import bge
import random

import Startup as S2
import Settings
import inputs
import logo_fades

dict = bge.logic.globalDict
#====================================     

State = type("State", (object,), {})
#====================================     
class State(object):
    def __init__(self, FSM):
        self.FSM = FSM
        self.timer = 0
        self.startTime = 0
    def Enter(self):
        self.timer = 0
        self.startTime = 0
    def Execute(self):
        print('Executing')
    def Exit(self):
        print('Exiting')

#==================================== 
            
class Example(State):
    def __init__(self,FSM):
        super(Example, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Example, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #self.FSM.ToTransition('toLand')
        
    def Exit(self):
        pass

#==================================== 

class Startup(State):
    def __init__(self,FSM):
        super(Startup, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Startup, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1

        #if self.FSM.stateLife == 2:
            #Startup.main(self.FSM.owner['cont'])

        self.FSM.ToTransition('toIniter')
        
    def Exit(self):
        pass

#==================================== 

class Initer(State):
    def __init__(self,FSM):
        super(Initer, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Initer, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1

        if self.FSM.stateLife == 2:
            print('--------- fsm running startup')
            S2.main(self.FSM.owner['cont'])
            #Settings.readSettings(self.FSM.owner['cont'])
            Settings.readSettings()
            Settings.setres()


        if self.FSM.stateLife == 120:

            self.FSM.ToTransition('toLoadLevel')
        
    def Exit(self):
        pass


#==================================== 

class LoadLevel(State):
    def __init__(self,FSM):
        super(LoadLevel, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(LoadLevel, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        print('fsm loading level')
        
        Settings.loadlevel()
        
        self.FSM.ToTransition('toGameOn')
    def Exit(self):
        pass

#==================================== 

class GameOn(State):
    def __init__(self,FSM):
        super(GameOn, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(GameOn, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #self.FSM.ToTransition('toLand')
        #print('game on')

        inputs.main()
        logo_fades.main()

    def Exit(self):
        pass

#==================================== 

class Reload(State):
    def __init__(self,FSM):
        super(Reload, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Reload, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        dict.clear()
        self.FSM.owner['cont'].activate(self.FSM.owner['cont'].actuators['restart'])
        #cont.activate(own.actuators['add_fade'])  
        #own['cont'] = cont

    def Exit(self):
        pass

#==================================== 