
import bge
from mathutils import Vector

def rot_deck(cont):
      
    scene = bge.logic.getCurrentScene()
    deck = scene.objects['npc_ed_deck']
    #rot = [ 0.0, 0.0, -1.570796327]
    print('rotating')
    deck.alignAxisToVect(Vector([1,0,0]),2,.25) 
    #rot = [ 0.0, 0.0, -0.10796327]
    #deck.applyRotation(rot,True) 
    #print('rotating')    
    
def collect_npc(cont):
    #add ed
    own = cont.owner
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
    objList = scene.objects   
    
    try:
        spawned_list = dict['spawned_npc_list']
        to_spawn = dict['to_spawn']
    except:
        dict['spawned_npc_list'] = []  
        dict['to_spawn'] = []  
        to_spawn = [] 
        own['npc_index'] = 0
    
    npc_index = 0
    for x in objList:
        
        if 'npc_skater' in x:
            
            dict['to_spawn'].append(x)
            print(x.name, 'added to list')
            
def spawn_npc(cont):
    own = cont.owner
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
    objList = scene.objects 
    to_spawn = dict['to_spawn']
    #print(to_spawn)
    npc_index = own['npc_index']
    
    if to_spawn:
        x = dict['to_spawn'][0]  
        print(x) 
        
        x['rpStartLoc'] = [0.0,0.0,0.0] 
        x['rpStartLoc_set'] = False
        x['move'] = False  
        x['npc_rotate'] = False 
        x['rotate_in'] = False
        x['move_to'] = False
        x['rotate_out'] = False      

        obj = scene.addObject('npc_ed_deck', own, 0)
        obj.setParent(x, False, False)            
        dict['spawned_npc_decks'].append(obj)
        obj = scene.addObject('npc_ed_trucks', own, 0)
        obj.setParent(x, False, False)            
        dict['spawned_npc_trucks'].append(obj)
        obj = scene.addObject('npc_ed_arm', own, 0)
        obj.setParent(x, False, False)
        dict['spawned_npcs'].append(obj)
                
        if npc_index == 0:        
            shirt = obj.children["npc:Zshirtt1"]
            shirt.color = [.9,.1,.1,1]
        if npc_index == 1:        
            shirt = obj.children["npc:Zshirtt1"]
            shirt.color = [.1,.9,.1,1]
        if npc_index == 2:        
            shirt = obj.children["npc:Zshirtt1"]
            shirt.color = [.1,.1,.9,1]                        
        
        
        #rot = [ 0.0, 0.0, 1.570796327]
        x['npc_index'] = npc_index
        npc_index += 1
        own['npc_index'] = npc_index
        print('adding npc character: ', x.name, obj.name)
        dict['to_spawn'].remove(x)
        
        shirt = scene.objects["npc:Zshirtt1"]
        shirt.color = [255,0,0,1]
    
def main():  
    cont = bge.logic.getCurrentController()
    own = cont.owner
    scene = bge.logic.getCurrentScene()
    #actu = cont.actuators['start_level']
    dict = bge.logic.globalDict
    objList = scene.objects
    mainDir = bge.logic.expandPath("//")
    skater = 'ai_ed'
    
    fileName = mainDir + "npc_skaters/" + str(skater) + '.blend'    
    
    path = bge.logic.expandPath(fileName)
    try:
        bge.logic.LibLoad(path, 'Scene') 
    except:
        print('trying to load', fileName, 'twice')    
    

    # get object named MainCharacter
    #armature = objList["npc_ed_arm"]
    #deck = objList['npc_ed_deck']
    #trucks = objList['npc_ed_trucks']

    # combine child's shape with parent's
    compound = False

    # child is solid
    ghost = False
    
    # rotate the game object 1.570796327 radians (90 degrees)
    #rot = [ 0.0, 0.0, -1.570796327]
    #deck.applyRotation(rot,False)     

    # set parent
    #for x in [armature, deck, trucks]:
        #x.setParent( own, compound, ghost) 
        #rot = [ 0.0, 0.0, -1.570796327]
        #deck.applyRotation(rot,True)     
            

       
    #rot = [ 0.0, 0.0, -1.570796327]
    #deck.applyRotation(rot,True) 
    print('loading ai') 

main()
        