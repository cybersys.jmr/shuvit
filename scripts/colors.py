import bge
from bge import texture


def main():
    
    
    dict = bge.logic.globalDict 
    scene = bge.logic.getCurrentScene()
    shirt = scene.objects["Char4:Zshirtt1"]
    deck = scene.objects["b_deck"]
    throw_deck = scene.objectsInactive["throw_deck"]
    throw_deck_trucks = scene.objectsInactive["throw_deck_trucks"]
    focus_deckA = scene.objectsInactive["focus_deckA"]
    focus_deckB = scene.objectsInactive["focus_deckB"]
    focus_deckA_trucks = scene.objectsInactive["focus_deckA_trucks"]
    focus_deckB_trucks = scene.objectsInactive["focus_deckB_trucks"]
    shoeR = scene.objects['Char4:Shoes02.R']
    shoeL = scene.objects['Char4:Shoes02.L']
    trucks = scene.objects['b_trucks']
    wheel1 = scene.objects['wheel1']
    wheel2 = scene.objects['wheel2']
    wheel3 = scene.objects['wheel3']
    wheel4 = scene.objects['wheel4']
    throw_deck_wheel1 = scene.objectsInactive["throw_deck_trucks_wheels"]
    throw_deck_wheel2 = scene.objectsInactive["throw_deck_trucks_wheels.001"]
    throw_deck_wheel3 = scene.objectsInactive["throw_deck_trucks_wheels.002"]
    throw_deck_wheel4 = scene.objectsInactive["throw_deck_trucks_wheels.003"]
    focus_deckA_wheel1 = scene.objectsInactive['focus_deckA_wheel1']
    focus_deckA_wheel2 = scene.objectsInactive['focus_deckA_wheel2']
    focus_deckB_wheel1 = scene.objectsInactive['focus_deckB_wheel1']
    focus_deckB_wheel2 = scene.objectsInactive['focus_deckB_wheel2']


    ##
    white = [ .9, .9, .9, 1.0]
    yellow = [ 0.6, 0.33, 0, 1.0]
    grey = [ .5, .5, .5, 1.0]
    red = [ .2, 0, 0, 1]
    orange = [.47, .1, 0, 1]
    deckbrown = [.2, .075, .001, 1]
    lightblue = [.375, .6, 1, 1]
    ## set colors
    shirtC = white
    deckC = red
    shoesC = grey    

    cont = bge.logic.getCurrentController()
    own = cont.owner

    #shirt.color = shirtC
    #print(dict['shirt_color'])
    scol = [dict['shirt_color_r'], dict['shirt_color_g'], dict['shirt_color_b'], 1]
    deckC = [dict['deck_color_r'], dict['deck_color_g'], dict['deck_color_b'], 1]
    #print(scol)
    shirt.color = scol
    deck.color = deckC
    throw_deck.color = deckC
    focus_deckA.color = deckC
    focus_deckB.color = deckC
    shoesC = [dict['shoe_color_r'], dict['shoe_color_g'], dict['shoe_color_b'], 1]
    shoeR.color = shoesC
    shoeL.color = shoesC
    
    trucks.color = [dict['trucks_r'], dict['trucks_g'], dict['trucks_b'], 1]
    throw_deck_trucks.color = [dict['trucks_r'], dict['trucks_g'], dict['trucks_b'], 1]
    focus_deckA_trucks.color = [dict['trucks_r'], dict['trucks_g'], dict['trucks_b'], 1]
    focus_deckB_trucks.color = [dict['trucks_r'], dict['trucks_g'], dict['trucks_b'], 1]
    wheel1.color = [dict['wheel1_r'], dict['wheel1_g'], dict['wheel1_b'], 1]
    wheel2.color = [dict['wheel2_r'], dict['wheel2_g'], dict['wheel2_b'], 1]
    wheel3.color = [dict['wheel3_r'], dict['wheel3_g'], dict['wheel3_b'], 1]
    wheel4.color = [dict['wheel4_r'], dict['wheel4_g'], dict['wheel4_b'], 1]
    throw_deck_wheel1.color = [dict['wheel1_r'], dict['wheel1_g'], dict['wheel1_b'], 1]
    throw_deck_wheel2.color = [dict['wheel2_r'], dict['wheel2_g'], dict['wheel2_b'], 1]
    throw_deck_wheel3.color = [dict['wheel3_r'], dict['wheel3_g'], dict['wheel3_b'], 1]
    throw_deck_wheel4.color = [dict['wheel4_r'], dict['wheel4_g'], dict['wheel4_b'], 1]
    focus_deckA_wheel1.color = [dict['wheel1_r'], dict['wheel1_g'], dict['wheel1_b'], 1]
    focus_deckA_wheel2.color = [dict['wheel2_r'], dict['wheel2_g'], dict['wheel2_b'], 1]
    focus_deckB_wheel1.color = [dict['wheel3_r'], dict['wheel3_g'], dict['wheel3_b'], 1]
    focus_deckB_wheel2.color = [dict['wheel4_r'], dict['wheel4_g'], dict['wheel4_b'], 1]    
    
    #print("set color", deck.color)


    # logo = dict['shirt_logo']
    # logo1 = shirt.meshes[0].materials[0].textures[6]
    # logo2 = shirt.meshes[0].materials[0].textures[7] 
    # logo3 = shirt.meshes[0].materials[0].textures[5]   
    # try:
    #     if logo == 1:
    #         logo1.diffuseIntensity = 1
    #         logo2.diffuseIntensity = 0
    #         logo1.diffuseFactor = 1
    #         logo2.diffuseFactor = 0
    #         logo3.diffuseIntensity = 0
    #         logo3.diffuseFactor = 0
    #     if logo == 2:
    #         logo1.diffuseIntensity = 0
    #         logo1.diffuseFactor = 0
    #         logo2.diffuseIntensity = 1 
    #         logo2.diffuseFactor = 1   
    #         logo3.diffuseIntensity = 0
    #         logo3.diffuseFactor = 0        
    #     if logo == 3:
    #         logo1.diffuseIntensity = 0
    #         logo1.diffuseFactor = 0
    #         logo2.diffuseIntensity = 0 
    #         logo2.diffuseFactor = 0                   
    #         logo3.diffuseIntensity = 1
    #         logo3.diffuseFactor = 1        
    #     if logo == 0:
    #         logo1.diffuseIntensity = 0
    #         logo2.diffuseIntensity = 0            
    #         logo1.diffuseFactor = 0
    #         logo2.diffuseFactor = 0
    #         logo3.diffuseIntensity = 0
    #         logo3.diffuseFactor = 0        
    # except:
    #     pass    

main()


def update_truck_tex():
    #deck graphic testing
    #pass
    import glob
    import platform
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
    deck = scene.objects["b_deck"]
    mainDir = bge.logic.expandPath("//")
    fileName = mainDir + "textures\\decks\\*.png"  
    if platform.system() == 'Linux':
        fileName = mainDir + 'textures//decks//*.png'  
    deckList = glob.glob(fileName)
    dict['deckList'] = deckList  
    ID = texture.materialID(deck, 'MAdeck')
    deck_texture = texture.Texture(deck, ID)
    new_source = texture.ImageFFmpeg(deckList[dict["deck_index"]])
    bge.logic.texture = deck_texture
    bge.logic.texture.source = new_source
    bge.logic.texture.refresh(False)
    deck_name = deckList[dict['deck_index']]
    deck_name = deck_name.replace(mainDir, '')
    deck_name = deck_name.replace('textures\decks', '' )
    deck_name = deck_name.replace('.png', '')   
    deck_name = deck_name.replace("\\", "")  
    print('deck texture updted to ', deck_name)
    dict['deck_name'] = deck_name
    
def update_shirt_tex():
    #pass
    #deck graphic testing
    import glob
    import platform
    scene = bge.logic.getCurrentScene()
    # dict = bge.logic.globalDict
    # #deck = scene.objects["deck"]
    # shirt = scene.objects["Char4:Zshirtt1"]
    # mainDir = bge.logic.expandPath("//")
    # fileName2 = mainDir + "textures\\shirt\\*.png"  
    # if platform.system() == 'Linux':
    #     fileName2 = mainDir + 'textures//shirt//*.png'  
    # shirtList = glob.glob(fileName2)
    # dict['shirtList'] = shirtList  
    # #print(shirtList, 'shirtList')
    # ID = texture.materialID(shirt, 'MAshirt')
    # shirt_texture = texture.Texture(shirt, ID)
    # new_source2 = texture.ImageFFmpeg(shirtList[dict["shirt_logo"]])
    # bge.logic.texture2 = shirt_texture
    # bge.logic.texture2.source = new_source2
    # bge.logic.texture2.refresh(False)
    # shirt_name = shirtList[dict['shirt_logo']]
    # shirt_name = shirt_name.replace(mainDir, '')
    # shirt_name = shirt_name.replace('textures\shirt', '' )
    # shirt_name = shirt_name.replace('.png', '')   
    # shirt_name = shirt_name.replace("\\", "")  
    # print('shirt texture updted to ', shirt_name)
    # dict['shirt_name'] = shirt_name    


