import utils
import bge

State = type("State", (object,), {})
#====================================     
class State(object):
    def __init__(self, FSM):
        self.FSM = FSM
        self.timer = 0
        self.startTime = 0
    def Enter(self):
        self.timer = 0
        self.startTime = 0
    def Execute(self):
        print('Executing')
    def Exit(self):
        print('Exiting')
#==================================== 
            
class Example(State):
    def __init__(self,FSM):
        super(Example, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Example, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #o = self.FSM.owner
        #g = o.me['game']
        #self.FSM.ToTransition('toNewState')
        #print('npc fsm')
    
    def Exit(self):
        pass

#====================================  