#sort

from bge import logic

printDebug = True    #Set to True to see some basic debug info

cont = logic.getCurrentController()
own = cont.owner

gameName = "shuvit"    #Set this to the name of your game!

path = logic.expandPath(own["filePath"])

positionList = []
orientationList = []
scaleList = []
colorList = []
stateList = []
visibilityList = []
skaterAnimList = []
skaterAnimFList = []
deckAnimList = []
deckAnimFList = []
srollVolList = []
srollPitchList = []
sgrind_cementVolList = []
sgrind_cementPitchList = []
sgrind_railVolList = []
sgrind_railPitchList = []
spopsoundList = []
slandsoundList = []
sdeckvisList = []
positiontdList = []
orientationtdList = []
srevertsoundList = []

allDataList = []
allDataPath = path+gameName+" replay.dat"

class writeData:

    def addPosition(obj, pos, cut):
        global positionList
        positionList.append(" "+str(obj))
        positionList.append(pos)
        
        if len(positionList) >= cut and cut > 0:
            del positionList[0]
            del positionList[0]
        
    def addOrientation(obj, ori, cut):
        global orientationData
        orientationList.append(" "+str(obj))
        orientationList.append(ori)
        
        if len(orientationList) >= cut and cut > 0:
            del orientationList[0]
            del orientationList[0]
        
    def addScale(obj, sca, cut):
        global scaleData
        scaleList.append(" "+str(obj))
        scaleList.append(sca)
        
        if len(scaleList) >= cut and cut > 0:
            del scaleList[0]
            del scaleList[0]
        
    def addColor(obj, col, cut):
        global colorData
        colorList.append(" "+str(obj))
        colorList.append(col)
        
        if len(colorList) >= cut and cut > 0:
            del colorList[0]
            del colorList[0]
        
    def addState(obj, sta, cut):
        global stateData
        stateList.append(" "+str(obj))
        stateList.append(sta)
        
        if len(stateList) >= cut and cut > 0:
            del stateList[0]
            del stateList[0]
            
    def addVisibility(obj, vis, cut):
        global visibilityData
        visibilityList.append(" "+str(obj))
        visibilityList.append(vis)
        if len(visibilityList) >= cut and cut > 0:
            del visibilityList[0]
            del visibilityList[0]
    def addSkaterAnim(obj, anim, cut):
        global skaterAnimData
        skaterAnimList.append(" "+str(obj))
        skaterAnimList.append(anim)
        
        if len(skaterAnimList) >= cut and cut > 0:
            del skaterAnimList[0]
            del skaterAnimList[0] 
    def addSkaterAnimF(obj, animf, cut):
        global skaterAnimFData
        skaterAnimFList.append(" "+str(obj))
        skaterAnimFList.append(animf)
        
        if len(skaterAnimFList) >= cut and cut > 0:
            del skaterAnimFList[0]
            del skaterAnimFList[0]  
    def addDeckAnim(obj, anim, cut):
        global deckAnimData
        deckAnimList.append(" "+str(obj))
        deckAnimList.append(anim)
        
        if len(deckAnimList) >= cut and cut > 0:
            del deckAnimList[0]
            del deckAnimList[0] 
    def addDeckAnimF(obj, animf, cut):
        global deckAnimFData
        deckAnimFList.append(" "+str(obj))
        deckAnimFList.append(animf)
        
        if len(deckAnimFList) >= cut and cut > 0:
            del deckAnimFList[0]
            del deckAnimFList[0]
###             
    def addSrollVol(obj, srollVol, cut):
        global srollVolData
        srollVolList.append(" "+str(obj))
        srollVolList.append(srollVol)
        if len(srollVolList) >= cut and cut > 0:
            del srollVolList[0]
            del srollVolList[0]
    def addSrollPitch(obj, srollPitch, cut):
        global srollPitchData
        srollPitchList.append(" "+str(obj))
        srollPitchList.append(srollPitch)
        if len(srollPitchList) >= cut and cut > 0:
            del srollPitchList[0]
            del srollPitchList[0] 
###             
    def addSgrind_cementVol(obj, sgrind_cementVol, cut):
        global sgrind_cementVolData
        sgrind_cementVolList.append(" "+str(obj))
        sgrind_cementVolList.append(sgrind_cementVol)
        if len(sgrind_cementVolList) >= cut and cut > 0:
            del sgrind_cementVolList[0]
            del sgrind_cementVolList[0]
    def addSgrind_cementPitch(obj, sgrind_cementPitch, cut):
        global sgrind_cementPitchData
        sgrind_cementPitchList.append(" "+str(obj))
        sgrind_cementPitchList.append(sgrind_cementPitch)
        if len(sgrind_cementPitchList) >= cut and cut > 0:
            del sgrind_cementPitchList[0]
            del sgrind_cementPitchList[0] 
###             
    def addSgrind_railVol(obj, sgrind_railVol, cut):
        global sgrind_railVolData
        sgrind_railVolList.append(" "+str(obj))
        sgrind_railVolList.append(sgrind_railVol)
        if len(sgrind_railVolList) >= cut and cut > 0:
            del sgrind_railVolList[0]
            del sgrind_railVolList[0]
    def addSgrind_railPitch(obj, sgrind_railPitch, cut):
        global sgrind_railPitchData
        sgrind_railPitchList.append(" "+str(obj))
        sgrind_railPitchList.append(sgrind_railPitch)
        if len(sgrind_railPitchList) >= cut and cut > 0:
            del sgrind_railPitchList[0]
            del sgrind_railPitchList[0] 
##
    def addSpopsound(obj, spopsound, cut):
        global spopsoundData
        spopsoundList.append(" "+str(obj))
        spopsoundList.append(spopsound)
        if len(spopsoundList) >= cut and cut > 0:
            del spopsoundList[0]
            del spopsoundList[0] 
##
    def addSlandsound(obj, slandsound, cut):
        global slandsoundData
        slandsoundList.append(" "+str(obj))
        slandsoundList.append(slandsound)
        if len(slandsoundList) >= cut and cut > 0:
            del slandsoundList[0]
            del slandsoundList[0] 
    
    def addSdeckvis(obj, sdeckvis, cut):
        global sdeckvisData
        sdeckvisList.append(" "+str(obj))
        sdeckvisList.append(sdeckvis)
        if len(sdeckvisList) >= cut and cut > 0:
            del sdeckvisList[0]
            del sdeckvisList[0]     
            
    def addtdPosition(obj, pos, cut):
        global positiontdList
        positiontdList.append(" "+str(obj))
        positiontdList.append(pos)
        
        if len(positiontdList) >= cut and cut > 0:
            del positiontdList[0]
            del positiontdList[0]
        
    def addtdOrientation(obj, ori, cut):
        global orientationtdData
        orientationtdList.append(" "+str(obj))
        orientationtdList.append(ori)
        
        if len(orientationtdList) >= cut and cut > 0:
            del orientationtdList[0]
            del orientationtdList[0]  
##
    def addSrevertsound(obj, srevertsound, cut):
        global srevertsoundData
        srevertsoundList.append(" "+str(obj))
        srevertsoundList.append(srevertsound)
        if len(srevertsoundList) >= cut and cut > 0:
            del srevertsoundList[0]
            del srevertsoundList[0]                       
                                                                                              
            
    def saveAll():

        allDataList = ["game="+gameName+"\n",
            " ".join(positionList),
            " ".join(orientationList),
            " ".join(scaleList),
            " ".join(colorList),
            " ".join(stateList),
            " ".join(visibilityList),
            " ".join(skaterAnimList),
            " ".join(skaterAnimFList),
            " ".join(deckAnimList),
            " ".join(deckAnimFList),
            " ".join(srollVolList),
            " ".join(srollPitchList), 
            " ".join(sgrind_cementVolList),
            " ".join(sgrind_cementPitchList),
            " ".join(sgrind_railVolList),
            " ".join(sgrind_railPitchList),
            " ".join(spopsoundList),
            " ".join(slandsoundList),
            " ".join(sdeckvisList),
            " ".join(positiontdList),
            " ".join(orientationtdList),
            " ".join(spopsoundList)]    
        try:
            with open(allDataPath,'w') as all:
                all.write("|".join(allDataList))
                all.close()
                if printDebug:
                    print("State recording saved successfuly to "+allDataPath)
        except IOError:
            if printDebug:
                print(allDataPath+"cannot be opened for writing")

class readData:
                                   
    def loadAll():
        
        global allDataList
        global positionList
        global orientationList
        global scaleList
        global colorList
        global stateList
        global visibilityList
        global skaterAnimList
        global skaterAnimFList
        global deckAnimList
        global deckAnimFList 
        global srollVolList
        global srollPitchList 
        global sgrind_cementVolList
        global sgrind_cementPitchList 
        global sgrind_railVolList
        global sgrind_railPitchList
        global spopsoundList
        global slandsoundList 
        global sdeckvisList     
        global positiontdList
        global orientationtdList 
        global srevertsoundList                                          
            
        if not allDataList:
            try:
                with open(allDataPath) as all:
                    data = all.read()
                    all.close()

                    allDataList = data.split("|",6)

                    loadedGameName = str(allDataList[0].split("=",1)[1]).strip()
                    try:
                        if loadedGameName == gameName:

                            positionList = allDataList[1].split()
                            print('1')
                            orientationList = allDataList[2].split()
                            print('2')
                            scaleList = allDataList[3].split()
                            print('3')
                            colorList = allDataList[4].split()
                            print('4')
                            stateList = allDataList[5].split()
                            print('5')
                            visibilityList = allDataList[6].split()
                            print('6')
                            skaterAnimList = allDataList[7].split()
                            print('7')
                            skaterAnimFList = allDataList[8].split()
                            print('8')
                            deckAnimList = allDataList[9].split()
                            print('9')
                            deckAnimFList = allDataList[10].split()
                            print('1')
                            srollVolList = allDataList[11].split()
                            print('1')
                            srollPitchList = allDataList[12].split()
                            print('1')
                            sgrind_cementVolList = allDataList[13].split()
                            print('1')
                            sgrind_cementPitchList = allDataList[14].split()
                            print('1')
                            sgrind_railVolList = allDataList[15].split() 
                            print('1')
                            sgrind_railPitchList = allDataList[16].split()
                            print('1')
                            spopsoundList = allDataList[17].split()
                            print('1')
                            slandsoundList = allDataList[18].split()
                            print('1')
                            sdeckvisList = allDataList[19].split()
                            print('1')
                            positiontdList = allDataList[20].split()
                            print('1')
                            orientationtdList = allDataList[21].split()  
                            print('1')                          
                            srevertsoundList = allDataList[22].split()
                            print('1')
                            
                            if printDebug:
                                print("Read file "+allDataPath)
                    except Exception as e:
                        print("error reading replay data", e)        
            #except IOError:
            except Exception as e:   
                if printDebug:
                    print("Error reading "+allDataPath, e)

    def returnPosition(objIndex, valueIndex):
        
        if positionList:
            name = positionList[objIndex-2]   #That -2 is a little workaround to a list index out of range error.
            position = positionList[valueIndex-2]
            name = name.replace(" ","")
            position = str(position)
            position = position.replace(","," ")
            pX = float(position.split()[0])
            pY = float(position.split()[1])
            pZ = float(position.split()[2])
            position = [pX,pY,pZ]
            return(name, position)

    def returnOrientation(objIndex, valueIndex):
        if orientationList:
            name = orientationList[objIndex-2]
            orientation = orientationList[valueIndex-2]
            name = name.replace(" ","")
            orientation = str(orientation)
            orientation = orientation.replace(","," ")
            orientation = orientation.split()
            return(name, orientation)

    def returnScale(objIndex, valueIndex):

        if scaleList:
            name = scaleList[objIndex-2]
            scale = scaleList[valueIndex-2]
            
            name = name.replace(" ","")
            
            scale = str(scale)
            scale = scale.replace(","," ")
            
            sX = float(scale.split()[0])
            sY = float(scale.split()[1])
            sZ = float(scale.split()[2])
            
            scale = [sX,sY,sZ]

            return(name, scale)

    def returnColor(objIndex, valueIndex):

        if colorList:
            name = colorList[objIndex-2]
            color = colorList[valueIndex-2]
            
            name = name.replace(" ","")
            
            color = str(color)
            color = color.replace(","," ")
            
            cR = float(color.split()[0])
            cG = float(color.split()[1])
            cB = float(color.split()[2])
            cA = float(color.split()[3])
            
            color = [cR,cG,cB,cA]

            return(name, color)
    
    def returnState(objIndex, valueIndex):
        
        if stateList:
            name = stateList[objIndex-2]
            state = stateList[valueIndex-2]
            
            name = name.replace(" ","")
            
            state = str(state)
            state = state.replace(","," ")
            state = int(state)
            
            return(name, state)
    
    def returnVisibility(objIndex, valueIndex):
        
        if visibilityList:
            name = visibilityList[objIndex-2]
            visible = visibilityList[valueIndex-2]
            
            name = name.replace(" ","")
            
            visible = str(visible)
            visible = visible.replace(","," ")
            
            return(name, visible)
        
    def returnSkaterAnim(objIndex, valueIndex):       
        if skaterAnimList:
            name = skaterAnimList[objIndex-2]
            skaterAnim = skaterAnimList[valueIndex-2]
            name = name.replace(" ","")           
            skaterAnim = str(skaterAnim)
            skaterAnim = skaterAnim.replace(","," ")           
            return(name, skaterAnim) 
        
    def returnSkaterAnimF(objIndex, valueIndex):       
        if skaterAnimFList:
            name = skaterAnimFList[objIndex-2]
            skaterAnimF = skaterAnimFList[valueIndex-2]
            name = name.replace(" ","")
            skaterAnimF = str(skaterAnimF)
            skaterAnimF = skaterAnimF.replace(","," ")
            return(name, skaterAnimF)  
        
    def returnDeckAnim(objIndex, valueIndex):    
        if deckAnimList:
            name = deckAnimList[objIndex-2]
            deckAnim = deckAnimList[valueIndex-2]
            name = name.replace(" ","")
            deckAnim = str(deckAnim)
            deckAnim = deckAnim.replace(","," ")
            return(name, deckAnim) 
        
    def returnDeckAnimF(objIndex, valueIndex):
        if deckAnimFList:
            name = deckAnimFList[objIndex-2]
            deckAnimF = deckAnimFList[valueIndex-2]
            name = name.replace(" ","")
            deckAnimF = str(deckAnimF)
            deckAnimF = deckAnimF.replace(","," ")            
            return(name, deckAnimF)
####
    def returnSrollVol(objIndex, valueIndex):
        if srollVolList:
            name = srollVolList[objIndex-2]
            srollVol = srollVolList[valueIndex-2]
            name = name.replace(" ","")
            srollVol = str(srollVol)
            srollVol = srollVol.replace(","," ")
            srollVol = float(srollVol)
            return(name, srollVol)
        
    def returnSrollPitch(objIndex, valueIndex):
        if srollPitchList:
            name = srollPitchList[objIndex-2]
            srollPitch = srollPitchList[valueIndex-2]
            name = name.replace(" ","")
            srollPitch = str(srollPitch)
            srollPitch = srollPitch.replace(","," ")
            srollPitch = float(srollPitch)
            return(name, srollPitch)                             ####
####
    def returnSgrind_cementVol(objIndex, valueIndex):
        if sgrind_cementVolList:
            name = sgrind_cementVolList[objIndex-2]
            sgrind_cementVol = sgrind_cementVolList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_cementVol = str(sgrind_cementVol)
            sgrind_cementVol = sgrind_cementVol.replace(","," ")
            sgrind_cementVol = float(sgrind_cementVol)
            return(name, sgrind_cementVol)
        
    def returnSgrind_cementPitch(objIndex, valueIndex):
        if sgrind_cementPitchList:
            name = sgrind_cementPitchList[objIndex-2]
            sgrind_cementPitch = sgrind_cementPitchList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_cementPitch = str(sgrind_cementPitch)
            sgrind_cementPitch = sgrind_cementPitch.replace(","," ")
            sgrind_cementPitch = float(sgrind_cementPitch)
            return(name, sgrind_cementPitch)                             ####              
####
    def returnSgrind_railVol(objIndex, valueIndex):
        if sgrind_railVolList:
            name = sgrind_railVolList[objIndex-2]
            sgrind_railVol = sgrind_railVolList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_railVol = str(sgrind_railVol)
            sgrind_railVol = sgrind_railVol.replace(","," ")
            sgrind_railVol = float(sgrind_railVol)
            return(name, sgrind_railVol)
        
    def returnSgrind_railPitch(objIndex, valueIndex):
        if sgrind_railPitchList:
            name = sgrind_railPitchList[objIndex-2]
            sgrind_railPitch = sgrind_railPitchList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_railPitch = str(sgrind_railPitch)
            sgrind_railPitch = sgrind_railPitch.replace(","," ")
            sgrind_railPitch = float(sgrind_railPitch)
            return(name, sgrind_railPitch)                             ####
    def returnSpopsound(objIndex, valueIndex):
        if spopsoundList:
            name = spopsoundList[objIndex-2]
            spopsound = spopsoundList[valueIndex-2]
            name = name.replace(" ","")
            spopsound = str(spopsound)
            spopsound = spopsound.replace(","," ")
            spopsound = int(spopsound)
            return(name, spopsound)
    def returnSlandsound(objIndex, valueIndex):
        if slandsoundList:
            name = slandsoundList[objIndex-2]
            slandsound = slandsoundList[valueIndex-2]
            name = name.replace(" ","")
            slandsound = str(slandsound)
            slandsound = slandsound.replace(","," ")
            slandsound = int(slandsound)
            return(name, slandsound) 
    def returnSdeckvis(objIndex, valueIndex):
        if sdeckvisList:
            name = sdeckvisList[objIndex-2]
            sdeckvis = sdeckvisList[valueIndex-2]
            name = name.replace(" ","")
            sdeckvis = str(sdeckvis)
            sdeckvis = sdeckvis.replace(","," ")
            sdeckvis = int(sdeckvis)
            return(name, sdeckvis) 
        
    def returntdPosition(objIndex, valueIndex):
        
        if positiontdList:
            name = positiontdList[objIndex-2]   #That -2 is a little workaround to a list index out of range error.
            position = positiontdList[valueIndex-2]
            name = name.replace(" ","")
            position = str(position)
            position = position.replace(","," ")
            pX = float(position.split()[0])
            pY = float(position.split()[1])
            pZ = float(position.split()[2])
            position = [pX,pY,pZ]
            return(name, position)

    def returntdOrientation(objIndex, valueIndex):
        if orientationtdList:
            name = orientationtdList[objIndex-2]
            orientation = orientationtdList[valueIndex-2]
            name = name.replace(" ","")
            orientation = str(orientation)
            orientation = orientation.replace(","," ")
            orientation = orientation.split()
            return(name, orientation)     
        
    def returnSrevertsound(objIndex, valueIndex):
        if srevertsoundList:
            name = srevertsoundList[objIndex-2]
            srevertsound = srevertsoundList[valueIndex-2]
            name = name.replace(" ","")
            srevertsound = str(srevertsound)
            srevertsound = srevertsound.replace(","," ")
            srevertsound = int(srevertsound)
            return(name, srevertsound)           
                       
####                

    def getLengthPos():
        if positionList:
            return(len(positionList))

    def getLengthOri():
        if orientationList:
            return(len(orientationList))

    def getLengthSca():
        if scaleList:
            return(len(scaleList))

    def getLengthCol():
        if colorList:
            return(len(colorList))
    
    def getLengthSta():
        if stateList:
            return(len(stateList))
    
    def getLengthVis():
        if visibilityList:
            return(len(visibilityList))
    def getLengthSkaterAnim():
        if skaterAnimList:
            return(len(skaterAnimList)) 
    def getLengthSkaterAnimF():
        if skaterAnimFList:
            return(len(skaterAnimFList)) 
    def getLengthDeckAnim():
        if deckAnimList:
            return(len(deckAnimList)) 
    def getLengthDeckAnimF():
        if deckAnimFList:
            return(len(deckAnimFList))
    def getLengthSrollVol():
        if srollVolList:
            return(len(srollVolList)) 
    def getLengthSrollPitch():
        if srollPitchList:
            return(len(srollPitchList)) 
    def getLengthSgrind_cementVol():
        if sgrind_cementVolList:
            return(len(sgrind_cementVolList)) 
    def getLengthSgrind_cementPitch():
        if sgrind_cementPitchList:
            return(len(sgrind_cementPitchList))
    def getLengthSgrind_railVol():
        if sgrind_railVolList:
            return(len(sgrind_railVolList)) 
    def getLengthSgrind_railPitch():
        if sgrind_railPitchList:
            return(len(sgrind_railPitchList))
    def getLengthSpopsound():
        if spopsoundList:
            return(len(spopsoundList)) 
    def getLengthSlandsound():
        if slandsoundList:
            return(len(slandsoundList)) 
    def getLengthSdeckvis():
        if sdeckvisList:
            return(len(sdeckvisList))   
    def getLengthtdPos():
        if positiontdList:
            return(len(positiontdList))
    def getLengthtdOri():
        if orientationtdList:
            return(len(orientationtdList))
    def getLengthSrevertsound():
        if srevertsoundList:
            return(len(srevertsoundList))    
             
                                                                                                    