import bge
#import Settings

def main():

    cont = bge.logic.getCurrentController()
    own = cont.owner
    dict = bge.logic.globalDict
    scene = bge.logic.getCurrentScene()
    black = scene.objects['black']
    shuvit_logo = scene.objects['shuvit_logo']
    scenes = bge.logic.getSceneList()
    
    if dict['overlay_fadein'] == 1:
        for i in scenes:
            if i.name == "fade_in":
                scene2 = i
                scene2.end()         
        cont.activate(own.actuators['add_fade'])  
        
    if dict['overlay_fadeout'] == 1:
        dict['overlay_fadeout'] = 0    
        black.playAction('black_fade', 100,200, layer=2, play_mode=0) 
        shuvit_logo.playAction('logo_fade', 100,200, layer=0, play_mode=0)  
        
    if dict['load_timer'] > 0:
        dict['load_timer'] -= 1
    if dict['load_timer'] == 3:
        scenes = bge.logic.getSceneList()

        dict['load_timer'] == 0
        cont.activate(own.actuators['reload_game'])
    
#main()
