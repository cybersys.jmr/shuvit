import bge

def main(cont):
    own = cont.owner
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
    
    mainDir = bge.logic.expandPath("//")
    fileName = mainDir + 'assets/maps/' + dict['map'] + '/map.blend'   
    path = bge.logic.expandPath(fileName)
    bge.logic.LibLoad(path, 'Scene', load_actions=True) 
    