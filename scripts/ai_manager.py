def printplaying(skater,deck):
    splaying_layers = "S: "
    playing_layers = "D: "
    tplaying_layers = "T: "
    for x in range(9999):
        if skater.isPlayingAction(x):
        #if trucks.isPlayingAction(x):
        #if skater.isPlayingAction(x):                        
            splaying_layers += str(x)
            splaying_layers += " "        
        if deck.isPlayingAction(x):
        #if trucks.isPlayingAction(x):
        #if skater.isPlayingAction(x):                        
            playing_layers += str(x)
            playing_layers += " "
          
    print(splaying_layers, playing_layers)

def main():

    frame_rate = 60    #Set this to the framerate of your game.
    optimization = 1    #An integer, must be at least 1! Higher value will make a smaller saved file size. I do not reccomend setting it to anything other than 1 though.
    #recording_cutoff = 6974#7300 #3600   #Frames at which the oldest recorded frame will be deleted. 0 is no cutoff.
    
    recorder_state = 1   #This is the main state of the recorder object. State will be reverted to this value when the user breaks out of the playback.
    allow_playback_break = True   #Allow the player to resume playing from the position in the recording.

    from bge import logic
    import aiRecord
    import aiSortData
    import npcChangeAnim
    from mathutils import Vector
    import math
    
    debugg = True

    cont = logic.getCurrentController()
    own = cont.owner
    scene = logic.getCurrentScene()
    dict = logic.globalDict
    own['valueIndex'] = own['valueIndex'] + 2
    #dict['recorder_on'] = recorder_on
    npc_index = own['npc_index']
    skater = dict['spawned_npcs'][npc_index]
    recorder_on = dict.get('recorder_on')
    own['recorder_on'] = recorder_on
    #print("recorder on: ", recorder_on) 
    own["playback"] = True
    recording_cutoff = own['replay_length']
    idle_timer = own['idle_timer']
    npc_playback = own['npc_playback']
    
    
    
    if 'data_loaded' not in own:
        #own['data_loaded'] = 1
        aiRecord.loadData(cont, own)
        if debugg: print('ai_loadData')
        
    if npc_playback == True:
        cube = own
        aiRecord.loadData(cont, own)   
        #own['move'] = False          
        #....................

        valueIndex = own['valueIndex']
        n = (valueIndex / recording_cutoff) * 1000
        n = int(round(n))
        #print(own['valueIndex'])
        if own['objIndex'] < (recording_cutoff-3):
            own['objIndex'] = own['objIndex'] + 2
        else:
            own['objIndex'] = 4   
            print('replay end')    
            npcChangeAnim.main(cont)
            #own['npc_replay_name'] = '30secA.dat'
            own['inited'] = False
            own['rotate_in'] = True
            own['move_to'] = True
            own['rotate_out'] = True 
            aiRecord.loadData(cont, own)
    else:
        if idle_timer < 5:
            idle_timer += 1
            #print('idling', idle_timer)
        else:
            idle_timer = 0
            own['npc_rotate'] = True                 
            
    if own['move'] == True:
        speed = .15
        target = own['rpStartLoc']
        my_vector = Vector([target[0], target[1], target[2]])
        length = (own.worldPosition - my_vector).length
        obj = own['target_object_name']
        vect = own.getVectTo(obj)   
        if length < .1:
            own['move'] = False
            obj = own['target_object_name']
    own['idle_timer'] = idle_timer   
    
    try:
        obj = own['target_object_name']
        #print(obj.worldPosition)
    except:
        pass    
    
    
    #11111111
    if own['rotate_in'] == False and own['move_to'] == False and own['rotate_out'] == False:
        own['npc_playback'] = True
        #obj = own['target_object_name']
        #obj.endObject()
        #print('ending target')    
    if own['rotate_in'] == True:
        tActu = cont.actuators['npcTracking']
        tActu.object = own['target_object_name']
        try:
            own['rotate_in_timer'] = own['rotate_in_timer'] + 1
        except:
            own['rotate_in_timer'] = 0
        if own['rotate_in_timer'] < 120:    
            cont.activate(tActu)
            skater.playAction('reg_idle1', 0,120, layer=2, play_mode=1, speed=.5)  
        else:
            cont.deactivate(tActu) 
            own['rotate_in'] = False
            own['rotate_in_timer'] = 0
            skater.stopAction(2)

    elif own['move_to'] == True:
        target = own['rpStartLoc']
        #print(target, 'target', own.worldPosition, '++++++')
        my_vector = Vector([target[0], target[1], target[2]])
        length = (own.worldPosition - my_vector).length
        sActu = cont.actuators['npcSteering']
        sActu.target = own['target_object_name']
        #length = round(length, 7)
        if length > .1:
            #print(length, 'length < .1')
            #pass
            cont.activate(sActu)
            skater.playAction('reg_nwalk', 0,35, layer=2, play_mode=1, speed=.5)
        else:
            cont.deactivate(sActu)
            own['move_to'] = False
            print(length, 'length met')
            skater.stopAction(2)

    elif own['rotate_out'] == True:    
        #own['rpStartZ'] = 180
        #print(own['rpStartZ'], 'start')
        xyz = own.localOrientation.to_euler()
        #print(xyz[2], 'non euler')
        rotz = math.degrees(xyz[2])
        xyz[2] = xyz[2] + 1.57
        rotz_b = rotz + 180
        if rotz_b > 360:
            rotz_b -= 360
        srotz_b = own['rpStartZ'] + 180
        if srotz_b > 360:
            srotz_b -= 360
        #print (rotz_b, srotz_b, '---rotations')        
        #print (rotz, own['rpStartZ'], '---rotations')
        num = rotz - (own['rpStartZ'])
        amt = .03
        amt2 = 1
        
        
        local = own.worldOrientation.inverted() * ( own['target_object_name'].worldPosition - own.worldPosition) 

        if local.x >0:
            #target is in front
            #print('in front')
            pass
        else:
            #target is behind
            #print('behind')
            pass
        if local.y > 0:
                 #object on right
            #print('on right')
            rot = 'right'
        else:
                 #object is on left 
            #print('on left')
            rot = 'left'       
        
        
        
        skater.playAction('reg_idle1', 0,120, layer=2, play_mode=1, speed=.5)
        if abs(num) < amt2:
            own['npc_playback'] = True
            own['npc_rotate'] = False
            print('----starting replay', num)
            own['rotate_out'] = False
            obj = own['target_object_name']
            obj.endObject()
            print('ending target') 
            skater.stopAction(2)            
        #elif rotz > own['rpStartZ']:
        elif rot == 'right':    
            own.applyRotation([0,0,amt], True)
            own.worldPosition = own['rpStartLoc']
        #elif rotz < own['rpStartZ']:
        elif rot == 'left':    
            own.applyRotation([0,0,-amt], True) 
            own.worldPosition = own['rpStartLoc']                
    #print(own['rotate_in'], own['move_to'], own['rotate_out'])                          
main()