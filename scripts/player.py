import bge
import FSM
from mathutils import Vector
import math
import mathutils
import random

dict = bge.logic.globalDict

class Player:
	def __init__(self, cont):
		self.cont = cont
		self.obj = cont.owner
		self.FSM = FSM.PlayerFSM(self)
		self.walking = True
		self.last_pos = self.obj.worldPosition.copy()
		self.throw_timer = 0
		self.throw_deck = None
		self.throw_deck_empty = self.obj.childrenRecursive["throw_deck_empty"]
		self.step = True
		self.dropin_obj = None
		self.joystick_active_frame = 0
		self.joystick_idle = False
		self.idle_cam_dir = True
		#self.throw_deck_obj = dict['level_scene'].objects['throw_deck']
		dict['walking'] = True
		#print(self.obj.childrenRecursive)
		#self.arm = self.obj.childrenRecursive['Char4']
		self.arm = None

	def update(self):
		self.FSM.Execute()
		self.last_pos = self.obj.worldPosition.copy()

	def get_ground_ray(self):
		v = self.obj.worldOrientation.col[0]
		tv = v.normalized()
		axis = 2
		distance = -50
		start = self.obj.worldPosition.copy()
		back_start = start + 0.15 * tv
		start.z += .1
		end = Vector.copy(self.obj.worldPosition + (self.obj.worldOrientation.col[axis] * distance))
		ray = self.obj.rayCast(end, back_start, 0, 'ground', 1, 0, 0, 53247)
		v = self.obj.linearVelocity.copy()
		v = self.obj.worldOrientation.col[0]
		tv = v.normalized()
		front_start = start - 0.30 * tv
		start.z += .1
		front_end = Vector(front_start + (self.obj.worldOrientation.col[axis] * distance))
		frontray = self.obj.rayCast(front_end, front_start, 0, 'ground', 1, 0, 0, 53247)
		#if ray[0] == None and frontray[0] == None:
		cray = self.obj.rayCast(end, start, 0, 'ground', 1, 0, 0, 53247)

		#bge.render.drawLine(front_start, front_end, [1,0,0])
		#bge.render.drawLine(back_start, end, [0,1,0])
		#bge.render.drawLine(start, end, [0,0,1])
		return [ray, frontray, cray]



	def get_dropin_rays(self):
		v = self.obj.worldOrientation.col[0]
		tv = v.normalized()
		axis = 2
		distance = -50
		start = self.obj.worldPosition.copy()
		back_start = start + 0.1 * tv
		start.z += .1
		end = Vector.copy(self.obj.worldPosition + (self.obj.worldOrientation.col[axis] * distance))
		ray = self.obj.rayCast(end, back_start, 0, 'ground', 1, 0, 0, 53247)
		
		v = self.obj.linearVelocity.copy()
		v = self.obj.worldOrientation.col[0]
		tv = v.normalized()
		front_start = start - 0.1 * tv
		start.z += .1
		front_end = Vector(front_start + (self.obj.worldOrientation.col[axis] * distance))
		frontray = self.obj.rayCast(front_end, front_start, 0, 'ground', 1, 0, 0, 53247)
		cray = self.obj.rayCast(end, start, 0, 'ground', 1, 0, 0, 53247)
		return [ray, frontray, cray]		


	def get_hang_ray(self):

		v = self.obj.worldOrientation.col[0]
		tv = v.normalized()

		axis = 2
		distance = -5
		start = self.obj.worldPosition.copy()
		start.z += 1.8
		mstart = start - 0.40 * tv
		
		end = Vector(mstart + (self.obj.worldOrientation.col[axis] * distance))
		ray = self.obj.rayCast(end, mstart, 0, 'ground', 1, 1, 0, 53247)
		#ray = self.obj.rayCast(end, mstart, 0, '', 1, 1, 0, 53247)
		#bge.render.drawLine(mstart, end, [0,0,1])

		return ray

	def get_hang_align_ray(self):

		v = self.obj.worldOrientation.col[0]
		tv = v.normalized()

		axis = 0
		distance = -2
		start = self.obj.worldPosition.copy()
		start.z += 1
		#mstart = start - 0.40 * tv
		#mstart = start * tv
		mstart = start 

		end = Vector(mstart + (self.obj.worldOrientation.col[axis] * distance))
		#ray = self.obj.rayCast(end, mstart, 0, 'ground', 1, 0, 0, 53247)
		ray = self.obj.rayCast(end, mstart, 0, '', 1, 0, 0, 53247)
		#if ray[0] != None:
			#print(ray[0], 'player funct')
		#bge.render.drawLine(mstart, end, [0,1,0])
		
		return ray



	def get_ground_dist(self, groundray):
		gr = groundray[0]
		fgr = groundray[1]
		cgr = groundray[2]
		d = None
		
		if gr[0] != None:
			p_z = self.obj.worldPosition.z
			g_z = gr[1].z
			distance_to_ground = p_z - g_z
			p2 = None
			if fgr[0] != None:
				p2 = p_z - fgr[1].z 

				if p2 < distance_to_ground:
					distance_to_ground = p2
			
			if cgr[0] != None:
				p2 = p_z - cgr[1].z 

				if p2 < distance_to_ground:
					distance_to_ground = p2

			return distance_to_ground



	def get_hang_dist(self, groundray):
		if groundray[0] != None:
			p_z = self.obj.worldPosition.z
			g_z = groundray[1].z
			distance_to_ground = p_z - g_z
			return distance_to_ground


	def set_walk_z(self, dist):
		if dist < .6:
			target_dist = .3
			self.obj.worldPosition.z += (target_dist - dist)
			self.obj.linearVelocity.z = 0

	def align_walk_z(self):
		self.obj.alignAxisToVect([0,0,1], 2, .08)

	def walk_movement(self):
		
		moving = False
		o = self.obj
		

		o.linearVelocity.y *= .9
		o.linearVelocity.x *= .95

		if dict['kb_w'] == 2 or dict['lUD'] < -.05:
			#walk_blend(self)

			#run
			if (dict['kb_lsh'] == 2 or dict['aBut'] == 1) and o.linearVelocity.x > -dict['max_run_vel']:
				#self.run_speed = 1.5
				#o.applyForce([-dict['run_force'], 0, 0], True)
				o.linearVelocity.x -= .2

			#walk
			elif o.linearVelocity.x > -dict['max_walk_vel']:
				#o.applyForce([-dict['walk_force'], 0, 0], True)
				o.linearVelocity.x -= .1


		if dict['kb_s'] == 2 or dict['lUD'] > .06:
			#o.applyForce([10, 0, 0], True)
			if o.linearVelocity.x < (dict['max_walk_vel'] / 2):
				o.linearVelocity.x += .05
			#print('w')

		if dict['kb_a'] == 2 or dict['lLR'] < -.03:
			o.applyRotation([0, 0, dict['walk_turn_amt']], True)
			#print('w')
	
		if dict['kb_d'] == 2 or dict['lLR'] > .03:
			o.applyRotation([0, 0, -dict['walk_turn_amt']], True)



		#print('linvel', o.linearVelocity)

		return moving

	def move_walk_cam(self):
		amt = -.05
		amt2 = -.07
		if dict['rLR'] > .04:
			dict['camera'].applyMovement([amt, 0, 0], True)
		elif dict['rLR'] < -.04:
			dict['camera'].applyMovement([-amt, 0, 0], True)	

		if dict['rUD'] > .04:
			dict['camera'].applyMovement([0, -amt2, 0], True)
		elif dict['rUD'] < -.04:
			dict['camera'].applyMovement([0, amt2, 0], True)	

	def check_reset_point(self):
		if dict['ddPad'] == 1:
			spawn_pos = self.obj['spawn_pos']
			spawn_rot = self.obj['spawn_rot']
			spawn_cam_pos = self.obj['spawn_cam_pos']
			spawn_cam_rot = self.obj['spawn_cam_rot']
			try:
				self.obj.worldPosition = (spawn_pos[0], spawn_pos[1], (spawn_pos[2] + .1))
				self.obj.worldOrientation = [[spawn_rot[0][0], spawn_rot[0][1],spawn_rot[0][2]], [spawn_rot[1][0],spawn_rot[1][1], spawn_rot[1][2]], [0.0, 0.0, 1.0]]
				dict['camera'].worldPosition = (spawn_cam_pos[0], spawn_cam_pos[1], (spawn_cam_pos[2]))
				dict['camera'].worldOrientation = [[spawn_cam_rot[0][0], spawn_cam_rot[0][1], spawn_cam_rot[0][2]], [spawn_cam_rot[1][0], spawn_cam_rot[1][1], spawn_cam_rot[1][2]], [0.0, 0.0, 1.0]]
			except:
				self.obj.worldPosition = (0, 0, .1)
				self.obj.worldOrientation = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]

			if self.obj["spawn_stance"] == 1:
				self.obj.setLinearVelocity([.1,0,0], 1)

			else:
				self.obj.setLinearVelocity([-.1,0,0], 1)

		if dict['udPad'] == 1:
			self.obj['spawn_pos'] = [self.obj.worldPosition[0], self.obj.worldPosition[1], self.obj.worldPosition[2]]
			self.obj['spawn_rot'] = [[self.obj.worldOrientation[0][0], self.obj.worldOrientation[0][1],self.obj.worldOrientation[0][2]], [self.obj.worldOrientation[1][0], self.obj.worldOrientation[1][1], self.obj.worldOrientation[1][2]], self.obj.worldOrientation[2][2]]
			self.obj['spawn_cam_pos'] = [dict['camera'].worldPosition[0], dict['camera'].worldPosition[1], dict['camera'].worldPosition[2]]
			self.obj['spawn_cam_rot'] = [[dict['camera'].worldOrientation[0][0], dict['camera'].worldOrientation[0][1],dict['camera'].worldOrientation[0][2]], [dict['camera'].worldOrientation[1][0], dict['camera'].worldOrientation[1][1], dict['camera'].worldOrientation[1][2]], dict['camera'].worldOrientation[2][2]]
			stance = self.obj["stance"]
			self.obj["spawn_stance"] = stance


	def hang_move(self):
		if dict['lLR'] > .04:
			#self.arm.playAction('c_hanghopleft', 0, 45, layer=3, play_mode=0, speed=.75, blendin=10)
			self.arm.playAction('c_shimmy_l', 0, 34, layer=3, play_mode=0, speed=.75, blendin=10)
			self.obj.applyForce([0,235,0], True)
		if dict['lLR'] < -.04:
			#self.arm.playAction('c_hanghopright', 0, 45, layer=3, play_mode=0, speed=.75, blendin=10)
			self.arm.playAction('c_shimmy_r', 0, 34, layer=3, play_mode=0, speed=.75, blendin=10)
			self.obj.applyForce([0,-235,0], True)


	def check_throw(self):

		def throw(strength):
			print('throwing with strength', strength)
			if self.throw_deck != None:
				self.throw_deck.endObject()
				self.throw_deck = None
			self.throw_deck = dict['level_scene'].addObject('throw_deck', self.throw_deck_empty, 0)
			self.throw_deck.worldOrientation = self.throw_deck_empty.worldOrientation
			#self.throw_deck.applyForce([0, 0, -300], True)
			#-z = forward +y = height
			self.throw_deck.applyForce([0, 150, -200], True)
			self.hide_deck()

		if self.throw_deck == None:	
			strength = self.throw_timer
			
			if strength > 19:
				strength = 19

			if dict['bBut'] == 0 and dict['last_bBut'] == 1:
				self.throw_timer = 0
				self.arm.playAction('c_throw', 19, 45, layer=7, play_mode=0, speed=.75, blendin=5)
				self.deck_arm.playAction('b_throw', 19, 45, layer=7, play_mode=0, speed=.75, blendin=5)
				throw(strength)
			elif dict['bBut'] == 1:
				self.arm.playAction('c_throw', 0, 45, layer=7, play_mode=1, speed=.75, blendin=5)
				self.deck_arm.playAction('b_throw', 0, 45, layer=7, play_mode=1, speed=.75, blendin=5)
				self.arm.setActionFrame(strength, 7)
				self.deck_arm.setActionFrame(strength, 7)
				self.throw_timer += 1
			else:
				self.throw_timer = 0
		else:
			if dict['bBut'] == 0 and dict['last_bBut'] == 1:
				self.show_deck()
				self.throw_deck.endObject()
				self.throw_deck = None

	def drop_deck(self):

		if self.throw_deck == None:	
			self.throw_deck = dict['level_scene'].addObject('throw_deck', self.throw_deck_empty, 0)
			self.throw_deck.worldOrientation = self.throw_deck_empty.worldOrientation
			#self.throw_deck.applyForce([0, 0, -300], True)
			#-z = forward +y = height
			self.throw_deck.applyMovement([-.5, 0, .5], True)
			self.throw_deck.worldPosition.z += .4
			self.throw_deck.applyForce([0, 50, 75], True)
			self.hide_deck()

		
			



		#if self.throw_timer > 0:
	def hide_deck(self):
		for x in self.deck_arm.childrenRecursive:
			x.visible = False
			
	def show_deck(self):
		for x in self.deck_arm.childrenRecursive:
			x.visible = True

	def get_vert_rot(self, own, object):

			if 'inited' not in object:
				object['inited'] = True

				for mesh in object.meshes:
					red_verts = []
					green_verts = []
					for m_index in range(len(mesh.materials)):
						for v_index in range(mesh.getVertexArrayLength(m_index)):
							vertex = mesh.getVertex(m_index, v_index)
							if vertex.color[0] > .8:
								loc = object.worldTransform * vertex.XYZ
								red_verts.append(loc.freeze())
							if vertex.color[1] > .8:
								loc = object.worldTransform * vertex.XYZ
								green_verts.append(loc.freeze())
							   
				red_verts = set(red_verts)
				#for v in red_verts:
					#print(v, 'red_vert')
				green_verts = set(green_verts)               
				object['red_verts'] = red_verts
				object['green_verts'] = green_verts
				
				size_red = len(object['red_verts'])
				kd_red = mathutils.kdtree.KDTree(size_red)
				size_green = len(object['green_verts'])
				kd_green = mathutils.kdtree.KDTree(size_green)    

				for i, v in enumerate(object['red_verts']):
					kd_red.insert(v, i)
				for i, v in enumerate(object['green_verts']):
					kd_green.insert(v, i)

				kd_red.balance()
				kd_green.balance() 
				object['kd_red'] = kd_red
				object['kd_green'] = kd_green    
				#print('kd built -------')
				#print(own['kd_red'])
			# Find the closest points to the player
			#co_find = control_bottom.worldPosition
			co_find = own.worldPosition
			found_red = object['kd_red'].find_n(co_find, 2)
			found_green = object['kd_green'].find_n(co_find, 1)    
			
			primary = Vector(found_red[0][0])
			secondary = Vector(found_red[1][0])

			lineVector = secondary - primary
			lineVector2 = primary - secondary
			if found_green != []:
				lv_green = Vector(found_green[0][0])
				
			eul = Vector((1, 0, 0)).rotation_difference(lineVector).to_euler()        
			
			te = dict['level_scene'].objects['temp_empty']
			

			rotation = te.worldOrientation.to_euler()
			if abs(rotation.z) > 3:
				te.applyRotation([0, 0, 1.570796*2], True)
			
			xyz = te.localOrientation.to_euler()
			xyz[0] = math.radians(0)
			te.localOrientation = xyz.to_matrix()

			if lineVector == Vector([0.0, 0.0, 0.0]):
				#print('vector is empty')
				pass
			else:
				te.alignAxisToVect(lineVector, 0, 1)
			
			te.worldPosition = primary

			if found_green != []:
				local = te.worldOrientation.inverted() * (lv_green - te.worldPosition)
				if local.y < 0:
					#print('flip vector')
					eul = Vector((1, 0, 0)).rotation_difference(lineVector2).to_euler() 
					if lineVector2 == Vector([0.0, 0.0, 0.0]):
						#print('linVector2 is empty')
						pass
					else:
						te.alignAxisToVect(lineVector2, 0, 1)
					#print('align2', lineVector2)
					te.worldPosition = secondary
				else:
					#print('align1', lineVector)  
					pass


			#flip z axis

			#print(local, 'local')
			
			myhead = te.worldOrientation.col[2]
			worldUp = Vector([0,0,1])
			#worldY = Vector([0,1,0])
			dot_p = myhead.dot(worldUp)

			if dot_p > 0.9:
				pass
			else:
				#print('flip grind object rotation')
				te.applyRotation([1.570796*2, 0, 0], True)




			return eul

	def move_to_te(self):
		o = self.obj
		te = dict['level_scene'].objects['temp_empty']
		te.applyRotation([0,0,-3.14/2], True)
		te.worldPosition.z += .3
		o.worldPosition = te.worldPosition
		o.worldOrientation = te.worldOrientation
		o.linearVelocity = [0,0,0]

	def check_pause(self):
		if dict['npause'] == True:
			self.FSM.FSM.ToTransition('toPause')

	def check_idle(self, resetter):
		idle = True
		sens = .03
		
		if resetter:
			idle = False
		else:
			if abs(dict['lUD']) > sens:
				idle = False
			elif abs(dict['lLR']) > sens:
				idle = False
			elif abs(dict['rUD']) > sens:
				idle = False
			elif abs(dict['rLR']) > sens:
				idle = False
			elif abs(dict['rTrig']) > sens:
				idle = False
			elif abs(dict['lTrig']) > sens:
				idle = False
			elif dict['aBut'] == True:
				idle = False
			elif dict['bBut'] == True:
				idle = False
			elif dict['xBut'] == True:
				idle = False
			elif dict['yBut'] == True:
				idle = False
			elif dict['udPad'] == True:
				idle = False
			elif dict['ddPad'] == True:
				idle = False
			elif dict['ldPad'] == True:
				idle = False
			elif dict['rdPad'] == True:
				idle = False

		if self.joystick_idle == True and idle == False:
			self.arm.stopAction(6)		
			self.deck_arm.stopAction(6)		
			self.joystick_idle = False
		if not idle:
			self.joystick_active_frame = dict['game_life']
		else:
			idle_time = dict['game_life'] - self.joystick_active_frame
			if idle_time > 400:
				self.joystick_idle = True

	def idle_anim(self):
		if self.joystick_idle:
			#if self.arm.isPlayingAction(6):
			
			if 1 == 2:
				pass
			else:
				if dict['game_life'] % 280 == 1:

						# 'reg_idle2_', 1, 120,
						# #deck action name, start, end frames  
						# 'b_reg_idle2', 1, 120,

					#choice = random

					s_list = ['reg_idle1', 'reg_idle2_', 'reg_idle3', 'reg_idle4', 'reg_idle5', 'reg_idle6', 'reg_idle7']
					d_list = ['b_reg_idle1', 'b_reg_idle2', 'b_reg_idle3', 'b_reg_idle4', 'b_reg_idle5', 'b_reg_idle6', 'b_reg_idle7']

					random_index = random.randrange(len(s_list))
					
					self.arm.playAction(s_list[random_index], 1, 120, layer=6, play_mode=0, speed=.5, blendin=15)
					self.deck_arm.playAction(d_list[random_index], 1, 120, layer=6, play_mode=0, speed=.5, blendin=15)
					#print('play an idle', s_list[random_index], d_list[random_index])
					
				if dict['game_life'] % 1280 == 1:				
					self.idle_cam_dir = not self.idle_cam_dir
			
				#print('modded', dict['game_life'] % 280, dict['game_life'])
				if self.idle_cam_dir:
					dict['camera'].applyMovement([.0005, 0, 0], True)
					dict['camera'].applyRotation([.0005, 0, .0005], True)
				else:
					dict['camera'].applyMovement([-.0005, 0, 0], True)
					dict['camera'].applyRotation([-.0005, 0, -.0005], True)
				# move_len = 2048
		  #       if own['idlecampos_x'] < move_len:
		  #           own['idlecampos_x'] += 1
		  #       if own['idlecampos_x'] == move_len:
		  #          own['idlecampos_x'] = 0

		  #       if own['idlecampos_x'] < (move_len / 2):
		  #           move = [.0001, 0, 0]
		  #           freecam.applyMovement( move, True)
		  #           freecam.applyRotation([.0001, 0, .0001], True)
		  #       if own['idlecampos_x'] > (move_len / 2):
		  #           move = [-.0001, 0, 0]
		  #           freecam.applyMovement(move, True)
		  #           freecam.applyRotation([-.0001, 0, -.0001], True)


def update(cont):
	#print('-------------player updating----------------')
	dict = bge.logic.globalDict
	#own = cont.owner

	if 'player_class' not in dict:
		#dict['playerFSM'] = FSM.PlayerFSM(own)
		dict['player_class'] = Player(cont)

	#dict['playerFSM'].Execute()
	dict['player_class'].update()
