import bge

def main(cont):
    #cont = bge.logic.getCurrentController() 
    print('Startup.py running ----------')
    own = cont.owner
    scenes = bge.logic.getSceneList()

    for i in scenes:
        if i.name == "main":
            main = i
            main.world.zenithColor = [0, 0, 0, 0]
            main.world.horizonColor = [0, 0, 0, 0] 

    scene = bge.logic.getCurrentScene()
    black = scene.objects['black']
    shuvit_logo = scene.objects['shuvit_logo']
    black.playAction('black_fade', 0,100, layer=1, play_mode=0)
    shuvit_logo.playAction('logo_fade', 0,100, layer=4, play_mode=0)  
    print('black on') 
    dict = bge.logic.globalDict
    dict['overlay_fadein'] = 0
    dict['overlay_fadeout'] = 0
    dict['load_timer'] = 0
    dict['reload_timer'] = 0
    dict['menu_idle_timer'] = 0

    if not any(bge.logic.joysticks):
        print("joystick not connnected")
        dict['joy_con'] = 0
    else:    
        dict['joy_con'] = 1

    dict['kb_a'] = 0
    dict['kb_d'] = 0
    dict['kb_w'] = 0
    dict['kb_s'] = 0
    dict['kb_q'] = 0
    dict['kb_e'] = 0
    dict['kb_z'] = 0
    dict['kb_c'] = 0 
    dict['kb_en'] = 0    
    dict['kb_la'] = 0
    dict['kb_ra'] = 0
    dict['kb_ua'] = 0
    dict['kb_da'] = 0 
    dict['kb_lsh'] = 0
    dict['kb_space'] = 0    
    
    dict['aBut'] = 0.0
    dict['bBut'] = 0.0
    dict['xBut'] = 0.0
    dict['yBut'] = 0.0
    dict['lBump'] = 0.0
    dict['rBump'] = 0.0
    dict['bkBut'] = 0.0
    dict['stBut'] = 0.0
    dict['xbBut'] = 0.0
    dict['ltsBut'] = 0.0
    dict['rtsBut'] = 0.0
    dict['ldPad'] = 0.0
    dict['rdPad'] = 0.0
    dict['udPad'] = 0.0
    dict['ddPad'] = 0.0
    dict['rUD'] = 0.0
    dict['lUD'] = 0.0
    dict['rLR'] = 0.0
    dict['lLR'] = 0.0
    dict['rmUD'] = 0.0
    dict['lmUD'] = 0.0
    dict['rmLR'] = 0.0
    dict['lmLR'] = 0.0
    dict['mTrig'] = 0.0 
    dict['lTrig'] = 0.0
    dict['rTrig'] = 0.0 
    
    dict['last_ltsBut'] = 0.0
    dict['last_rtsBut'] = 0.0   
    dict['last_ldPad'] = 0 
    
    dict['mu_last_track'] = ''
    dict['mu_artist'] = ''
    dict['mu_title'] = ''    
    
    dict['joy_con'] = 1
    dict['last_joy_con'] = 1
    dict['last_driving'] = False
    dict['driving_reset'] = False
    
    dict['spawned_npc_decks'] = []
    dict['spawned_npc_trucks'] = []    
    dict['spawned_npcs'] = []
    dict['char_loaded'] = 0
    dict['man_sens_l'] = .03
    dict['man_sens_r'] = .08
    dict['walk'] = 1
    dict['cur_ccH_targetHeight'] = .3
    dict['music_start'] = False  
    dict['npause'] = False
    dict['playback'] = False 
    dict['sunx'] = 1.0
    dict['suny'] = 1.0
    dict['sunz'] = 1.0 
    dict['replay_loop_start'] = 0
    dict['replay_loop_end'] = 0
    dict['replay_record_length'] = 2600#240 #1200 #7300
    dict['rp_keyframes'] = []
    dict['rp_positions'] = []
    dict['temp_list'] = []
    dict['scene_inited'] = True
    dict['p1c'] = None
    dict['sun_lights'] = []
    dict['ambient_lights'] = []

    dict['joy_con'] = False
    
    
#main()
