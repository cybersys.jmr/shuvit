We'd love for you to be able to contribute to this project; if you're interested, you'll need the following:

Latest stable version of UPBGE.
The lastest release of Shuvit (v0.1.2b)
The latest .blend file (shuvit_v0.1.2b.blend as of time of writing this)

Now that you've got those, follow these steps:

1. Put the .blend fild in the folder with the Shuvit executable
2. Open UPBGE
3. In UPBGE go to File > User Preferences. Select the "File" tab. Make sure "Auto Run Python Scripts" is checked. Save preferences.
4. Choose File -> open
5. Navigate to the folder where you put the .blend file and select it
6. Click "open blender file" in UPBGE
7. Wait for it to import
8. ????
9. Profit
