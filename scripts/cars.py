import bge
import random
import FSM
import astar
import observer

car_colors = [[.1, .1, .1, 1], [0.0, 0.0, 0.25, 1], [0.1, 0.03, 0.02, 1], [.4, .3, .08, 1], [.1, .1, .1, 1], [.05, .12, .05, 1], [.3, .05, 0, 1], [.2, .03, .3, 1], [.3, 0, 0, 1], [.3, .3, .3, 1], [.3, .25, .15, 1], [1,1,1,1], [1, 1, 0, 1]]

def car_run_check(self):
	output = False
	for x in bge.logic.getCurrentScene().objects:
		if 'car_park_empty' in x.name:
			self.parking_spots.append(x)
			x['in_use'] = False
			print('this is a parking spot')
			output = True
	return output
			

def get_parking_spots():
	op = []
	for obj in bge.logic.getCurrentScene().objects:
		if 'parking' in obj:
			ps = ParkingSpot(obj, 'available')
			if 'perp' in obj:
				ps.type = 'perp'
			else:
				ps.type = 'parallel'
			op.append(ps)
	return op

def get_intersections():
	op = []
	for obj in bge.logic.getCurrentScene().objects:
		if 'intersection' in obj:
			op.append(obj)
	return op		

def add_car(self, x):
	print('put car here', x.obj.worldPosition)
	car = bge.logic.getCurrentScene().addObject('car_collider', x.obj, 0)
	car.worldOrientation = x.obj.worldOrientation
	car.worldPosition.z += .8
	if x.type == 'perp':
		car.applyMovement([7, 0, 0], True)
	else:
		car.applyMovement([0, -6, 0], True)
	car.suspendDynamics()
	#car.suspendPhysics()	
	car.name = 'lcar' + str(len(self.manager.cars))
	color = random.choice(car_colors)
	car.color = color
	for y in car.children:
		y.color = color
	print('setting car color to ', car.color)
	x.status = 'in_use'
	return car

#====================================  

class ParkingSpot:
	def __init__(self, obj, status):
		self.obj = obj
		self.type = None
		self.status = status

class Car:
	def __init__(self, own, start_empty):
		self.manager = own
		self.life = 0
		self.start_empty = start_empty
		self.obj = add_car(self, self.start_empty)
		self.speed_targ = self.manager.default_speed
		self.speed_inc = 800
		self.FSM = FSM.CarFSM(self)
		self.active = False
		self.target = None
		self.lane_point = self.obj.worldPosition
		self.point = self.obj.worldPosition
		self.last_lane_point = self.obj.worldPosition
		self.path = None
		self.path_index = 0
		self.path_display = []
		self.manager.pub.register("path found", self)

	def Execute(self):
		self.FSM.Execute()
		self.life += 1

	def ReceiveMessage(self, message):
		if self == message[1]:
			self.path = message[2]
			if self.path != []:
				#print(self.start_empty.type, 'target type')
				if self.start_empty.type == 'perp':
					self.FSM.FSM.ToTransition('toExitPerpPark')
				else:
					self.FSM.FSM.ToTransition('toExitParallelPark')
					#print('setting goal available after failure', self.FSM.owner.target.obj.worldPosition)
					self.FSM.owner.target.status = 'available'
			else:
				#print('path failed for', self)
				self.FSM.FSM.ToTransition('toEnterParallelPark')

		
class CarManager:
	def __init__(self, own):
		self.parent = own
		self.navmesh =None
		self.pub = observer.Publisher(['path found', 'working'])
		self.navmesh2 =  astar.Astar('nav_points', self.pub)
		self.cars = []
		self.max_cars = 10
		self.max_active = 6
		self.targets = []
		self.target_loc = None
		self.parking_spots = get_parking_spots()
		self.intersections = get_intersections()
		self.active = True
		self.cars_active = []
		self.cars_loaded = False
		self.life = 0
		self.default_speed = 8.0#5.0
		self.lane_position = 2.5#1.75
		
	def load_cars(self):
		iter_ = 0
		if 'car_collider' in bge.logic.getCurrentScene().objectsInactive:
			start_choices = self.parking_spots.copy()
			while len(self.cars) < self.max_cars:
				start_choice = random.choice(start_choices)
				start_choices.remove(start_choice)
				car_ = Car(self, start_choice)
				self.cars.append(car_)	
			self.cars_loaded = True	

		else:
			mainDir = bge.logic.expandPath("//")
			car = 'car1'
			fileName = mainDir + "assets/" + str(car) + '.blend'    
			path = bge.logic.expandPath(fileName)
			print('loading car')
			try:
				bge.logic.LibLoad(path, 'Scene') 
			except:
				print('loading', fileName, 'failed')   

	def activator_check(self):
		if len(self.cars_active) < self.max_active:
			l = []
			for c in self.cars:
				if not c.active:
					l.append(c)
			car = random.choice(l)
			self.cars_active.append(car)
			car.active = True
			car.FSM.FSM.ToTransition('toRequestPath')
			#print('activate car', car)

	def update(self):
		self.life += 1
		if self.cars_loaded:
			if self.life % 120 == 0:
				self.activator_check()

			for car in self.cars_active:
				car.Execute()
			self.navmesh2.update()			

		else:
			self.load_cars()
			
def Execute(cont):
	own = cont.owner
	if 'car_manager' not in own:
		own['car_manager'] = CarManager(own)
	car_man = own['car_manager']	
	if car_man.active:

		car_man.update()
