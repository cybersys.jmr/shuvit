import bge
import random
import FSM
import astar
import observer

car_colors = [[.2,.01,.01,1], [.5,.5,.4,1], [.005,.01,.015,1], [.005, .1, 0.003, 1], [.1, .1, .1, 1]]

npcs = ['carlos', 'maria', 'denise', 'charlie']

def idle_run_check(self):
	output = False
	for x in bge.logic.getCurrentScene().objects:
		if 'walker_idle_spot' in x.name:
			self.idle_spots.append(x)
			x['in_use'] = False
			output = True
	print(output, 'walker idle spots')
	return output
			

def get_idle_spots():
	op = []
	for obj in bge.logic.getCurrentScene().objects:
		if 'idle_spot' in obj.name:
			ps = IdleSpot(obj, 'available')
			if 'corner' in obj:
				ps.type = 'corner'
			op.append(ps)
	print('&&&&&& idle spots')
	print(op)		
	return op

def get_intersections():
	op = []
	for obj in bge.logic.getCurrentScene().objects:
		if 'intersection' in obj:
			op.append(obj)
	return op		

def add_walker(self, x):
	print(x.obj.worldPosition, '--walker here')
	#walker = bge.logic.getCurrentScene().addObject('larryCube', x.obj, 0)
	#walker = bge.logic.getCurrentScene().addObject('npcCube', x.obj, 0)
	choice = random.choice(npcs)
	walker = bge.logic.getCurrentScene().addObject(choice, x.obj, 0)
	print(walker.worldPosition)
	
	#walker.worldPosition = x.obj.worldPosition
	walker.worldOrientation = x.obj.worldOrientation
	walker.worldPosition.z += 2.8
	walker.name = 'lwalker' + str(len(self.manager.walkers))
	walker.childrenRecursive['shirt'].color = [random.random(),random.random(),random.random(),1]
	walker['npc'] = True
	x.status = 'in_use'

	return walker

#====================================  

class IdleSpot:
	def __init__(self, obj, status):
		self.obj = obj
		self.status = status
		self.type = None

class Walker:
	def __init__(self, own, start_empty):
		self.manager = own
		self.life = 0
		self.start_empty = start_empty
		self.obj = add_walker(self, self.start_empty)
		self.speed_targ = self.manager.default_speed
		self.speed_inc = 100
		self.FSM = FSM.WalkerFSM(self)
		self.active = False
		self.target = start_empty
		self.lane_point = self.obj.worldPosition
		self.last_lane_point = self.obj.worldPosition
		self.path = None
		self.path_index = 0
		self.path_display = []
		self.cla = None
		self.manager.pub.register("path found", self)
		#self.obj.worldPosition = self.start_empty.obj.worldPosition

	def Execute(self):
		self.FSM.Execute()
		self.life += 1

	def ReceiveMessage(self, message):
		#print(message[0])
		if message[0] == 'path':
			if self == message[1]:
				self.path = message[2]
				self.FSM.FSM.ToTransition('toExitParallelPark')
				#print('path got')
		elif message[0] == 'hit':
			if self.obj == message[2]:
				#print('something was hit')
				active_ = False
				if self in self.manager.walkers_active:
					active_ = True
					#print(self.FSM.FSM.curState.__class__.__name__, 'name')
					if self.FSM.FSM.curState.__class__.__name__ == 'NavigateToTarget':
						self.FSM.FSM.ToTransition('toWalkingHitBySkater')
				else:
					self.manager.walkers_active.append(self)
					self.FSM.FSM.ToTransition('toHitBySkater')
				#print('i was hit', message[2], active_)		

class WalkerManager:
	def __init__(self, own):
		self.parent = own
		self.navmesh = None
		self.pub = observer.Publisher(['path found', 'working'])
		self.navmesh2 =  astar.Astar('walker_nav_points', self.pub)
		self.walkers = []
		self.max_walkers = 16
		self.max_active = 8
		self.targets = []
		self.target_loc = None
		self.idle_spots = get_idle_spots()
		self.active = True
		self.walkers_active = []
		self.walkers_loaded = False
		self.life = 0
		self.default_speed = 1.0#5.0
		self.lane_position = 0.5#1.75

	def load_walkers(self):
		iter_ = 0
		if npcs[0] in bge.logic.getCurrentScene().objectsInactive:
			start_choices = self.idle_spots.copy()
			while len(self.walkers) < self.max_walkers:
				#get starting position
				start_choice = random.choice(start_choices)
				start_choices.remove(start_choice)

				walker_ = Walker(self, start_choice)
				#walker_.cla = self
				self.walkers.append(walker_)

			for x in self.idle_spots:
				iter_ += 1	

			self.walkers_loaded = True	

		else:
			for obj in npcs:
				mainDir = bge.logic.expandPath("//npc_walkers/")
				npc = obj
				fileName = mainDir + str(npc) + '.blend'    
				path = bge.logic.expandPath(fileName)
				print('loading npc')
				try:
					bge.logic.LibLoad(fileName, 'Scene', load_actions=True) 
					print(' loaded', obj)
				except Exception as e:
					print('loading', fileName, 'failed', e)   

	def activator_check(self):
		if len(self.walkers_active) < self.max_active:
			l = []
			for c in self.walkers:
				if not c.active:
					l.append(c)
			walker = random.choice(l)
			self.walkers_active.append(walker)
			walker.active = True
			walker.obj.worldPosition = walker.start_empty.obj.worldPosition
			walker.FSM.FSM.ToTransition('toRequestPath')
			#print('activating walker')

	def update(self):
		self.life += 1
		if self.walkers_loaded:
			if self.life % 180 == 0:
				self.activator_check()

			for walker in self.walkers_active:
				walker.Execute()
			self.navmesh2.update()

		else:
			self.load_walkers()

def Execute(cont):
	own = cont.owner
	if 'walker_manager' not in own:
		own['walker_manager'] = WalkerManager(own)
	walker_man = own['walker_manager']	
	if walker_man.active:

		walker_man.update()
