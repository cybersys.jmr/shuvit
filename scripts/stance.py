import bge
from bge import logic

def main(cont):
    own = cont.owner
    STANCE_SENS = 0.05
    STANCE = own["stance"]
    jumpstance = own['jump_stance']
    grindpos = own['grindpos']
    # use game object axis
    local = True
    try:
        #stance detection   
        if own['ground'] == True and (own['grindpos'] != 'reg_board' or own['grindOld'] == False) and own['invert_on'] == False:
        #if own['ground'] == True and (own['grindOld'] == False) and own['invert_on'] == False:            
            #print("detect stance") 
            LVX = own.localLinearVelocity.x
            if LVX > STANCE_SENS:
                STANCE = 1
                
            if LVX < (-STANCE_SENS):
                STANCE = 0
    except:
        print("no stance")

    own["stance"] = STANCE           
    dict = bge.logic.globalDict 
    dict['stance'] = STANCE

