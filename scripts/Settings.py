import bge
import GameLogic
import configobj
from configobj import ConfigObj

mainDir = GameLogic.expandPath("//")
fileName = mainDir + "config.ini"

config = ConfigObj(fileName, interpolation=True)
dict = bge.logic.globalDict
#map_config = {}


print('###############start game##########')
dict['music_player'] = 1

def to_dict3(section, key):
    #dict = bge.logic.globalDict
    print(section, key)
    #d = dict['map']
    fileName2 = GameLogic.expandPath("//assets/maps/" + dict['map'] + '/map_settings.ini')    
    map_config = ConfigObj(fileName2, interpolation=True)   
    value = map_config[key]
    if isinstance(value, str):
        if value.isdigit():
            value = int(value)
        else:
            try:
                value = float(value)
            except:
                pass
    #print(dict['mapDict'])
    dict[key] = value    


def to_dict(section, key):
    dict = bge.logic.globalDict
    value = config[key]
    if isinstance(value, str):
        if value.isdigit():
            value = int(value)
        else:
            try:
                value = float(value)
            except:
                pass
    dict[key] = value  

    
def readMap():
    fileName = GameLogic.expandPath("//assets/maps/" + dict['map'] + '/map_settings.ini')    
    #fileName = mainDir + dict['character'] + ".ini"
    #print('loading character settings', fileName)
    map_config = ConfigObj(fileName, interpolation=True)
    print('map_config', map_config)
    dict['mapDict'] = map_config
    # def to_dict2(section, key):
    #     #dict = bge.logic.globalDict
    #     value = map_config[key]
    #     if isinstance(value, str):
    #         if value.isdigit():
    #             value = int(value)
    #         else:
    #             try:
    #                 value = float(value)
    #             except:
    #                 pass
    #     dict[key] = value 

def readChar():
    mainDir = GameLogic.expandPath("//characters/")    
    fileName = mainDir + dict['character'] + ".ini"
    #print('loading character settings', fileName)
    char_config = ConfigObj(fileName, interpolation=True)
    
    
    def to_dict2(section, key):
        #dict = bge.logic.globalDict
        value = char_config[key]
        if isinstance(value, str):
            if value.isdigit():
                value = int(value)
            else:
                try:
                    value = float(value)
                except:
                    pass
        dict[key] = value  
        #print(key, value)      
    
    
    char_config.walk(to_dict2, char_config)
    
def writeChar():
    #dict = bge.logic.globalDict
    mainDir = GameLogic.expandPath("//characters/")    
    fileName = mainDir + dict['character'] + ".ini"
    print('writing character settings', fileName)
    char_config = ConfigObj(fileName, interpolation=True)  
    
    def from_dict2(section, key):
        char_config[key] = dict[key]
    
    char_config.walk(from_dict2)  
    #char_config[key] = dict[key] 
    char_config.write()
    

def readSettings():
    config.walk(to_dict)    
    dict['mapDict'] = {}  
    fileName2 = GameLogic.expandPath("//assets/maps/" + dict['map'] + '/map_settings.ini')    
    map_config = ConfigObj(fileName2, interpolation=True)   
    map_config.walk(to_dict3)         
    #readMap()
    readChar()

def from_dict(section, key):
    #dict = bge.logic.globalDict
    config[key] = dict[key]

def writeSettings():
    config.walk(from_dict)  
    config.write()  
    #dict = bge.logic.globalDict
     
def setres():
    #import GameLogic
    #import bge
    scenes = bge.logic.getSceneList()
    main_scene = [scene for scene in scenes if scene.name=="main"][0]
    own = main_scene.objects['Empty']
    cont = bge.logic.getCurrentController()
    own2 = cont.owner
    dict = bge.logic.globalDict 
    resx = int(dict["resx"])
    resy = int(dict["resy"])
    fullscreen = bge.render.getFullScreen()
    #print("fullscreen = ", fullscreen)
    if fullscreen != True:
        if dict['fullscreen_on'] == 1:
            bge.render.setFullScreen(True)
    if fullscreen == True:
        if dict['fullscreen_on'] == 0:
            bge.render.setFullScreen(False) 
            
    bge.render.setWindowSize(resx, resy)   
    bge.render.setVsync(dict['vsync_on'])      #doesn't work?

    #print("resolution = ", resx, resy)       
        
def loadlevel():
    scenes = bge.logic.getSceneList()
    main_scene = [scene for scene in scenes if scene.name=="main"][0]
    own = main_scene.objects['Empty']
    cont = GameLogic.getCurrentController()
    own2 = cont.owner

    dict = bge.logic.globalDict #Get the global dictionary
    dict['scene_inited'] = False
    if own["framerate"] == 1:    
        bge.render.showFramerate(1)
    if own["profile"] ==1:    
        bge.render.showProfile(1)   
    act = own2.actuators["level"]
    #dict = bge.logic.globalDict
    
    fileName2 = GameLogic.expandPath("//assets/maps/" + dict['map'] + '/map_settings.ini')    
    map_config = ConfigObj(fileName2, interpolation=True)   
    map_config.walk(to_dict3)  
    
    level = dict['map_template']
    dict['level'] = level
    #level = dict['level']
    print('loading template level', level)
    act.scene = level 
    cont.activate(own2.actuators["level"]) 
    dict['npause'] = False 
    
def timer():
    #dict = bge.logic.globalDict    
    
    if dict['reload_timer'] == 1:
        scene = bge.logic.getCurrentScene()
            
    if dict['reload_timer'] > 0:
       dict['reload_timer'] -= 1         
