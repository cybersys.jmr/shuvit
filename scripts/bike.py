

import bge
from mathutils import Vector

dict = bge.logic.globalDict
scene = bge.logic.getCurrentScene()
skater = scene.objects["Char4"]
TURN_SENS = .04
TURNAMT = .03
AIR_TURMAMT = .07
ACCEL_AMT = .06
JUMP_AMT = 1300


def turnL(own, gray):
    if gray.positive:
        own.applyRotation([0,0,TURNAMT], True)
    else:            
        own.applyRotation([0,0,AIR_TURMAMT], True)
def turnR(own, gray):
    if gray.positive:
        own.applyRotation([0,0,-TURNAMT], True)
    else:            
        own.applyRotation([0,0,-AIR_TURMAMT], True)    
def reset_timers(own, q1oncd, q2oncd, q3oncd, q4oncd, q5oncd, q6oncd, q7oncd, q8oncd):
     q1oncd = 0
     q2oncd = 0 
     q3oncd = 0 
     q4oncd = 0 
     q5oncd = 0 
     q6oncd = 0 
     q7oncd = 0 
     q8oncd = 0 
     own["Q1oncd"] = 0
     own["Q2oncd"] = 0
     own["Q3oncd"] = 0
     own["Q4oncd"] = 0
     own["Q5oncd"] = 0
     own["Q6oncd"] = 0
     own["Q7oncd"] = 0
     own["Q8oncd"] = 0


def get_physics_action(cont, own):
    #print(dict['lLR'])
    action = 'None'
    gray = cont.sensors['r_Ground']
    
    lLR = dict['lLR']
    lUD = dict['lUD']
    rLR = dict['rLR']
    rUD = dict['rUD']
    lTrig = dict['lTrig']
    rTrig = dict['rTrig']
    aBut = dict['aBut']
    bBut = dict['bBut']
    xBut = dict['xBut']
    yBut = dict['yBut']
    lBump = dict['lBump']
    rBump = dict['rBump']
    bkBut = dict['bkBut']
    stBut = dict['stBut']
    xbBut = dict['xbBut']
    ltsBut = dict['ltsBut']
    rtsBut = dict['rtsBut']
    ldPad = dict['ldPad']
    rdPad = dict['rdPad']
    udPad = dict['udPad']
    ddPad = dict['ddPad']    
    
    q1oncd = own["Q1oncd"]
    q2oncd = own["Q2oncd"]
    q3oncd = own["Q3oncd"]
    q4oncd = own["Q4oncd"]
    q5oncd = own["Q5oncd"]
    q6oncd = own["Q6oncd"]
    q7oncd = own["Q7oncd"]
    q8oncd = own["Q8oncd"]
    q1oncdl = own["Q1oncdl"]
    q2oncdl = own["Q2oncdl"]
    q3oncdl = own["Q3oncdl"]
    q4oncdl = own["Q4oncdl"]
    q5oncdl = own["Q5oncdl"]
    q6oncdl = own["Q6oncdl"]
    q7oncdl = own["Q7oncdl"]
    q8oncdl = own["Q8oncdl"]

    q1on = 0
    q2on = 0
    q3on = 0
    q4on = 0
    q5on = 0
    q6on = 0
    q7on = 0
    q8on = 0
    lq1on = 0
    lq2on = 0
    lq3on = 0
    lq4on = 0
    lq5on = 0
    lq6on = 0
    lq7on = 0
    lq8on = 0
    
    countdown = 20    
    
    
    ##################
    ###realcontrols###
    ##################
    #       q1
    #    q8    q2
    # q7          q3
    #    q6    q4
    #       q5
    ##################    
    
    if lUD > .04 and lLR < -0.04 :
        lq6on = 1
        q6oncdl = countdown
        own["Q6oncdl"] = q6oncdl
        #print("lq6on")
    elif q6oncdl > 0:
        lq6on = 0
        q6oncdl = q6oncdl - 1
        own["Q6oncdl"] = q6oncdl
    #lq8
    if lUD < -.04 and lLR < -0.04 :
        lq8on = 1
        q8oncdl = countdown
        own["Q8oncdl"] = q8oncdl
        #print("lq8on")
    elif q8oncdl > 0:
        lq8on = 0
        q8oncdl = q8oncdl - 1
        own["Q8oncdl"] = q8oncdl
    #lq2
    if lUD < -.04 and lLR > 0.04 :
        lq2on = 1
        q2oncdl = countdown
        own["Q2oncdl"] = q2oncdl
        #print("lq2on")
    elif q2oncdl > 0:
        lq2on = 0
        q2oncdl = q2oncdl - 1
        own["Q2oncdl"] = q2oncdl
    #q4
    if lUD > 0.04 and lLR > 0.04 :
        lq4on = 1
        q4oncdl = countdown
        own["Q4oncdl"] = q4oncdl
        #print("lq4on")
    elif q4oncdl > 0:
        lq4on = 0
        q4oncdl = q4oncdl - 1
        own["Q4oncdl"] = q4oncdl  
    #q5
    if lUD > .070 and lq4on == 0 and lq6on == 0:
        lq5on = 1
        q5oncdl = countdown
        own["Q5oncdl"] = q5oncdl
        #print("lq5on")
    elif q5oncdl > 0:
        lq5on = 0
        q5oncdl = q5oncdl - 1
        own["Q5oncdl"] = q5oncdl   
    #q1    
    if lUD < -0.070 and lq8on !=1 and lq2on != 1:
        lq1on = 1
        q1oncdl = countdown
        own["Q1oncdl"] = q1oncdl
        #print("lq1on")              
    elif q1oncdl > 0:
        lq1on = 0
        q1oncdl = q1oncdl - 1
        own["Q1oncdl"] = q1oncdl   
    #q7
    if lLR < -0.070 and lq8on != 1 and lq6on != 1:
        lq7on = 1
        q7oncdl = countdown
        own["Q7oncdl"] = q7oncdl
        #print("lq7on")       
    elif q7oncdl > 0:
        lq7on = 0
        q7oncdl = q7oncdl - 1
        own["Q7oncdl"] = q7oncdl  
    #q3    
    if lLR > 0.070 and lq2on !=1 and lq4on != 1:
        lq3on = 1
        q3oncdl = countdown
        own["Q3oncdl"] = q3oncdl
        #print("lq3on") 
    elif q3oncdl > 0:
        lq3on = 0
        q3oncdl = q3oncdl - 1
        own["Q3oncdl"] = q3oncdl       
         
    ################
    #q6
    if rUD > .04 and rLR < -0.04 :
        q6on = 1
        q6oncd = countdown
        own["Q6oncd"] = q6oncd
        #print("q6on")
    elif q6oncd > 0:
        q6on = 0
        q6oncd = q6oncd - 1
        own["Q6oncd"] = q6oncd
        
    #q8
    if rUD < -.04 and rLR < -0.04 :
        q8on = 1
        q8oncd = countdown
        own["Q8oncd"] = q8oncd
        #print("q8on")
    elif q8oncd > 0:
        q8on = 0
        q8oncd = q8oncd - 1
        own["Q8oncd"] = q8oncd
    #q2
    if rUD < -.04 and rLR > 0.04 :
        q2on = 1
        q2oncd = countdown
        own["Q2oncd"] = q2oncd
        #print("q2on")
    elif q2oncd > 0:
        q2on = 0
        q2oncd = q2oncd - 1
        own["Q2oncd"] = q2oncd
        
    #q4
    if rUD > 0.04 and rLR > 0.04 :
        q4on = 1
        q4oncd = countdown
        own["Q4oncd"] = q4oncd
        #print("q4on")
    elif q4oncd > 0:
        q4on = 0
        q4oncd = q4oncd - 1
        own["Q4oncd"] = q4oncd
    #q5
    if rUD > .070 or dict['kb_space'] == 2:
        if q4on == 0 and q6on == 0:
            q5on = 1
            q5oncd = countdown
            own["Q5oncd"] = q5oncd  
    elif q5oncd > 0:
        q5on = 0
        q5oncd = q5oncd - 1
        own["Q5oncd"] = q5oncd              

    #q1    
    if rUD < -0.070:
        if q2on == 0 and q8on == 0:
            #print("q1on")
            q1on = 1
            q1oncd = countdown
            own["Q1oncd"] = q1oncd
    elif q1oncd > 0:
        q1on = 0
        q1oncd = q1oncd - 1
        own["Q1oncd"] = q1oncd            

            
      
    #q7
    if rLR < -0.070:
        if q8on == 0 and q6on == 0:
            q7on = 1
            q7oncd = countdown
            own["Q7oncd"] = q7oncd
           
    elif q7oncd > 0:
        q7on = 0
        q7oncd = q7oncd - 1
        own["Q7oncd"] = q7oncd
    #q3    
    if rLR > 0.070:
        if q4on == 0 and q2on == 0:
            q3on = 1
            q3oncd = countdown
            own["Q3oncd"] = q3oncd
    elif q3oncd > 0:
        q3on = 0
        q3oncd = q3oncd - 1
        own["Q3oncd"] = q3oncd
            
     
    
    if dict['aBut'] == True:
        action = 'forward'    
    if dict['lLR'] > TURN_SENS:
        action = 'turnR'
        turnR(own, gray)
    if dict['lLR'] < -TURN_SENS:
        #action = 'turnL'    
        turnL(own, gray)
    if (q5oncd > 0 and q1oncd > 0 and q5oncd < q1oncd) or (q5oncd > 0 and q8oncd > 0 and q5oncd < q8oncd) or (q5oncd > 0 and q2oncd > 0 and q5oncd < q2oncd):
        action = 'jump'
        reset_timers(own, q1oncd, q2oncd, q3oncd, q4oncd, q5oncd, q6oncd, q7oncd, q8oncd)           


    return action



def do_physics_action(cont, own, action):
#    if action == 'turnR':
#        own.applyRotation([0,0,-TURNAMT], True)
#    if action == 'turnL':
#        own.applyRotation([0,0,TURNAMT], True)
    if action == 'forward':
        own.linearVelocity.x = own.linearVelocity.x - ACCEL_AMT
    
    if action == 'jump':
        own.applyForce([0,0,JUMP_AMT], False)
        print('jump')    
    
    gray = cont.sensors['r_Ground']
    if gray.positive:
        own.alignAxisToVect(Vector(gray.hitNormal)*1, 2, .25)
    else:
        own.alignAxisToVect(Vector([0,0,1]),2,.005)     
    
    
    
    if gray.positive:
        own.linearVelocity.z = own.linearVelocity.z -.05    
        own.linearVelocity.y = 0  
def do_anims(cont, own):
    
    skater = scene.objects["Char4"]
    skater.playAction("reg_bike.001", 20,20, layer=2, play_mode=1, speed=1)
       
        
def main(cont):
    own = cont.owner
    try:
        bike = scene.objects['player_bike']
        #print('biking')
        if dict['yBut'] == True and dict['last_yBut'] == False:
            cont.activate(cont.actuators['walk'])
            pbike = scene.addObject('prop_bike', own, 0)
            pbike.localScale = [4.6, 4.6, 4.6]
            pbike.localScale = [1, 1, 1] 
            pbike.applyRotation([0,0,1.570796], False)
            pbike.worldPosition.y = pbike.worldPosition.y - 1.       
            bike.endObject()
            skater.stopAction(9)
        
        action = get_physics_action(cont, own)
        do_physics_action(cont, own, action)
        do_anims(cont,own)
    except:
        pass    