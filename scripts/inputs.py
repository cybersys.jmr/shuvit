################################################
#inputs.py                                     #
#inputs to global dict                         #
#shuvit.org                                    #
################################################

import bge
from bge import events
from bge import logic as G

def main():
#####
    #init
    cont = bge.logic.getCurrentController()
    scene = bge.logic.getCurrentScene()
    own = cont.owner
    dict = bge.logic.globalDict #Get the global dictionary
    aXis = cont.sensors['Joystick.001']
    bUtt = cont.sensors['Joystick'] 
    reduction = 400000
    axisTh = 0.03  
    
    joy_con = dict['joy_con']  
    scenes = bge.logic.getSceneList()
        
#soft controller mappings    
    lar_lts = dict['lar_lts']
    uad_lts = dict['uad_lts']
    lar_rts = dict['lar_rts']
    uad_rts = dict['uad_rts']
    lt = dict['lt'] 
    rt = dict['rt']
    a_but = dict['a_but'] 
    b_but = dict['b_but'] 
    x_but = dict['x_but'] 
    y_but = dict['y_but']
    l_bump = dict['l_bump']
    r_bump = dict['r_bump'] 
    bk_but = dict['bk_but']
    st_but = dict['st_but']
    xb_but = dict['xb_but']
    lts_pr = dict['lts_pr']
    rts_pr = dict['rts_pr']
    l_dp = dict['l_dp'] 
    r_dp = dict['r_dp'] 
    u_dp = dict['u_dp']
    d_dp = dict['d_dp']

    reduction = 400000
    axisTh = 0.03   
    
    
    keyboard = G.keyboard.events
    # KEY BINDINGS
    kb_a = events.AKEY 
    kb_d = events.DKEY 
    kb_w = events.WKEY
    kb_s = events.SKEY
    kb_q = events.QKEY
    kb_e = events.EKEY
    kb_z = events.ZKEY
    kb_c = events.CKEY 
    kb_q = events.QKEY      
    kb_en = events.ENTERKEY   
    kb_la = events.LEFTARROWKEY
    kb_ra = events.RIGHTARROWKEY
    kb_ua = events.UPARROWKEY
    kb_da = events.DOWNARROWKEY
    kb_lsh = events.LEFTSHIFTKEY
    kb_space = events.SPACEKEY
    #if keyboard[kb_a]:
    #print(keyboard[kb_a])
    
    dict['last_kb_a'] = dict['kb_a']
    dict['last_kb_d'] = dict['kb_d']
    dict['last_kb_w'] = dict['kb_w']
    dict['last_kb_s'] = dict['kb_s']
    dict['last_kb_q'] = dict['kb_q']
    dict['last_kb_e'] = dict['kb_e']
    dict['last_kb_z'] = dict['kb_z']
    dict['last_kb_c'] = dict['kb_c'] 
    dict['last_kb_q'] = dict['kb_q']  
    dict['last_kb_en'] = dict['kb_en']  
    dict['last_kb_la'] = dict['kb_la']
    dict['last_kb_ra'] = dict['kb_ra']
    dict['last_kb_ua'] = dict['kb_ua']
    dict['last_kb_da'] = dict['kb_da'] 
    dict['last_kb_lsh'] = dict['kb_lsh'] 
    dict['last_kb_space'] = dict['kb_space']    
    
    dict['kb_a'] = keyboard[kb_a]
    dict['kb_d'] = keyboard[kb_d]
    dict['kb_w'] = keyboard[kb_w]
    dict['kb_s'] = keyboard[kb_s]
    dict['kb_q'] = keyboard[kb_q]
    dict['kb_e'] = keyboard[kb_e]
    dict['kb_z'] = keyboard[kb_z]
    dict['kb_c'] = keyboard[kb_c] 
    dict['kb_q'] = keyboard[kb_q]    
    dict['kb_en'] = keyboard[kb_en]
    dict['kb_la'] = keyboard[kb_la]
    dict['kb_ra'] = keyboard[kb_ra]
    dict['kb_ua'] = keyboard[kb_ua]
    dict['kb_da'] = keyboard[kb_da] 
    dict['kb_lsh'] = keyboard[kb_lsh]
    dict['kb_space'] = keyboard[kb_space]      
    
    
    if not any(bge.logic.joysticks):
        #print("joystick not connnected")
        dict['joy_con'] = 0
    else:    
        dict['joy_con'] = 1 
        
    if dict['joy_con'] != dict['last_joy_con'] and own['counter'] > 500:
        if dict['joy_con'] == 0 and dict['last_joy_con'] == 1:
            if 'joy_warn' not in scenes:
                #add scene
                #print('print add joy overlay')
                cont.activate(cont.actuators['add_controller'])
        else:
            if dict['joy_con'] == 1 and dict['last_joy_con'] == 0:
                #remove scene            
                #print('print remove joy overlay')            
                cont.activate(cont.actuators['remove_controller'])
#get controller axis value 0-100   
    if dict['joy_con'] == 1: 
        lLR = aXis.axisValues[lar_lts] / reduction 
        lUD = aXis.axisValues[uad_lts] / reduction 
        rLR = aXis.axisValues[lar_rts] / reduction 
        rUD = aXis.axisValues[uad_rts] / reduction 
        lTrig = aXis.axisValues[lt] / reduction 
        rTrig = aXis.axisValues[rt] / reduction 

        aBut = bUtt.getButtonStatus(a_but)
        bBut = bUtt.getButtonStatus(b_but)
        xBut = bUtt.getButtonStatus(x_but)
        yBut = bUtt.getButtonStatus(y_but)
        lBump = bUtt.getButtonStatus(l_bump)
        rBump = bUtt.getButtonStatus(r_bump)
        bkBut = bUtt.getButtonStatus(bk_but)
        stBut = bUtt.getButtonStatus(st_but)
        xbBut = bUtt.getButtonStatus(xb_but)
        ltsBut = bUtt.getButtonStatus(lts_pr)
        rtsBut = bUtt.getButtonStatus(rts_pr)
        ldPad = bUtt.getButtonStatus(l_dp)
        rdPad = bUtt.getButtonStatus(r_dp)
        udPad = bUtt.getButtonStatus(u_dp)
        ddPad = bUtt.getButtonStatus(d_dp)

        
        #create modified axis values
        if lLR < -20:
            lmLR = round((lLR + 20) / 80 * 100, 0) 
        elif lLR > 20:
            lmLR = round((lLR - 20) / 80 * 100, 0)
        else: lmLR = 0  
              
        if lUD > 20:
            lmUD = round((lUD - 20) / 80 * 100, 0)
        elif lUD < -20:
            lmUD = round((lUD + 20) / 80 * 100, 0)
        else: lmUD = 0 

        if rLR < -20:
            rmLR = round((rLR + 20) / 80 * 100, 0)
        elif rLR > 20:
            rmLR = round((rLR - 20) / 80 * 100, 0)
        else: rmLR = 0         
        
        if rUD > 20:
            rmUD = round((rUD - 20) / 80 * 100, 0)
        elif rUD < -20:
            rmUD = round((rUD + 20) / 80 * 100, 0)
        else: rmUD = 0 
      
        if lTrig > 3:
            mTrig = lTrig * -1
        elif rTrig > 3:
            mTrig = rTrig    
        else: mTrig = 0
        #    

      
        
        
        dict['last_aBut'] = dict['aBut']
        dict['last_bBut'] = dict['bBut']
        dict['last_xBut'] = dict['xBut']
        dict['last_yBut'] = dict['yBut']
        dict['last_lBump'] = dict['lBump']
        dict['last_rBump'] = dict['rBump']
        dict['last_bkBut'] = dict['bkBut']
        dict['last_stBut'] = dict['stBut']
        dict['last_xbBut'] = dict['xbBut']
        dict['last_ltsBut'] = dict['ltsBut']
        dict['last_rtsBut'] = dict['rtsBut']
        dict['last_ldPad'] = dict['ldPad']
        dict['last_rdPad'] = dict['rdPad']
        dict['last_udPad'] = dict['udPad']
        dict['last_ddPad'] = dict['ddPad']
        dict['last_rUD'] = dict['rUD']
        dict['last_lUD'] = dict['lUD']
        dict['last_rLR'] = dict['rLR']
        dict['last_lLR'] = dict['lLR']
        dict['last_rmUD'] = dict['rmUD']
        dict['last_lmUD'] = dict['lmUD']
        dict['last_rmLR'] = dict['rmLR']
        dict['last_lmLR'] = dict['lmLR']
        dict['last_mTrig'] = dict['mTrig'] 
        dict['last_rTrig'] = dict['rTrig'] 
        dict['last_lTrig'] = dict['lTrig']    

        dict['aBut'] = aBut
        dict['bBut'] = bBut
        dict['xBut'] = xBut
        dict['yBut'] = yBut
        dict['lBump'] = lBump
        dict['rBump'] = rBump
        dict['bkBut'] = bkBut
        dict['stBut'] = stBut
        dict['xbBut'] = xbBut
        dict['ltsBut'] = ltsBut
        dict['rtsBut'] = rtsBut
        dict['ldPad'] = ldPad
        dict['rdPad'] = rdPad
        dict['udPad'] = udPad
        dict['ddPad'] = ddPad
        dict['rUD'] = rUD
        dict['lUD'] = lUD
        dict['rLR'] = rLR
        dict['lLR'] = lLR
        dict['rmUD'] = rmUD
        dict['lmUD'] = lmUD
        dict['rmLR'] = rmLR
        dict['lmLR'] = lmLR
        dict['mTrig'] = mTrig
        dict['rTrig'] = rTrig
        dict['lTrig'] = lTrig
        dict['last_joy_con'] = joy_con

    else:
        dict['aBut'] = 0
        dict['bBut'] = 0
        dict['xBut'] = 0
        dict['yBut'] = 0
        dict['lBump'] = 0
        dict['rBump'] = 0
        dict['bkBut'] = 0
        dict['stBut'] = 0
        dict['xbBut'] = 0
        dict['ltsBut'] = 0
        dict['rtsBut'] = 0
        dict['ldPad'] = 0
        dict['rdPad'] = 0
        dict['udPad'] = 0
        dict['ddPad'] = 0
        dict['rUD'] = 0
        dict['lUD'] = 0
        dict['rLR'] = 0
        dict['lLR'] = 0
        dict['rmUD'] = 0
        dict['lmUD'] = 0
        dict['rmLR'] = 0
        dict['lmLR'] = 0
        dict['mTrig'] = 0
        dict['rTrig'] = 0
        dict['lTrig'] = 0

        dict['last_aBut'] = 0
        dict['last_bBut'] = 0
        dict['last_xBut'] = 0
        dict['last_yBut'] = 0
        dict['last_lBump'] = 0
        dict['last_rBump'] = 0
        dict['last_bkBut'] = 0
        dict['last_stBut'] = 0
        dict['last_xbBut'] = 0
        dict['last_ltsBut'] = 0
        dict['last_rtsBut'] = 0
        dict['last_ldPad'] = 0
        dict['last_rdPad'] = 0
        dict['last_udPad'] = 0
        dict['last_ddPad'] = 0
        dict['last_rUD'] = 0
        dict['last_lUD'] = 0
        dict['last_rLR'] = 0
        dict['last_lLR'] = 0
        dict['last_rmUD'] = 0
        dict['last_lmUD'] = 0
        dict['last_rmLR'] = 0
        dict['last_lmLR'] = 0
        dict['last_mTrig'] = 0
        dict['last_rTrig'] = 0
        dict['last_lTrig'] = 0

        #dict['last_joy_con'] = 0    

    if dict['driving_reset'] == True:
        dict['driving_reset'] = False
        level = dict['level']

        scenelist = bge.logic.getSceneList()
        if level in scenelist:
            level = scenelist[level]
            level.resume()
            print('resuming', level)
            cube = level.objects['control_cube.002']
            #cube.suspendDynamics(False)
            #print(scenelist)
            cube.restoreDynamics()
            print(cube.isSuspendDynamics)

#main()

