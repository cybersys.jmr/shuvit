from bge import logic

printDebug = True    #Set to True to see some basic debug info

cont = logic.getCurrentController()
own = cont.owner

gameName = "shuvit"    #Set this to the name of your game!

path = logic.expandPath(own["filePath"])

positionList = []
orientationList = []
scaleList = []
colorList = []
stateList = []
visibilityList = []
skaterAnimList = []
skaterAnimFList = []
deckAnimList = []
deckAnimFList = []
srollVolList = []
srollPitchList = []
sgrind_cementVolList = []
sgrind_cementPitchList = []
sgrind_railVolList = []
sgrind_railPitchList = []
spopsoundList = []
slandsoundList = []
sdeckvisList = []
positiontdList = []
orientationtdList = []
srevertsoundList = []

allDataList = []
allDataPath = path+ own['npc_replay_name']

      

class readData:
                                   
    def loadAll(own):
#        
#        global allDataList
#        global positionList
#        global orientationList
#        global scaleList
#        global colorList
#        global stateList
#        global visibilityList
#        global skaterAnimList
#        global skaterAnimFList
#        global deckAnimList
#        global deckAnimFList 
#        global srollVolList
#        global srollPitchList 
#        global sgrind_cementVolList
#        global sgrind_cementPitchList 
#        global sgrind_railVolList
#        global sgrind_railPitchList
#        global spopsoundList
#        global slandsoundList 
#        global sdeckvisList     
#        global positiontdList
#        global orientationtdList 
#        global srevertsoundList  

#        own['allDataList'] = []
#        own['positionList'] = []
#        own['orientationList'] = []
#        own['scaleList'] = []
#        own['colorList'] = []
#        own['stateList'] = []
#        own['visibilityList'] = []
#        own['skaterAnimList'] = []
#        own['skaterAnimFList'] = []
#        own['deckAnimList'] = []
#        own['deckAnimFList'] = []
#        own['srollVolList'] = []
#        own['srollPitchList']  = []
#        own['sgrind_cementVolList'] = []
#        own['sgrind_cementPitchList']  = []
#        own['sgrind_railVolList'] = []
#        own['sgrind_railPitchList'] = []
#        own['spopsoundList'] = []
#        own['slandsoundList']  = []
#        own['sdeckvisList'] = []
#        own['positiontdList'] = []
#        own['orientationtdList']  = []
#        own['srevertsoundList']  = []
        
        allDataPath = path+ own['npc_replay_name']    
        
        #if not allDataList:
        if own['inited'] == False:
            #own['inited'] = True
            try:
                with open(allDataPath) as all:
                    data = all.read()
                    all.close()

                    allDataList = data.split("|",22)#6)
                    own['allDataList'] = allDataList
                    loadedGameName = str(allDataList[0].split("=",1)[1]).strip()
            
                    #skaterAnimList = allDataList[7].split()
                    #skaterAnimFList = allDataList[8].split()                    
                    
                    try:
                        if loadedGameName == gameName:

#                            positionList = allDataList[1].split()
#                            orientationList = allDataList[2].split()
#                            scaleList = allDataList[3].split()
#                            colorList = allDataList[4].split()
#                            stateList = allDataList[5].split()
#                            visibilityList = allDataList[6].split()
#                            skaterAnimList = allDataList[7].split()
#                            skaterAnimFList = allDataList[8].split()
#                            deckAnimList = allDataList[9].split()
#                            deckAnimFList = allDataList[10].split()
#                            srollVolList = allDataList[11].split()
#                            srollPitchList = allDataList[12].split()
#                            sgrind_cementVolList = allDataList[13].split()
#                            sgrind_cementPitchList = allDataList[14].split()
#                            sgrind_railVolList = allDataList[15].split() 
#                            sgrind_railPitchList = allDataList[16].split()
#                            spopsoundList = allDataList[17].split()
#                            slandsoundList = allDataList[18].split()
#                            sdeckvisList = allDataList[19].split()
#                            positiontdList = allDataList[20].split()
#                            orientationtdList = allDataList[21].split() 
#                            srevertsoundList = allDataList[22].split()
#                            
                            own['positionList'] = allDataList[1].split()
                            own['orientationList'] = allDataList[2].split()
                            own['scaleList'] = allDataList[3].split()
                            own['colorList'] = allDataList[4].split()
                            own['stateList'] = allDataList[5].split()
                            own['visibilityList'] = allDataList[6].split()
                            own['skaterAnimList'] = allDataList[7].split()
                            own['skaterAnimFList'] = allDataList[8].split()
                            own['deckAnimList'] = allDataList[9].split()
                            own['deckAnimFList'] = allDataList[10].split()
                            own['srollVolList'] = allDataList[11].split()
                            own['srollPitchList'] = allDataList[12].split()
                            own['sgrind_cementVolList'] = allDataList[13].split()
                            own['sgrind_cementPitchList'] = allDataList[14].split()
                            own['sgrind_railVolList'] = allDataList[15].split() 
                            own['sgrind_railPitchList'] = allDataList[16].split()
                            own['spopsoundList'] = allDataList[17].split()
                            own['slandsoundList'] = allDataList[18].split()
                            own['sdeckvisList'] = allDataList[19].split()
                            own['positiontdList'] = allDataList[20].split()
                            own['orientationtdList'] = allDataList[21].split() 
                            own['srevertsoundList'] = allDataList[22].split()             
                            
                            
                            

                            positionList = own['positionList'
                            ]
                            orientationList = own['orientationList']
                            scaleList = own['scaleList']
                            colorList = own['colorList']
                            stateList = own['stateList']
                            visibilityList = own['visibilityList']
                            skaterAnimList = own['skaterAnimList']
                            skaterAnimFList = own['skaterAnimFList']
                            deckAnimList = own['deckAnimList']
                            deckAnimFList = own['deckAnimFList']
                            srollVolList = own['srollVolList']
                            srollPitchList = own['srollPitchList']
                            sgrind_cementVolList = own['sgrind_cementVolList']
                            sgrind_cementPitchList = own['sgrind_cementPitchList']
                            sgrind_railVolList = own['sgrind_railVolList']
                            sgrind_railPitchList = own['sgrind_railPitchList']
                            spopsoundList = own['spopsoundList']
                            slandsoundList = own['slandsoundList']
                            sdeckvisList = own['sdeckvisList']
                            positiontdList = own['positiontdList']
                            orientationtdList = own['orientationtdList']
                            srevertsoundList = own['srevertsoundList']
                                                        
                            
                                           
                            
                            if printDebug:
                                print("Read file "+allDataPath)
                                own['inited'] = True
                    except:
                        print("error reading replay data")        
            except IOError:
                if printDebug:
                    print("Error reading "+allDataPath)

    def returnPosition(objIndex, valueIndex, own):
        positionList = own['positionList']
        if positionList:
            name = positionList[objIndex-2]   #That -2 is a little workaround to a list index out of range error.
            
            position = positionList[valueIndex-2]
            name = name.replace(" ","")
            #print(position)
            position = str(position)
            position = position.replace(","," ")
            pX = float(position.split()[0])
            pY = float(position.split()[1])
            pZ = float(position.split()[2])
            position = [pX,pY,pZ]
            #print(name, position)
            return(name, position)

    def returnOrientation(objIndex, valueIndex, own):
        orientationList = own['orientationList']
        if orientationList:
            name = orientationList[objIndex-2]
            orientation = orientationList[valueIndex-2]
            name = name.replace(" ","")
            orientation = str(orientation)
            orientation = orientation.replace(","," ")
            orientation = orientation.split()
            return(name, orientation)

    def returnScale(objIndex, valueIndex):

        if scaleList:
            name = scaleList[objIndex-2]
            scale = scaleList[valueIndex-2]
            
            name = name.replace(" ","")
            
            scale = str(scale)
            scale = scale.replace(","," ")
            
            sX = float(scale.split()[0])
            sY = float(scale.split()[1])
            sZ = float(scale.split()[2])
            
            scale = [sX,sY,sZ]

            return(name, scale)

    def returnColor(objIndex, valueIndex):

        if colorList:
            name = colorList[objIndex-2]
            color = colorList[valueIndex-2]
            
            name = name.replace(" ","")
            
            color = str(color)
            color = color.replace(","," ")
            
            cR = float(color.split()[0])
            cG = float(color.split()[1])
            cB = float(color.split()[2])
            cA = float(color.split()[3])
            
            color = [cR,cG,cB,cA]

            return(name, color)
    
    def returnState(objIndex, valueIndex):
        
        if stateList:
            name = stateList[objIndex-2]
            state = stateList[valueIndex-2]
            
            name = name.replace(" ","")
            
            state = str(state)
            state = state.replace(","," ")
            state = int(state)
            
            return(name, state)
    
    def returnVisibility(objIndex, valueIndex):
        
        if visibilityList:
            name = visibilityList[objIndex-2]
            visible = visibilityList[valueIndex-2]
            
            name = name.replace(" ","")
            
            visible = str(visible)
            visible = visible.replace(","," ")
            
            return(name, visible)
        
    def returnSkaterAnim(objIndex, valueIndex, own):  
        #print('getting anim list') 
        #print(objIndex-2)     
        skaterAnimList = own['skaterAnimList']
        #print(skaterAnimList)
        if skaterAnimList:
            name = skaterAnimList[objIndex-2]

            skaterAnim = skaterAnimList[valueIndex-2]
            name = name.replace(" ","")           
            skaterAnim = str(skaterAnim)
            skaterAnim = skaterAnim.replace(","," ") 
            if skaterAnim == 'control_cube.002':
                skaterAnim = skaterAnimList[valueIndex-3]
                name = name.replace(" ","")           
                skaterAnim = str(skaterAnim)
                skaterAnim = skaterAnim.replace(","," ") 
            #print(skaterAnim, (objIndex-2), (valueIndex-2)) 
            #print(skaterAnimList)         
            return(name, skaterAnim) 
        
    def returnSkaterAnimF(objIndex, valueIndex, own): 
        skaterAnimFList =  own['skaterAnimFList']      
        if skaterAnimFList:
            name = skaterAnimFList[objIndex-2]
            skaterAnimF = skaterAnimFList[valueIndex-2]
            name = name.replace(" ","")
            skaterAnimF = str(skaterAnimF)
            skaterAnimF = skaterAnimF.replace(","," ")
            #print('sOi', objIndex, 'sVi', valueIndex)
            return(name, skaterAnimF)  
        
    def returnDeckAnim(objIndex, valueIndex, own): 
        #print(objIndex-2)   
        deckAnimList = own['deckAnimList']
        if deckAnimList:
            name = deckAnimList[objIndex-2]
            deckAnim = deckAnimList[valueIndex-2]
            name = name.replace(" ","")
            deckAnim = str(deckAnim)
            deckAnim = deckAnim.replace(","," ")
            if deckAnim == 'control_cube.002':
                deckAnim = deckAnimList[valueIndex-3]
                name = name.replace(" ","")
                deckAnim = str(deckAnim)
                deckAnim = deckAnim.replace(","," ")
            #print(deckAnim, (objIndex-2), (valueIndex-2))
            #print('dOi', objIndex, 'dVi', valueIndex)            
            return(name, deckAnim) 
        
    def returnDeckAnimF(objIndex, valueIndex, own):
        deckAnimFList = own['deckAnimFList']
        if deckAnimFList:
            name = deckAnimFList[objIndex-2]
            deckAnimF = deckAnimFList[valueIndex-2]
            name = name.replace(" ","")
            deckAnimF = str(deckAnimF)
            deckAnimF = deckAnimF.replace(","," ")            
            return(name, deckAnimF)
###
    def returnSrollVol(objIndex, valueIndex):
        if srollVolList:
            name = srollVolList[objIndex-2]
            srollVol = srollVolList[valueIndex-2]
            name = name.replace(" ","")
            srollVol = str(srollVol)
            srollVol = srollVol.replace(","," ")
            srollVol = float(srollVol)
            return(name, srollVol)
        
    def returnSrollPitch(objIndex, valueIndex):
        if srollPitchList:
            name = srollPitchList[objIndex-2]
            srollPitch = srollPitchList[valueIndex-2]
            name = name.replace(" ","")
            srollPitch = str(srollPitch)
            srollPitch = srollPitch.replace(","," ")
            srollPitch = float(srollPitch)
            return(name, srollPitch)                             ####
###
    def returnSgrind_cementVol(objIndex, valueIndex):
        if sgrind_cementVolList:
            name = sgrind_cementVolList[objIndex-2]
            sgrind_cementVol = sgrind_cementVolList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_cementVol = str(sgrind_cementVol)
            sgrind_cementVol = sgrind_cementVol.replace(","," ")
            sgrind_cementVol = float(sgrind_cementVol)
            return(name, sgrind_cementVol)
        
    def returnSgrind_cementPitch(objIndex, valueIndex):
        if sgrind_cementPitchList:
            name = sgrind_cementPitchList[objIndex-2]
            sgrind_cementPitch = sgrind_cementPitchList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_cementPitch = str(sgrind_cementPitch)
            sgrind_cementPitch = sgrind_cementPitch.replace(","," ")
            sgrind_cementPitch = float(sgrind_cementPitch)
            return(name, sgrind_cementPitch)                             ####              
###
    def returnSgrind_railVol(objIndex, valueIndex):
        if sgrind_railVolList:
            name = sgrind_railVolList[objIndex-2]
            sgrind_railVol = sgrind_railVolList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_railVol = str(sgrind_railVol)
            sgrind_railVol = sgrind_railVol.replace(","," ")
            sgrind_railVol = float(sgrind_railVol)
            return(name, sgrind_railVol)
        
    def returnSgrind_railPitch(objIndex, valueIndex):
        if sgrind_railPitchList:
            name = sgrind_railPitchList[objIndex-2]
            sgrind_railPitch = sgrind_railPitchList[valueIndex-2]
            name = name.replace(" ","")
            sgrind_railPitch = str(sgrind_railPitch)
            sgrind_railPitch = sgrind_railPitch.replace(","," ")
            sgrind_railPitch = float(sgrind_railPitch)
            return(name, sgrind_railPitch)                             ####
    def returnSpopsound(objIndex, valueIndex):
        if spopsoundList:
            name = spopsoundList[objIndex-2]
            spopsound = spopsoundList[valueIndex-2]
            name = name.replace(" ","")
            spopsound = str(spopsound)
            spopsound = spopsound.replace(","," ")
            spopsound = int(spopsound)
            return(name, spopsound)
    def returnSlandsound(objIndex, valueIndex):
        if slandsoundList:
            name = slandsoundList[objIndex-2]
            slandsound = slandsoundList[valueIndex-2]
            name = name.replace(" ","")
            slandsound = str(slandsound)
            slandsound = slandsound.replace(","," ")
            slandsound = int(slandsound)
            return(name, slandsound) 
    def returnSdeckvis(objIndex, valueIndex):
        if sdeckvisList:
            name = sdeckvisList[objIndex-2]
            sdeckvis = sdeckvisList[valueIndex-2]
            name = name.replace(" ","")
            sdeckvis = str(sdeckvis)
            sdeckvis = sdeckvis.replace(","," ")
            sdeckvis = int(sdeckvis)
            return(name, sdeckvis) 
        
    def returntdPosition(objIndex, valueIndex):
        
        if positiontdList:
            name = positiontdList[objIndex-2]   #That -2 is a little workaround to a list index out of range error.
            position = positiontdList[valueIndex-2]
            name = name.replace(" ","")
            position = str(position)
            position = position.replace(","," ")
            pX = float(position.split()[0])
            pY = float(position.split()[1])
            pZ = float(position.split()[2])
            position = [pX,pY,pZ]
            return(name, position)

    def returntdOrientation(objIndex, valueIndex):
        if orientationtdList:
            name = orientationtdList[objIndex-2]
            orientation = orientationtdList[valueIndex-2]
            name = name.replace(" ","")
            orientation = str(orientation)
            orientation = orientation.replace(","," ")
            orientation = orientation.split()
            return(name, orientation)     
        
    def returnSrevertsound(objIndex, valueIndex):
        if srevertsoundList:
            name = srevertsoundList[objIndex-2]
            srevertsound = srevertsoundList[valueIndex-2]
            name = name.replace(" ","")
            srevertsound = str(srevertsound)
            srevertsound = srevertsound.replace(","," ")
            srevertsound = int(srevertsound)
            return(name, srevertsound)           
                       
###                

    def getLengthPos(own):
        positionList = own['positionList'] 
        if positionList:
            return(len(positionList))

    def getLengthOri(own):
        orientationList = own['orientationList']
        if orientationList:
            return(len(orientationList))

    def getLengthSca():
        if scaleList:
            return(len(scaleList))

    def getLengthCol():
        if colorList:
            return(len(colorList))
    
    def getLengthSta():
        if stateList:
            return(len(stateList))
    
    def getLengthVis():
        if visibilityList:
            return(len(visibilityList))
    def getLengthSkaterAnim(own):
        skaterAnimList = own['skaterAnimList']
        if skaterAnimList:
            return(len(skaterAnimList)) 
    def getLengthSkaterAnimF(own):
        skaterAnimFList = own['skaterAnimFList']
        if skaterAnimFList:
            return(len(skaterAnimFList)) 
    def getLengthDeckAnim(own):
        deckAnimList = own['deckAnimList']
        if deckAnimList:
            return(len(deckAnimList)) 
    def getLengthDeckAnimF(own):
        deckAnimFList = own['deckAnimFList']
        if deckAnimFList:
            #print(len(deckAnimFList), 'deckanimflisht')
            return(len(deckAnimFList))
    def getLengthSrollVol():
        if srollVolList:
            return(len(srollVolList)) 
    def getLengthSrollPitch():
        if srollPitchList:
            return(len(srollPitchList)) 
    def getLengthSgrind_cementVol():
        if sgrind_cementVolList:
            return(len(sgrind_cementVolList)) 
    def getLengthSgrind_cementPitch():
        if sgrind_cementPitchList:
            return(len(sgrind_cementPitchList))
    def getLengthSgrind_railVol():
        if sgrind_railVolList:
            return(len(sgrind_railVolList)) 
    def getLengthSgrind_railPitch():
        if sgrind_railPitchList:
            return(len(sgrind_railPitchList))
    def getLengthSpopsound():
        if spopsoundList:
            return(len(spopsoundList)) 
    def getLengthSlandsound():
        if slandsoundList:
            return(len(slandsoundList)) 
    def getLengthSdeckvis():
        if sdeckvisList:
            return(len(sdeckvisList))   
    def getLengthtdPos():
        if positiontdList:
            return(len(positiontdList))
    def getLengthtdOri():
        if orientationtdList:
            return(len(orientationtdList))
    def getLengthSrevertsound():
        if srevertsoundList:
            return(len(srevertsoundList))    
             
                                                                                                    