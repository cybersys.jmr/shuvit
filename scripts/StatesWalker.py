import utils
import bge
import random
from mathutils import Vector

#====================================     

def mark_path(path, y):
    iter_ = 0
    for x in path:
        pm = bge.logic.getCurrentScene().addObject('path_marker', y.obj, 0)
        pm.worldPosition = path[iter_]    
        iter_ += 1
        if iter_ == 1:
            pm.color = [0,1,0,.4]
        if iter_ == (len(path) ):
            pm.color = [1,0,0,.4]
        if iter_ == (len(path) +1):
            pm.color = [1,0,1,.4]    
        y.path_display.append(pm)

def clear_markers(self):
    for x in self.FSM.owner.path_display:
        try:
            x.endObject()
        except:
            pass
            
def get_ground_ray(self):
    Axis = 2
    Distance = -10
    end = self.obj.worldPosition + (self.obj.worldOrientation.col[Axis]*Distance)
    start = self.obj.worldPosition.copy()
    ground_ray = self.obj.rayCast(end, start, 6,'', 1, 0)
    return ground_ray

def set_height(self):
    ground_ray = get_ground_ray(self)
    target_height = 0.9
    hitpoint = ground_ray[1]
    try:
        dist = self.obj.getDistanceTo(hitpoint)
        if dist < target_height:
            self.obj.worldPosition.z += target_height - dist
            self.obj.linearVelocity.z = 0
        self.obj.linearVelocity.y *= .1
    except:
        pass

def align_to_road(self):
    ground_ray = get_ground_ray(self)
    try:
        self.obj.alignAxisToVect(ground_ray[2], 2, .15)
    except:
        pass
    
def find_new_parking(self):
    potentials = []
    for x in self.manager.idle_spots:
        if x.status == 'available':
            potentials.append(x)
    for x in potentials:
        min_dist = 45
        dist = self.obj.getDistanceTo(x.obj)
        if dist < min_dist:
            potentials.remove(x)

    if len(potentials) > 0:
        new_parking = random.choice(potentials)
        path2 = self.manager.navmesh2.queue_path(self.start_empty.obj.worldPosition, new_parking.obj.worldPosition, self)
        return new_parking, path2
    else:
        self.FSM.FSM.ToTransition('toEnterParallelPark')

def get_lane_point(self):
    self.point = self.path[self.path_index]
    if self.point != self.last_lane_point:
        v = Vector([self.last_lane_point.x - self.point.x, self.last_lane_point.y - self.point.y, 0])
        tv = v.normalized()
        nv = Vector([-tv.y, tv.x, 0]) #rotate 90 degrees
        self.last_lane_point = self.lane_point
        self.lane_point = self.point + self.manager.lane_position * nv
    else:
        pass

def goal_reached(self):
    print(self.target.type, 'type')
    if self.target.type == 'corner':
        print('this is a corner')
        self.FSM.FSM.ToTransition('toDance1')
    else:
        self.FSM.FSM.ToTransition('toEnterParallelPark')

def update_point(self):
    if self.path_index >= (len(self.path)): #at the end
        goal_reached(self)
        #self.FSM.FSM.ToTransition('toEnterParallelPark')
    else: #do normal path
        check_dist = 2.5
        if self.path_index == len(self.path) - 1:
            check_dist = 1.0
        dist = self.obj.getDistanceTo(self.lane_point)
        self.point = self.path[self.path_index]
        if dist < check_dist:
            get_lane_point(self)   
            if self.path_index > (len(self.path)):
                pass
            else:
                self.path_index += 1
         
def align_to_point(self):
    v = self.obj.getVectTo(self.lane_point)[1]
    v.z = 0
    self.obj.alignAxisToVect(v, 0, .1)

def delta_to_vect(self):
    v = self.obj.getVectTo(self.lane_point)[1]
    delta = self.last_lane_point - self.lane_point
    delta = delta.cross(v)
    delta_mult = -.1
    mult = 1.0
    deltamove = delta[2] * delta_mult

    #check if in front
    local = self.obj.worldOrientation.inverted() * (self.lane_point - self.obj.worldPosition) 
    f = deltamove * 50
    if local < 0:
        f *= -1
    self.obj.applyForce([0, f, 0], True)
            
def apply_gas(self):
    if self.obj.linearVelocity.x < self.speed_targ:
        self.obj.applyForce([self.speed_inc, 0, 0], True)

def walk_anim(self):
    if self.obj.linearVelocity.x > .5:
        arm = self.obj.children['npc']
        #arm.playAction('g_walk2', 1,62, layer=1, play_mode=0, speed=.5)
        arm.playAction('npc_walk', 1,30, layer=1, play_mode=0, speed=.5, blendin=10) 


def starting_mod(self):
    path2 = self.path
    path2.append(self.target.obj.worldPosition) 
    path2.append(self.target.obj.worldPosition)        

#====================================     

State = type("State", (object,), {})
#====================================     
class State(object):
    def __init__(self, FSM):
        self.FSM = FSM
        self.timer = 0
        self.startTime = 0
    def Enter(self):
        self.timer = 0
        self.startTime = 0
    def Execute(self):
        print('Executing')
    def Exit(self):
        print('Exiting')
#==================================== 
            
class Example(State):
    def __init__(self,FSM):
        super(Example, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        #self.FSM.owner.obj.worldPosition = self.FSM.owner.start_empty.obj.worldPosition
        #print('physics resumed')
        super(Example, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #print('doing example')
        
    def Exit(self):
        pass

class ExitParallelPark(State):
    def __init__(self,FSM):
        super(ExitParallelPark, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        set_height(self.FSM.owner)
        #self.FSM.owner.obj.restorePhysics()
        #self.FSM.owner.obj.restoreDynamics()
        self.FSM.owner.obj.linearVelocity = [0,0,0]
        self.FSM.owner.path_index = 0
        try:
            self.FSM.owner.point = self.FSM.owner.path[self.FSM.owner.path_index]
        except:
            self.FSM.ToTransition('toEnterParallelPark')    
        self.FSM.owner.target.status = 'targetted'
        self.FSM.owner.start_empty.status = 'available'
        if self.FSM.owner.start_empty.type == 'corner':
            self.FSM.owner.obj.children['npc'].stopAction(1)
        else:
            self.FSM.owner.obj.children['npc'].playAction('npc_sit', 90,1, layer=1, play_mode=0, speed=.5, blendin=10)
        starting_mod(self.FSM.owner)
        print('physics resumed')
        super(ExitParallelPark, self).Enter()        
        
    def Execute(self):
        set_height(self.FSM.owner)
        self.FSM.stateLife += 1
        if self.FSM.stateLife > 30:
            v = self.FSM.owner.obj.getVectTo(self.FSM.owner.path[0])
            self.FSM.owner.obj.alignAxisToVect(v[1], 0, .01)
            self.FSM.owner.obj.alignAxisToVect([0,0,1], 2, 1)
            if self.FSM.stateLife > 220:
                self.FSM.ToTransition('toNavigateToTarget')

    def Exit(self):
        pass        

#====================================  
            
class EnterParallelPark(State):
    def __init__(self,FSM):        
        super(EnterParallelPark, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        #self.FSM.owner.obj.worldPosition = self.FSM.owner.target.obj.worldPosition.copy()
        #self.FSM.owner.obj.worldOrientation = self.FSM.owner.target.obj.worldOrientation.copy()
        #self.FSM.owner.obj.applyMovement([0, 0, 0], True)
        self.FSM.owner.target.status = 'in_use'
        #self.FSM.owner.obj.worldPosition.z += .9
        self.FSM.owner.active = False
        self.FSM.owner.start_empty = self.FSM.owner.target
        self.FSM.owner.last_point = self.FSM.owner.target.obj.worldPosition.copy()
        self.FSM.owner.lane_point = self.FSM.owner.target.obj.worldPosition.copy()
        self.FSM.owner.last_lane_point = self.FSM.owner.target.obj.worldPosition.copy()
        self.FSM.owner.point = self.FSM.owner.target.obj.worldPosition.copy()
        self.FSM.owner.path_index = 0
        self.FSM.owner.path = None
        self.lerp_len = 90
        self.s_p = self.FSM.owner.obj.worldPosition.copy()
        self.s_o = self.FSM.owner.obj.worldOrientation.copy()
        self.FSM.owner.obj.linearVelocity = [0, 0, 0]
        self.e_p = self.FSM.owner.target.obj.worldPosition.copy()
        self.e_o = self.FSM.owner.target.obj.worldOrientation.copy()
        #self.FSM.owner.obj.children['npc'].playAction('npc_sit', 1,90, layer=1, play_mode=0, speed=.5, blendin=10)
        set_height(self.FSM.owner)
        super(EnterParallelPark, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        
        if self.FSM.stateLife > self.lerp_len:
            #pass
            self.FSM.owner.manager.walkers_active.remove(self.FSM.owner)
            if self.FSM.owner.start_empty.type == 'corner':
                self.FSM.owner.obj.children['npc'].stopAction(1)
            else:
                self.FSM.owner.obj.children['npc'].playAction('npc_sit', 1,90, layer=1, play_mode=0, speed=.5, blendin=10)

        else:

            l = self.s_p.lerp(self.e_p, (self.FSM.stateLife / self.lerp_len))
            o = self.s_o.lerp(self.e_o, (self.FSM.stateLife / self.lerp_len))
            #self.FSM.owner.obj.worldPosition.x = lerp()
            self.FSM.owner.obj.worldPosition.x = l.x
            self.FSM.owner.obj.worldPosition.y = l.y
            self.FSM.owner.obj.worldOrientation = o
        #     v = self.FSM.owner.obj.getVectTo(self.FSM.owner.target.obj.worldPosition)[1]
        #     v.z = 0
        #     self.FSM.owner.obj.alignAxisToVect(v, 0, .15)
        #     align_to_road(self.FSM.owner)


        #     print('linvel', self.FSM.owner.obj.linearVelocity)
        #     if self.FSM.owner.obj.linearVelocity.x < self.FSM.owner.speed_targ:
                
        #         self.FSM.owner.obj.applyForce([self.FSM.owner.speed_inc, 0, 0], True)

        # if self.FSM.stateLife == 360:
        #     #self.FSM.owner.manager.walkers_active.remove(self.FSM.owner)
        #     pass
        #     #self.FSM.owner.obj.suspendDynamics()
        #     #self.FSM.owner.obj.suspendPhysics()
    def Exit(self):
        pass

#==================================== 

class NavigateToTarget(State):
    def __init__(self,FSM):
        super(NavigateToTarget, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        
        super(NavigateToTarget, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        update_point(self.FSM.owner)

        align_to_point(self.FSM.owner)
        align_to_road(self.FSM.owner)
        set_height(self.FSM.owner)
        delta_to_vect(self.FSM.owner)
        apply_gas(self.FSM.owner)
        walk_anim(self.FSM.owner)

        if self.FSM.stateLife > 30 * 180 * 2:
            self.FSM.ToTransition('toEnterParallelPark')

    def Exit(self):
        pass        

#==================================== 

class Activate(State):
    def __init__(self,FSM):
        super(Activate, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1
        super(Activate, self).Enter()        
        
    def find_target(self):
        pass

    def drive_to_point(self):
        pass
        
    def Execute(self):
        self.FSM.stateLife += 1

    def Exit(self):
        pass

#====================================  

class RequestPath(State):
    def __init__(self,FSM):
        super(RequestPath, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1

        self.FSM.owner.target, self.FSM.owner.path = find_new_parking(self.FSM.owner)
        self.FSM.owner.path_index = 0
        super(RequestPath, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1

    def Exit(self):
        pass

#====================================  

class HitBySkater(State):
    def __init__(self,FSM):
        super(HitBySkater, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1

        #self.FSM.owner.target, self.FSM.owner.path = find_new_parking(self.FSM.owner)
        #self.FSM.owner.path_index = 0
        self.FSM.owner.obj.children['npc'].playAction('npc_ran_into', 1,50, layer=1, play_mode=0, speed=.5, blendin=10) 
        super(HitBySkater, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #print('doing hit state')
        dict = bge.logic.globalDict
        v = self.FSM.owner.obj.getVectTo(dict['cc'])[1]
        v.z = 0
        self.FSM.owner.obj.alignAxisToVect(v, 0, .05)
        if self.FSM.stateLife == (50 * 2):
            self.FSM.owner.obj.children['npc'].playAction('npc_argue', 1,210, layer=1, play_mode=1, speed=.5, blendin=10) 
        if self.FSM.stateLife > ((50 + 210) * 2):    
            self.FSM.ToTransition('toEnterParallelPark')
            self.FSM.owner.obj.children['npc'].playAction('npc_walk', 1,30, layer=1, play_mode=1, speed=.5, blendin=10) 
    def Exit(self):
        pass

#====================================  

class WalkingHitBySkater(State):
    def __init__(self,FSM):
        super(WalkingHitBySkater, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1

        #self.FSM.owner.target, self.FSM.owner.path = find_new_parking(self.FSM.owner)
        #self.FSM.owner.path_index = 0
        self.FSM.owner.obj.children['npc'].playAction('npc_ran_into', 1,50, layer=1, play_mode=0, speed=.5, blendin=10) 
        super(WalkingHitBySkater, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        #print('doing hit state')
        #self.FSM.owner.arm.playAction('npc_walk', 1,30, layer=1, play_mode=0, speed=.5) 
        dict = bge.logic.globalDict
        v = self.FSM.owner.obj.getVectTo(dict['cc'])[1]
        v.z = 0
        self.FSM.owner.obj.alignAxisToVect(v, 0, .05)
        if self.FSM.stateLife == (50 * 2):
            self.FSM.owner.obj.children['npc'].playAction('npc_argue', 1,210, layer=1, play_mode=1, speed=.5, blendin=10) 
        if self.FSM.stateLife > ((50 + 210) * 2):
            print('resume path')
            self.FSM.ToTransition('toNavigateToTarget')
    def Exit(self):
        pass


#====================================  

class Dance1(State):
    def __init__(self,FSM):
        super(Dance1, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1

        #self.FSM.owner.target, self.FSM.owner.path = find_new_parking(self.FSM.owner)
        #self.FSM.owner.path_index = 0
        self.FSM.owner.obj.children['npc'].playAction('npc_dance_1', 10,110, layer=1, play_mode=1, speed=.5, blendin=10) 
        super(Dance1, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        if self.FSM.stateLife > 1200:
            #self.FSM.owner.obj.worldPosition = self.FSM.owner.target.obj.worldPosition
            self.FSM.owner.start_empty = self.FSM.owner.target
            self.FSM.ToTransition('toGoToSleep')
            #self.FSM.ToTransition('toEnterParallelPark')
    def Exit(self):
        pass

#====================================  

class GoToSleep(State):
    def __init__(self,FSM):
        super(GoToSleep, self).__init__(FSM)    
        
    def Enter(self):
        self.FSM.stateLife = 1

        self.FSM.owner.obj.children['npc'].playAction('npc_dance_1', 10,110, layer=1, play_mode=1, speed=.5, blendin=10) 
        super(GoToSleep, self).Enter()        
        
    def Execute(self):
        self.FSM.stateLife += 1
        if self.FSM.owner.start_empty.type == 'corner':
            self.FSM.owner.obj.children['npc'].playAction('npc_dance_1', 1,1, layer=1, play_mode=1, speed=.5, blendin=20) 
            if self.FSM.stateLife > 30:
                self.FSM.ToTransition('toRequestPath')    
        if self.FSM.owner.start_empty.type == 'bench':
            pass
    def Exit(self):
        pass
