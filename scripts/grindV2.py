"""controls grind physics and sends results to controller2.py for setting the grind animations."""

import bge
import math
import mathutils
from mathutils import Vector

def main():


    cont = bge.logic.getCurrentController()
    own = cont.owner
    player = cont.owner
    scene = bge.logic.getCurrentScene()
    dict = bge.logic.globalDict
    grindEmpty = scene.objects['grindEmpty']
    grindDar = grindEmpty.sensors['grindDar2']
    rGround = cont.sensors['r_Ground']
    control_bottom = scene.objects['control_bottom']
    grindTouch = control_bottom.sensors['grindCol_bottom']
    invertCol = cont.sensors['invertCol']
    invertObjs = invertCol.hitObjectList
    invert_on = own['invert_on']
    detectedObjects = grindDar.hitObjectList
    lastObject = own['grindTrackObj']
    nearestObject = None
    minDist = None
    test = own["test"]
    grindold = own["grindOld"]
    lgobj = own["lGobj"]
    countdown = own['grindCountdown']
    ray = cont.sensors["gRay"] # get the ray sensor attached to the controller named GuardDuty
    gt_cd = own["gt_cd"]
    pos = ray.hitPosition # get position where the game object hit by ray
    linVelocity = own.getLinearVelocity(False)
    linVelocity2 = own.getLinearVelocity(True)
    pos[2] = pos[2] + 1
    norm = ray.hitNormal
    STANCE = own["stance"]
    orientation = own.worldOrientation
    test = ray.hitObject
    xyz = own.worldOrientation.to_euler()
    rotz = math.degrees(xyz[2])
    roty = math.degrees(xyz[1])
    roty = abs(roty)
    sub = 0
    align_strength = .1#.3
    align_strength2 = .2
    touched = grindTouch.triggered
    lgf = own['last_grind_frame']
    gsf = own['grind_start_frame']
    framenum = own['framenum']
    last_ground_frame = own['lF_ground_frame']
    frames_since_ground = framenum - last_ground_frame
    frames_since_grind = framenum - lgf
    frames_grinding = framenum - gsf
    #if framenum - lgf > 60:
    minus = framenum - gsf
    grindspeed_rail = .996 #multiplied by current speed
    grindspeed_cement = .995 #multiplied by current speed
    grindDar_timer = own["grindDar_timer"]
    last_grindDar = 0
    last_manual = own["manual"]
    joinit = 0
    rot = own.getAxisVect([0.0, 0.0, 1.0])
    dropin = own['dropinTimer']
    nearestObject = None
    last_hit = own['grindDar_hit']
    jump_timer = own['jump_timer']
    grind_jump = own['grind_jump']
    lastGround = own['lF_ground2']
    OG_PLAYER_POS = own.worldPosition
    grindvect = None
    grindyvect = None
    skipcol = 0





    def get_vert_rot(own, object):

        if 'inited' not in object:
            object['inited'] = True

            for mesh in object.meshes:
                red_verts = []
                green_verts = []
                for m_index in range(len(mesh.materials)):
                    for v_index in range(mesh.getVertexArrayLength(m_index)):
                        vertex = mesh.getVertex(m_index, v_index)
                        if vertex.color[0] > .8:
                            loc = object.worldTransform * vertex.XYZ
                            red_verts.append(loc.freeze())
                        if vertex.color[1] > .8:
                            loc = object.worldTransform * vertex.XYZ
                            green_verts.append(loc.freeze())
                           
            red_verts = set(red_verts)
            #for v in red_verts:
                #print(v, 'red_vert')
            green_verts = set(green_verts)               
            object['red_verts'] = red_verts
            object['green_verts'] = green_verts
            
            size_red = len(object['red_verts'])
            kd_red = mathutils.kdtree.KDTree(size_red)
            size_green = len(object['green_verts'])
            kd_green = mathutils.kdtree.KDTree(size_green)    

            for i, v in enumerate(object['red_verts']):
                kd_red.insert(v, i)
            for i, v in enumerate(object['green_verts']):
                kd_green.insert(v, i)

            kd_red.balance()
            kd_green.balance() 
            object['kd_red'] = kd_red
            object['kd_green'] = kd_green    
            #print('kd built -------')
            #print(own['kd_red'])
        # Find the closest points to the player
        co_find = control_bottom.worldPosition
        found_red = object['kd_red'].find_n(co_find, 2)
        found_green = object['kd_green'].find_n(co_find, 1)    
        
        primary = Vector(found_red[0][0])
        secondary = Vector(found_red[1][0])

        lineVector = secondary - primary
        lineVector2 = primary - secondary
        if found_green != []:
            lv_green = Vector(found_green[0][0])
            
        eul = Vector((1, 0, 0)).rotation_difference(lineVector).to_euler()        
        
        te = scene.objects['temp_empty']
        

        rotation = te.worldOrientation.to_euler()
        if abs(rotation.z) > 3:
            te.applyRotation([0, 0, 1.570796*2], True)
        
        xyz = te.localOrientation.to_euler()
        xyz[0] = math.radians(0)
        te.localOrientation = xyz.to_matrix()

        if lineVector == Vector([0.0, 0.0, 0.0]):
            #print('vector is empty')
            pass
        else:
            te.alignAxisToVect(lineVector, 0, 1)
        
        te.worldPosition = primary

        if found_green != []:
            local = te.worldOrientation.inverted() * (lv_green - te.worldPosition)
            if local.y < 0:
                #print('flip vector')
                eul = Vector((1, 0, 0)).rotation_difference(lineVector2).to_euler() 
                if lineVector2 == Vector([0.0, 0.0, 0.0]):
                    #print('linVector2 is empty')
                    pass
                else:
                    te.alignAxisToVect(lineVector2, 0, 1)
                #print('align2', lineVector2)
                te.worldPosition = secondary
            else:
                #print('align1', lineVector)  
                pass


        #flip z axis

        #print(local, 'local')
        
        myhead = te.worldOrientation.col[2]
        worldUp = Vector([0,0,1])
        #worldY = Vector([0,1,0])
        dot_p = myhead.dot(worldUp)

        if dot_p > 0.9:
            pass
        else:
            #print('flip grind object rotation')
            te.applyRotation([1.570796*2, 0, 0], True)




        return eul 













    #print(detectedObjects)
    if grindDar.positive:
        get_vert_rot(own, grindDar.hitObjectList[0])

    try:
        no_grind_pull = own['no_grind_pull']
    except:
        own['no_grind_pull'] = 0
        no_grind_pull = 0

    since_jumped = framenum - own['last_jump_frame']
    #no grind
    no_grind = 1
    if jump_timer > 50:
        own['no_grind_timer'] = 0
    if own['no_grind_timer'] > 0:
        no_grind = 0
        own['no_grind_timer'] -= 1

    if (own['fak_nmanual'] == 1 or own['reg_nmanual'] == 1 or own['fak_manual'] == 1 or own['reg_manual'] == 1):
        manual = 1
    else:
        manual = 0

    own["nogrindsound"] = 0
    no_grind = 1
    
    if (framenum - own['last_vert_frame'] < 40) and own['vert'] == 0:
        no_grind = 1
        #print('no_grind = 1 last vert = ', (framenum - own['last_vert_frame']))
    if dict['rBump'] == 1 or (framenum - own['last_vert_frame'] > 40):
        no_grind = 0

    if own['LAST_GRIND']:
        no_grind = 0

    if grindold == 1:
        no_grind = 0
    if dict['lBump'] and dict['rBump']:
        no_grind = 1
    elif (dict['lBump'] and not dict['rBump']) or (dict['rBump'] and not dict['lBump']):
        no_grind = 0       
#---------###########___###########-----------
#start new                     #######
##----------##############----------############

#grindvect func
    grindvect = None
    grindyvect = None
    #lastobj = own["grindTouchobj"]
    grindpos = own["grindpos"]
    player_e = own.worldOrientation.to_euler()
    player_rotz = math.degrees(player_e[2])
    velxb = linVelocity2.x
    player_e = own.worldOrientation.to_euler()

    ogrot = own.orientation
    ogrot = player_e


    #print(touched, 'touched')

    if (framenum - gsf) > 15:
        velx = linVelocity2.x
        vely = linVelocity2.y
        #print("limit speed")
    else:
        velx = linVelocity2.x
        vely = linVelocity2.y
    if abs(own['pregrind_vely']) > abs(vely) and own['LAST_GRIND'] == 0:
        #convely = own['pregrind_vely']
        #print('changing convely')
        convely = vely
    else:
        convely = vely

    def grindvect(obj):
        STANCE = own['stance']
        if obj != None:
            grinder_e = obj.worldOrientation.to_euler()
            grinder_rotz = math.degrees(grinder_e[2])
            rot = player_rotz - grinder_rotz
        if rot >= 0 and rot < 45:
            grindvect = "pos"
        if rot >= 45 and rot < 90:
            grindvect = "pos"
            grindyvect = "pos"
        if rot >= 90 and rot < 135:
            grindvect = "neg"
            grindyvect = "neg"
        if rot >= 135 and rot < 180:
            grindvect = "neg"
        if rot >= 180 and rot < 225:
            grindvect = "neg"
        if rot >= 225 and rot < 270:
            grindvect = "neg"
            grindyvect = "pos"
        if rot >= 270 and rot < 315:
            grindvect = "pos"
            grindyvect = "neg"
        if rot >= 315:
            grindvect = "pos"
        if rot < 0 and rot >= -45:
            grindvect = "pos"
        if rot < -45 and rot >= -90:
            grindvect = "pos"
            grindyvect = "neg"
        if rot < -90 and rot >= -135:
            grindvect = "neg"
            grindyvect = "pos"
        if rot < -135 and rot >= -180:
            grindvect = "neg"
        if rot < -180 and rot >= -225:
            grindvect = "neg"
        if rot < -225 and rot >= -270:
            grindvect = "neg"
            grindyvect = "neg"
        if rot < -270 and rot >= -315:
            grindvect = "pos"
            grindyvect = "pos"
        if rot < -315:
            grindvect = "pos"

        rot = round((rot * .01), 1)
        rot *= 100

        #print(own['inverting'])
        #if frames_grinding > 20 and own['inverting'] == False:
        #print('obj', grindDar.hitObjectList[0])
        if 'edge' in grindDar.hitObjectList[0]:
            #print('do not fucking change stance')




            if frames_grinding > 20:    
                #print("setting stance")
                if (rot == 90 or rot == -270) and STANCE == True:
                    #print("90 fak stance")
                    own['stance'] = 1
                    STANCE = 1
                if (rot == 90 or rot == -270) and STANCE == False:
                    #print("90 fak stance")
                    own['stance'] = 1
                    STANCE = 1
                if (rot == -90 or rot == 270) and STANCE == True:
                    #print("-90 reg stance")
                    own['stance'] = 0
                    STANCE = 0
                if (rot == -90 or rot == 270) and STANCE == False:
                    #print("-90 reg stance")
                    own['stance'] = 0
                    STANCE = 0

    def grindrotvel(obj):
        joinit = 0
        grinder_e = obj.worldOrientation.to_euler()
        grinder_rotz = math.degrees(grinder_e[2])
        grinder_vect = obj.getAxisVect([1, 0, 0])
        grinder_vectz = obj.getAxisVect([0, 0, 1])
        rot = player_rotz - grinder_rotz
        rayhit = ray.hitObject
        own['grind_rot'] = rot
        vect = obj.getAxisVect([1.0, 0.0, 0.0])
        xyzb = obj.worldOrientation.to_euler()
        rotzb = math.degrees(xyzb[2])
        grindpos = "None"
        align = "None"
        sub_converted = 0
        rotz = math.degrees(xyz[2])
        roty = math.degrees(xyz[1])
        bsrot = [0.0, 0.0, 1.570796327]
        negbsrot = [0.0, 0.0, -1.570796327]
        zvel = own.linearVelocity
        zvel = zvel.z * .95

        if grindold == 0:
            if rotz < 0:
                rotz = rotz + 360
            if rotzb < 0:
                rotzb = rotzb + 360
            if rotz > rotzb:
                sub_converted = rotz - rotzb
            if rotz < rotzb:
                sub_converted = rotzb - rotz

        #z align

        stre = .15#.075#.15
        #player.alignAxisToVect(grinder_vectz, 2, stre)

        if rot >= 0 and rot < 45:
            player.alignAxisToVect(grinder_vect, 0, align_strength)
            grindvect = "pos"
            #player.applyForce([0, 0, 0], True)
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-1")


        elif rot >= 45 and rot < 90:
            player.alignAxisToVect([-grinder_vect.y, grinder_vect.x, 0], 0, align_strength)
            grindvect = "pos"
            align = "pos"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            grindyvect = "pos"

        elif rot >= 90 and rot < 135:
            grinder_vect = grinder_vect * -1
            player.alignAxisToVect([grinder_vect.y, -grinder_vect.x, 0], 0, align_strength)
            grindvect = "neg"
            align = "neg"
            grindyvect = "neg"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            #print("-bs2, don't switch?")

        elif rot >= 135 and rot < 180:
            player.alignAxisToVect(-grinder_vect, 0, align_strength)
            grindvect = "neg"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-2")

        elif rot >= 180 and rot < 225:
            player.alignAxisToVect(-grinder_vect, 0, align_strength)
            grindvect = "neg"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-3")

        elif rot >= 225 and rot < 270:
            player.alignAxisToVect([grinder_vect.y, -grinder_vect.x, 0], 0, align_strength)
            grindvect = "neg"
            align = "pos"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            grindyvect = "pos"
            #print("bs3")

        elif rot >= 270 and rot < 315:
            player.alignAxisToVect([grinder_vect.y, -grinder_vect.x, 0], 0, align_strength)
            grindvect = "pos"
            align = "neg"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            grindyvect = "neg"
            #print("-bs4")

        elif rot >= 315:
            player.alignAxisToVect(grinder_vect, 0, align_strength)
            grindvect = "pos"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-4")

#-------------------------------------------------------------
        elif rot < 0 and rot >= -45:
            player.alignAxisToVect(grinder_vect, 0, align_strength)
            grindvect = "pos"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-5")
        elif rot < -45 and rot >= -90:
            grinder_vect = grinder_vect * -1
            player.alignAxisToVect([-grinder_vect.y, grinder_vect.x, 0], 0, align_strength)
            grindvect = "pos"
            align = "neg"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            #print("-bs5")
            grindyvect = "neg"
        elif rot < -90 and rot >= -135:
            player.alignAxisToVect([grinder_vect.y, -grinder_vect.x, 0], 0, align_strength)
            grindvect = "neg"
            align = "pos"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            grindyvect = "pos"
            #print("bs6")
        elif rot < -135 and rot >= -180:
            player.alignAxisToVect(-grinder_vect, 0, align_strength)
            grindvect = "neg"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-6")
        elif rot < -180 and rot >= -225:
            player.alignAxisToVect(-grinder_vect, 0, align_strength)
            grindvect = "neg"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-7")
        elif rot < -225 and rot >= -270:
            grinder_vect = grinder_vect * -1
            player.alignAxisToVect([grinder_vect.y, -grinder_vect.x, 0], 0, align_strength)
            grindvect = "neg"
            align = "neg"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            grindyvect = "neg"
            #print("-bs7")
        elif rot < -270 and rot >= -315:
            player.alignAxisToVect([-grinder_vect.y, grinder_vect.x, 0], 0, align_strength)
            grindvect = "pos"
            align = "pos"
            player.setLinearVelocity([0, convely, zvel], 1)
            grindpos = "reg_board"
            grindyvect = "pos"
            #print("bs8")
        elif rot < -315:
            player.alignAxisToVect(grinder_vect, 0, align_strength)
            grindvect = "pos"
            player.setLinearVelocity([velxb, 0.0, zvel], 1)
            grindpos = "reg_5050"
            #print("50-8")

        own['grindpos'] = grindpos
        own['grindvect'] = grindvect
        own['grindervect'] = grinder_vect
        try:
            own['grindyvect'] = grindyvect

        except:
            pass

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    def grind(obj):
        try:
            grindyvect = own['grindyvect']
        except:
            grindyvect = 0
        grindvect = own['grindvect']

        # if 'rail' in obj:
        #     own["grindSound"] = "rail"
        # else:
        #     own["grindSound"] = "concrete"
        own['lGobj'] = obj

        grinding = 1

        framenum = own['framenum']
        lastairframe = own['lF_air_frame']
        jumpside = 0
        if (framenum - lastairframe) < 10:
            player_pos = own['jumpPos']
            jumpside = 1
        else:
            player_pos = OG_PLAYER_POS
        player_pos = own.worldPosition
        try:
            if 1 == 1:
                grinder_pos = obj.worldPosition
                worldVect = [1, 0, 0]
                vect = obj.getAxisVect(worldVect)
                grinder_axis = [0, 1, 0]
                try:
                    delta = player_pos - grinder_pos
                except:
                    pass
                delta = delta.cross(vect)
                if delta[2] >= 0:
                    grindside = "right"

                else:
                    grindside = "left"
                edge = 0
                if 'edge' in obj:
                    edge = 1
                #print('grind on', grindside, edge)
                if (delta[2] > .00001 or delta[2] < -.00001) and (delta[2] < 1.8 or delta[2] < -1.8):
                    rotation = own.worldOrientation[2] - obj.worldOrientation[2]
                    player_e = own.worldOrientation.to_euler()
                    player_rotz = math.degrees(player_e[2])
                    grinder_e = obj.worldOrientation.to_euler()
                    grinder_rotz = math.degrees(grinder_e[2])
                    deltamove = delta[2] * .3

                    #5050
                    if own["grindpos"] == "reg_5050":
                        #print("missing delta")
                        if (jumpside == 1 or jumpside == 0) and grindvect == "pos":
                            move = [0, deltamove, 0]
                            #print("1deltamove", deltamove)
                            own.applyMovement(move, True)
                        if (jumpside == 1 or jumpside == 0) and grindvect == "neg":
                            move = [0, -deltamove, 0]
                            #print("2deltamove -", deltamove)
                            own.applyMovement(move, True)
                    #board
                    if own["grindpos"] == "reg_board":
                        if grindvect == "neg":
                            if (jumpside == 1 or jumpside == 0) and grindyvect == "pos":
                                move = [-deltamove, 0, 0]
                                #print("3deltamove", deltamove)
                                own.applyMovement(move, True)
                            if (jumpside == 1 or jumpside == 0) and grindyvect == "neg":
                                move = [deltamove, 0, 0]
                                #print("4deltamove -", deltamove)
                                own.applyMovement(move, True)
                        if grindvect == "pos":
                            if (jumpside == 1 or jumpside == 0) and grindyvect == "pos":
                                move = [deltamove, 0, 0]
                                #print("5deltamove", deltamove)
                                own.applyMovement(move, True)
                            if (jumpside == 1 or jumpside == 0) and grindyvect == "neg":
                                move = [-deltamove, 0, 0]
                                own.applyMovement(move, True)
                own['grindside'] = grindside
        except:
            print('something in grinding is wrong')
            #pass
        #speed limit
        velo = own.getLinearVelocity(True)
        touchobj = obj
        if touchobj != None and gt_cd == 0:
            if 'rail' in touchobj:
                newvelx = velo.x * grindspeed_rail
                newvely = velo.y * grindspeed_rail
            else:
                newvelx = velo.x * grindspeed_cement
                newvely = velo.y * grindspeed_cement
            player.setLinearVelocity([newvelx, newvely, velo.z], 1)
            




            player_rot = own.worldOrientation[2]
            grinder_rot = touchobj.worldOrientation[2]
            player_rot = own.getAxisVect([0.0, 0.0, 1.0])
            xyz3 = own.worldOrientation.to_euler()
            player_rot = math.degrees(xyz3[2])
            xyz4 = touchobj.worldOrientation.to_euler()
            grinder_rot = math.degrees(xyz4[2])
            if player_rot < 0:
                player_rot = abs(player_rot) + 180
            if grinder_rot < 0:
                grinder_rot = abs(grinder_rot) + 180
            subtraction = player_rot - grinder_rot

    def invertpos(detectedObjects):
        minDist = None
        nearestObject = None
        # for obj in detectedObjects:
        #     dist = own.getDistanceTo(obj)
        #     if (minDist is None or dist < minDist):
        #         nearestObject = obj
        #         minDist = dist
        # if nearestObject != None:
        #     own.linearVelocity.x = 0
        #     own.linearVelocity.y = 0

    #if grindDar.positive == False and grindTouch.triggered and grindold == 0 and dropin == 0 and grind_jump == 0 and own["grindoutturn"] == 0 and gt_cd == 0 and manual == 0 and no_grind == 0 and nearestObject != lastObject and own['grindjumpturn'] == 0 and own['gt_cd2'] == 0 and own['air_mover'] == False:
        #print("touching, no dar")
        #pass
    #print(grindold, "grindold")
    skipper = 0
    if (grindDar.positive or grindTouch.positive) and no_grind == 0:
        if grindDar.positive:
            detectedObjects = grindDar.hitObjectList
            dist = 0.0
            for obj in detectedObjects:
                #dist = own.getDistanceTo(obj)
                if (minDist is None or dist < minDist):
                    #nearestObject = obj
                    minDist = dist
        #elif grindTouch.positive:
            #nearestObject = grindTouch.hitObject
            #dist = .7
            #print('using touch object')
        
        nearestObject = scene.objects['temp_empty']
        dist = own.getDistanceTo(nearestObject)    

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        #init grind

        #print('invert on', invert_on)
        if invert_on:
            invertpos(detectedObjects)
            own['inverting'] = True
            print('calling invert funct')
        else:
            own['inverting'] = False
        grind_dist = .7#.8#.6
        skipper = 0
        #print(lastobj.worldPosition)
        #if dist < grind_dist and dropin == 0 and manual == 0 and no_grind == 0 and own['grindoutturn'] == 0 and own['manual'] == 0 and own['gt_cd2'] < 40 and invert_on == False and own['air_mover'] == False and no_grind_pull == 0 and own['last_invert'] == False and own['grindjumpturn'] == 0 and grind_jump == 0 and (own['airup'] == 0 or dict['rBump'] == 1 or dict['lBump'] == 1):
        if dist < grind_dist and dropin == 0 and manual == 0 and no_grind == 0 and own['grindoutturn'] == 0 and own['manual'] == 0 and own['gt_cd2'] < 40 and own['air_mover'] == False and no_grind_pull == 0 and own['grindjumpturn'] == 0 and grind_jump == 0 and (own['airup'] == 0 or dict['rBump'] == 1 or dict['lBump'] == 1):            
            #print("grinding")
            grind_height_dif = (own.worldPosition[2] - nearestObject.worldPosition[2]) - .287

            if grind_height_dif > -.05:#02:

                if own['grinder_rot'] != None:
                    grot = nearestObject.getAxisVect([1, 0.0, 0.0]).x
                    dif = (grot - own['grinder_rot'])
                    if dif > .8 or dif < - .8:
                        print('skipping grind, angle is', dif)
                        skipper = 1

                own['grinder_rot'] = nearestObject.getAxisVect([1, 0, 0]).x
                if skipper != 1:
                    grindvect(nearestObject)
                    grindrotvel(nearestObject)
                    grind(nearestObject)
                    grindold = True
                    own['grindold_timer'] = 0
                    own["grindHit"] = True
                    own['grindnew_timer'] += 1
                    if own['grindnew_timer'] < 1:
                        if grindTouch.triggered:
                            nearpos = nearestObject.worldPosition.copy
                            nearpos.z += .2975
                            if hitNormal != None:
                                stre = .15
                                #own.alignAxisToVect(hitNormal, 2, stre)
                            #own.worldPosition = [nearpos.x, nearpos.y, nearpos.z + .2975]
                            own.worldPosition.lerp(own.worldPosition, nearpos, .0025)
                            print('new moving world')
                    
    else:
        own['grindold_timer'] += 1
        own["grindHit"] = False
        own['inverting'] = False

    if own['grindold_timer'] > 5:
        grindold = False
        own['grind_start_frame'] = framenum
        own["grindSound"] = None

    if own['grindHit'] == False:
        own['grindnew_timer'] = 0

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#if you are touching grinder and last_manual is off keep manual off
    if (own['fak_nmanual'] == 1 or own['reg_nmanual'] == 1 or own['fak_manual'] == 1 or own['reg_manual'] == 1) and lastGround == True and grindold == False:
        manual = 1
        if last_manual == 0 and grindTouch.triggered == True:
            manual = 0
        own["nogrindsound"] = 1
    grindhit = own["grindHit"]

    if abs(own['pregrind_vely']) > abs(vely) and own['LAST_GRIND'] == 0:
        convely = own['pregrind_vely']
    else:
        convely = vely

    off = 0

    if touched == True:
        own["grindTouchobj"] = grindTouch.hitObject
    else:
        own["grindTouchobj"] = None

    if ray.triggered and dropin == 0:
        lgobj = ray.hitObject
        own['lGobj'] = lgobj
        obj = ray.hitObject
        linVelocity3 = own.getLinearVelocity(True)
        grindpos2 = own["grindpos"]
        if obj is not None:
            if 'rail' in ray.hitObject:
                own["grindSound"] = "rail"
            else:
                own["grindSound"] = "concrete"
        own['lGobj'] = lgobj
    else:
        lgobj = "empty"
        own['lGobj'] = lgobj
    def timer():
        countdown = own["grindCountdown"]
        if touched == 1:
            countdown = 20
        else:
            countdown = countdown - 1
        own['grindCountdown'] = countdown
    def aligntimer():
        if own['grindOld'] == 0 and ray.triggered == 1:
            own['aligntimer'] = 20
        if own['grindOld'] == 1 and ray.triggered == 1:
            own['aligntimer'] = own['aligntimer'] - 1
        if own['grindOld'] == 0 and ray.triggered == 0:
            own['aligntimer'] = 0

    def invertpos(xyz):
        minDist = None
        nearestObject = None
        print('doing invert funct')
        # for obj in invertObjs:
        #     dist = own.getDistanceTo(obj)
        #     if (minDist is None or dist < minDist):
        #         nearestObject = obj
        #         minDist = dist
        # vect = nearestObject.getAxisVect([1.0, 0.0, 0.0])
        # own.worldPosition = [nearestObject.worldPosition.x, nearestObject.worldPosition.y, nearestObject.worldPosition.z +.1]

    def set_pregrind_vel():
        if grindDar.triggered == False and ray.triggered == False:
            own['pregrind_velx'] = linVelocity2.x
            own['pregrind_vely'] = linVelocity2.y
            own['pregrind_velz'] = linVelocity2.z

    def stop_bounce():
        if grindTouch.triggered and no_grind == 0 and grind_jump == 0 and jump_timer < 30:
            linVelocity4 = own.getLinearVelocity(True)
            if linVelocity4.z > 0:
                own.setLinearVelocity([linVelocity4.x, linVelocity4.y, 0], 1)

    if jump_timer < 30:
        own['grind_jump'] = 0
    if grindold == False and ray.triggered:
        own['grind_start_frame'] = own['framenum']

    if grindDar_timer > 0:
        grindDar_timer -= 1
    if ray.triggered == False and touched == True  and own['footplant_on'] == False and manual == 0 and own['gt_cd2'] < 55 and own['grindoutturn'] == 0 and own['grindjumpturn'] == 0:
        obj = grindTouch.hitObject
        try:
            nearpos = obj.worldPosition
        except:
            pass

    def slow_roty():

        constraint = cont.actuators['gConstraint']
        if (own['framenum'] - own['grind_start_frame'] < 40) and touched == 1:
            cont.activate(constraint)
        else:
            cont.deactivate(constraint)
    timer()
    aligntimer()
    stop_bounce()
    slow_roty()


    own['grindOld'] = grindold
    own['test'] = test
    own['grindTrackObj'] = nearestObject
    own['grindDar_hit'] = grindDar.positive
    own['lastTouched'] = touched
    own['grindTouch'] = grindTouch.triggered
    own["grindDar_timer"] = grindDar_timer
    own["last_grindDar"] = last_grindDar
    own['last_roty'] = roty
    linVelocity3 = own.getLinearVelocity(True)
    set_pregrind_vel()
    own['last_z'] = own.worldPosition.z
    own["manual"] = manual
