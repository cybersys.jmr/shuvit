import bpy
 
class OBJECT_PT_pushpuller(bpy.types.Header):
    bl_label = "Push Puller"
    bl_space_type = "TEXT_EDITOR"
    bl_region_type = "HEADER"
    bl_context = "object"
 
    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="PHYSICS")
 
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        split = row.split(percentage=0.5)
        col_left = split.column()
        col_right = split.column()
        col_right.operator("object.puller", text="Pull All")            
        col_left.operator("object.pusher", text="Push All")
        
class OBJECT_OT_pusher(bpy.types.Operator):
    bl_label = "Pusher"
    bl_idname = "object.pusher"
    bl_description = "Push text data block changes"
 
    def execute(self, context):
        area = bpy.context.area
        for text in bpy.data.texts:
            area.spaces[0].text = text
            if text.filepath != '' and text.is_dirty:
                bpy.ops.text.save() 
                print("Saving: ", text.filepath)       
        self.report({'INFO'}, "Performing push")
        return {'FINISHED'}

class OBJECT_OT_pullerer(bpy.types.Operator):
    bl_label = "Puller"
    bl_idname = "object.puller"
    bl_description = "Pull text data block changes"
 
    def execute(self, context):
        area = bpy.context.area
        self.report({'INFO'}, "Performing pull")
        for text in bpy.data.texts:
            area.spaces[0].text = text
            if text.filepath != '':
                try:
                    bpy.ops.text.reload()
                except:
                    print("error reloading: ", text.filepath)    
            if text.filepath != '' and text.is_dirty:
                bpy.ops.text.save()
        return {'FINISHED'}

def register():
    bpy.utils.register_module(__name__)
 
def unregister():
    bpy.utils.unregister_module(__name__)
 
if __name__ == "__main__":
    register()