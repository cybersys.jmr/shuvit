import bpy

#context & scene for testing
context = bpy.context
scene = context.scene

#active pose bone as example
#bone = context.active_pose_bone
bone2 = None
# other armature other bone
arm2 = scene.objects.get("Char4")
shoe = scene.objects.get("Char4:Shoes02.L")

bone_list = ["foot.ik.R", "foot.ik.L", "hips", "root", "spine", "spine-1", "chest", "chest-1", "clavicle.L", "clavicle.R", "deltoid.L", "deltoid.R", "upper_arm.fk.L", "upper_arm.fk.R",  "forearm.fk.L", "forearm.fk.R", "hand.fk.L", "hand.fk.R", "neck", "head", "master"]

for b in bone_list:
    bone = arm2.pose.bones.get(b)


    if arm2 is not None:
        bone2 = arm2.pose.bones.get("Shoes02.L")
        #print('got', bone2)
    # give it a copy rotation constraint
    if bone is not None:
        crc = bone.constraints.new('COPY_ROTATION')
        crc.target = shoe
        crc.name = "rd_cr"
        crc.influence = 0
        
        crc = bone.constraints.new('COPY_LOCATION')
        crc.target = shoe
        crc.name = "rd_cl"
        crc.influence = 0    