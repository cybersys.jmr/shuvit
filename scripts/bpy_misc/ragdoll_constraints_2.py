import bpy
context = bpy.context



BoneList = ["upper_arm.fk.R", "upper_arm.fk.L", "head", "neck", "clavicle.R", "clavicle.L", "deltoid.R", "deltoid.L", "forearm.fk.R", "forearm.fk.L", "hand.fk.R", "hand.fk.L", "chest", "spine-1", "spine", "root", "hips", "foot.ik.R", "foot.ik.L"]
TargetBone = "Char4:Shoes02.R"


for BoneName in BoneList:
    bpy.data.objects["Char4"].data.bones[BoneName].select = True    
    

print(context.selected_pose_bones)

for pb in context.selected_pose_bones:
    cl = (pb.constraints.get("rd_cl")
            or pb.constraints.new(type='COPY_LOCATION'))
    # set some properties
    cl.name = "rd_cl"
    cl.target = bpy.data.objects[TargetBone]
    cl.influence = 0

    cr = (pb.constraints.get("rd_cr")
            or pb.constraints.new(type='COPY_ROTATION'))
    # set some properties
    cr.name = "rd_cr"
    cr.target = bpy.data.objects[TargetBone]
    cr.influence = 0



