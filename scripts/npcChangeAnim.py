#npcChangeAnim

import bge
import random

a_starts = ['20secA.dat', '20secE.dat', 'shopSaEa40a.dat', 'shopSaEb40a.dat', 'shopSaEb20a.dat', 'shopSaEa30a.dat']
a_ends = ['20secA.dat', '20secE.dat', 'shopSaEa40a.dat', 'shopSbEa50a.dat', 'shopSbEa60a.dat', 'shopSaEa30a.dat']
b_starts = ['shopSbEb40sec.dat', 'shopSbEa50a.dat', 'shopSbEa60a.dat', 'shopSbEb50a.dat']
b_ends = ['shopSbEb40sec.dat', 'shopSaEb40a.dat', 'shopSaEb20a.dat', 'shopSbEb50a.dat']

def main(cont):
    own = cont.owner
    own['npc_playback'] = False
    
    if own['npc_replay_name'] in a_ends:
        num = len(a_starts) - 1
        ran = random.randint(0,num)
        own['npc_replay_name'] = a_starts[ran]
    elif own['npc_replay_name'] in b_ends:
        num = len(b_starts) - 1
        ran = random.randint(0,num)
        own['npc_replay_name'] = b_starts[ran]        
    
    if own['npc_replay_name'] == '20secA.dat':
        own['replay_length'] = 2400
    if own['npc_replay_name'] == '20secE.dat':
        own['replay_length'] = 2400    
    if own['npc_replay_name'] == 'shopSaEa40a.dat':
        own['replay_length'] = 6500 
    if own['npc_replay_name'] == 'shopSaEb40a.dat':
        own['replay_length'] = 5500  
    if own['npc_replay_name'] == 'shopSbEa50a.dat':
        own['replay_length'] = 6500  
    if own['npc_replay_name'] == 'shopSbEb40sec.dat':
        own['replay_length'] = 6500 
    if own['npc_replay_name'] == 'shopSaEb20a.dat':
        own['replay_length'] = 2300     
    if own['npc_replay_name'] == 'shopSbEa60a.dat':
        own['replay_length'] = 7100 
    if own['npc_replay_name'] == 'shopSaEa30a.dat':
        own['replay_length'] = 3200 
    if own['npc_replay_name'] == 'shopSbEb50a.dat':
        own['replay_length'] = 6000                                             

    print('Playing NPC-Replay:', own['npc_replay_name'])

    
    own['rpStartLoc_set'] = False  