import bge
import colors

def main():
    
    dict = bge.logic.globalDict

    if dict['scene_inited'] == False:
        cont = bge.logic.getCurrentController()
        scene = bge.logic.getCurrentScene()
        own = cont.owner
        
        own['last_stBut'] = False
        level = dict['level']
        
        cam = scene.objects['Camera.003']
        curscene = bge.logic.getCurrentScene()
        dict['level_scene'] = curscene

        dict['overlay_fadeout'] = 1
        scenes = bge.logic.getSceneList()
        own['pre_pause_linvel'] = 0.0

        objList = scene.objects

        parent = objList["control_cube.002"]
        dict['cc'] = parent
        # combine child's shape with parent's
        compound = True
        obj = scene.objects['Char4']
        deck_obj = scene.objects['deck_arm']
        dict['p1c'] = own
        dict['p1'] = obj
        dict['p1d'] = deck_obj
        # child is solid
        ghost = False
        # set parent
        obj.setParent( parent, compound, ghost) 
        parent.worldPosition.z += .75
        parent.applyRotation([0,0,3.14], True) 
      
        scene.gravity = [0,0,dict['gravity']]    
        for i in scenes:
            if i.name == "main":
                main = i
        
        for i in scenes:
            if i.name == "fade_out":
                print('ending fade_out', scenes)
                scene2 = i
                scene2.end()  
         
        if dict['bc'] == 1:
            cont.activate(cam.actuators["bc"])

        else:
            cont.activate(cam.actuators["bc_off"])  
        if dict['hdr'] == 1:
            cont.activate(cam.actuators["hdr"])

        else:
            cont.activate(cam.actuators["hdr_off"])
                                
        if dict['ao'] == 1:
            cont.activate(cam.actuators["ao"])

        else:
            cont.activate(cam.actuators["ao_off"])                    

        if dict['dof_on'] == 1:
            cont.activate(cam.actuators["DOF"])

        else:
            cont.activate(cam.actuators["DOF_off"])
            
        if dict['bloom_on'] == 1:
            cont.activate(cam.actuators["Bloom"])

        else:
            cont.activate(cam.actuators["bloom_off"])
            
        #print(dict['shadow_on'], 'shadow_on')
           
        #if dict['shadow_on'] == 1:
           #bge.render.setGLSLMaterialSetting("shadows", 1) 
        #else:
           #bge.render.setGLSLMaterialSetting("shadows", 0)  

        if dict['music_player'] == 1:
            dict['music_start'] =  True   
            print('starting music') 
        else:
            print('not starting music', dict['music_player'])        
        
        dict['sun_lights'] = []    
        dict['ambient_lights'] = []    
        for x in scene.lights:
            if 'hemi' in x:
                if 'down' in x:
                    x.energy = dict['ambient_strength']
                    dict['ambient_lights'].append(x)
                else:
                    x.energy = dict['ambient_strength'] * .7    
                    dict['ambient_lights'].append(x)

            if 'sun' in x:
                x.energy = dict['sun_strength']
                dict['sun_lights'].append(x)
        #scene.world.envLightEnergy = dict['ambient_strength']   
        #scene.world.envLightEnergy = 0
        
        cam['BC_BRIGHTNESS'] = dict['BC_BRIGHTNESS'] 
        cam['BC_CONTRAST'] = dict['BC_CONTRAST']
        cam['bc'] = dict['bc']  
        cam['hdr'] = dict['hdr']
        cam['avgL'] = dict['avgL']
        cam['HDRamount'] = dict['HDRamount']
        cam['ao'] = dict['ao']  
        cam['onlyAO'] = dict['onlyAO']
        cam['aowidth'] = dict['aowidth']
        cam['aoradius'] = dict['aoradius']  
        cam['ambient_strength'] = dict['ambient_strength']
        cam['sun_strength'] = dict['sun_strength']
        cam['sun_pos_x'] = dict['sun_rot_x']
        cam['sun_pos_y'] = dict['sun_rot_y']
        #cam['shadow_on'] = dict['shadow_on']
        cam['dof_on'] = dict['dof_on']
        cam['bloom_on'] = dict['bloom_on'] 
        cam['fxaa'] = dict['fxaa']
        cam['FXAA_SPAN_MAX'] = dict['FXAA_SPAN_MAX']     
        own['grindnew_timer'] = 0   
        own['last_ramp'] = 0
        own['requestAction'] = ''
        own['l_actionState'] = ''
        own['actionState'] = ''
        own['transition_timer'] = 0
        own['transitioning'] = 0
        own['driving'] = False
        dict['last_driving'] = False
        own['last_roty'] = 0.0
        
        own['walk_targ_speed'] = 2
        own['walk_fast_targ_speed'] = 5
        own['walk_speed'] = 0
        own['walk_inc'] = .2
        own['walk_jump_timer'] = 0
        own['no_grind_timer'] = 0
        own['last_jump_frame'] = 0
        own['grinder_rot'] = None
        own['inc'] = 0
        own['vert'] = 0
        own['last_vert_frame'] = 0
        own['ragdoll_active'] = False
        own['rd_to_delete'] = None
        own['ragdoll_out'] = 0
        own['lland'] = ''
        own['grinding'] = False
        own['ground_since'] = 100
        own['last_transition_frame'] = 0
        #own['aState'] = 'reg_roll'
        
        colors.update_shirt_tex() 
        colors.update_truck_tex()
        
        print('scene init ran')
        dict['scene_inited'] = True


        material_list = []
        for obj in scene.objects:
            for mesh in obj.meshes:
                for material in mesh.materials:
                    material_list.append(material.name)
        for obj in scene.objectsInactive:
            for mesh in obj.meshes:
                for material in mesh.materials:
                    material_list.append(material.name)


        scene.objects['camposEmpty'].applyRotation([.3, .3, 0], True)
        material_list = set(material_list)
        print('number of materials:', len(material_list))
      