#record
from bge import logic
from mathutils import Vector
from aiSortData import *
import math

cont = logic.getCurrentController()
own = cont.owner
scene = logic.getCurrentScene()
cube = cont.owner
#obj = scene.objects["control_cube.002"]
#sound_empty = scene.objects['replay_sound_empty']
#path = logic.expandPath(own["filePath"])
dict = logic.globalDict
PAF = 0
DAF = 0

npc_index = own['npc_index']
print(dict['spawned_npc_decks'], npc_index, 'spawned deck list')
deck = dict['spawned_npc_decks'][npc_index]
trucks = dict['spawned_npc_trucks'][npc_index]
#deck = scene.objects[deck]

#print(own.children)
#deck = own.children['npc_ed_deck']
#trucks = own.children["npc_ed_trucks"]

#throw_deck_empty = scene.objects["throw_deck_empty"]
#wheel1 = trucks.children["npc_ed_rollen.000"]
#wheel2 = trucks.children["npc_ed_rollen.001"]
#wheel3 = trucks.children["npc_ed_rollen.002"]
#wheel4 = trucks.children["npc_ed_rollen.003"]

#try:
#    throw_deck = scene.objects['throw_deck']
#except:
#    throw_deck = None    

#deckact = deck.actuators["Visibility"]
#trucksact = trucks.actuators["Visibility"]
#wheel1act = wheel1.actuators["Visibility"]
#wheel2act = wheel2.actuators["Visibility"]
#wheel3act = wheel3.actuators["Visibility"]
#wheel4act = wheel4.actuators["Visibility"]

class getData:



    def savePosition(obj, cut):
        position = [Vector(obj.worldPosition)[0],
            Vector(obj.worldPosition)[1],
            Vector(obj.worldPosition)[2]]
        position = str(position).strip("[]")
        position = position.replace(", ",",")
        writeData.addPosition(obj, position, cut)
     
    def saveOrientation(obj, cut): 
        orientation = [Vector(obj.localOrientation.to_euler())[0],
            Vector(obj.localOrientation.to_euler())[1],
            Vector(obj.localOrientation.to_euler())[2]]
        orientation = str(orientation).strip("[]")
        orientation = orientation.replace(", ",",")     
        writeData.addOrientation(obj, orientation, cut)
                    
                                              
              
def loadData(cont, own):
    
    npc_index = own['npc_index']
    #print(dict['spawned_npc_decks'], npc_index, 'spawned deck list')
    deck = dict['spawned_npc_decks'][npc_index]
    trucks = dict['spawned_npc_trucks'][npc_index]    
    skater = dict['spawned_npcs'][npc_index]
    cube = own 
    #Function for loading the data from
    #the disk and setting it.
    #print('load_data', own)
    objIndex = own["objIndex"]
    #print(own['objIndex'], 'objindex')
    own["valueIndex"] = objIndex + 1
    valueIndex = own["valueIndex"]
    playbackSpeed = own["playbackSpeed"]
    loadedPosition = own["loadedPosition"]
    loadedOrientation = own["loadedOrientation"]
#    print(loadedPosition)
#    loadedScale = own["loadedScale"]
#    loadedColor = own["loadedColor"]
#    loadedVisibility = own["loadedVisibility"]
    loadedSkaterAnim = own["loadedSkaterAnim"]
    loadedSkaterAnimf = own["loadedSkaterAnimF"]
    loadedDeckAnim = own["loadedDeckAnim"]
    loadedDeckAnimf = own["loadedDeckAnimF"]
#    loadedSrollVol = own["loadedSrollVol"]
#    loadedSrollPitch = own["loadedSrollPitch"]
#    loadedSgrind_cementlVol = own["loadedSgrind_cementVol"]
#    loadedSgrind_cementPitch = own["loadedSgrind_cementPitch"]
#    loadedSgrind_railVol = own["loadedSgrind_railVol"]
#    loadedSgrind_railPitch = own["loadedSgrind_railPitch"] 
#    loadedSpopsound = own["loadedSpopsound"]            
#    loadedSlandsound = own["loadedSlandsound"] 
#    loadedSdeckvis = own["loadedSdeckvis"]  
#    loadedtdPosition = own["loadedtdPosition"]
#    loadedtdOrientation = own["loadedtdOrientation"]      
#    loadedSrevertsound = own["loadedSrevertsound"] 
       
    #skater = own.children["npc_ed_arm"]
    #deck = own.children["deck"]
    #trucks = own.children["trucks"]
    
    num = 1
    obj = own

                     
    readData.loadAll(own)    #Load the file!!!
    
    #-----Position-----#
    own["lengthPos"] = readData.getLengthPos(own)
    lengthPos = own["lengthPos"] 
    positionList = own['positionList']
    if lengthPos:
        if objIndex > lengthPos-1:
            own["objIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthPos-2

        name, position = readData.returnPosition(objIndex, valueIndex, own)
        name2, orientation = readData.returnOrientation(objIndex, valueIndex, own)
        try:
            if own['rpStartLoc_set'] == False:
                
                
                
                own['rpStartLoc'] = position
                print('setting npc_start_pos', position)
                
                own['npc_playback'] = False
                own['move'] = True
                tracker = scene.addObject('npc_tracker', own, 0)
                own['target_object_name'] = tracker
                tracker.worldPosition = position
                
                oXYZ = own.localOrientation.to_euler()

                oXYZ[0] = float(orientation[0])
                oXYZ[1] = float(orientation[1])
                oXYZ[2] = float(orientation[2])                
                tracker.localOrientation = oXYZ.to_matrix()
                rot = [ 0.0, 0.0, -1.570796327]
                #tracker.applyRotation(rot,True) 
    
                rotz = math.degrees(oXYZ[2])
                rotz -= 90
                if rotz < -180:
                    rotz = rotz + 360
                own['rpStartZ'] = rotz               
                
            else:  
                cube.worldPosition = position
                deck.worldPosition = position
                trucks.worldPosition = position
                
            if 'data_loaded' not in own:
                own['data_loaded'] = 1 
                cube.worldPosition = position
                deck.worldPosition = position
                trucks.worldPosition = position                   
                
        except Exception as e:
            print(e)
            print('npc positioning not working')
            #pass
    #-----Orientation-----#
    own["lengthOri"] = readData.getLengthOri(own)   
    lengthOri = own["lengthOri"]

    valueIndex = own['valueIndex']
    #print(valueIndex, objIndex, 'value Index')
    #if lengthPos:
    if lengthOri:
        if valueIndex > lengthOri-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthOri-2
        name, orientation = readData.returnOrientation(objIndex, valueIndex, own)
        oXYZ = own.localOrientation.to_euler()
        oXYZ[0] = float(orientation[0])
        oXYZ[1] = float(orientation[1])
        oXYZ[2] = float(orientation[2])    
        try:
            if own['rpStartLoc_set'] == True:
                own.localOrientation = oXYZ.to_matrix()
                deck.localOrientation = oXYZ.to_matrix()
                trucks.localOrientation = oXYZ.to_matrix()
                rot = [ 0.0, 0.0, -1.570796327]
                own.applyRotation(rot,True) 
            else:
                own['rpStartLoc_set'] = True
        except:
            print('npc orientation not working')
#    #-----Scale-----#
#    own["lengthSca"] = readData.getLengthSca()
#    lengthSca = own["lengthSca"]
#    
#    if lengthSca:
#    
#        if valueIndex > lengthSca-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSca-2
#       
#        name, scale = readData.returnScale(objIndex, valueIndex)
#        
#        if name in scene.objects:
#            try:
#                scene.objects[name].localScale = scale
#            except:
#                pass
#    #-----Color-----#
#    own["lengthCol"] = readData.getLengthCol()   
#    lengthCol = own["lengthCol"]
#    
#    if lengthCol:

#        if valueIndex > lengthCol-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthCol-2

#        name, color = readData.returnColor(objIndex, valueIndex)
#        
#        if name in scene.objects:
#            try:
#                scene.objects[name].color = color
#            except:
#                pass
#    #-----Visibility-----#
#    own["lengthVis"] = readData.getLengthVis()
#    lengthVis = own["lengthVis"]
#    
#    if lengthVis:

#        if valueIndex > lengthVis-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthVis-2

#        name, visible = readData.returnVisibility(objIndex, valueIndex)
#        
#        if name in scene.objects:
#            try:
#                scene.objects[name].visible = int(visible)
#            except:
#                pass
    #-----Skater Animation Name-----#
    own["lengthSkaterAnim"] = readData.getLengthSkaterAnim(own)
    lengthSkaterAnim = own["lengthSkaterAnim"]
    #print("lengthskateranim", lengthSkaterAnim)
    if lengthSkaterAnim:

        if valueIndex > lengthSkaterAnim-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSkaterAnim-2

        name, skateranim = readData.returnSkaterAnim(objIndex, valueIndex, own)
        name, PAF = readData.returnSkaterAnimF(objIndex, valueIndex, own)
        PAF = round(float(PAF), 1)
        #print(valueIndex, skateranim, PAF,  'sa')
        PAF = float(PAF)
        if skater in own.children:
            #print("name in")
            try:
                skater.stopAction(0)
                skater.stopAction(1)
                skater.stopAction(2)
                skater.stopAction(3)
                skater.stopAction(9999)
                if skater != '' and skateranim != 'control_cube.002':
                    skater.playAction(skateranim, PAF,PAF, layer=2, play_mode=1, speed=1)
                #print("Playing: ", skater, skateranim, PAF)
            except:
                print("something is wrong")
                
    #-----Deck Animation Name-----#
    own["lengthDeckAnim"] = readData.getLengthDeckAnim(own)
    lengthDeckAnim = own["lengthDeckAnim"]
    #print("lengthDeckanim", lengthDeckAnim)
    if lengthDeckAnim:

        if valueIndex > lengthDeckAnim-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthDeckAnim-2

        name, deckanim = readData.returnDeckAnim(objIndex, valueIndex, own)
        name, DAF = readData.returnDeckAnimF(objIndex, valueIndex, own)
        name = 'npc_ed_deck'

        DAF = float(DAF)
        if deck in own.children:
            #print("name in")
            try:
                if deck != '' and deckanim != 'control_cube.002':
                    deck.playAction(deckanim, DAF,DAF, layer=2, play_mode=1, speed=1)
                    trucks.playAction(deckanim, DAF,DAF, layer=2, play_mode=1, speed=1)
                #print("Playing: ", deckanim, PAF)
                #print("Playing: ", deck, deckanim, DAF)
            except:
                print("deck something is wrong")
                #pass  

##
#    #-----sroll-----#
#    own["lengthSrollVol"] = readData.getLengthSrollVol()
#    lengthSrollVol = own["lengthSrollVol"]
#    if lengthSrollVol:
#        if valueIndex > lengthSrollVol-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSrollVol-2
#        name, srollVol = readData.returnSrollVol(objIndex, valueIndex)
#        name, srollPitch = readData.returnSrollPitch(objIndex, valueIndex)    
#        if name in scene.objects:
#            try:
#                cube = scene.objects["control_cube.002"]
#                srollVol = round(srollVol, 2)
#                act = cube.actuators["sroll"]
#                if srollVol < .12:
#                    act.volume = srollVol
#                    act.pitch = srollPitch
#                act.startSound()
#            except:
#                pass
####            
##
#    #-----grind cement-----#
#    own["lengthSgrind_cementVol"] = readData.getLengthSgrind_cementVol()
#    lengthSgrind_cementVol = own["lengthSgrind_cementVol"]
#    if lengthSgrind_cementVol:
#        if valueIndex > lengthSgrind_cementVol-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSgrind_cementVol-2
#        name, sgrind_cementVol = readData.returnSgrind_cementVol(objIndex, valueIndex)
#        name, sgrind_cementPitch = readData.returnSgrind_cementPitch(objIndex, valueIndex)    
#        if name in scene.objects:
#            try:
#                cube = scene.objects["control_cube.002"]
#                sgrind_cementVol = round(sgrind_cementVol, 2)
#                act = cube.actuators["grind_cement"]
#                if sgrind_cementVol < .2:
#                    act.volume = sgrind_cementVol
#                    act.pitch = sgrind_cementPitch
#                act.startSound()
#                
#            except:
#                pass
####    
##
#    #-----grind rail-----#
#    own["lengthSgrind_railVol"] = readData.getLengthSgrind_railVol()
#    lengthSgrind_railVol = own["lengthSgrind_railVol"]
#    if lengthSgrind_railVol:
#        if valueIndex > lengthSgrind_railVol-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSgrind_railVol-2
#        name, sgrind_railVol = readData.returnSgrind_railVol(objIndex, valueIndex)
#        name, sgrind_railPitch = readData.returnSgrind_railPitch(objIndex, valueIndex)    
#        if name in scene.objects:
#            try:
#                cube = scene.objects["control_cube.002"]
#                sgrind_railVol = round(sgrind_railVol, 2)
#                act = cube.actuators["grind_rail"]
#                if sgrind_railVol < .2:
#                    act.volume = sgrind_railVol
#                    act.pitch = sgrind_railPitch
#                act.startSound()
#                #print("grindsound = ", sgrind_railVol, sgrind_railPitch)
#            except:
#                pass
####  
##
#    #-----pop sound-----#
#    own["lengthSpopsound"] = readData.getLengthSpopsound()
#    lengthSpopsound = own["lengthSpopsound"]
#    if lengthSpopsound:
#        if valueIndex > lengthSpopsound-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSpopsound-2
#        name, spopsound = readData.returnSpopsound(objIndex, valueIndex)   
#        if name in scene.objects:
#            #act = sound_empty.actuators["pop"]
#            try:
#                #cube = scene.objects[sound_empty]
#                spopsound = round(spopsound, 2)
#                act = cube.actuators["pop"]
#                #act = sound_empty.actuators["pop"]
#                if spopsound == 1:
#                    #act.volume_maximum = .7
#                    #act.is3D = True
#                    #act.distance_reference = 10.0
#                    #act.distance_maximum = 50.0
#                    act.volume = .6
#                    act.startSound()
#                #print("grindsound = ", spopsound, sgrind_railPitch)
#            except:
#                print("sound passed")
#                pass
####
##
#    #-----land sound-----#
#    own["lengthSlandsound"] = readData.getLengthSlandsound()
#    lengthSlandsound = own["lengthSlandsound"]
#    if lengthSlandsound:
#        if valueIndex > lengthSlandsound-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSlandsound-2
#        name, slandsound = readData.returnSlandsound(objIndex, valueIndex)   
#        if name in scene.objects:
#            try:
#                cube = scene.objects["control_cube.002"]
#                slandsound = round(slandsound, 2)
#                act = cube.actuators["land"]
#                if slandsound == 1:
#                    act.volume = .6
#                    act.startSound()
#                #print("grindsound = ", slandsound, sgrind_railPitch)
#            except:
#                pass
####  
####
##
#    #-----land sound-----#
#    own["lengthSdeckvis"] = readData.getLengthSdeckvis()
#    lengthSdeckvis = own["lengthSdeckvis"]
#    if lengthSdeckvis:
#        if valueIndex > lengthSdeckvis-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSdeckvis-2
#        name, sdeckvis = readData.returnSdeckvis(objIndex, valueIndex)   
#        
#        
#        if name in scene.objects:
#            try:
#                cube = scene.objects["control_cube.002"]
#                if sdeckvis == 1:
#                    #print('setting deck visible')
#                    deckact.visibility = True
#                    trucksact.visibility = True
#                    wheel1act.visibility = True
#                    wheel2act.visibility = True
#                    wheel3act.visibility = True
#                    wheel4act.visibility = True   
#                    cont.activate(deck.actuators['Visibility'])
#                    cont.activate(trucks.actuators['Visibility'])
#                    cont.activate(wheel1.actuators['Visibility'])
#                    cont.activate(wheel2.actuators['Visibility'])
#                    cont.activate(wheel3.actuators['Visibility'])
#                    cont.activate(wheel4.actuators['Visibility'])  
#                    for n in scene.objects:
#                        if 'throw_deck' in n.name and 'empty' not in n.name:
#                            n.endObject() 
##                    if 'throw_deck' in scene.objects:
#                            #print('ending td', n)
#                            cont.activate(throw_deck.actuators['end_throw_deck'])                   
#                     
#                    #throwdeck.visibility = False                               
#                else:
#                    #print('setting deck invisible')
#                    deckact.visibility = False
#                    trucksact.visibility = False
#                    wheel1act.visibility = False
#                    wheel2act.visibility = False
#                    wheel3act.visibility = False
#                    wheel4act.visibility = False   
#                    cont.activate(deck.actuators['Visibility'])
#                    cont.activate(trucks.actuators['Visibility'])
#                    cont.activate(wheel1.actuators['Visibility'])
#                    cont.activate(wheel2.actuators['Visibility'])
#                    cont.activate(wheel3.actuators['Visibility'])
#                    cont.activate(wheel4.actuators['Visibility'])  
#                    #if throw_deck == None:
#                    if 'throw_deck' not in scene.objects:
#                        #print('no throwdeck')
#                        #cont.deactivate(throw_deck.actuators['end_throw_deck']) 
#                        #throw_deck_empty.wordPosition.z = throw_deck_empty.wordPosition.z + 1
#                        cont.activate(throw_deck_empty.actuators['throw_dec_act']) 
#                        #scene.addObject('throw_deck')
#                    #throwdeck.visibility = True
#                    throw_deck.suspendDynamics()

#            except:
#                pass
###    
#    #-----Position-----#
#    own["lengthtdPos"] = readData.getLengthtdPos()
#    lengthPos = own["lengthtdPos"] 
#    
#    if lengthPos:
#        
#        if objIndex > lengthPos-1:
#            own["objIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthPos-2

#        name, position = readData.returntdPosition(objIndex, valueIndex)
#        name = 'throw_deck'
#        if name in scene.objects:
#            try:
#                scene.objects[name].worldPosition = position
#                #print('recording tdPos', position)
#            except:
#                pass
#    #-----Orientation-----#
#    own["lengthtdOri"] = readData.getLengthtdOri()   
#    lengthOri = own["lengthtdOri"]
#    
#    #if lengthPos:
#    if lengthOri:
#  
#        if valueIndex > lengthOri-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthOri-2

#        name, orientation = readData.returntdOrientation(objIndex, valueIndex)
#        name = 'throw_deck'
#        if name in scene.objects:
#            
#            oXYZ = scene.objects[name].localOrientation.to_euler()

#            oXYZ[0] = float(orientation[0])
#            oXYZ[1] = float(orientation[1])
#            oXYZ[2] = float(orientation[2])
#            
#            try:
#                #print('recording tdOri')
#                scene.objects[name].localOrientation = oXYZ.to_matrix()
#            except:
#                pass

##
#    #-----revert sound-----#
#    own["lengthSrevertsound"] = readData.getLengthSrevertsound()
#    lengthSrevertsound = own["lengthSrevertsound"]
#    if lengthSrevertsound:
#        if valueIndex > lengthSrevertsound-1:
#            own["valueIndex"] = 0
#        if objIndex < 0:
#            own["objIndex"] = lengthSrevertsound-2
#        name, srevertsound = readData.returnSrevertsound(objIndex, valueIndex)   
#        if name in scene.objects:
#            try:
#                #cube = scene.objects[sound_empty]
#                srevertsound = round(srevertsound, 2)
#                act = cube.actuators["revertSound"]
#                if srevertsound == 1:
#                    act.startSound()
#                #print("grindsound = ", spopsound, sgrind_railPitch)
#            except:
#                print("sound passed")
#                pass

                                            
                       
def main(recording_cutoff, cc):
    pass
              
            
         
def breakOut():
    pass
