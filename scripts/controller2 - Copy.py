#shuvit.org

# control script

import bge
import GameLogic
import ctypes
import math
from sys import platform
from mathutils import Vector
import mathutils
import joytimers
import sound_man

def main():
    scene = bge.logic.getCurrentScene()

    #number of frames / length of the location timers
    countdown = 20

    cont = GameLogic.getCurrentController()
    char = bge.constraints.getCharacter
    own = cont.owner
    dict = bge.logic.globalDict #Get the global dictionary

    #last frame stuff
    lGRAB_ON = own["lGRAB_ON"]
    lastgrab = own["lastGrab"]
    lastpump = own["lastPump"]
    lf_ground = own["lF_ground"]
    lastopos = own["last_Opos"]
    lastnopos = own["last_nOpos"]
    LAST_STANCE = own["last_stance"]
    LAST_GROUND = own["last_ground"]
    lastStop = own["lastStop"]
    LAST_LEFT = own["LAST_LEFT"]
    last_trick_string = dict.get('last_trick_sting')
    lasta = own["lasta"]
    lastx = own["lastx"]
    lastaf = own["lastaf"]
    lastxf = own["lastxf"]
    lgf = own['last_grind_frame']
    frame = own['framenum']
    frames_since_grinding = frame - lgf
    ground_since = own["framenum"] - own['lF_air_frame']
    gsf = frame - own['grind_start_frame']
    own['last_footplant'] = own['footplant_on']
    lastaBut_ground = own['lastaBut_ground']
    lastxBut_ground = own['lastxBut_ground']
    aBut_ground = own['aBut_ground']
    xBut_ground = own['xBut_ground']
    own['last_manual_v2'] = own['manual_v2']
    lRequest = own['requestAction']
    lAs = own['actionState']


    #current frame stuff
    GRAB_ON = own["GRAB_ON"]
    pumpon = own["Pump"]
    vib_countdown = own["vib_Countdown"]
    grindold = own['grindOld']
    LAST_GRIND = own["LAST_GRIND"]
    grindTouch = cont.sensors['grindCol_bottom']
    touched = grindTouch.triggered
    leftgrabon = own["Leftgrabon"]
    rightgrabon = own["Rightgrabon"]
    linvel = own.getLinearVelocity
    zvel = own.worldLinearVelocity.z
    own["Zvel"] = zvel
    r_ground = cont.sensors["r_Ground"]
    trigger = r_ground.triggered
    STANCE = own["stance"]
    motion = cont.actuators['MotionPump']
    linVelocity = own.getLinearVelocity(True)
    countdown = own['countdown']
    jump_timer = own['jump_timer']
    pos = own.localPosition
    rot = own.getAxisVect([0.0, 0.0, 1.0])
    gray = cont.sensors["gRay"]
    #pop = cont.actuators["pop"]
    grindHit = own["grindHit"]
    #grindCement = cont.actuators["grind_cement"]
    #grindRail = cont.actuators["grind_rail"]
    grindSound = own["grindSound"]
    skater = scene.objects["Char4"]
    deck = scene.objects["b_deck"]
    trucks = scene.objects["b_trucks"]
    wheel1 = scene.objects["wheel1"]
    wheel2 = scene.objects["wheel2"]
    wheel3 = scene.objects["wheel3"]
    wheel4 = scene.objects["wheel4"]
    turnsens = .04
    turnsens2 = .06 #air_turn_boost
    grindDar = own["grindDar_hit"]
    grindEmpty = scene.objects["grindEmpty"]
    grindDar2 = grindEmpty.sensors["grindDar2"]
    #invertCol = cont.sensors["invertCol"]
    cam = scene.objects["Camera.003"]
    freecam = scene.objects["freecam"]
    followcam = scene.objects["followcam"]
    coping_on = own["coping"]
    invert_on = own['invert_on']
    grab_type = own['grab_type']
    c_ground = cont.sensors['c_ground']
    #
    control_bottom = scene.objects['control_bottom']
    invertTouch = control_bottom.sensors['grindCol_bottom']
    wallride = own["wallride"]
    wallride_col = cont.sensors['wallride']
    wallrideL = cont.sensors['wallrideL']
    wallrideR = cont.sensors['wallrideR']
    wallrideconstL = cont.actuators['wallrideconstL']
    wallrideconstR = cont.actuators['wallrideconstR']
    
    own['pop_sound'] = 0
    own['land_sound'] = 0
    own['revert_sound'] = 0
    own['grinding'] = False
    #own['fall'] = 0

    fliplay = 3

    #setable
    MAX_VEL = dict['MAX_VEL']
    SPEEDUP = dict['SPEEDUP']
    SPEEDPUMP = dict['SPEEDPUMP']
    SPEEDPUMPFAST = dict['SPEEDPUMPFAST']
    PUMP_SPEED_SENS = dict['PUMP_SPEED_SENS']
    PUMP_SENS = dict['PUMP_SENS']
    ACCEL = dict['ACCEL']
    CRUISE = dict['CRUISE']
    COUNTDOWN = dict['COUNTDOWN'] #pump and speed stuff
    JUMPHEIGHT = dict['JUMPHEIGHT'] #775#750
    JUMPSTRENGTH = 0
    own['flip_manual_stance'] = 0
    LAND_LAYER = 100
    LAND_END = 20

    lLR = dict['lLR']
    lUD = dict['lUD']
    rLR = dict['rLR']
    rUD = dict['rUD']
    lTrig = dict['lTrig']
    rTrig = dict['rTrig']
    aBut = dict['aBut']
    bBut = dict['bBut']
    xBut = dict['xBut']
    yBut = dict['yBut']
    lBump = dict['lBump']
    rBump = dict['rBump']
    bkBut = dict['bkBut']
    stBut = dict['stBut']
    xbBut = dict['xbBut']
    ltsBut = dict['ltsBut']
    rtsBut = dict['rtsBut']
    ldPad = dict['ldPad']
    rdPad = dict['rdPad']
    udPad = dict['udPad']
    ddPad = dict['ddPad']


    #land balance
    land_overRideL = False
    land_overRideR = False
    land_overRideLb = False
    land_overRideRb = False

    joytimers.main(own)
    sr = own['jsr']
    sl = own['jsl']

    #2019 ground ray detection
    Axis = 2
    Distance = -10
    End = own.worldPosition + (own.worldOrientation.col[Axis]*Distance)
    Start = own.worldPosition.copy()
    if own['LAST_GRIND']:
        groundRay = own.rayCast(End, Start, 10,'ground', 1, 0, 0, 53247)
    else:
        groundRay = own.rayCast(End, Start, 10,'ground', 1, 0, 0, 61439)
    grindRay = own.rayCast(End, Start, 1,'grind', 1, 1, 0, 61439)
    
    if groundRay[0]:

        localHitDist = (own.worldPosition - groundRay[1]).length
        if localHitDist < .4 and own['jump_timer'] < 32:
            onGround = True
        else:
            onGround = False   
       
    else:
        #if own['LAST_GRIND'] == True:
        onGround = False
        #print('not on ground', grindRay[0], own['LAST_GRIND'])
        if grindRay[0] and own['LAST_GRIND'] == True:

            onGround = True    
    if onGround: 
        own['wallride_off'] = 0
    #print(linVelocity.y / linVelocity.x, 'linvels')
    #print(r_ground.triggered)
    try:
        balance = linVelocity.y / linVelocity.x
    except:
        balance = 0
    #print(balance)
    #if ground_since > 0 and ground_since < 5 and jump_timer < 30:
    min_speed = .8
    if ground_since > -1 and ground_since < 60 and (abs(linVelocity.x) > min_speed or abs(linVelocity.y) > min_speed):
        #print(balance, 'balance')
        sens = 2
        sens2 = .8
        sens3 = 1.5
        if balance > sens or balance < - sens:
            #fall
            #if ground_since > -1 and ground_since < 10:
            #print(own['l_actionState'], touched)
            if own['l_actionState'] != None:
                if 'land' in own['l_actionState'] and touched == False:
                    own['fall'] = 1
                    own['last_fall_frame'] = frame
            #pass
        if balance > sens3:
            #print('land2!!!!!!')
            land_overRideR = True
        elif balance < -sens3:
            #print('land3!!!!!!')
            land_overRideL = True
        elif balance > sens2:
            #print('land2!!!!!!')
            land_overRideRb = True
        elif balance < -sens2:
            #print('land3!!!!!!')
            land_overRideLb = True
    #print(linVelocity.y, '********linvel')

    #fliptricks after manuals
    if (frame - own['last_manual_frame']) < 25:
        flip_start_lay = 8
        flipspeed = .4
        own['flipspeed'] = .4
        own['flip_start_lay'] = 8
        #print("chopping flip start")
    else:
        flip_start_lay = 1
        own['flip_start_lay'] = 1
        flipspeed = .6
        own['flipspeed'] = .6
    def printplaying():
        splaying_layers = "S: "
        playing_layers = "D: "
        tplaying_layers = "T: "
        for x in range(9900):
            if skater.isPlayingAction(x):
            #if trucks.isPlayingAction(x):
            #if skater.isPlayingAction(x):
                splaying_layers += str(x)
                splaying_layers += " "
            if deck.isPlayingAction(x):
            #if trucks.isPlayingAction(x):
            #if skater.isPlayingAction(x):
                playing_layers += str(x)
                playing_layers += " "
            if trucks.isPlayingAction(x):
            #if trucks.isPlayingAction(x):
            #if skater.isPlayingAction(x):
                tplaying_layers += str(x)
                tplaying_layers += " "
        print(splaying_layers, playing_layers, tplaying_layers)

    if aBut == True and lasta == False and onGround:
        aBut_ground = True
        own['aBut_ground'] = True
    #else:
    if aBut == False:
        aBut_ground = False
        own['aBut_ground'] = False
    if xBut == True and lastx == False and onGround:
        xBut_ground = True
        own['xBut_ground'] = True
    #else:
    if xBut == False:
        xBut_ground = False
        own['xBut_ground'] = False

    def killact(layer):
        if skater.isPlayingAction(layer):
            skater.stopAction(layer)
        if deck.isPlayingAction(layer):
            deck.stopAction(layer)
        if trucks.isPlayingAction(layer):
            trucks.stopAction(layer)

    def killall():
        for x in range(9000):
            skater.stopAction(x)
            deck.stopAction(x)
            trucks.stopAction(x)

    def check_fall():
        playing_frame = 20
        #hitDistance = own.worldPosition.z - r_ground.hitPosition.z
        if skater.isPlayingAction(fliplay):
            playing_frame = skater.getActionFrame(fliplay)
        #if onGround and skater.isPlayingAction(fliplay) and own['jump_timer'] < 40 and playing_frame < 14 and playing_frame > 3:
        if onGround and skater.isPlayingAction(fliplay) and own['jump_timer'] < 45 and playing_frame < 16 and playing_frame > 3:    
            hitDistance = own.worldPosition[2] - r_ground.hitPosition[2]
            if hitDistance < dict['fall_height']:
                own['fall'] = 1
                own['last_fall_frame'] = frame
                if STANCE == 0:
                    own['requestAction'] = 'reg_air-walk_air'
                if STANCE == 1:
                    own['requestAction'] = 'fak_air-walk_air'
        if dict['aBut'] == 1 and dict['bBut'] == 1:
            own['fall'] = 1    
            own['last_fall_frame'] = frame        
    check_fall()

    def check_landing():
        lf_ground = own["lF_ground"]
        STANCE = own["stance"]
        playing_action_frame = skater.getActionFrame(LAND_LAYER)

        if ground_since == 4:
            own['no_grind'] = False
            own['jump_from_trans'] = 0
            nearestObject = None
            minDist = None
            detectedObjects = grindDar2.hitObjectList
            if grindDar == 0:
                lastheight = own["air_height"]
                pos = own.worldPosition.z
                dist = lastheight - pos
                dist = dist * 2
                if dist > 1:
                    dist = 1
                #own.actuators["land"].volume = dist
                #sact = own.actuators["land"]                
                #sact.stopSound()
                #cont.activate(own.actuators["land"])
                cam['sndmgr'].queue_sound(['land', own, cam])
                own['land_sound'] = 1
            if grindDar == 1:
                pass
            own["vib_Countdown"] = 14
            cont.activate(own.actuators["Vibration"])
            if own['manual_v2'] == 0 and grindDar == 0:
                print('landing')
                if STANCE == 0:
                    own['requestAction'] = 'reg_land'
                elif STANCE == 1:
                    own['requestAction'] = 'fak_land'

        vib_countdown = own["vib_Countdown"]
        if vib_countdown > 0:
            vib_countdown = vib_countdown - 1
            own["vib_Countdown"] = vib_countdown
        if vib_countdown == 1:
            #stopAnims()
            stance()

        #if lf_ground == False and r_ground.triggered == True and own['jump_timer'] < 40 and grindHit == False and touched == False and LAST_GRIND == False:
        #print(touched, LAST_GRIND)
        #if lf_ground == False and r_ground.triggered == True and own['jump_timer'] < 40  and LAST_GRIND == False and jump_timer != 0:
        #if lf_ground == False and r_ground.triggered == True:
        if ground_since == 3 and own['jump_timer'] < 40:
            print('---landing-b')
            if STANCE == 0:
                own['requestAction'] = 'reg_land'
            elif STANCE == 1:
                own['requestAction'] = 'fak_land'
        lf_ground = onGround
        own["lF_ground"] = lf_ground

    #air anim
    if not onGround:
        #pass
        if STANCE == 0:
            own['requestAction'] = 'reg_air'
            own['manual_v2_timer'] = 50
        if STANCE == 1:
            own['requestAction'] = 'fak_air'
            own['manual_v2_timer'] = 50
    else:
        if STANCE == 0:
            own['requestAction'] = 'reg_roll'
        if STANCE == 1:
            own['requestAction'] = 'fak_roll'

    #check manual_v2
    if (rUD > dict['man_sens_l'] and rUD < dict['man_sens_r']) or (rUD < -dict['man_sens_l'] and rUD > -dict['man_sens_r']):
    #if (rUD > .04 and rUD < .07):
        #print("zoned")
        timer = own['manual_v2_timer']
        timer = timer + 1
        if timer > 20:
            own['manual_v2'] = 1
        own['manual_v2_timer'] = timer
    if rUD < dict['man_sens_l'] and rUD > -dict['man_sens_l']:
        own['manual_v2_timer'] = 0
        own['manual_v2'] = 0
        own['manual_v2_type'] = None
    if own['manual_v2'] == 1:
        if own['last_manual_v2'] == 0:
            if STANCE == 0:
                if rUD > dict['man_sens_l'] and rUD < dict['man_sens_r']:
                    #print("reg manual")
                    own['manual_v2_type'] = 'reg manual'
                if rUD < -dict['man_sens_l'] and rUD > -dict['man_sens_r']:
                    own['manual_v2_type'] = 'reg nose manual'

            if STANCE == 1:
                if rUD > dict['man_sens_l'] and rUD < dict['man_sens_r']:
                    own['manual_v2_type'] = 'fak manual'
                    if lRequest == 'reg_air_tail to manual':
                        #print('chaaaaange init')
                        own['manual_v2_type'] = 'fak nose manual'
                if rUD < -dict['man_sens_l'] and rUD > -dict['man_sens_r']:
                    own['manual_v2_type'] = 'fak nose manual'

        else:
            #print("flip stance")
            if own['jump_stance'] == 0:
                #print('flipstance 0')
                if own['manual_v2_type'] == 'fak manual':
                    own['manual_v2_type'] = 'reg nose manual'
                if own['manual_v2_type'] == 'fak nose manual':
                    own['manual_v2_type'] = 'reg manual'
                if lAs == 'fak_air_tail to manual' and own['manual_v2_type'] == 'reg manual':
                    #print('---requesting reg nose manual')
                    own['manual_v2_type'] = 'reg nose manual'
                if lAs == 'fak_air_nose to nmanual' and own['manual_v2_type'] == 'reg nose manual':
                    #print('---requesting reg nose manual')
                    own['manual_v2_type'] = 'reg manual'
            if own['jump_stance'] == 1:
                #print('flipstance 1')
                if own['manual_v2_type'] == 'reg manual':
                    own['manual_v2_type'] = 'fak nose manual'
                if own['manual_v2_type'] == 'reg nose manual':
                    #print("asdfasdfasdfsdfasdfasdf")
                    own['manual_v2_type'] = 'fak manual'
                if lAs == 'reg_air_tail to manual' and own['manual_v2_type'] == 'fak manual':
                    #print('---requesting fak nose manual')
                    own['manual_v2_type'] = 'fak nose manual'
                if lAs == 'reg_air_nose to nmanual' and own['manual_v2_type'] == 'fak nose manual':
                    #print('---requesting fak nose manual')
                    own['manual_v2_type'] = 'fak manual'

        if STANCE == 0 and (own['manual_v2_type'] == 'fak manual' or own['actionState'] == 'fak-reg_nmanual'):
            own['manual_v2_type'] = 'reg nose manual'
        if STANCE == 1 and (own['manual_v2_type'] == 'reg manual' or own['actionState'] == 'reg-fak_nmanual'):
            own['manual_v2_type'] = 'fak nose manual'
        if STANCE == 0 and (own['manual_v2_type'] == 'fak nose manual' or own['actionState'] == 'fak-reg_manual'):
            own['manual_v2_type'] = 'reg manual'
        if STANCE == 1 and (own['manual_v2_type'] == 'reg nose manual' or own['actionState'] == 'reg-fak_manual'):
            own['manual_v2_type'] = 'fak manual'

    #check manual
    ####
    #reg
    if rUD > dict['man_sens_l'] and rUD < dict['man_sens_r'] and STANCE == 0 and rLR < dict['man_sens_side'] and rLR > -dict['man_sens_side']:
        timer = own["reg_manual_timer"]
        timer = timer + 1
        own["reg_manual_timer"] = timer
        if timer > 20 and rUD < dict['man_sens_l']:
            own["reg_manual"] = 0
    if rUD <= dict['man_sens_l'] and (STANCE == 1 or STANCE == 0) and rUD >= -dict['man_sens_l']:
        own["reg_manual_timer"] = 0
        own["reg_manual"] = 0
    #####
    if own["reg_manual_timer"] > 10 and own["fak_manual"] == 0 and own['reg_nmanual'] == 0:
        own["reg_manual"] = 1
    ######
    if own["reg_manual_timer"] == 0:
        own["reg_manual"] = 0

    ####
    #fak
    if rUD > dict['man_sens_l'] and rUD < dict['man_sens_r'] and STANCE == 1 and rLR < dict['man_sens_side'] and rLR > -dict['man_sens_side']:
        timer = own["fak_manual_timer"]
        timer = timer + 1
        own["fak_manual_timer"] = timer
        if timer > 20:
            own["fak_manual"] = 0
    if rUD <= dict['man_sens_l'] and (STANCE == 1 or STANCE == 0) and rUD >= -dict['man_sens_l']:
        own["fak_manual_timer"] = 0
        own["fak_manual"] = 0
    if own["fak_manual_timer"] > 10 and own["reg_manual"] == 0:
        own["fak_manual"] = 1
    if own["fak_manual_timer"] == 0:
        own["fak_manual"] = 0

    #####
    #reg nmanual
    if rUD < -dict['man_sens_l'] and rUD > -dict['man_sens_r'] and STANCE == 0 and rLR < dict['man_sens_side'] and rLR > -dict['man_sens_side']:
        timer = own["reg_nmanual_timer"]
        timer = timer + 1
        own["reg_nmanual_timer"] = timer
        if timer > 20:
            own["reg_nmanual"] = 0
    if rUD >= -dict['man_sens_l'] and (STANCE == 1 or STANCE == 0) and rUD <= dict['man_sens_l']:
        own["reg_nmanual_timer"] = 0
        own["reg_nmanual"] = 0

    if own["reg_nmanual_timer"] > 10 and own["fak_nmanual"] == 0 and own['reg_manual'] == 0 and own['fak_manual'] == 0:
        if own['last_fak_manual'] == False and own['last_reg_manual'] == False and own['last_fak_manual'] == False:
            own["reg_nmanual"] = 1

    if own["reg_nmanual_timer"] == 0:
        own["reg_nmanual"] = 0

    #####
    #fak nmanual
    #print (rUD)
    if rUD < -dict['man_sens_l'] and rUD > -dict['man_sens_r'] and STANCE == 1 and rLR < dict['man_sens_side'] and rLR > -dict['man_sens_side']:
        timer = own["fak_nmanual_timer"]
        timer = timer + 1
        own["fak_nmanual_timer"] = timer
        if timer > 20:
            own["fak_nmanual"] = 0
    if rUD >= -dict['man_sens_l'] and (STANCE == 1 or STANCE == 0) and rUD <= dict['man_sens_l']:
        own["fak_nmanual_timer"] = 0
        own["fak_nmanual"] = 0
    if own["fak_nmanual_timer"] > 10 and own["reg_nmanual"] == 0 and own['fak_manual'] == 0 and own['reg_manual'] == 0:
        own["fak_nmanual"] = 1
    if own["fak_nmanual_timer"] == 0 and own['reg_manual'] == 0:
        own["fak_nmanual"] = 0
    if own['last_fak_nmanual'] == 1 and own['fak_nmanual'] == 0:
        pass

    def reg_stance_left_off():
        LAST_LEFT = own["LAST_LEFT"]
        own["LAST_LEFT"] = 0

    def reg_stance_right_off():
        LAST_RIGHT = own["LAST_RIGHT"]
        own["LAST_RIGHT"] = 0

    def reg_stance_on(inc):
        if own["revert_timer"] < 1 and own['manual_v2'] == 0:
            own['requestAction'] = 'reg_roll'

    def reg_manual_on():
        if lLR > turnsens:
            own['requestAction'] = 'reg_manual_right'
        elif lLR < -turnsens:
            own['requestAction'] = 'reg_manual_left'
        else:
            own['requestAction'] = 'reg_manual'        

    def fak_manual_on():
        if lLR > turnsens:
            own['requestAction'] = 'fak_manual_right'
        elif lLR < -turnsens:
            own['requestAction'] = 'fak_manual_left'
        else:
            own['requestAction'] = 'fak_manual'  

    def reg_nmanual_on():

        if lLR > turnsens:
            own['requestAction'] = 'reg_nmanual_right'
        elif lLR < -turnsens:
            own['requestAction'] = 'reg_nmanual_left'
        else:
            own['requestAction'] = 'reg_nmanual'  


    def fak_nmanual_on():

    #def fak_manual_on():
        if lLR > turnsens:
            own['requestAction'] = 'fak_nmanual_right'
        elif lLR < -turnsens:
            own['requestAction'] = 'fak_nmanual_left'
        else:
            own['requestAction'] = 'fak_nmanual'          

    if own['l_actionState'] == 'reg_manual_revert_ccw':
        own['manual_v2_type'] = 'fak nose manual'
        fak_nmanual_on()
    if own['l_actionState'] == 'reg_manual_revert_cw':
        own['manual_v2_type'] = 'fak nose manual'
        fak_nmanual_on()


    ###########

    def reg_stanceinc_on():
        pass

    def reg_stanceinc_off():
        pass

    def reg_stance_left_on():
        if own['manual_v2'] == 0 and own["revert_timer"] < 1 and own['gt_cd2'] < 1 and ground_since > 30:
            if own["Pump"] == 1:
                own['requestAction'] = 'reg_pump_left'
            else:
                own['requestAction'] = 'reg_turnLeft'
            LAST_LEFT = own["LAST_LEFT"]
            playing_action_frame = skater.getActionFrame(LAND_LAYER)
            if LAST_LEFT == 0 or (playing_action_frame > (LAND_END - 2) and playing_action_frame < (LAND_END - 1)):
                LAST_LEFT = 1
            own["LAST_LEFT"] = 1

    def reg_stance_right_on():
        if own['manual_v2'] == 0 and own["revert_timer"] < 1 and own['gt_cd2'] < 1 and ground_since > 30:
            if own["Pump"] == 1:
                own['requestAction'] = 'reg_pump_right'
            else:
                own['requestAction'] = 'reg_turnRight'
            LAST_RIGHT = own["LAST_RIGHT"]
            playing_action_frame = skater.getActionFrame(LAND_LAYER)
            if LAST_RIGHT == 0 or (playing_action_frame > (LAND_END - 2) and playing_action_frame < (LAND_END - 1)):
                LAST_RIGHT = 1
            own["LAST_RIGHT"] = 1

    def fak_stance_on(inc):
        if own['manual_v2'] == 0 and own["revert_timer"] < 1 and own['gt_cd2'] < 1:
            own['requestAction'] = 'fak_roll'

    def fak_stance_left_on():
        if own['manual_v2'] == 0 and own["revert_timer"] < 1 and own['gt_cd2'] < 1 and ground_since > 30:
            if own["Pump"] == 1:
                own['requestAction'] = 'fak_pump_left'
            else:
                own['requestAction'] = 'fak_turnLeft'
            LAST_LEFT_FAK = own["LAST_LEFT_FAK"]
            playing_action_frame = skater.getActionFrame(LAND_LAYER)
            if LAST_LEFT_FAK == 0 or (playing_action_frame > (LAND_END - 2) and playing_action_frame < (LAND_END - 1)):
                LAST_LEFT_FAK = 1
            own["LAST_LEFT_FAK"] = 1
    def fak_stance_left_off():
        LAST_LEFT_FAK = own["LAST_LEFT_FAK"]
        own["LAST_LEFT_FAK"] = 0

    def fak_stance_right_on():
        if own['manual_v2'] == 0 and own["revert_timer"] < 1 and own['gt_cd2'] < 1 and ground_since > 30:
            if own["Pump"] == 1:
                own['requestAction'] = 'fak_pump_right'
            else:
                own['requestAction'] = 'fak_turnRight'
            LAST_RIGHT_FAK = own["LAST_RIGHT_FAK"]
            own["LAST_RIGHT_FAK"] = 1
    def fak_stance_right_off():
        LAST_RIGHT_FAK = own["LAST_RIGHT_FAK"]
        own["LAST_RIGHT_FAK"] = 0

    def reg_air_on():
        own['requestAction'] = 'reg_air'

    def reg_air_off():
        pass
    def fak_air_on():
        flipping = skater.isPlayingAction(fliplay)
        if flipping == False:
            own['requestAction'] = 'fak_air'

    def fak_air_off():
        pass
    def stance():
        rotation = .96
        turnsens = 0.06
        if rot.z < .995: inc = 1
        else: inc = 0
        #print(rot.z)
        own['inc'] = inc
        flipping = False
        #ground
        ground_since = own["framenum"] - own['lF_air_frame']
        inc = 0
        if rot.z < .98:
            inc = 1

        if onGround and grindHit == False and flipping == False and own["wallride"] == None and own['revert_timer'] < 2:
            if STANCE == 0:
                reg_manual = own['reg_manual']
                fak_manual = own["fak_manual"]
                reg_nmanual = own['reg_nmanual']
                fak_nmanual = own["fak_nmanual"]
                if LAST_STANCE != STANCE or LAST_GROUND != onGround:
                    reg_air_off()
                    fak_air_off()
                if reg_manual == 0 and fak_manual == 0:
                    last_manual = own['last_reg_manual']
                    reg_stance_on(inc)
                if own['manual_v2_type'] == 'reg manual':
                    reg_manual_on()
                elif own['manual_v2_type'] == 'reg nose manual':
                    reg_nmanual_on()
                elif own['manual_v2_type'] == 'fak manual':
                    fak_manual_on()
                elif own['manual_v2_type'] == 'fak nose manual':
                    fak_nmanual_on()
            if STANCE == 1:
                reg_manual = own['reg_manual']
                fak_manual = own["fak_manual"]
                reg_nmanual = own['reg_nmanual']
                fak_nmanual = own["fak_nmanual"]
                if LAST_STANCE != STANCE or LAST_GROUND != onGround:
                    reg_air_off()
                    fak_air_off
                if fak_manual == 0:
                    last_manual = own['last_fak_manual']
                    fak_stance_on(inc)
                if own['manual_v2_type'] == 'fak manual':
                    fak_manual_on()
                elif own['manual_v2_type'] == 'fak nose manual':
                    fak_nmanual_on()
                elif own['manual_v2_type'] == 'reg manual':
                    reg_manual_on()
                elif own['manual_v2_type'] == 'reg nose manual':
                    reg_nmanual_on()
            #if own["Pump"] == False:
            if (lLR < -turnsens or dict['kb_la'] == 2) and STANCE == 0:
                reg_stance_left_on()
            if lLR > -turnsens or LAST_GRIND != grindHit or dict['kb_la'] == 0:
                reg_stance_left_off()
            if (lLR > turnsens or dict['kb_ra'] == 2) and STANCE == 0:
                reg_stance_right_on()
            if lLR < turnsens or LAST_GRIND != grindHit or dict['kb_ra'] == 0:
                reg_stance_right_off()
            if (lLR < -turnsens or dict['kb_la'] == 2) and STANCE == 1:
                fak_stance_left_on()
            if lLR > -turnsens or LAST_GRIND != grindHit or dict['kb_la'] == 0:
                fak_stance_left_off()
            if (lLR > turnsens or dict['kb_ra'] == 2) and STANCE == 1:
                fak_stance_right_on()
            if lLR < turnsens or LAST_GRIND != grindHit or dict['kb_ra'] == 0:
                fak_stance_right_off()
        #air

        if not onGround and flipping == False:
            if STANCE == 0:
                if LAST_STANCE != STANCE or LAST_GROUND != onGround:
                    fak_air_off()
                    reg_stance_left_off()
                    reg_stance_right_off()
                    fak_stance_left_off()
                    fak_stance_right_off()
                reg_air_on()
            if STANCE == 1:
                if LAST_STANCE != STANCE or LAST_GROUND != onGround:
                    reg_air_off()
                    reg_stance_left_off()
                    reg_stance_right_off()
                    fak_stance_left_off()
                    fak_stance_right_off()
                fak_air_on()


    ###################
    #trick definitions#
    ###################
    def jump():
        #print("jump funct")
        jump_timer = own['jump_timer']
        cont.deactivate(wallrideconstL)
        cont.deactivate(wallrideconstR)
        own['last_jump_frame'] = frame
        own['no_grind_timer'] = 0

        if JUMPSTRENGTH != 0:
            height = JUMPSTRENGTH * JUMPHEIGHT
        else:
            height = JUMPHEIGHT
        #if jump_timer == 0:
        if jump_timer < 50:
            jump_timer = 60
            own['jump_timer'] = jump_timer

        if zvel < 7 and jump_timer == 60:
            cam['sndmgr'].queue_sound(['pop', own, cam])
            #cont.activate(own.actuators["pop"])
            own['pop_sound'] = 1
            #print("jump")
            #
            posx = own.worldPosition[0]
            posy = own.worldPosition[1]
            posz = own.worldPosition[2]
            jumppos = Vector((posx, posy, posz))
            own['jumpPos'] = jumppos
            force = [0.0, 0.0, height]
            # use local axis
            local = False
            # apply force
            try:
                if 'trans' in r_ground.hitObject:
                    #print('jump from trans')
                    own['jump_from_trans'] = 1
                    own['trans_jump_obj'] = r_ground.hitObject
            except:
                #print('trans jump broke')
                pass
            if grindHit == False:
                if rot.z < .7:
                    rotamt = rot.z * .5
                    own.applyRotation([0, rotamt, 0], True)
                    own['no_grind'] = True
                    own['no_grind_pull'] = 1

                force2 = [0.0, 0, 150]
                #no jump if on ramp
                if rot.z < .93:
                    force2 = [0,0,0]

                own.applyForce(force, local)
                
                own.applyForce(force2, True)
                #print("apply jump force1")
            if grindHit == True:
                linvelloc = own.getLinearVelocity(True)
                own.applyForce(force, True)
                force2 = [0.0, 0, 150]
                own.applyForce(force2, True)
                linvelloc2 = own.getLinearVelocity(True)
                force = (linvelloc.x, linvelloc.y, linvelloc2.z)
                own.setLinearVelocity(force, True)
                #print("apply jump force2 grindHit")

            own['jump_stance'] = STANCE
            if STANCE == True:
                own["jump_stance"] = 1
            if STANCE == False:
                own["jump_stance"] = 0
            own["jump_stance"] = own["stance"]
            if grindHit == True:
                own['grind_jump'] = 1
                if lLR > turnsens or lLR < -turnsens or lUD > turnsens or lUD < -turnsens:
                    own['grindjumpturn'] = True
        num = 1
        if num == 1:
            rString = "R"
            lString = "L"
            cont.deactivate(wallrideconstL)
            cont.deactivate(wallrideconstR)
            if own["wallride"] == "R":
                force = [0, 170, 0]
                own.applyForce(force, True)
                #cont.activate(own.actuators["pop2"])
                cam['sndmgr'].queue_sound(['pop', own, cam])
                jump_timer = 60
                own['jump_timer'] = jump_timer
            if own["wallride"] == "L":
                force = [0, -170, 0] #325
                own.applyForce(force, True)
                #cont.activate(own.actuators["pop2"])
                cam['sndmgr'].queue_sound(['pop', own, cam])
                jump_timer = 60
                own['jump_timer'] = jump_timer

        if own['jump_timer'] == 60:
            own['jump_timer'] = 59

    def jump_Timer():
        jump_timer = own['jump_timer']
        if jump_timer > 0:
            jump_timer = jump_timer - 1
            own['jump_timer'] = jump_timer
        if own['grindjumpturn'] == True:
            jump_timer = 30
            own['jump_timer'] = jump_timer

    def pump():
        velocity = own['velocity']
        #switch
        local = True
        downforce = dict['pump_downforce']
        lF_air_frame = own['lF_air_frame']
        frames_since_ground = own['framenum'] - own['lF_air_frame']
        #if linVelocity.x < MAX_VEL and linVelocity.x >= -0 and STANCE == 1 and grindHit == False and frames_since_ground > 30 and own['last_manual_v2'] == 0:
        if STANCE == 1 and grindHit == False and frames_since_ground > 30 and own['last_manual_v2'] == 0:
            countdown = COUNTDOWN
            yvel = linVelocity.x + SPEEDPUMP
            yvel2 = linVelocity.x + SPEEDPUMPFAST
            own['countdown'] = countdown
            force = [(yvel), 0, linVelocity.z + downforce]
            force2 = [(yvel2), 0, linVelocity.z + downforce]
            if linVelocity.x < MAX_VEL and linVelocity.x >= 0:
                if rot.z < PUMP_SENS and rot.z > PUMP_SPEED_SENS:
                    own.setLinearVelocity(force, local)
                if rot.z < PUMP_SENS and rot.z <= PUMP_SPEED_SENS:
                    own.setLinearVelocity(force2, local)
            if own['requestAction'] not in ['fak_pump_left', 'fak_pump_right'] and own['l_actionState'] not in ['fak_turnLeft', 'fak_turnRight']:
                own['requestAction'] = 'fak_pump'
        #reg
        #if linVelocity.x > -MAX_VEL and linVelocity.x <= 0 and STANCE == 0 and grindHit == False and frames_since_ground > 30 and own['last_manual_v2'] == 0:
        if STANCE == 0 and grindHit == False and frames_since_ground > 30 and own['last_manual_v2'] == 0:
            countdown = COUNTDOWN
            yvel = linVelocity.x - SPEEDPUMP
            yvel2 = linVelocity.x - SPEEDPUMPFAST
            own['countdown'] = countdown
            force = [(yvel), 0, linVelocity.z + downforce]
            force2 = [(yvel2), 0, linVelocity.z + downforce]
            if linVelocity.x > -MAX_VEL and linVelocity.x <= 0:
                if rot.z < PUMP_SENS and rot.z > PUMP_SPEED_SENS:
                    own.setLinearVelocity(force, local)
                if rot.z < PUMP_SENS and rot.z <= PUMP_SPEED_SENS:
                    own.setLinearVelocity(force2, local)
            if own['requestAction'] not in ['reg_pump_left', 'reg_pump_right'] and own['l_actionState'] not in ['reg_turnLeft', 'reg_turnRight']:
                own['requestAction'] = 'reg_pump'
        own["Pump"] = True
        own["lastPump"] = True

    def roll():
        if onGround:
            pass
    def stop():
        if (linVelocity.x < .05 and linVelocity.x > -.05) or onGround == False:
            can_stop = 0
            own["lastStop"] = True
        else:
            can_stop = 1
        if can_stop == 1:
            if onGround and STANCE == False:
                own['requestAction'] = 'reg_stop'
                print('reg_stop')
            elif onGround and STANCE == True:
                own['requestAction'] = 'fak_stop'
            yvel = linVelocity.x * .985
            force = [(yvel), 0, linVelocity.z]
            own.setLinearVelocity(force, True)

        if linVelocity.x < .05 and linVelocity.x > -.05 and own["lastStop"] == True:
            own["lastStop"] == False

    def oposin():
        if ground_since > 30:
            if (onGround) and STANCE == False and (own['manual'] == 0 or own['requestAction'] == 'fak_roll'):
                if grindold == 0:
                    #print('opos request', own['requestAction'])
                    
                    if own['requestAction'] == 'reg_turnRight':
                        own['requestAction'] = 'reg_pump_right'
                    elif own['requestAction'] == 'reg_turnLeft':
                        own['requestAction'] = 'reg_pump_left'                        
                    else:
                        own['requestAction'] = 'reg_opos'

            elif (onGround) and STANCE == True and (own['manual'] == 0 or own['requestAction'] == 'fak_roll'):
                if grindold == 0:
                    #print('opos request', own['requestAction'])
                    if own['requestAction'] == 'fak_turnRight':
                        own['requestAction'] = 'fak_pump_right'
                    elif own['requestAction'] == 'fak_turnLeft':
                        own['requestAction'] = 'fak_pump_left'                        
                    else:                    
                        own['requestAction'] = 'fak_opos'
            #print('result', own['requestAction'])            
            own["last_Opos"] = True

    def noposin():
        if ground_since > 30:
            if (onGround) and STANCE == False and own['manual'] == 0:
                if grindold == 0:
                    own['requestAction'] = 'reg_nopos'
            elif (onGround) and STANCE == True and own['manual'] == 0:
                if grindold == 0:
                    own['requestAction'] = 'fak_nopos'
            own["last_nOpos"] = True

    def aollie():
        print("ollie")
        #dict['trick_string'] = 'Ollie'
        
        STANCE = own["stance"]
        wallride = own["wallride"]
        dict['trick_string'] = 'Ollie'
        if (onGround and STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_ollie'
            jump()
        elif (onGround and STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_ollie'
            jump()

    def nollie():
        print("nollie")
        dict['trick_string'] = 'Nollie'
        
        STANCE = own["stance"]
        if (onGround and STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own['requestAction'] = 'fak_nollie'
            own["wallride_off"] = 1
            jump()
        elif (onGround and STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie'
            jump()


    def kickflip():
        print("kickflip")

        STANCE = own["stance"]
        print(flip_start_lay)
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_kickflip'
            if onGround:
                jump()
            dict['trick_string'] = 'Kickflip'
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_heelflip'
            if onGround:
                jump()
            if own["wallride"] != 1: jump()
            dict['trick_string'] = 'Heelflip'
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_kickflip'
            if own["wallride"] != None:
                jump()
            dict['trick_string'] = 'Kickflip'
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_heelflip'
            if own["wallride"] != None:
                jump()
            dict['trick_string'] = 'Heelflip'

    def inward_heelflip():
        print("inward heelflip")
        dict['trick_string'] = 'Inward Heelflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_inward_heelflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_inward_heelflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def hardflip():
        print("hardflip")
        dict['trick_string'] = 'Hardflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_hardflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_hardflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_inward_heelflip():
        print("nollie inward heelflip")
        dict['trick_string'] = 'Nollie Inward Heelflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_inward_heelflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_inward_heelflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_hardflip():
        print("nollie hardflip")
        dict['trick_string'] = 'Nollie Hardflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_hardflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_hardflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()


    def varial_kickflip():
        print("varial kickflip")
        dict['trick_string'] = 'Varial Kickflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_varial_kickflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_varial_kickflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_varial_kickflip():
        print("nollie varial kickflip")
        dict['trick_string'] = 'Nollie Varial Kickflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_varial_kickflip'
            if onGround:
                jump()
        elif (onGround and STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_varial_kickflip'
            jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_varial_heelflip():
        print("nollie varial heelflip")
        dict['trick_string'] = 'Nollie Varial Heelflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_varial_heelflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_varial_heelflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def varial_heelflip():
        print("varial heelflip")
        dict['trick_string'] = 'Varial Heelflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_varial_heelflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_varial_heelflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_kickflip():
        print("kickflip")
        STANCE = own["stance"]
        dict['trick_string'] = 'Nollie Kickflip'
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_kickflip'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_kickflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1

    def heelflip():
        print("heelflip")

        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_heelflip'
            if onGround:
                jump()
            dict['trick_string'] = 'Heelflip'
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_kickflip'
            if onGround:
                jump()
            dict['trick_string'] = 'Kickflip'
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1

    def nollie_heelflip():
        print("heelflip")
        dict['trick_string'] = 'Nollie Heelflip'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_heelflip'
            if onGround:
                jump()
        elif (STANCE == True) or (grindHit == True and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_heelflip'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def shuvit():
        print("shuvit")
        dict['trick_string'] = 'Shuvit'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_shuvit'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_fsshuvit'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def shuvit360():
        print("360shuvit")
        dict['trick_string'] = '360 Shuvit'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_shuvit360'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_shuvit360'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def fsshuvit360():
        print("360shuvit")
        dict['trick_string'] = 'FS 360 Shuvit'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_fsshuvit360'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_fsshuvit360'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_shuvit():
        print("shuvit")
        dict['trick_string'] = 'Nollie Shuvit'
        STANCE = own["stance"]
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_shuvit'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_shuvit'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def fsshuvit():
        dict['trick_string'] = 'FS Shuvit'
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_fsshuvit'
            if onGround:
                jump()
        elif (STANCE == True) or ((grindHit == True or wallride != None) and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_shuvit'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    def nollie_fsshuvit():
        dict['trick_string'] = 'Nollie FS Shuvit'
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_fsshuvit'
            if onGround:
                jump()
        elif (STANCE == True) or (grindHit == True and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_fsshuvit'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    #nollie_shuvit360()
    def nollie_shuvit360():
        dict['trick_string'] = 'Nollie FS Shuvit 360'
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_shuvit360'
            if onGround:
                jump()
        elif (STANCE == True) or (grindHit == True and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_shuvit360'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()

    #nollie_fsshuvit360()
    def nollie_fsshuvit360():
        dict['trick_string'] = 'Nollie FS Shuvit 360'
        if (STANCE == False) or ((grindHit == True or wallride != None) and STANCE == False):
            own["wallride_off"] = 1
            own['requestAction'] = 'reg_nollie_fsshuvit_360'
            if onGround:
                jump()
        elif (STANCE == True) or (grindHit == True and STANCE == True):
            own["wallride_off"] = 1
            own['requestAction'] = 'fak_nollie_fsshuvit_360'
            if onGround:
                jump()
        if not onGround and STANCE == False:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
        elif not onGround and STANCE == True:
            own["wallride_off"] = 1
            if own["wallride"] != None:
                jump()
#-------
    def reg_frigid_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Frigid'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'reg_frigid'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def reg_fsonefoot_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'FS One Foot'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'reg_fsonefoot'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def reg_onefoot_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'One Foot'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'reg_onefoot'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fak_frigid_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Frigid'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_frigid'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def fak_fsonefoot_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'FS One Foot'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_fsonefoot'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def fak_onefoot_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'One Foot'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_onefoot'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

#----
    def reg_judo_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Judo'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'reg_judo'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fak_judo_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Judo'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_judo'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def frontside_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Mute'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'frontside_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def backside_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Backside'
        if GRAB_ON == True and not onGround and aBut == True:
            own['requestAction'] = 'reg_fp_rback'
        elif GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'backside_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fakfrontside_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Mute'
        if GRAB_ON == True and not onGround and aBut == True:
            own['requestAction'] = 'fak_fp_rback'

        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_frontside_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fakbackside_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Backside'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_backside_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    #nose/tail grabs
    def frontside_nose_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'FS Nose'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'frontside_nose_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def frontside_tail_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'FS Tail'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'frontside_tail_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def backside_nose_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'BS Nose'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'backside_nose_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED
    def backside_tail_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'BS Tail'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'backside_tail_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def reg_airwalk():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'Airwalk'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'reg_airwalk'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    #switch
    def fak_frontside_nose_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'FS Nose'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_frontside_nose_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fak_frontside_tail_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'FS Tail'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_frontside_tail_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fak_backside_nose_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'BS Nose'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_backside_nose_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    def fak_backside_tail_grab_on():
        GRAB_PLAYED = own["GRAB_PLAYED"]
        GRAB_ON = own["GRAB_ON"]
        dict['trick_string'] = 'BS Tail'
        if GRAB_ON == True and GRAB_PLAYED == False and not onGround:
            own['requestAction'] = 'fak_backside_tail_grab'
            GRAB_PLAYED = True
            own["GRAB_PLAYED)"] = GRAB_PLAYED

    #--------------------

    def SWAG():
        if onGround:
            SWAG = own["swag"]
            SWAG = own.getLinearVelocity(True)
            SWAG = SWAG[1]
            own["swag"] = SWAG
            if STANCE == True:
                if SWAG > 1 and SWAG < 2:
                    rotation = [0.0, 0, (SWAG / 200)]
                    own.applyRotation( rotation, True)
                elif SWAG > 2:
                    rotation = [0.0, 0, (SWAG / 50)]
                    own.applyRotation( rotation, True)
                elif SWAG < -1 and SWAG > -2:
                    rotation = [0.0, 0, (SWAG / 200)]
                    own.applyRotation( rotation, True)
                elif SWAG < -2:
                    rotation = [0.0, 0, (SWAG / 50)]
                    own.applyRotation( rotation, True)
            if STANCE == False:
                if SWAG > 1 and SWAG < 2:
                    rotation = [0.0, 0, (-SWAG / 200)]
                    own.applyRotation( rotation, True)
                elif SWAG > 2:
                    rotation = [0.0, 0, (-SWAG / 50)]
                    own.applyRotation( rotation, True)
                elif SWAG < -1 and SWAG > -2:
                    rotation = [0.0, 0, (-SWAG / 200)]
                    own.applyRotation( rotation, True)
                elif SWAG < -2:
                    rotation = [0.0, 0, (-SWAG / 50)]
                    own.applyRotation( rotation, True)

    def air():
        Axis = 2
        Distance = -2
        End = own.worldPosition + (own.worldOrientation.col[Axis]*Distance)
        Start = own.worldPosition
        localRay_v = own.rayCast(End, Start, 0, 'vert', 1, 1, 0)

        d2 = 10000
        if onGround:
            d2 = own.getDistanceTo(gray.hitPosition)

        if d2 > .3 and own['airup'] == 1 and localRay_v[0] == None:
            #pass
            own.alignAxisToVect(gray.hitNormal, 2, .03)
        if d2 > .3 and own['airup'] == 0 and localRay_v[0] == None:

            distance = own.getDistanceTo(gray.hitPosition)
            since_grind_buf = 3
            if gray.hitObject != None and grindDar2.triggered == False and (frame - lgf) > since_grind_buf:
                #add or colliding
                if distance < .5:
                    own.alignAxisToVect(gray.hitNormal, 2, .1)
                elif distance >= .5 and distance < 1.75:
                    own.alignAxisToVect(gray.hitNormal, 2, .05)
                elif distance >= 1.75:
                    own.alignAxisToVect([0.0, 0.0, 1.0], 2, .03)
                else:
                    own.alignAxisToVect([0.0, 0.0, 1.0], 2, .03)
            if grindDar2.triggered and (frame - lgf) > since_grind_buf:
                if distance < .5:
                    own.alignAxisToVect(gray.hitNormal, 2, .1)
                elif distance >= .5 and distance < 1.75:
                    own.alignAxisToVect(gray.hitNormal, 2, .03)
                elif distance >= 1.75:
                    own.alignAxisToVect([0.0, 0.0, 1.0], 2, .03)
        elif onGround:
            #if d2 > .4:
               #own.alignAxisToVect([0.0, 0.0, 1.0], 2, .03)
            pass
    def stopAnims():
        pass

    def isplaying():
    #    for x in range(9000):
    #        l1 = deck.isPlayingAction(x)
    #        if l1 == True:
    #            print(x)
        pass

    def nextframe():
        framenumber = own["framenum"]
        framenumber = framenumber + 1
        if framenumber == 900000:
            framenumber = 0
        own["framenum"] = framenumber
        own['last_roll_frame'] = framenumber

    def push():
        local = True
        #print("push")
        linVelocity15 = own.linearVelocity
        #if linVelocity15.x < MAX_VEL and linVelocity15.x >= -0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0 and own['last_footplant'] == False:
        if linVelocity15.x >= -0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0 and own['last_footplant'] == False:    
            if linVelocity15.x < MAX_VEL:
                countdown = COUNTDOWN
                yvel = linVelocity15.x + SPEEDUP
                own['countdown'] = countdown
                force = [(yvel), 0, linVelocity15.z]
                own.setLinearVelocity(force, local)
            own['requestAction'] = 'fak_push_goof'
        #switch
        #if linVelocity15.x > -MAX_VEL and linVelocity15.x < 0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0:
        if linVelocity15.x < 0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0:    
            if linVelocity15.x > -MAX_VEL:
                countdown = COUNTDOWN
                yvel = linVelocity15.x - SPEEDUP
                own['countdown'] = countdown
                force = [(yvel), 0, linVelocity15.z]
                own.setLinearVelocity(force, local)
            own['requestAction'] = 'reg_push'

    def push_goof():
        linVelocity15 = own.linearVelocity
        local = True
        #print("push goof")
        #if linVelocity15.x < MAX_VEL and linVelocity15.x >= -0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0:
        if  linVelocity15.x >= -0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0:    
            if linVelocity15.x < MAX_VEL:
                countdown = COUNTDOWN
                yvel = linVelocity15.x + SPEEDUP
                own['countdown'] = countdown
                force = [(yvel), 0, linVelocity15.z]
                own.setLinearVelocity(force, local)
            own['requestAction'] = 'fak_push'
        #switch
        #if linVelocity15.x > -MAX_VEL and linVelocity15.x < 0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0:
        if  linVelocity15.x < 0 and onGround and own['hippy'] == 0 and own['last_hippy'] == 0:    
            if linVelocity15.x > -MAX_VEL:
                countdown = COUNTDOWN
                yvel = linVelocity15.x - SPEEDUP
                own['countdown'] = countdown
                force = [(yvel), 0, linVelocity15.z]
                own.setLinearVelocity(force, local)
            own['requestAction'] = 'reg_push_goof'

    def brfoot():
        if STANCE == 0:
            own['requestAction'] = 'reg_brfoot'
        if STANCE == 1:
            own['requestAction'] = 'fak_brfoot'
        jump()

    def frfoot():
        if STANCE == 0:
            own['requestAction'] = 'reg_frfoot'
        if STANCE == 1:
            own['requestAction'] = 'fak_frfoot'
        jump()

    def blfoot():
        if STANCE == 0:
            own['requestAction'] = 'reg_blfoot'
        if STANCE == 1:
            own['requestAction'] = 'fak_blfoot'
        jump()

    def flfoot():
        if STANCE == 0:
            own['requestAction'] = 'reg_flfoot'
        if STANCE == 1:
            own['requestAction'] = 'fak_flfoot'
        jump()

    def rollsound():
        #onground
        if onGround and grindDar == 0 and own['invert_on'] == 0:
            num1 = .05
            num2 = .25
            v = .1
            p = 1
            if linVelocity.x <= -num1 and linVelocity.x >= num1:
                #own.actuators["sroll"].volume = .0001
                #cont.deactivate(own.actuators["sroll"])
                #own.actuators["sroll"].stopSound()
                cam['sndmgr'].stop_sound(['roll'])
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .02
                #own.actuators["sroll"].pitch = .65
                v = .02
                p = .65
            num1 = .25
            num2 = .5
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .03
                #own.actuators["sroll"].pitch = .7
                v = .03
                p = .7
            num1 = .5
            num2 = .75
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .04
                #own.actuators["sroll"].pitch = .75
                v = .04
                p = .75
            num1 = .75
            num2 = 1
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .05
                #own.actuators["sroll"].pitch = .8
                v = .05
                p = .8
            num1 = 1
            num2 = 1.5
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .06
                #own.actuators["sroll"].pitch = .85
                v = .06
                p = .85
            num1 = 1.5
            num2 = 2
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .07
                #own.actuators["sroll"].pitch = .9
                v = .07
                p = .9
            num1 = 2
            num2 = 3
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .08
                #own.actuators["sroll"].pitch = .95
                v = .08
                p = .95
            num1 = 3
            num2 = 4
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .09
                #own.actuators["sroll"].pitch = 1
                v = .09
                p = 1.0
            num1 = 4
            num2 = 5
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .11
                #own.actuators["sroll"].pitch = 1.05
                v = .11
                p = 1.05
            num1 = 5
            num2 = 6
            if (linVelocity.x > num1 and linVelocity.x < num2) or (linVelocity.x < -num1 and linVelocity.x > -num2):
                #own.actuators["sroll"].volume = .1
                #own.actuators["sroll"].pitch = 1.1
                v = .1
                p = 1.1
            #own.actuators['sroll'].mode = 2
            #cont.activate(own.actuators["sroll"])
            v = v * 2
            cam['sndmgr'].queue_sound(['roll', own, cam, v, p])    

        if grindDar == 1 or own['invert_on'] == 1:
            #own.actuators["sroll"].volume = .0001
            #cont.deactivate(own.actuators["sroll"])
            #own.actuators["sroll"].stopSound()
            cam['sndmgr'].stop_sound(['roll'])

        #in air
        if not onGround:
            #own.actuators["sroll"].volume = .0001
            #cont.deactivate(own.actuators["sroll"])
            #own.actuators["sroll"].stopSound()
            cam['sndmgr'].stop_sound(['roll'])
        
        #act = cont.actuators["sroll"]
        #own['sroll_vol'] = act.volume
        #own['sroll_pitch'] = act.pitch

    def wheelroll():
        #still
        if linVelocity.x <= -0.05 and linVelocity.x >= 0.05:
            wheel1.stopAction(2)
            wheel2.stopAction(2)
            wheel3.stopAction(2)
            wheel4.stopAction(2)
        #regular
        # if linVelocity.x > 0.05 and linVelocity.x < .5:
        #     wheel2.playAction("roll1.001", 1, 20, layer=2, play_mode=0, speed=.25)
        #     wheel3.playAction("roll2.001", 1, 20, layer=2, play_mode=0, speed=.25)
        #     wheel4.playAction("roll3.001", 1, 20, layer=2, play_mode=0, speed=.25)
        #     wheel1.playAction("roll4.001", 1, 20, layer=2, play_mode=0, speed=.25)
        # if linVelocity.x > 0.5 and linVelocity.x < 1:
        #     wheel2.playAction("roll1.001", 1, 20, layer=2, play_mode=1, speed=1)
        #     wheel3.playAction("roll2.001", 1, 20, layer=2, play_mode=1, speed=1)
        #     wheel4.playAction("roll3.001", 1, 20, layer=2, play_mode=1, speed=1)
        #     wheel1.playAction("roll4.001", 1, 20, layer=2, play_mode=1, speed=1)
        # if linVelocity.x > 1 and linVelocity.x < 4:
        #     wheel2.playAction("roll1.001", 1, 20, layer=2, play_mode=1, speed=1.5)
        #     wheel3.playAction("roll2.001", 1, 20, layer=2, play_mode=1, speed=1.5)
        #     wheel4.playAction("roll3.001", 1, 20, layer=2, play_mode=1, speed=1.5)
        #     wheel1.playAction("roll4.001", 1, 20, layer=2, play_mode=1, speed=1.5)
        # if linVelocity.x > 4:
        #     wheel2.playAction("roll1.001", 1, 20, layer=2, play_mode=1, speed=2)
        #     wheel3.playAction("roll2.001", 1, 20, layer=2, play_mode=1, speed=2)
        #     wheel4.playAction("roll3.001", 1, 20, layer=2, play_mode=1, speed=2)
        #     wheel1.playAction("roll4.001", 1, 20, layer=2, play_mode=1, speed=2)
        # #switch
        # if linVelocity.x < -0.05 and linVelocity.x > -.5:
        #     wheel2.playAction("roll1.001", 20, 1, layer=2, play_mode=0, speed=.25)
        #     wheel3.playAction("roll2.001", 20, 1, layer=2, play_mode=0, speed=.25)
        #     wheel4.playAction("roll3.001", 20, 1, layer=2, play_mode=0, speed=.25)
        #     wheel1.playAction("roll4.001", 20, 1, layer=2, play_mode=0, speed=.25)
        # if linVelocity.x < -0.5 and linVelocity.x > -1:
        #     wheel2.playAction("roll1.001", 20, 1, layer=2, play_mode=1, speed=1)
        #     wheel3.playAction("roll2.001", 20, 1, layer=2, play_mode=1, speed=1)
        #     wheel4.playAction("roll3.001", 20, 1, layer=2, play_mode=1, speed=1)
        #     wheel1.playAction("roll4.001", 20, 1, layer=2, play_mode=1, speed=1)
        # if linVelocity.x < -1 and linVelocity > -4:
        #     wheel2.playAction("roll1.001", 20, 1, layer=2, play_mode=1, speed=1.5)
        #     wheel3.playAction("roll2.001", 20, 1, layer=2, play_mode=1, speed=1.5)
        #     wheel4.playAction("roll3.001", 20, 1, layer=2, play_mode=1, speed=1.5)
        #     wheel1.playAction("roll4.001", 20, 1, layer=2, play_mode=1, speed=1.5)
        # if linVelocity.x < -4:
        #     wheel2.playAction("roll1.001", 20, 1, layer=2, play_mode=1, speed=2)
        #     wheel3.playAction("roll2.001", 20, 1, layer=2, play_mode=1, speed=2)
        #     wheel4.playAction("roll3.001", 20, 1, layer=2, play_mode=1, speed=2)
        #     wheel1.playAction("roll4.001", 20, 1, layer=2, play_mode=1, speed=2)

    def turn():
        #rotamt = .02
        rotamt = dict['turn_rotation']
        linVelocity2 = own.getLinearVelocity(True)
        #speed = .004
        speed = dict['turn_addx']
        manual = 0
        multer = abs(round(((round(lLR, 2) * 100) / 8), 1))
        if dict['kb_ra'] == 2 or dict['kb_la'] == 2:
            multer = 1
        if own['manual_v2'] == 1:
            manual = 1
            if abs(linVelocity.x) < 2:
                speed = dict['turn_addx'] + .001

        jumpstance = own['jump_stance']
        if lLR > turnsens or lLR < -turnsens:
            own["turn"] = 1
        else:
            own["turn"] = 0
        if lUD > turnsens or lUD < -turnsens:
            own["turnud"] = 1
        else:
            own["turnud"] = 0

        turnVel = .01
    #light
    #if manual and grindhit = true
        # if lLR > turnsens and lLR < (turnsens * 1.3) and (grindHit == False or (manual == 1 and grindHit == True)):
        #     rotation = [0.0, 0.0, (-rotamt)]
        #     local = False # use world axis
        #     #own.applyRotation( rotation, local)

        #     if onGround:
        #         #print("light turn")
        #         if STANCE == 0:
        #             own.setLinearVelocity([linVelocity2.x - speed, linVelocity2.y, linVelocity2.z], 1)
        #         if STANCE == 1:
        #             own.setLinearVelocity([linVelocity2.x + speed, linVelocity2.y, linVelocity2.z], 1)
        # if lLR < -turnsens and lLR > (turnsens * -1.3) and (grindHit == False or (manual == 1 and grindHit == True)):
        #     rotation = [0.0, 0.0, rotamt]
        #     local = False # use world axis
        #     #own.applyRotation( rotation, local)

        #     if onGround:
        #         #print("light turn")
        #         if STANCE == 0:
        #             own.setLinearVelocity([linVelocity2.x - speed, linVelocity2.y, linVelocity2.z], 1)
        #         if STANCE == 1:
        #             own.setLinearVelocity([linVelocity2.x + speed, linVelocity2.y, linVelocity2.z], 1)
    #medium
        max_amt = 3
        if (lLR > (turnsens * 1) or dict['kb_ra'] == 2) and (grindHit == False or (manual == 1 and grindHit == True)):
            #turn left
            if rot.z < .4:
                rotation = [0.0, 0.0, ((-rotamt * 5) * multer)]
            if rot.z < .6 and rot.z > .4:
                rotation = [0.0, 0.0, ((-rotamt * 2) * multer)]
            else:
                rotation = [0.0, 0.0, ((-rotamt * 1.6) * multer)]
            local = True # use world axis
            own.applyRotation( rotation, local)
            #print('rotation = ', rotation)
            if onGround:
                #print("med turn")
                if STANCE == 0:
                    own.setLinearVelocity([linVelocity2.x - speed, linVelocity2.y - turnVel, linVelocity2.z], 1)
                if STANCE == 1:
                    own.setLinearVelocity([linVelocity2.x + speed, linVelocity2.y - turnVel, linVelocity2.z], 1)
        elif (lLR < (-turnsens * 1) or dict['kb_la'] == 2) and (grindHit == False or (manual == 1 and grindHit == True)):
            #turn right                     
            #vert     
            if rot.z < .4:
                rotation = [0.0, 0.0, ((rotamt * 5) * multer)]
            #near vert    
            if rot.z < .6 and rot.z > .4:
                rotation = [0.0, 0.0, ((rotamt * 2) * multer)]
                
            #normal    
            else:
                rotation = [0.0, 0.0, ((rotamt * 1.6) * multer)]
    
            local = True # use world axis
            own.applyRotation( rotation, local)
            #print('rotation = ', rotation)
            if onGround:
                #print("med turn")
                if STANCE == 0:
                    own.setLinearVelocity([linVelocity2.x - speed, linVelocity2.y, linVelocity2.z], 1)
                if STANCE == 1:
                    own.setLinearVelocity([linVelocity2.x + speed, linVelocity2.y, linVelocity2.z], 1)

    #air
        if not onGround and (lLR > turnsens or dict['kb_ra'] == 2) and (grindHit == False or (manual == 1 and grindHit == True)) and own["wallride"] == None:
            if STANCE == 0:
                own.applyRotation([0, 0, -dict['turn_air_rotation']], 1)
            if STANCE == 1:
                own.applyRotation([0, 0, -dict['turn_air_rotation']], 1)
        if not onGround and (lLR < -turnsens or dict['kb_la'] == 2) and (grindHit == False or (manual == 1 and grindHit == True)) and own["wallride"] == None:
            if STANCE == 0:
                own.applyRotation([0, 0, dict['turn_air_rotation']], 1)
            if STANCE == 1:
                own.applyRotation([0, 0, dict['turn_air_rotation']], 1)

    ###########
    def grindsound():
        dropin = own['dropinTimer']
        lif = own['last_invert_frame']
        #if grindSound != None and grindHit == True and own['nogrindsound'] == 0:
        if own['LAST_GRIND'] and own['nogrindsound'] == 0:  
        #if own['LAST_GRIND']: 
            if abs(linVelocity.x) > abs(linVelocity.y):
                vel = linVelocity.x
            elif abs(linVelocity.x) < abs(linVelocity.y):
                vel = linVelocity.y
            else:
                vel = 0
            v = .0001
            p = 1.0
            #if grindSound == "rail":
                #cont.deactivate(own.actuators['grind_cement'])
            num1 = .000
            num2 = .05
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .0001
                #own.actuators["grind_rail"].pitch = .6
                v = .00001
                p = .6
            num1 = .05
            num2 = .25
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .05
                #own.actuators["grind_rail"].pitch = .7
                v = .05
                p = .7
            num1 = .25
            num2 = .5
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .1
                #own.actuators["grind_rail"].pitch = .75
                v = .1
                p = .75
            num1 = .5
            num2 = .75
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .14
                #own.actuators["grind_rail"].pitch = .8
                v = .14
                p = .8
            num1 = .75
            num2 = 1
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .17
                #own.actuators["grind_rail"].pitch = .85
                v = .17
                p = .85
            num1 = 1
            num2 = 1.25
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .18
                #own.actuators["grind_rail"].pitch = .9
                v = .18
                p = .9
            num1 = 1.25
            num2 = 2
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .19
                #own.actuators["grind_rail"].pitch = .95
                v = .19
                p = .95
            num1 = 2
            num2 = 40
            if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
                #own.actuators["grind_rail"].volume = .19
                #own.actuators["grind_rail"].pitch = 1
                v = .19
                p = 1.0
            #cont.activate(own.actuators['grind_rail'])

            if grindSound == 'rail':
                cam['sndmgr'].queue_sound(['grind_rail', own, cam, v, p]) 
            elif grindSound == 'concrete':
                cam['sndmgr'].queue_sound(['grind_cement', own, cam, v, p])     

            # if grindSound == "concrete":
            # #play sound
            #     cont.deactivate(own.actuators['grind_rail'])
            #     num1 = .000
            #     num2 = .05
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .01
            #         own.actuators["grind_cement"].pitch = .6
            #     num1 = .05
            #     num2 = .25
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .05
            #         own.actuators["grind_cement"].pitch = .7

            #     num1 = .25
            #     num2 = .5
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .1
            #         own.actuators["grind_cement"].pitch = .75
            #     num1 = .5
            #     num2 = .75
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .14
            #         own.actuators["grind_cement"].pitch = .8
            #     num1 = .75
            #     num2 = 1
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .17
            #         own.actuators["grind_cement"].pitch = .85
            #     num1 = 1
            #     num2 = 1.25
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .18
            #         own.actuators["grind_cement"].pitch = .9
            #     num1 = 1.25
            #     num2 = 2
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .19
            #         own.actuators["grind_cement"].pitch = .95
            #     num1 = 2
            #     num2 = 40
            #     if (vel > num1 and vel < num2) or (vel < -num1 and vel > -num2):
            #         own.actuators["grind_cement"].volume = .19
            #         own.actuators["grind_cement"].pitch = 1
            #     cont.activate(own.actuators['grind_cement'])
        else:
            #cont.deactivate(own.actuators['grind_cement'])
            #cont.deactivate(own.actuators['grind_rail'])
            cam['sndmgr'].stop_sound(['grind_rail']) 
            cam['sndmgr'].stop_sound(['grind_cement']) 

    def record_grindsound():
        pass
        # dropin = own['dropinTimer']
        # lif = own['last_invert_frame']
        # if grindHit == True and dropin == 0 and own['invert_on'] == 0 and own["LAST_GRIND"] == True and own["nogrindsound"] == 0 and (frame - lif > 13):
        #     if grindSound == "concrete":
        #         act = own.actuators["grind_cement"]
        #         own['grindcement_vol'] = act.volume
        #         own['grindcement_pitch'] = act.pitch
        #     elif grindSound == "rail":
        #         act = own.actuators["grind_rail"]
        #         own['grindrail_vol'] = act.volume
        #         own['grindrail_pitch'] = act.pitch
        # if own["LAST_GRIND"] == False:
        #     own['grindcement_vol'] = 0
        #     own['grindcement_pitch'] = 0
        #     own['grindrail_vol'] = 0
        #     own['grindrail_pitch'] = 0

    def grind():
        grindsound()
        STANCE = own["stance"]
        jumpstance = own["jump_stance"]
        lif = frame - own['last_invert_frame']
        giniter = 0
        if own['LAST_GRIND'] == False and grindHit == True:
            own['grindstartFrame'] = own['framenum']
            giniter = 1
        if grindHit == True and own['invert_on'] == 0 and own['footplant_on'] == False and own['manual'] == 0 and lif > 40 and own['dropinTimer'] < 30 and giniter == 0:
            own['grinding'] = True
            control_bottom = scene.objects['control_bottom']
            grindTouch = control_bottom.sensors['grindCol_bottom']
            edge = 0
            try:
                if 'edge' in grindTouch.hitObject:
                    #print('Edge')
                    edge = 1
            except:
                pass

            gblend = 1
            if LAST_GRIND == 0:
                gt = own['grindType']
            tempstance = 3
            if jumpstance != 3:
                tempstance = jumpstance
            else:
                tempstance = STANCE
            #print(own['grindpos'], own['grindType'], 'pos n type111')
            grindpos = own['grindpos']
            if grindpos == "reg_5050" and own['grindType'] == "empty":
                own['grindType'] = grindpos
            if own['grindType'] == "empty" and grindpos == 'reg_board':
                if STANCE == 0:
                    own['grindType'] = "reg_bsboard"
                else:
                    own['grindType'] = "fak_bsboard"


            # if own['grindType'] == '' and grindpos == 'reg_board':
            #     if STANCE == 0:
            #         own['grindType'] = "reg_bsboard"
            #     else:
            #         own['grindType'] = "fak_bsboard"

            # if own['grindType'] == '' and grindpos == 'reg_board':
            #     own['grindType'] = 'reg_bsboard'

            #print(own['grindpos'], own['grindType'], 'pos n type')

            if STANCE == True:
                if own['grindType'] == "reg_bsboard":
                    own['grind_stance'] = 0
                    own['requestAction'] = 'reg_bsboard'
                elif own['grindType'] == "fak_bsboard":
                    own['grind_stance'] = 1
                    own['requestAction'] = 'fak_bsboard'
                elif own['grindType'] == "reg_fsboard":
                    own['grind_stance'] = 0
                    own['requestAction'] = 'reg_fsboard'

                elif own['grindType'] == "fak_noseg":
                    own['requestAction'] = 'fak_noseg'
                elif own['grindType'] == "fak_nosegR":
                    own['requestAction'] = 'fak_nosegr'
                elif own['grindType'] == "fak_nosegL":
                    own['requestAction'] = 'fak_nosegl'
                elif own['grindType'] == "fak_tailg":
                    own['requestAction'] = 'fak_tailg'
                elif own['grindType'] == "fak_tailgR":
                    own['requestAction'] = 'fak_tailgr'
                elif own['grindType'] == "fak_tailgL":
                    own['requestAction'] = 'fak_tailgl'

                elif own['grindType'] == "reg_tailg":
                    own['requestAction'] = 'fak_noseg'
                elif own['grindType'] == "reg_tailgR":
                    own['requestAction'] = 'fak_nosegr'
                elif own['grindType'] == "reg_tailgL":
                    own['requestAction'] = 'fak_nosegl'
                elif own['grindType'] == "reg_noseg":
                    own['requestAction'] = 'fak_tailg'
                elif own['grindType'] == "reg_nosegR":
                    own['requestAction'] = 'fak_tailgr'
                elif own['grindType'] == "reg_nosegL":
                    own['requestAction'] = 'fak_tailgl'

                elif own['grindType'] == "reg_tailslide":
                    own['requestAction'] = 'reg_tailslide'
                    if edge == 1:
                        own['requestAction'] = 'fak_noseslide'
                        print('s = true')
                elif own['grindType'] == "reg_noseslide":
                    own['requestAction'] = 'reg_noseslide'
                    if edge == 1:
                        own['requestAction'] = 'fak_tailslide'
                        print('s = true')
                    print('reg_noseslide---', STANCE)
                elif own['grindType'] == "fak_tailslide":
                    own['requestAction'] = 'fak_tailslide'
                elif own['grindType'] == "fak_noseslide":
                    own['requestAction'] = 'fak_noseslide'
                    print('fak_noseslide', STANCE)

                elif own['grindType'] == "reg_tailslide" and edge == 1:
                    own['requestAction'] = 'fak_noseslide'
                    print('-----flip stance')
                    STANCE = 0
                    own['grind_stance'] = 1
                elif own['grindType'] == "fak_tailslide":
                    own['requestAction'] = 'fak_tailslide'
                    print('-----flip stance')
                    own['grind_stance'] = 1
                    own['grind_stance'] = 0
                elif own['grindType'] == "reg_noseslide" and edge == 1:
                    own['requestAction'] = 'fak_tailslide'
                    own['grind_stance'] = 0
                    print('-----flip stance')
                    own['grind_stance'] = 1
                    print('reg_noseslide', STANCE)
                elif own['grindType'] == "fak_noseslide":
                    own['requestAction'] = 'fak_noseslide'
                    print('-----flip stance')
                    own['grind_stance'] = 1
                    own['grind_stance'] = 0
                else:
                    if STANCE == 0:
                        own['requestAction'] = 'reg_5050'
                        dict['trick_string'] = '5050'
                        #print('5050 a')
                    if STANCE == 1:
                        own['requestAction'] = 'fak_5050'
                        dict['trick_string'] = '5050'
                        #print('5050 b')

            elif STANCE == False:
                if own['grindType'] == "reg_bsboard":
                    own['grind_stance'] = 0
                    own['requestAction'] = 'reg_bsboard'
                elif own['grindType'] == "fak_bsboard":
                    own['grind_stance'] = 1
                    own['requestAction'] = 'fak_bsboard'
                elif own['grindType'] == "reg_tailg":
                    own['grind_stance'] = 0
                    own['requestAction'] = 'reg_tailg'
                elif own['grindType'] == "reg_tailgR":
                    own['requestAction'] = 'reg_tailgr'
                elif own['grindType'] == "reg_tailgL":
                    own['requestAction'] = 'reg_tailgl'
                elif own['grindType'] == "reg_noseg":
                    own['requestAction'] = 'reg_noseg'
                elif own['grindType'] == "reg_nosegR":
                    own['requestAction'] = 'reg_nosegr'
                elif own['grindType'] == "reg_nosegL":
                    own['requestAction'] = 'reg_nosegl'
                elif own['grindType'] == "reg_tailslide":
                    own['requestAction'] = 'reg_tailslide'
                    #if edge == 1:
                        #own['requestAction'] = 'fak_noseslide'
                       # print('s = false')
                elif own['grindType'] == "reg_noseslide":
                    own['requestAction'] = 'reg_noseslide'
                    #if edge == 1:
                        #own['requestAction'] = 'fak_tailslide'
                        #print('s = false')
                elif own['grindType'] == "fak_tailslide":
                    own['requestAction'] = 'fak_tailslide'
                    if edge == 1:
                        own['requestAction'] = 'reg_noseslide'
                        print('s = false')
                elif own['grindType'] == "fak_noseslide":
                    own['requestAction'] = 'fak_noseslide'
                    if edge == 1:
                        own['requestAction'] = 'reg_tailslide'
                        print('s = false')
                elif own['grindType'] == "fak_noseg":
                    own['requestAction'] = 'reg_tailg'
                elif own['grindType'] == "fak_nosegR":
                    own['requestAction'] = 'reg_tailgr'
                elif own['grindType'] == "fak_nosegL":
                    own['requestAction'] = 'reg_tailgl'
                elif own['grindType'] == "fak_tailg":
                    own['requestAction'] = 'reg_noseg'
                elif own['grindType'] == "fak_tailgR":
                    own['requestAction'] = 'reg_nosegr'
                elif own['grindType'] == "fak_tailgL":
                    own['requestAction'] = 'reg_nosegl'

                # elif own['grindType'] == "fak_tailslide" and edge == 1:
                #     own['grind_stance'] = 1
                #     own['requestAction'] = 'reg_noseslide'
                # elif own['grindType'] == "reg_noseslide" and edge == 1:
                #     own['grind_stance'] = 0
                #     own['requestAction'] = 'reg_noseslide'
                #     print('reg_noseslide', STANCE)
                elif own['grindType'] == "fak_noseslide" and edge == 1:
                    own['grind_stance'] = 1
                    own['requestAction'] = 'reg_tailslide'
                else:
                    if STANCE == 0:
                        own['requestAction'] = 'reg_5050'
                        #print('5050 c')
                    if STANCE == 1:
                        own['requestAction'] = 'fak_5050'
                        #print('5050 d')

            #print(own['requestAction'], 'end ra', own['grindType'])
            if 'fak' in own['l_actionState']:
                STANCE = 1
                own['lg_stance'] = 1
                lg_stance = 1
            if 'reg' in own['l_actionState']:
                STANCE = 0
                own['lg_stance'] = 0
                lg_stance = 0
            own['stance'] = STANCE

            if own['requestAction'] == 'reg_5050' and own['grindpos'] == 'reg_board':
                own['requestAction'] = 'reg_bsboard'
            if own['requestAction'] == 'fak_5050' and own['grindpos'] == 'reg_board':
                own['requestAction'] = 'fak_bsboard'        


    def rotmult():
        if onGround:
            num = ((rot.z * -1) +1)
            num = num * 100

    def airup():
        if not onGround:
            pos = own.worldPosition
            pos = own.worldPosition.z
            last_pos = own['last_posz']
            if last_pos - pos > 0:
                own['airup'] = 0
            if last_pos - pos < 0:
                own['airup'] = 1
            if pos > own['last_posz']:
                own["air_height"] = pos
            own['last_posz'] = pos
        else:
            own['airup'] = 0

    def onramp():
        if r_ground.positive:
            if 'ramp' in r_ground.hitObject:
                own['onramp'] = 60
                own['last_ramp'] = frame
                own['no_grind_timer'] = 60
                #print('on ramp')
            else:
                own['onramp'] = 0

        else:

            if own['onramp'] > 0:
                own['onramp'] -= 1

    def grindtype(gtype):
        own['grindType'] = gtype

    def coping():
        if invertTouch.positive:
            own["coping"] = 1
        else:
            own["coping"] = 0

    def onboard():
        #if own['walk'] == 1 and own['lasty'] == False and own['requestAction'] not in ['reg_dropin', 'fak_dropin']:
        if own['walk'] == 1 and own['lasty'] == False:    
            cont.deactivate(cam.actuators['replayCam'])
            cont.activate(cam.actuators['Camera'])
            #killall()
            #walklay = 40
            ob_speed = .5
            if STANCE == 0:
                #killall()
                #print('dropincol', own['dropinCol'].positive)
                #if own['dropinCol'].positive == True:
                if own['dropinTimer'] > 0:    
                    own['requestAction'] = 'reg_dropin'
                    own['dropinTimer'] = 60

                    force = (linVelocity.x -1, linVelocity.y, linVelocity.z)
                    own.setLinearVelocity(force, True)
                    
                    print('dropping in ')
                else:
                    own['requestAction'] = 'reg_onboard'
                    force = (linVelocity.x -1, linVelocity.y, linVelocity.z)
                    if own['dropinTimer'] == 0:
                        own.setLinearVelocity(force, True)
            if STANCE == 1:
                killall()
                if own['dropinCol'] == True:
                    own['requestAction'] = 'fak_dropin'
                    own['dropinTimer'] = 60
                else:
                    own['requestAction'] = 'fak_onboard'
                    force = (linVelocity.x +1, linVelocity.y, linVelocity.z)
                    if own['dropinTimer'] == 0:
                        own.setLinearVelocity(force, True)

        else:
            num = own['dropinTimer']
            if num > 0:
                num = num - 1
            own['dropinTimer'] = num

    def offboard():
        wheel1.stopAction(2)
        wheel2.stopAction(2)
        wheel3.stopAction(2)
        wheel4.stopAction(2)
        #cont.deactivate(own.actuators["sroll"])
        #sact = own.actuators["sroll"]
        #sact.volume = .0001
        #own.actuators["sroll"].stopSound()
        cam['sndmgr'].stop_sound(['roll'])
        own['offboard_vel'] = own.linearVelocity
        own['walk_timer'] = 0

    def resetjumpstance():
        if LAST_GRIND == True and grindHit == False:
            own["lF_ground_frame"] = own['framenum']
            lfg = own["lF_ground_frame"]
            #print(lfg, "----- this?")
            if own['jump_stance'] != 3:
                own['jump_stance'] = 3

    def grass():
        try:
            if 'grass' in r_ground.hitObject:
                linVel = own.getLinearVelocity(True)
                linvelx = linVel.x * dict['grass_mult']
                own.setLinearVelocity((linvelx, linVel.y, linVel.z), 1)
        except:
            pass

    def grindoutair():
        skippy = 0
        linVel = own.getLinearVelocity(True)
        STANCE = own['stance']
        if own['LAST_GRIND'] == True and grindHit == True:
            own['lg_stance'] = STANCE
        lg_stance = own['lg_stance']
        own.actuators["grindoutair"].useLocalLinV = True
        if own['grindjumpturn'] == True and own['grindCountdown'] > 12 and skippy == 0:
            if 'fak' in own['l_actionState']:
                STANCE = 1
                own['lg_stance'] = 1
                lg_stance = 1
                #print('changing stance 1')
            if 'reg' in own['l_actionState']:
                STANCE = 0
                own['lg_stance'] = 0
                lg_stance = 0
                #print('changing stance 0')
            own['stance'] = STANCE
            grindoutspeed = .1
            grindoutspeed2 = 5.4
            if lLR > turnsens:
                if own['last_grindpos'] == 'reg_5050':
                    if STANCE == 0:
                        own.actuators["grindoutair"].linV = [0, grindoutspeed, 0]
                    else:
                        own.actuators["grindoutair"].linV = [0, -grindoutspeed, 0]
                    cont.activate(own.actuators["grindoutair"])

            if lLR < -turnsens:
                if own['last_grindpos'] == 'reg_5050':
                    if STANCE == 0:
                        own.actuators["grindoutair"].linV = [0, -grindoutspeed, 0]
                    else:
                        own.actuators["grindoutair"].linV = [0, grindoutspeed, 0]
                    cont.activate(own.actuators["grindoutair"])
            if lUD > turnsens:
                js = own['lg_stance']
                linvelx = own.getLinearVelocity(True)
                if own['last_grindpos'] == 'reg_board':
                    if js == 1:
                       own.actuators["grindoutair"].linV = [-grindoutspeed, 0, 0]
                       cont.activate(own.actuators["grindoutair"])
                    else:
                        own.actuators["grindoutair"].linV = [grindoutspeed, 0, 0]
                        cont.activate(own.actuators["grindoutair"])
            if lUD < -turnsens:
                js = own['lg_stance']
                if own['last_grindpos'] == 'reg_board':
                    if js == 0:
                        own.actuators["grindoutair"].linV = [-grindoutspeed, 0, 0]
                        cont.activate(own.actuators["grindoutair"])
                    else:
                        own.actuators["grindoutair"].linV = [grindoutspeed, 0, 0]
                        cont.activate(own.actuators["grindoutair"])

        if LAST_GRIND == False and onGround and own['grindjumpturn'] == True and own['grindCountdown'] < 1:
            own['grindjumpturn'] = False
        if onGround:
            own['grind_jump'] = False

        if own['grindjumpturn'] == False or own['grindCountdown'] < 19:
            cont.deactivate(own.actuators["grindoutair"])
    #print(STANCE)
    def set_last_grindpos():
        if own['grindpos'] != None:
            own['last_grindpos'] = own['grindpos']
    def air_pos():
        grindpos = own['grindpos']
        GRAB_ON = own["GRAB_ON"]
        wr = own["wallride"]
        jump_timer = own['jump_timer']
        if rUD > .040 and not onGround and GRAB_ON == False and wr == None and jump_timer < 30:
            if STANCE == 0:
                own['requestAction'] = 'reg_air_tail'
            else:
                own['requestAction'] = 'fak_air_tail'
        elif rUD < -.040 and not onGround and GRAB_ON == False and wr == None and jump_timer < 30:
            if STANCE == 0:
                own['requestAction'] = 'reg_air_nose'
            else:
                own['requestAction'] = 'fak_air_nose'

    def air_turn_boost():
        pass

    def revert():
        local = True
        rot = [0.0, 0.0, 3.14]
        own.applyRotation(rot, local)
        framenum = own['framenum']
        last_ground_frame = own['lF_ground_frame']
        lF_air_frame = own['lF_air_frame']
        frames_since_ground = framenum - lF_air_frame
        if own['manual_v2_type'] == 'reg manual':
            own['requestAction'] = 'reg_manual_revert_ccw'
        elif own['manual_v2_type'] == 'reg nose manual':
            own['requestAction'] = 'reg_nmanual_revert_ccw'
        elif own['manual_v2_type'] == 'fak manual':
            own['requestAction'] = 'fak_manual_revert_ccw'
        elif own['manual_v2_type'] == 'fak nose manual':
            own['requestAction'] = 'fak_nmanual_revert_ccw'
        else:
            if STANCE == 0:
                own['requestAction'] = 'revert1'
            else:
                own['requestAction'] = 'fak_revert1'
        own['revert_timer'] = 20
        #cont.activate(own.actuators["revertSound"])
        cam['sndmgr'].queue_sound(['revert', own, cam])
        own['revert_sound'] = 1
        dict['trick_string'] = 'Revert'

    def revert2():
        local = True
        rot = [0.0, 0.0, -3.14]
        own.applyRotation(rot, local)
        if own['manual_v2_type'] == 'reg manual':
            own['requestAction'] = 'reg_manual_revert_cw'
        elif own['manual_v2_type'] == 'reg nose manual':
            own['requestAction'] = 'reg_nmanual_revert_cw'
        elif own['manual_v2_type'] == 'fak manual':
            own['requestAction'] = 'fak_manual_revert_cw'
        elif own['manual_v2_type'] == 'fak nose manual':
            own['requestAction'] = 'fak_nmanual_revert_cw'
        else:
            if STANCE == 0:
                own['requestAction'] = 'revert2'
            else:
                own['requestAction'] = 'fak_revert2'
        own['revert_timer'] = 20
        #cont.activate(own.actuators["revertSound"])
        cam['sndmgr'].queue_sound(['revert', own, cam])
        own['revert_sound'] = 1
        dict['trick_string'] = 'Revert'

    def revert3():
        local = True
        rot = [0.0, 0.0, 3.14]
        own.applyRotation(rot, local)
        if own['manual_v2_type'] == 'reg manual':
            own['requestAction'] = 'reg_manual_revert_ccw'
        elif own['manual_v2_type'] == 'reg nose manual':
            own['requestAction'] = 'reg_nmanual_revert_ccw'
        elif own['manual_v2_type'] == 'fak manual':
            own['requestAction'] = 'fak_manual_revert_ccw'
        elif own['manual_v2_type'] == 'fak nose manual':
            own['requestAction'] = 'fak_nmanual_revert_ccw'
        else:
            if STANCE == 0:
                own['requestAction'] = 'revert3'
            else:
                own['requestAction'] = 'fak_revert1'
        own['revert_timer'] = 20
        #cont.activate(own.actuators["revertSound"])
        cam['sndmgr'].queue_sound(['revert', own, cam])
        own['revert_sound'] = 1
        dict['trick_string'] = 'Revert'

    def revert4():
        local = True
        rot = [0.0, 0.0, 3.14]
        own.applyRotation(rot, local)
        if own['manual_v2_type'] == 'reg manual':
            own['requestAction'] = 'reg_manual_revert_cw'
        elif own['manual_v2_type'] == 'reg nose manual':
            own['requestAction'] = 'reg_nmanual_revert_cw'
        elif own['manual_v2_type'] == 'fak manual':
            own['requestAction'] = 'fak_manual_revert_cw'
        elif own['manual_v2_type'] == 'fak nose manual':
            own['requestAction'] = 'fak_nmanual_revert_cw'
        else:
            if STANCE == 0:
                own['requestAction'] = 'revert4'
            else:
                own['requestAction'] = 'fak_revert2'
        own['revert_timer'] = 20
        #cont.activate(own.actuators["revertSound"])
        cam['sndmgr'].queue_sound(['revert', own, cam])
        own['revert_sound'] = 1
        dict['trick_string'] = 'Revert'

    def powerslide():
        own['powerslide_counter'] = own['powerslide_counter'] + 1
        since_walk = own['framenum'] - own['last_walk_frame']
        since_grind = own['framenum'] - own['last_grind_frame']
        if own['powerslide_counter'] > 15 and since_walk > 100 and since_grind > 40:
            own['powerslide_on'] = 1
            if STANCE == 0:
                own['powerslide'] = "reg2"
            if STANCE == 1:
                own['powerslide'] = "fak1"
            linVelocity4 = own.getLinearVelocity(True)
            if own['powerslide_counter'] > 15 and own['powerslide_counter'] < 18:
                newx = linVelocity4.x * .9
            else:
                newx = linVelocity4.x * .98
            force = [newx, linVelocity4.y, linVelocity4.z]
            own.setLinearVelocity(force, True)
    def powerslide2():
        own['powerslide_counter'] = own['powerslide_counter'] + 1
        since_walk = own['framenum'] - own['last_walk_frame']
        since_grind = own['framenum'] - own['last_grind_frame']
        if own['powerslide_counter'] > 15 and since_walk > 100 and since_grind > 100:
            own['powerslide_on'] = 1
            if STANCE == 0:
                own['powerslide'] = "reg1"
            if STANCE == 1:
                own['powerslide'] = "fak2"
            linVelocity4 = own.getLinearVelocity(True)
            if own['powerslide_counter'] > 15 and own['powerslide_counter'] < 18:
                newx = linVelocity4.x * .9
            else:
                newx = linVelocity4.x * .98
            force = [newx, linVelocity4.y, linVelocity4.z]
            own.setLinearVelocity(force, True)

    def powerslide_state():
        if own['powerslide_on'] == 1:
            if own['powerslide'] == "reg2":
                own['requestAction'] = 'reg_fs_powerslide'
            if own['powerslide'] == "fak1":
                own['requestAction'] = 'fak_fs_powerslide'
            if own['powerslide'] == "reg1":
                own['requestAction'] = 'reg_powerslide'
            if own['powerslide'] == "fak2":
                own['requestAction'] = 'fak_powerslide'

    def powerslide_sound():
        sact = own.actuators["powerslide_sound"]
        if own['powerslide_on'] == 1:
            sact = own.actuators["powerslide_sound"]
            sact.volume = .2
            new_pitch = (abs(linVelocity.x) / 3)
            if new_pitch < 1.1 and new_pitch > .5:
                sact.pitch = new_pitch
            elif new_pitch <= .5:
                sact.pitch = .5
            else:
                sact.pitch = 1.1
            cont.activate(own.actuators["powerslide_sound"])
        else:
            sact.volume = .0
            cont.deactivate(own.actuators["powerslide_sound"])

    def check_powerslide():
        try:
            own['last_powerslide_on'] = own['powerslide_on']
        except:
            pass
        psxvel = abs(linVelocity.x)
        if ((lUD > .08 or lUD < -.08) and own['manual_v2'] == False and onGround and psxvel > .1 and own["Pump"] == False) and rUD < turnsens and rUD > -turnsens and rLR < turnsens and rLR > - turnsens:
            if lUD > .08:
                powerslide()
            else:
                powerslide2()
        else:
            own['powerslide_on'] = 0
            own['powerslide_counter'] = 0
            if own['last_powerslide_on'] == 0:
                own['powerslide'] = None

    def killopos():
        pass



    stopAnims()
    #nextframe()
    air()
    stance()
    turn()
    #rotmult()
    airup()
    onramp()
    #speedmult()
    coping()
    resetjumpstance()
    grass()
    grindoutair()
    set_last_grindpos()
    air_pos()
    air_turn_boost()
    record_grindsound()
    check_powerslide()
    powerslide_state()
    powerslide_sound()
    killopos()
    grind()
    #onboard()




    ##################
    ###realcontrols###
    ##################
    #       q1
    #    q8    q2
    # q7          q3
    #    q6    q4
    #       q5
    ##################
    
    own['set_revert_timer'] = 0
    ground_since = own["framenum"] - own['lF_air_frame']
    if (ground_since > 80 and frames_since_grinding > 40) and ((rLR > -turnsens and rLR < turnsens and rUD > -turnsens and rUD < turnsens) or own['manual'] == 1):

        if sl.q3 < sl.q4 < sl.q5 < sl.q6 < sl.q7:
            if onGround:
                revert()
                sl.reset()
                own['set_revert_timer'] = 1
        if sl.q3 > sl.q4 > sl.q5 > sl.q6 > sl.q7:
            if onGround:
                revert2()
                sl.reset()
                own['set_revert_timer'] = 1
        if sl.q7 < sl.q8 < sl.q1 < sl.q2 < sl.q3:        
            if onGround:
                revert3()
                own['set_revert_timer'] = 1
                sl.reset()
        if sl.q7 > sl.q8 > sl.q1 > sl.q2 > sl.q3:
            if onGround:
                revert4()
                own['set_revert_timer'] = 1
                sl.reset()

    if rUD > .070 or dict['kb_space'] == 2:
        oposin()
    ground_since = own["framenum"] - own['lF_air_frame']
    if ((rTrig > 0.02 or dict['kb_lsh'] == 2) and GRAB_ON == False and onGround) or (lTrig > 0.02 and GRAB_ON == False and onGround) and ground_since > 10:
        pump()
    else:
        own["lastPump"] = False
        own["Pump"] = False
    own['jump_stance'] = STANCE
    if rUD > .02 and grindHit == True:
        grindpos = own['grindpos']
        jumpstance = own["jump_stance"]
        if LAST_GRIND == False:
            if grindpos == "reg_stall":
                grindtype("nose_stall") #change
            elif grindpos == "fak_stall":
                grindtype("nose_stall")
            elif grindpos == "reg_5050":
                if jumpstance != 3:
                    if jumpstance == 0:
                        if rLR > .03:
                            grindtype("reg_tailgR")
                        elif rLR < -.03:
                            grindtype("reg_tailgL")
                        else:
                            grindtype("reg_tailg")
                    if jumpstance == 1:
                        if rLR > .03:
                            grindtype("fak_tailgR")
                        elif rLR < -.03:
                            grindtype("fak_tailgL")
                        else:
                            grindtype("fak_tailg")
                else:
                    if STANCE == 0:
                        if rLR > .03:
                            grindtype("reg_tailgR")
                        elif rLR < -.03:
                            grindtype("reg_tailgL")
                        else:
                            grindtype("reg_tailg")
                    if STANCE == 1:
                        grindtype("fak_tailg")
            elif grindpos == "reg_board":
                if jumpstance != 3:
                    if jumpstance == 0:
                        grindtype("reg_tailslide")
                    if jumpstance == 1:
                        grindtype("fak_tailslide")
                    STANCE = jumpstance
                    own['stance'] = STANCE
                else:
                    if STANCE == 0:
                        grindtype("reg_tailslide")
                    if STANCE == 1:
                        grindtype("fak_tailslide")
            else:
                if jumpstance == 0:
                    grindtype("reg_tailg")
                if jumpstance == 1:
                    grindtype("fak_tailg")

    #if q5oncd > 0:
    if sl.q5 > 0:
        own["last_Opos"] = False
    #q1
    if rUD < -0.070:
        noposin()
    if rUD < -0.020 and grindHit == True:
        grindpos = own['grindpos']
        jumpstance = own["jump_stance"]
        if LAST_GRIND == False:
            if grindpos == "reg_5050":
                if jumpstance != 3:
                    if jumpstance == 0:
                        if rLR > .02:
                            grindtype("reg_nosegR")
                        elif rLR < -.02:
                            grindtype("reg_nosegL")
                        else:
                            grindtype("reg_noseg")
                    if jumpstance == 1:
                        if rLR > .02:
                            grindtype("fak_nosegR")
                        elif rLR < -.02:
                            grindtype("fak_nosegL")
                        else:
                            grindtype("fak_noseg")
                else:
                    if STANCE == 0:
                        if rLR > .02:
                            grindtype("reg_nosegR")
                        elif rLR < -.02:
                            grindtype("reg_nosegL")
                        else:
                            grindtype("reg_noseg")
                    if STANCE == 1:
                        if rLR > .02:
                            grindtype("fak_nosegR")
                        elif rLR < -.02:
                            grindtype("fak_nosegL")
                        else:
                            grindtype("fak_noseg")
            elif grindpos == "reg_board":
                if jumpstance != 3:
                    if jumpstance == 0:
                        grindtype("reg_noseslide")
                    if jumpstance == 1:
                        grindtype("fak_noseslide")
                    STANCE = jumpstance
                    own['stance'] = STANCE
                else:
                    if STANCE == 0:
                        grindtype("reg_noseslide")
                    if STANCE == 1:
                        grindtype("fak_noseslide")

    if rUD > -0.020 and rUD <= .020 and LAST_GRIND == False:
        if own['grindpos'] == 'reg_5050':
            grindtype("reg_5050")
        if own['grindpos'] == 'fak_5050':
            grindtype("fak_5050")

    if sl.q1 > 0:
        own["last_nOpos"] = False

###keyboard fliptricks
    if dict['kb_space'] == 3:
        if dict['kb_a'] == 2:
            kickflip()
        elif dict['kb_d'] == 2:
            heelflip()
        elif dict['kb_w'] == 2:
            fsshuvit()
        elif dict['kb_s'] == 2:
            shuvit()
        else:
            aollie()


##########################################
    #trick calls
##########################################
    
    # hard flip
    # 6 > 5 > 8
    if sr.q6 > 0 and sr.q5 > 0 and sr.q8 > 0 and sr.q6 <= sr.q5 <= sr.q8:   
        print ("Hardflip --- 2")
        dict['trick_string'] = 'Hardflip'
        hardflip()
        sr.reset()

    # inward heelflip
    # 4 > 5 > 2
    if sr.q4 > 0 and sr.q5 > 0 and sr.q2 > 0 and sr.q4 <= sr.q5 <= sr.q2:
        inward_heelflip()
        sr.reset()

    # nollie hard flip
    # 8 > 1 > 6
    if sr.q8 > 0 and sr.q1 > 0 and sr.q6 > 0 and sr.q8 <= sr.q1 <= sr.q6:
        nollie_hardflip()
        sr.reset()

    # nollie inward heelflip
    # 2 > 1 > 4
    if sr.q2 > 0 and sr.q1 > 0 and sr.q4 > 0 and sr.q2 <= sr.q1 <= sr.q4:
        nollie_inward_heelflip()
        sr.reset()

    #360 shuvit
    if sr.q3 > 0 and sr.q4 > 0 and sr.q5 > 0 and sr.q6 > 0 and sr.q7 > 0 and sr.q7 <= sr.q6 <= sr.q5 <= sr.q4 <= sr.q3:
        fsshuvit360()
        sr.reset()

    #360 fs shuvit
    if sr.q3 > 0 and sr.q4 > 0 and sr.q5 > 0 and sr.q6 > 0 and sr.q7 > 0 and sr.q7 >= sr.q6 >= sr.q5 >= sr.q4 >= sr.q3:
        shuvit360()
        sr.reset()

    #nollie 360 shuvit
    if sr.q7 > 0 and sr.q8 > 0 and sr.q1 > 0 and sr.q2 > 0 and sr.q3 > 0 and sr.q7 <= sr.q8 <= sr.q1 <= sr.q2 <= sr.q3:
        nollie_shuvit360()
        sr.reset()
        print('*nollie 360 shuvit')

    #nollie fs 360 shuvit
    if sr.q7 > 0 and sr.q8 > 0 and sr.q1 > 0 and sr.q2 > 0 and sr.q3 > 0 and sr.q3 <= sr.q2 <= sr.q1 <= sr.q8 <= sr.q7:
        nollie_fsshuvit360()
        sr.reset()
        print('*nollie fs 360 shuvit')

    # varial heelflip
    # 7 < 6 < 2
    if sr.q7 > 0 and sr.q6 > 0 and sr.q2 > 0 and sr.q7 <= sr.q6 <= sr.q2:
        varial_heelflip()
        sr.reset()

    # varial kickflip
    # 3 < 4 < 8
    if sr.q3 > 0 and sr.q4 > 0 and sr.q8 > 0 and sr.q3 <= sr.q4 <= sr.q8: 
        varial_kickflip()
        sr.reset()

    # nollie varial kickflip
    # 3 < 2 < 6
    if sr.q3 > 0 and sr.q2 > 0 and sr.q6 > 0 and sr.q3 <= sr.q2 <= sr.q6:
        nollie_varial_kickflip()
        sr.reset()

    # nollie varial heelflip
    # 7 < 8 < 4
    if sr.q7 > 0 and sr.q8 > 0 and sr.q4 > 0 and sr.q7 <= sr.q8 <= sr.q4: 
        nollie_varial_heelflip()
        sr.reset()

    #ollie
    if sr.q5 > 0 and sr.q1 > 0 and sr.q5 < sr.q1:
        aollie()
        sr.reset()        

    #nollie
    if sr.q5 > 0 and sr.q1 > 0 and sr.q5 > sr.q1:
        nollie()
        sr.reset()

    #kickflip
    if sr.q5 > 0 and sr.q8 > 0 and sr.q5 < sr.q8:
        kickflip()
        sr.reset()

    #nollie kickflip
    if sr.q1 > 0 and sr.q6 > 0 and sr.q2 < sr.q6:
        nollie_kickflip()
        sr.reset()

    #heelflip
    if sr.q5 > 0 and sr.q2 > 0 and sr.q5 < sr.q2:
        heelflip()
        sr.reset()

    #nollie_heelflip
    if sr.q1 > 0 and sr.q4 > 0 and sr.q1 < sr.q4:
        nollie_heelflip()
        sr.reset()

    #shuvit
    if sr.q7 > 0 and sr.q5 > 0 and sr.q6 > 0 and sr.q5 <= sr.q6 <= sr.q7:
        print(sr.q3, sr.q4, sr.q5, sr.q6, sr.q7)
        shuvit()
        sr.reset()

    #nollie_shuvit
    if sr.q7 > 0 and sr.q1 > 0 and sr.q8 > 0 and sr.q1 <= sr.q8 <= sr.q7:
        nollie_shuvit()
        sr.reset()
        print('*nollie shuvit')

    #fsshuvit
    if sr.q4 > 0 and sr.q5 > 0 and sr.q3 > 0 and sr.q5 <= sr.q4 <= sr.q3:
        fsshuvit()
        sr.reset()

    #nollie_fsshuvit
    if sr.q3 > 0 and sr.q1 > 0 and sr.q2 > 0 and sr.q1 <= sr.q2 <= sr.q3:
        nollie_fsshuvit()
        sr.reset()
        print('*nollie fs shuvit')

    def hippy_jump():
        STANCE = own["stance"]
        if own['hippy'] == 1 and own['last_hippy'] == 0:
            if STANCE == 0:
                own['requestAction'] = 'reg_hippy_in'
            if STANCE == 1:
                own['requestAction'] = 'fak_hippy_in'
        if own['hippy'] == 1 and own['last_hippy'] == 1:
            if STANCE == 0:
                own['requestAction'] = 'reg_hippy_in'
            if STANCE == 1:
                own['requestAction'] = 'fak_hippy_in'
        if own['hippy'] == 0 and own['last_hippy'] == 1:
            if rLR > turnsens:
                local = True
                rot = [0.0, 0.0, 3.14]
                own.applyRotation(rot, local)
                if STANCE == 0:
                    own['requestAction'] = 'reg_hippy_ncw'
                if STANCE == 1:
                    own['requestAction'] = 'fak_hippy_ncw'
            elif rLR < -turnsens:
                local = True
                rot = [0.0, 0.0, -3.14]
                own.applyRotation(rot, local)
                if STANCE == 0:
                    own['requestAction'] = 'fak_hippy_nccw'
                if STANCE == 1:
                    own['requestAction'] = 'reg_hippy_nccw'
    ##straight
            else:
                if STANCE == 0:
                    own['requestAction'] = 'reg_hippy'
                if STANCE == 1:
                    own['requestAction'] = 'fak_hippy'

    #pushing and hippy jumps
    since_a = frame - lastaf
    since_x = frame - lastxf
    cush = 10
    hippy = 0
    own['hippy'] = 0

    if xBut == 1 and aBut == 1 and (lTrig < 0.02 and rTrig < 0.02) and onGround:
        #F"hippy")
        hippy = 1
        own['hippy'] = 1

    hippy_jump()
    if ((since_a > cush and aBut == 1 and lasta == 1) or dict['kb_ua'] == 2) and (lTrig < 0.02 and rTrig < 0.02) and own['last_hippy'] == 0:
        if xBut == 0 and hippy == 0 and (lastaBut_ground == True or dict['kb_ua'] == 2):
            push()
    if since_x > cush and xBut == 1 and lastx == 1 and (lTrig < 0.02 and rTrig < 0.02) and own['last_hippy'] == 0:
        if aBut == 0 and hippy == 0 and lastxBut_ground == True:
            push_goof()
    #cavemans
    if LAST_GRIND == False and grindHit == False and onGround:
        if (aBut == 0 and own['lastaBut_ground'] == 1) and lTrig > 0.02:
            brfoot()
        if (aBut == 0 and own['lastaBut_ground'] == 1) and rTrig > 0.02:
            frfoot()
        if (xBut == 0 and own['lastxBut_ground'] == 1) and lTrig > 0.02:
            blfoot()
        if (xBut == 0 and own['lastxBut_ground'] == 1) and rTrig > 0.02:
            flfoot()

    #push b button
    if bBut == 1:
        if linVelocity.x > .05 or linVelocity.x < -.05:
            stop()
        elif linVelocity.x < .05 or linVelocity.x > -.05:
            if own["lastStop"] == True:
                own["lastStop"] = False
    elif bBut == 0:
        own["lastStop"] = False
    ##### falls
    if own['fall'] == 1:
        offboard()
        own['getoffboard'] = True
    ####

    #y button
    if (own['lasty'] == 1 and yBut == 0) or dict['kb_q'] == 3:
        offboard()

    if own["GRAB_ON"] == True:
        playing = skater.isPlayingAction(fliplay)
        if playing == 1:
            frame = skater.getActionFrame(fliplay)
            if frame > 16:
                skater.stopAction(fliplay)

    flipping = skater.isPlayingAction(fliplay)

    #reg_ollie_north
    if rTrig < .02 and lTrig < .02 and not onGround and flipping == False and aBut == True:
        if STANCE == 0:
            own['requestAction'] = 'reg_ollie_south'
        else:
            own['requestAction'] = 'fak_ollie_north'
    if rTrig < .02 and lTrig < .02 and not onGround and flipping == False and xBut == True:
        if STANCE == 0:
            own['requestAction'] = 'reg_ollie_north'
        else:
            own['requestAction'] = 'fak_ollie_south'

    #frontside grab
    if rTrig > 0.02 and not onGround and flipping == False:
        GRAB_ON = True
        own["GRAB_ON"] = GRAB_ON
        #print(rTrig, GRAB_ON)
        if aBut == False and xBut == False:
            if STANCE == False and rUD >= -.07 and rUD <= .07:
                frontside_grab_on()
            elif STANCE == True and rUD >= -.07 and rUD <= .07:
                fakbackside_grab_on()
            #front_nose
            if STANCE == True and rUD < -0.070:
                fak_backside_nose_grab_on()
            #front_tail
            elif STANCE == True and rUD > 0.07:
                fak_backside_tail_grab_on()
            #
            if STANCE == False and rUD < -0.070:
                if bBut == True:
                    reg_airwalk()
                else:
                    frontside_nose_grab_on()
            #front_tail
            elif STANCE == False and rUD > 0.07:
                frontside_tail_grab_on()
        elif aBut == True and xBut == False:
            if STANCE == False:
                reg_fsonefoot_grab_on()
            else:
                fak_onefoot_grab_on()
        elif aBut == False and xBut == True:
            if STANCE == False:
                reg_frigid_grab_on()
            else:
                fak_judo_grab_on()

    #backside grab
    if lTrig > 0.02 and not onGround and flipping == False:
        GRAB_ON = True
        own["GRAB_ON"] = GRAB_ON
        #print(rTrig)
        if aBut == False and xBut == False:
            if STANCE == False and rUD >= -.07 and rUD <= .07:
                backside_grab_on()
            elif STANCE == True and rUD >= -.07 and rUD <= .07:
                fakfrontside_grab_on()
            #front_nose
            if STANCE == True and rUD < -0.070:
                fak_frontside_nose_grab_on()
            #front_tail
            elif STANCE == True and rUD > 0.07:
                fak_frontside_tail_grab_on()
            #front_nose
            if STANCE == False and rUD < -0.070:
                if bBut == True:
                    reg_airwalk()
                else:
                    backside_nose_grab_on()
            #front_tail
            elif STANCE == False and rUD > 0.07:
                backside_tail_grab_on()

        elif aBut == True and xBut == False:
            if STANCE == False:
                reg_judo_grab_on()
            else:
                fak_frigid_grab_on()
        elif aBut == False and xBut == True:
            if STANCE == False:
                reg_onefoot_grab_on()
            else:
                fak_frigid_grab_on()

    #kill grabs
    if lTrig <= 0.02 and GRAB_ON == True and lTrig >= -.02 and rTrig <= .02 and rTrig >= -.2:
        GRAB_ON = False
        own["GRAB_ON"] = GRAB_ON
        GRAB_PLAYED = False
        own["GRAB_PLAYED"] = GRAB_PLAYED

    #frontside pump #backside pump
    def footplant():
        framenum = own['framenum']
        last_ground_frame = own['lF_ground_frame']
        lF_air_frame = own['lF_air_frame']
        frames_since_ground = framenum - lF_air_frame
        frames_since_grind = framenum - own['last_grind_frame']
        if grindHit == True and aBut == True and ground_since < 10:
            own.linearVelocity.x = 0
            own.linearVelocity.y = 0
            own['footplant_on'] = 1
        if grindHit == False:
            own['footplant_on'] = 0
        if own['footplant_on'] == True and rUD < turnsens and rUD > -turnsens:
            own.linearVelocity.x = 0
            own.linearVelocity.y = 0
        if own['footplant_on'] == 1 and own['last_footplant'] == True and own['jump_timer'] < 50:
            if skater.isPlayingAction(2925) == False:
                if STANCE == 0:
                    own['requestAction'] = 'reg_fp_rback'
                if STANCE == 1:
                    own['requestAction'] = 'fak_fp_rback'

    def invert():
        if grindHit == True and own['grindpos'] == 'reg_board' and gsf < 10 and lBump == 1 and rBump == 0:
            
            own['invert_on'] = 1
            own.linearVelocity.x = 0
            own.linearVelocity.y = 0
        else:
            if lBump == 0:
                own['invert_on'] = 0    
        if own["coping"] == 1 and own['invert_on'] == 1:
            if own['invert_on'] == 1 and own['last_invert'] == False:
                cont.activate(own.actuators['invertOn_sound'])
                if STANCE == False:
                    own['invert_type'] = "reg_back_invert_in"
                if STANCE == True:
                    own['invert_type'] = "fak_fr_invert"
        if own['invert_on'] == 0 and own['last_invert'] == True:
            own['last_invert_frame'] = frame
            cont.activate(own.actuators['invertOff_Sound'])

        if own['invert_on'] == 1 and own['invert_type'] != None:
            if own['invert_type'] == "reg_back_invert_in":
                own['requestAction'] = 'reg_back_invert'

            if own['invert_type'] == "fak_fr_invert":
                own['requestAction'] = 'fak_fr_invert'
        lif = own['last_invert_frame']
        if frame - lif > 3 and own['invert_on'] == 0:
            own['invert_type'] = None

        #print(own['requestAction'])

    invert()
    footplant()
    #print(own['invert_on'], 'invert_on')
    def reset_pos():
        #reset
        if ddPad == 1:
            spawn_pos = own['spawn_pos']
            spawn_rot = own['spawn_rot']
            spawn_cam_pos = own['spawn_cam_pos']
            spawn_cam_rot = own['spawn_cam_rot']
            try:
                own.worldPosition = (spawn_pos[0], spawn_pos[1], (spawn_pos[2] + .1))
                own.worldOrientation = [[spawn_rot[0][0], spawn_rot[0][1],spawn_rot[0][2]], [spawn_rot[1][0],spawn_rot[1][1], spawn_rot[1][2]], [0.0, 0.0, 1.0]]
                cam.worldPosition = (spawn_cam_pos[0], spawn_cam_pos[1], (spawn_cam_pos[2]))
                cam.worldOrientation = [[spawn_cam_rot[0][0], spawn_cam_rot[0][1], spawn_cam_rot[0][2]], [spawn_cam_rot[1][0], spawn_cam_rot[1][1], spawn_cam_rot[1][2]], [0.0, 0.0, 1.0]]
            except:
                own.worldPosition = (0, 0, .1)
                own.worldOrientation = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]

            if own["spawn_stance"] == 1:
                own.setLinearVelocity([.1,0,0], 1)

            else:
                own.setLinearVelocity([-.1,0,0], 1)

        if udPad == 1:

            own['spawn_pos'] = [own.worldPosition[0], own.worldPosition[1], own.worldPosition[2]]
            own['spawn_rot'] = [[own.worldOrientation[0][0], own.worldOrientation[0][1],own.worldOrientation[0][2]], [own.worldOrientation[1][0], own.worldOrientation[1][1], own.worldOrientation[1][2]], own.worldOrientation[2][2]]
            own['spawn_cam_pos'] = [cam.worldPosition[0], cam.worldPosition[1], cam.worldPosition[2]]
            own['spawn_cam_rot'] = [[cam.worldOrientation[0][0], cam.worldOrientation[0][1],cam.worldOrientation[0][2]], [cam.worldOrientation[1][0], cam.worldOrientation[1][1], cam.worldOrientation[1][2]], cam.worldOrientation[2][2]]
            stance = own["stance"]
            own["spawn_stance"] = stance

    #start button
    if stBut == True:
        #own.actuators["sroll"].volume = .0001
        #cont.deactivate(own.actuators["sroll"])
        #own.actuators["sroll"].stopSound()
        cam['sndmgr'].stop_sound(['roll'])
    if (stBut == False and dict['last_stBut'] == True) or dict['kb_en'] == 1:
        if own['pause_on'] == True:
            own['pause_on'] = False
        else:
            own['pause_on'] = True

    def ylimit():
        lgf = own['last_grind_frame']
        frame = own['framenum']
        frames_since_grinding = frame - lgf
        ylimit = dict['ylimit']
        if ground_since < 20 and ground_since > 1 and grindHit == 0 and own['jump_timer'] < 40:
            ylimit = ylimit + ((ground_since - 15) * -.005)

            yvel = own.linearVelocity.y
            yvel = yvel *.03
            if STANCE == 0:
                yvel *= -1
            try:
                if own.linearVelocity.y > .01 or own.linearVelocity.y < -.01:
                    own.applyRotation([0, 0, yvel], True)

                if ylimit > .99999:
                    ylimit = .9999
            except:
                pass
        else:
            if onGround and touched == False and grindHit == 0 and frames_since_grinding > 20:
                yvel = own.linearVelocity.y
                yvel = yvel *.03
                if STANCE == 0:
                    yvel *= -1
                try:
                    if own.linearVelocity.y > .01 or own.linearVelocity.y < -.01:
                        own.applyRotation([0, 0, yvel], True)

                except:
                    pass

    def getoffboard():
        lasty = own['lasty']
        getoffboard = own['getoffboard']
        if getoffboard == 1 and own['fall'] == False:
            own['getoffboard'] = 0
        if (yBut == False and lasty == True) or dict['kb_q'] == 3:
            own['getoffboard'] = 1

    def switchcam():
        pass
    #    if ltsBut == False and own['lastlts'] == True and rtsBut == False:
    #        if own['camera'] == 1:
    #            own['camera'] = 0
    #        else:
    #            own['camera'] = 1
    #    if rtsBut == False and own['lastrts'] == True and ltsBut == False:
    #        if own['camera'] == 2:
    #            own['camera'] = 0
    #        else:
    #            own['camera'] = 2
    #followcam
    def move_followcam():
        if own['camera'] == 2:
            #if rtsBut == False and own['lastrts'] == True:
            if dict['last_bkBut'] == True and bkBut == False:
                #print("activate move followcam")
                if own['move_followcam'] == False:
                    own['move_followcam'] = True
                else:
                    own['move_followcam'] = False
            if own['move_followcam'] == True:
                #act = followcam.actuators[
                camspeed1 = .015
                camspeed2 = .055
                camrot1 = .005
                camrot2 = .02
                #up
                if lUD < -0.080:
                    followcam.actuators["up"].dLoc = [0, 0, -camspeed2]
                    cont.activate(followcam.actuators["up"])
                else:
                    cont.deactivate(followcam.actuators["up"])
    #            #down
                if lUD > .080:
                    followcam.actuators["down"].dLoc = [0, 0, camspeed2]
                    cont.activate(followcam.actuators["down"])
                else:
                    cont.deactivate(followcam.actuators["down"])
    #            #left
                if lLR < -0.080:
                    followcam.actuators["left"].dLoc = [-camspeed2, 0, 0]
                    cont.activate(followcam.actuators["left"])
                else:
                    cont.deactivate(followcam.actuators["left"])
    #            #right
                if lLR > 0.080:
                    followcam.actuators["right"].dLoc = [camspeed2, 0, 0]
                    cont.activate(followcam.actuators["right"])
                else:
                    cont.deactivate(followcam.actuators["right"])
                #up
                if rUD < -0.080:
                    followcam.actuators["rotup"].dLoc = [0, 0, camrot2]
                    cont.activate(followcam.actuators["rotup"])
                else:
                    cont.deactivate(followcam.actuators["rotup"])
    #            #down
                if rUD > .080:
                    followcam.actuators["rotdown"].dLoc = [0, 0, -camrot2]
                    cont.activate(followcam.actuators["rotdown"])
                else:
                    cont.deactivate(followcam.actuators["rotdown"])
    #            #left
                if rLR < -0.080:
                    followcam.actuators["rotleft"].dRot = [0, 0, camrot2]
                    cont.activate(followcam.actuators["rotleft"])
                else:
                    cont.deactivate(followcam.actuators["rotleft"])
    #            #right
                if rLR > 0.080:
                    followcam.actuators["rotright"].dRot = [0, 0, -camrot2]
                    cont.activate(followcam.actuators["rotright"])
                else:
                    cont.deactivate(followcam.actuators["rotright"])

    #*********************************************

                if lUD > -0.080 and lUD < -0.030:
                    followcam.actuators["up"].dLoc = [0, 0, -camspeed1]
                    cont.activate(followcam.actuators["up"])
                else:
                    cont.deactivate(followcam.actuators["up"])
    #            #down
                if lUD < .080 and lUD > .03:
                    followcam.actuators["down"].dLoc = [0, 0, camspeed1]
                    cont.activate(followcam.actuators["down"])
                else:
                    cont.deactivate(followcam.actuators["down"])
    #            #left
                if lLR > -0.080 and lLR < -0.030:
                    followcam.actuators["left"].dLoc = [-camspeed1, 0, 0]
                    cont.activate(followcam.actuators["left"])
                else:
                    cont.deactivate(followcam.actuators["left"])
    #            #right
                if lLR < .080 and lLR > .03:
                    followcam.actuators["right"].dLoc = [camspeed1, 0, 0]
                    cont.activate(followcam.actuators["right"])
                else:
                    cont.deactivate(followcam.actuators["right"])
                #up
                if rUD > -0.080 and rUD < -0.030:
                    followcam.actuators["rotup"].dRot = [camrot1, 0, 0]
                    cont.activate(followcam.actuators["rotup"])
                else:
                    cont.deactivate(followcam.actuators["rotup"])
    #            #down
                if rUD < .080 and rUD > .03:
                    followcam.actuators["rotdown"].dRot = [-camrot1, 0, 0]
                    cont.activate(followcam.actuators["rotdown"])
                else:
                    cont.deactivate(followcam.actuators["rotdown"])
    #            #left
                if rLR > -0.080 and rLR < -0.030:
                    followcam.actuators["rotleft"].dRot = [0, 0, camrot1]
                    cont.activate(followcam.actuators["rotleft"])
                else:
                    cont.deactivate(followcam.actuators["rotleft"])
    #            #right
                if rLR < .080 and rLR > .03:
                    followcam.actuators["rotright"].dRot = [0, 0, -camrot1]
                    cont.activate(followcam.actuators["rotright"])
                else:
                    cont.deactivate(followcam.actuators["rotright"])
    def move_flycam():
        if own['camera'] == 1:
            if dict['last_bkBut'] == True and bkBut == False:
                if own['move_freecam'] == False:
                    own['move_freecam'] = True
                else:
                    own['move_freecam'] = False
            if own['move_freecam'] == True:
                camspeed1 = .015
                camspeed2 = .055
                camrot1 = .005
                camrot2 = .02
                #up
                if lUD < -0.080:
                    freecam.actuators["up"].dLoc = [0, 0, -camspeed2]
                    cont.activate(freecam.actuators["up"])
                else:
                    cont.deactivate(freecam.actuators["up"])
    #            #down
                if lUD > .080:
                    freecam.actuators["down"].dLoc = [0, 0, camspeed2]
                    cont.activate(freecam.actuators["down"])
                else:
                    cont.deactivate(freecam.actuators["down"])
    #            #left
                if lLR < -0.080:
                    freecam.actuators["left"].dLoc = [-camspeed2, 0, 0]
                    cont.activate(freecam.actuators["left"])
                else:
                    cont.deactivate(freecam.actuators["left"])
    #            #right
                if lLR > 0.080:
                    freecam.actuators["right"].dLoc = [camspeed2, 0, 0]
                    cont.activate(freecam.actuators["right"])
                else:
                    cont.deactivate(freecam.actuators["right"])
                #up
                if rUD < -0.080:
                    freecam.actuators["rotup"].dRot = [camrot2, 0, 0]
                    cont.activate(freecam.actuators["rotup"])
                else:
                    cont.deactivate(freecam.actuators["rotup"])
    #            #down
                if rUD > .080:
                    freecam.actuators["rotdown"].dRot = [-camrot2, 0, 0]
                    cont.activate(freecam.actuators["rotdown"])
                else:
                    cont.deactivate(freecam.actuators["rotdown"])
    #            #left
                if rLR < -0.080:
                    freecam.actuators["rotleft"].dRot = [0, 0, camrot2]
                    cont.activate(freecam.actuators["rotleft"])
                else:
                    cont.deactivate(freecam.actuators["rotleft"])
    #            #right
                if rLR > 0.080:
                    freecam.actuators["rotright"].dRot = [0, 0, -camrot2]
                    cont.activate(freecam.actuators["rotright"])
                else:
                    cont.deactivate(freecam.actuators["rotright"])

    #*********************************************

                if lUD > -0.080 and lUD < -0.030:
                    freecam.actuators["up"].dLoc = [0, 0, -camspeed1]
                    cont.activate(freecam.actuators["up"])
                else:
                    cont.deactivate(freecam.actuators["up"])
    #            #down
                if lUD < .080 and lUD > .03:
                    freecam.actuators["down"].dLoc = [0, 0, camspeed1]
                    cont.activate(freecam.actuators["down"])
                else:
                    cont.deactivate(freecam.actuators["down"])
    #            #left
                if lLR > -0.080 and lLR < -0.030:
                    freecam.actuators["left"].dLoc = [-camspeed1, 0, 0]
                    cont.activate(freecam.actuators["left"])
                else:
                    cont.deactivate(freecam.actuators["left"])
    #            #right
                if lLR < .080 and lLR > .03:
                    freecam.actuators["right"].dLoc = [camspeed1, 0, 0]
                    cont.activate(freecam.actuators["right"])
                else:
                    cont.deactivate(freecam.actuators["right"])
                #up
                if rUD > -0.080 and rUD < -0.030:
                    freecam.actuators["rotup"].dRot = [camrot1, 0, 0]
                    cont.activate(freecam.actuators["rotup"])
                else:
                    cont.deactivate(freecam.actuators["rotup"])
    #            #down
                if rUD < .080 and rUD > .03:
                    freecam.actuators["rotdown"].dRot = [-camrot1, 0, 0]
                    cont.activate(freecam.actuators["rotdown"])
                else:
                    cont.deactivate(freecam.actuators["rotdown"])
    #            #left
                if rLR > -0.080 and rLR < -0.030:
                    freecam.actuators["rotleft"].dRot = [0, 0, camrot1]
                    cont.activate(freecam.actuators["rotleft"])
                else:
                    cont.deactivate(freecam.actuators["rotleft"])
    #            #right
                if rLR < .080 and rLR > .03:
                    freecam.actuators["rotright"].dRot = [0, 0, -camrot1]
                    cont.activate(freecam.actuators["rotright"])
                else:
                    cont.deactivate(freecam.actuators["rotright"])

    if not onGround:
        own["lF_air_frame"] = own["framenum"]
    if onGround:
        own["lF_ground_frame"] = own["framenum"]

    if own['LAST_GRIND'] == True:
        own['last_grind_frame'] = own['framenum']
    if own['LAST_GRIND'] == False and onGround and own['jump_timer'] == 0:
        if own['jump_stance'] != 3:
            own['jump_stance'] = 3

    def control_calib():
        #controller calibration test
        scenes = bge.logic.getSceneList()
        controller_calib = [scene for scene in scenes if scene.name=="controller_calib"][0]
        cq1 = controller_calib.objects['q1']
        cq2 = controller_calib.objects['q2']
        cq3 = controller_calib.objects['q3']
        cq4 = controller_calib.objects['q4']
        cq5 = controller_calib.objects['q5']
        cq6 = controller_calib.objects['q6']
        cq7 = controller_calib.objects['q7']
        cq8 = controller_calib.objects['q8']
        if lq1on == 1:
            cq1["q1"] = 1
        else:
            cq1["q1"] = 0
        if lq2on == 1:
            cq2["q2"] = 1
        else:
            cq2["q2"] = 0
        if lq3on == 1:
            cq3["q3"] = 1
        else:
            cq3["q3"] = 0
        if lq4on == 1:
            cq4["q4"] = 1
        else:
            cq4["q4"] = 0
        if lq5on == 1:
            cq5["q5"] = 1
        else:
            cq5["q5"] = 0
        if lq6on == 1:
            cq6["q6"] = 1
        else:
            cq6["q6"] = 0
        if lq7on == 1:
            cq7["q7"] = 1
        else:
            cq7["q7"] = 0
        if lq8on == 1:
            cq8["q8"] = 1
        else:
            cq8["q8"] = 0
    def grind_turn():
        jumping = None
        gotcd = 25
        force = [0, 0, 0]
        grindHit = own['grindTouch']
        sincegrinding = own['framenum']  - own['grindstartFrame']
        STANCE = own['stance']
        if rUD > turnsens or rLR > turnsens or rUD < -turnsens or rLR < -turnsens:
            jumping = True
        if grindHit == False:
            cont.deactivate(own.actuators['grindoutRight'])
            cont.deactivate(own.actuators['grindoutLeft'])
        if (LAST_GRIND == True and jumping == None and sincegrinding > 35 and ground_since > 20) or own['invert_on'] == True and LAST_GRIND == 1:
            outloc = 0.022
            bsoutloc = .001#.07
            bsoutvel = .01#.1
            outrot = .06#2 #0.012
            outrot2 = .24
            outact = own.actuators["grindoutRight"]
            STANCE = own['grind_stance']
            if 'fak' in own['l_actionState']:
                STANCE = 1
            if 'reg' in own['l_actionState']:
                STANCE = 0
            if own['grindpos'] == 'reg_5050':
                if sl.q3 >= countdown or sl.q2 >= countdown or dict['kb_ra'] == 2:
                #if lq3on == 1 or lq2on or dict['kb_ra'] == 2:
                    if own['gt_cd2'] == 0:
                        own['gt_cd2'] = 60

                    if STANCE == True:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'fak right'
                    if STANCE == False:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'reg right'
                    own["grindoutturn"] = gotcd
                if sl.q7 >= countdown or sl.q8 >= countdown or dict['kb_la'] == 2:
                #if lq7on == 1 or lq8on or dict['kb_la'] == 2:
                    if own['gt_cd2'] == 0:
                        own['gt_cd2'] = 60
                    if STANCE == True:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'fak left'
                    if STANCE == False:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'reg left'
                    own["grindoutturn"] = gotcd
                #if lq4on == 1:
                if sl.q4 >= countdown:   
                    if own['gt_cd2'] == 0:
                        own['gt_cd2'] = 60
                    if STANCE == True:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'fak fak right'
                    if STANCE == False:
                        if own['grind_out_type'] == None:
                             own['grind_out_type'] = 'reg fak right'
                    own["grindoutturn"] = gotcd
                #if lq6on == 1:
                if sl.q6 >= countdown:
                    if own['gt_cd2'] == 0:
                        own['gt_cd2'] = 60
                    if STANCE == True:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'fak fak left'
                    if STANCE == False:
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'reg fak left'
                    own["grindoutturn"] = gotcd
            #use stance for 5050 and grindstance for boards
            if own['grindpos'] == 'reg_board':
                outvel = own.getLinearVelocity(1)
                outact.dLoc = [0, 0, 0]
                outact.dRot = [0, 0, 0]


                #if lq5on == 1 or dict['kb_da'] == 2:
                if sl.q5 >= countdown or dict['kb_da'] == 2:
                    if own['gt_cd2'] == 0:
                        own['gt_cd2'] = 60
                    if STANCE == True:

                        if own['footplant_on'] == True:
                            own.setLinearVelocity([(outvel.x + -bsoutvel), outvel.y, outvel.z], 1)
                            cont.activate(own.actuators["grindoutRight"])
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'bs fak back'
                        #own.applyRotation((0, .15, 0), True)
                        print('board out')
                    if STANCE == False:

                        if own['footplant_on'] == True:
                            own.setLinearVelocity([(outvel.x + bsoutvel), outvel.y, outvel.z], 1)
                            cont.activate(own.actuators["grindoutRight"])
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'bs reg back'
                        #own.applyRotation((0, -.15, 0), True)
                        print('board out')
                    own["grindoutturn"] = gotcd
                    own['invert_on'] = 0
                if sl.q1 >= countdown or dict['kb_ua'] == 2:
                #if lq1on == 1 or dict['kb_ua'] == 2:
                    if own['gt_cd2'] == 0:
                        own['gt_cd2'] = 60
                    if STANCE == True:
                        if own['footplant_on'] == True:
                            own.setLinearVelocity([(outvel.x + bsoutvel), outvel.y, outvel.z], 1)
                            #cont.activate(own.actuators["grindoutRight"])
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'bs fak forward'
                        #own.applyRotation((0, .15, 0), True)
                    if STANCE == False:
                        if own['footplant_on'] == True:
                            own.setLinearVelocity([(outvel.x + -bsoutvel), outvel.y, outvel.z], 1)
                            #cont.activate(own.actuators["grindoutRight"])
                        if own['grind_out_type'] == None:
                            own['grind_out_type'] = 'bs reg forward'
                        #own.applyRotation((0, -.15, 0), True)
                    own["grindoutturn"] = gotcd
                    own['invert_on'] = 0
            #if lq1on or lq2on or lq3on or lq4on or lq5on or lq6on or lq7on or lq8on:
            if sl.q1 >= countdown or sl.q2 >= countdown or sl.q3 >= countdown or sl.q4 >= countdown or sl.q5 >= countdown or sl.q6 >= countdown or sl.q7 >= countdown:
                gt_cd = own['gt_cd']
                if gt_cd == 0:
                    gt_cd = 30
                    own['gt_cd'] = gt_cd

            if own['grindoutturn'] == 0:
                cont.deactivate(own.actuators["grindoutRight"])
                own["grindoutturn"] = 0
            gt_cd = own['gt_cd']
            if gt_cd > 0:
                own['gt_cd'] = gt_cd - 1
            else:
                own['gt_cd'] = 0
    #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        reg_move = .05
        reg_rot = 0.02#2
        reg_rot2 = .26
        reg_move2 = .13
        bsforce = 1#50
        bs_dloc = .04
        outvel = own.linearVelocity
        if own["coping"] == 1:
            reg_rot = reg_rot * 3
        if own['gt_cd2'] > 50 and own['jump_timer'] < 20:
            if own['grind_out_type'] == 'reg right':
                own.applyMovement([0, reg_move, 0], True)
                own.applyRotation([0, 0, -reg_rot], True)
            if own['grind_out_type'] == 'fak right':
                own.applyMovement([0, -reg_move, 0], True)
                own.applyRotation([0, 0, -reg_rot], True)
            if own['grind_out_type'] == 'reg left':
                own.applyMovement([0, -reg_move, 0], True)
                own.applyRotation([0, 0, reg_rot], True)
            if own['grind_out_type'] == 'fak left':
                own.applyMovement([0, reg_move, 0], True)
                own.applyRotation([0, 0, reg_rot], True)
            if own['grind_out_type'] == 'reg fak right':
                if own['gt_cd2'] > 55:
                    own.applyMovement([0, reg_move2, 0], True)
                own.applyRotation([0, 0, reg_rot2], True)
            if own['grind_out_type'] == 'fak fak right':
                if own['gt_cd2'] > 55:
                    own.applyMovement([0, -reg_move2, 0], True)
                own.applyRotation([0, 0, reg_rot2], True)
            if own['grind_out_type'] == 'reg fak left':
                if own['gt_cd2'] > 55:
                    own.applyMovement([0, -reg_move2, 0], True)
                own.applyRotation([0, 0, -reg_rot2], True)
            if own['grind_out_type'] == 'fak fak left':
                if own['gt_cd2'] > 55:
                    own.applyMovement([0, reg_move2, 0], True)
                own.applyRotation([0, 0, -reg_rot2], True)
            if own['grind_out_type'] == 'reg right' or own['grind_out_type'] == 'reg left' or own['grind_out_type'] == 'fak right' or own['grind_out_type'] == 'fak left':
                own.setLinearVelocity([outvel.x * 1.01, outvel.y, outvel.z], True)
            if own['grind_out_type'] == 'reg fak right' or own['grind_out_type'] == 'reg fak left' or own['grind_out_type'] == 'fak fak right' or own['grind_out_type'] == 'fak fak left':
                if own['gt_cd2'] == 51:
                    if STANCE == False:
                        own.applyForce([-200, 0, 0], True)
                        own['stance'] = True
                        STANCE = True
                    if STANCE == True:
                        own.applyForce([200, 0, 0], True)
                        own['stance'] = False
                        STANCE = False
            if own['grind_out_type'] == 'bs reg back' or own['grind_out_type'] == 'bs fak forward':
                if own['gt_cd2'] > 50:
                    if STANCE == True:
                        own.applyForce([bsforce, 0, 0], True)
                    if STANCE == False:
                        own.applyForce([bsforce, 0, 0], True)
                    own.applyMovement([bs_dloc, 0, 0], True)
            if own['grind_out_type'] == 'bs fak back' or own['grind_out_type'] == 'bs reg forward':
                if own['gt_cd2'] > 50:
                    if STANCE == True:
                        own.applyForce([-bsforce, 0, 0], True)
                    if STANCE == False:
                        own.applyForce([-bsforce, 0, 0], True)
                    own.applyMovement([-bs_dloc, 0, 0], True)

        if own['gt_cd2'] > 0:
            own['gt_cd2'] -= 1
        if own['gt_cd2'] == 0:
            own['grind_out_type'] = None

    if own["grindoutturn"] > 0:
        own["grindoutturn"] = own["grindoutturn"] - 1


    def air_height():
        height = own["air_height"]
        if lf_ground == True and not onGround:
            height = 0

    #wallride
    def wallride():
        wallride_off = own['wallride_off']
        wallride_off = False
        distance = own.getDistanceTo(gray.hitPosition)
        upforce = 25
        sideforce = 15
        if wallrideL.triggered or wallrideR.triggered:
            print('----wall triggered', wallride_off, onGround)
        try:
            obj = gray.hitObject
            objpos = obj.worldPosition
            ownpos = own.worldPosition
            distance = ownpos.z - objpos.z
        except:
            #print('gray broke')
            pass
        if wallrideL.triggered and wallride_off == 0 and not onGround:
            own['jump_timer'] = 0
            if not onGround:
                own["wallride"] = "L"
                cont.activate(wallrideconstL)
                force = [0, sideforce, upforce]
                own.applyForce(force, 1)
                #align
                print('wallride')
                if STANCE == 0:
                    own['requestAction'] = 'reg_wall_r'
                else:
                    own['requestAction'] = 'fak_wall_l'

        else:
            if own["wallride"] != "R":
                own["wallride"] = None
            cont.deactivate(wallrideconstL)

        if wallrideR.triggered and wallride_off == 0:
            own['jump_timer'] = 0
            if not onGround:
                own["wallride"] = "R"
                cont.activate(wallrideconstR)
                force = [0, -sideforce, upforce]
                own.applyForce(force, 1)
                print('wallride')
                if STANCE == 0:
                    own['requestAction'] = 'reg_wall_l'
                else:
                    own['requestAction'] = 'fak_wall_r'
        else:
            if own["wallride"] != "L":
                own["wallride"] = None
            cont.deactivate(wallrideconstR)

    def wallride_sound():
        sact = own.actuators["wallSound"]
        #print('wr as', own['aState'])
        #if own["wallride"] != None and (own['actionState'] == 'reg_wall_r' or own['actionState'] == 'reg_wall_l' or own['actionState'] == 'fak_wall_r' or own['actionState'] == 'fak_wall_l'):
        if  own['aState'] in ['reg_wall_r', 'reg_wall_l', 'fak_wall_r', 'fak_wall_l'] and not skater.isPlayingAction(3):
            sact.volume = .2
            cont.activate(own.actuators["wallSound"])
            print('wall sound act')
        else:
            sact.volume = .001
            sact.stopSound()
    if own['revert_timer'] > 0:
        own['revert_timer'] = own['revert_timer'] - 1

    def shutoff_timers():
        if (LAST_GRIND == False and grindHit == True) or (jump_timer > 10 and own['wallride'] == None):
            sr.reset()
            sl.reset()

    def grindout_cleanup():
        lgf = own['last_grind_frame']

    def trans_jump():
        if own['jump_from_trans'] == 1:
            ground_ray = cont.sensors['ground_look']
            jump_obj = own['trans_jump_obj']
            jump_obj = scene.objects[str(own['trans_jump_obj'])]
            worldVect = [1, 0, 0]
            vect = jump_obj.getAxisVect(worldVect)
            go = jump_obj.worldOrientation
            grinder_axis = [0, 1, 0]
            player_pos = own.worldPosition
            jump_obj = jump_obj.worldPosition
            try:
                delta = player_pos - jump_obj
            except:
                pass
            delta = delta.cross(vect)
            deltamove = delta[1] * .25
            move = [deltamove, 0, 0]
            if abs(deltamove) < 1 and delta[1] < 1:
                own.applyMovement(move, True)

    def air_mover():
        stren = 140 # 120
        stren2 = 10 #7
        if not onGround and jump_timer > 53 and frames_since_grinding > 50:
            if lUD > 0.075:
                if STANCE == 0:
                    force2 = [stren, 0, 10]
                if STANCE == 1:
                    force2 = [-stren, 0, 10]
                own.applyForce(force2, True)
                own['no_grind_pull'] = 1
        if not onGround and jump_timer > 53 and frames_since_grinding > 50:
            if lUD < -0.075:
                own['no_grind_pull'] = 1
        if not onGround and jump_timer > 10 and lUD > 0.075 and frames_since_grinding > 50:
            if STANCE == 0:
                force2 = [stren2, 0, 10]
            if STANCE == 1:
                force2 = [-stren2, 0, 10]
            own.applyForce(force2, True)
            own.applyForce([0, 0, 5], False)
            own['air_mover'] = True
            own['no_grind_pull'] = 1
        else:
            own['air_mover'] = False

    def dropinRotate():
        if own['dropinTimer'] > 30 and own['dropinTimer'] < 40:
            rot = [0.01, 0.0, 0]
            vel = [linVelocity.x - .25, linVelocity.y, linVelocity.z - .01]
            if STANCE == 1:
                rot = [-0.01, 0.0, 0]
                vel = [linVelocity.x + .25, linVelocity.y, linVelocity.z - .01]
            local = True
            own.applyRotation(rot, local)
            own.setLinearVelocity(vel, local)
        if own['dropinTimer'] == 50:
            if STANCE == 1:
                force = (linVelocity.x +1, linVelocity.y, linVelocity.z)
            else:
                force = (linVelocity.x -1, linVelocity.y, linVelocity.z)
            own.setLinearVelocity(force, True)

    framenum = own['framenum']
    last_ground_frame = own['lF_ground_frame']
    lF_air_frame = own['lF_air_frame']
    frames_since_ground = framenum - lF_air_frame
    if framenum - lF_air_frame > 30:
        own['no_grind_pull'] = 0

    def wallhit():
        linvelx = linVelocity.x
        lastx = own["linvelx"]
        wallsens = cont.sensors['wall']
        if frames_since_ground > 20 and (abs(lastx) - abs(linvelx)) > .5 and wallsens.positive:
            velx = linVelocity.x * -1.5
            if velx > 0 and velx < 1:
                velx = 1
            if velx < 0 and velx > -1:
                velx = -1
            force = (velx, linVelocity.y, linVelocity.z)
            own.setLinearVelocity(force, True)

    def grind_but_align():
        if rBump == True:
            own.alignAxisToVect([0.0, 0.0, 1.0], 2, .06)

    def ragdoll_out():
        rdmax = 120
        if own['ragdoll_out'] > 0:
            print('deactivating ragdoll')
            stre = 1 - (own['ragdoll_out'] / rdmax)
            own['ragdoll_out'] += 1
            if own['ragdoll_out'] == rdmax:
                stre = 0
            if own['ragdoll_out'] > rdmax:
                own['ragdoll_out'] = 0
                stre = 0
                
            armature = scene.objects['Char4']
            for const in armature.constraints:
                #print(const, 'const')
                if ('rd_cl' in str(const) or 'rd_cr' in str(const)):
                    const.enforce = stre  



    def land_mod():
        if own['requestAction'] == 'reg_land':
            own['EaRequest'] = 'land'
            if land_overRideL == True:
                own['requestAction'] = 'reg_landL'
                print('requesting unbalanced land1')
                own['lland'] = own['requestAction']
            elif land_overRideR == True:
                own['requestAction'] = 'reg_landR'
                print('requesting unbalanced land2')
                own['lland'] = own['requestAction']
            elif land_overRideLb == True:
                own['requestAction'] = 'reg_landLb'
                print('requesting unbalanced land1b')
                own['lland'] = own['requestAction']
            elif land_overRideRb == True:
                own['requestAction'] = 'reg_landRb'
                print('requesting unbalanced land2b')
                own['lland'] = own['requestAction']
            else:
                own['lland'] = own['requestAction']    

        if own['requestAction'] == 'fak_land':
            if land_overRideL == True:
                own['requestAction'] = 'fak_landL'
                print('requesting unbalanced landf1')
                own['lland'] = own['requestAction']
            elif land_overRideR == True:
                own['requestAction'] = 'fak_landR'
                print('requesting unbalanced landf2')
                own['lland'] = own['requestAction']
            elif land_overRideLb == True:
                own['requestAction'] = 'fak_landLb'
                print('requesting unbalanced landf1b')
                own['lland'] = own['requestAction']
            elif land_overRideRb == True:
                own['requestAction'] = 'fak_landRb'
                print('requesting unbalanced landf2b')
                own['lland'] = own['requestAction']
            else:
                own['lland'] = own['requestAction']   

    def incline_action():
        own['ground_since'] = ground_since
        if own['rotz'] < .97:
            if own['requestAction'] == 'reg_roll':
                own['requestAction'] = 'reg_opos'
            if own['requestAction'] == 'reg_turnLeft':
                own['requestAction'] = 'reg_pump_left'
            if own['requestAction'] == 'reg_turnRight':
                own['requestAction'] = 'reg_pump_right'

            if own['requestAction'] == 'fak_roll':
                own['requestAction'] = 'fak_opos'
            if own['requestAction'] == 'fak_turnLeft':
                own['requestAction'] = 'fak_pump_left'
            if own['requestAction'] == 'fak_turnRight':
                own['requestAction'] = 'fak_pump_right'        

    jump_Timer()
    check_landing()
    nextframe()
    wheelroll()
    rollsound()
    reset_pos()
    ylimit()
    getoffboard()
    #switchcam()
    move_flycam()
    move_followcam()
    grind_turn()
    air_height()
    wallride()
    wallride_sound()
    shutoff_timers()
    grindout_cleanup()
    air_mover()
    dropinRotate()
    onboard()
    grind_but_align()
    land_mod()
    incline_action()
    #ragdoll_out()
    linvelx = own.getLinearVelocity(True)
    own["linvelx"] = linvelx.x
    LAST_STANCE = STANCE
    LAST_STANCE = own["stance"]
    own["last_stance"] = STANCE
    own["lastGrab"] = GRAB_ON
    own['ground'] = onGround
    own['last_ground'] = onGround
    own["LAST_GRIND"] = grindHit
    own["rotz"] = rot.z
    own['walk'] = 0
    own['lasty'] = yBut
    own['lastlts'] = ltsBut
    own['lastrts'] = rtsBut
    own["lF_ground2"] = own["lF_ground"]
    own['last_invert'] = invert_on
    own['grab_type'] = grab_type
    own['last_last_manual'] = own['last_manual']
    own['last_manual'] = own['manual']
    own['last_reg_manual'] = own['reg_manual']
    own['last_fak_manual'] = own['fak_manual']
    own['last_fak_nmanual'] = own['fak_nmanual']
    own['last_reg_nmanual'] = own['reg_nmanual']
    if own['manual'] == 1:
        own['last_manual_frame'] = frame

    own['last_hippy'] = own['hippy']
    if aBut == 1 and own["lasta"] == 0:
        own["lastaf"] = frame
    if xBut == 1 and own["lastx"] == 0:
        own["lastxf"] = frame
    own["lasta"] = aBut
    own["lastx"] = xBut
    own['lastaBut_ground'] = aBut_ground
    own['lastxBut_ground'] = xBut_ground
    own["last_sel"] = own["sel"]
    own["sel"] = bkBut
    dict['walk'] = 0

    Axis = 2
    Distance = -10
    End = own.worldPosition + (own.worldOrientation.col[Axis]*Distance)
    Start = own.worldPosition
    #localRay = own.rayCast(End, Start, 0,'ground', 0, 0, 0)
    localRay = groundRay
    own['vert'] = 0
    own['transition'] = False
    aligned = False
    #if localRay[0] or grindRay[0]:
    if localRay[0]:

        #if grindRay[0] and not localRay[0]:
            #localRay = grindRay
        if 'transition' in localRay[0]:
            own['transition'] = True
            own['last_transition_frame'] = own['framenum']
            #print('transition')
        localHitDist = (own.worldPosition - localRay[1]).length
        #print('gh', localHitDist)
        if localHitDist < .4:
            if own['jump_timer'] < 30:
                own.localLinearVelocity.z *= .9

        #if localHitDist < .287:
        if localHitDist < .295:
            if own['jump_timer'] < 30:
                own.localLinearVelocity.z = 0
            own.applyMovement((0, 0, (.295 - (localHitDist))), True)
            #own.applyMovement((0, 0, (.32 - (localHitDist))), True)
            #own.applyMovement((0, 0, (.5 - localHitDist)), True)
            #print('moving new ray a')
            #if localRay[2] != [0, 0, -1] and grindHit == 0:
            if localRay[2] != [0, 0, -1]:    
                #own.alignAxisToVect(localRay[2], 2, .25)
                own.alignAxisToVect(localRay[2], 2, .5)
                aligned = True
            #print('localRay')
           
        if localHitDist > .31 and localHitDist < 2:
            own.alignAxisToVect(localRay[2], 2, .05)
            #print('new align')
    #else:
        #print('no localRay')
        else:
            if own['jump_timer'] == 0:
                y1 = Start
                y2 = Start
                y1.x += .25
                y2.x -= .25
                r1 = own.rayCast(End, y1, 0, 'ground', 1, 0, 0)
                r2 = own.rayCast(End, y2, 0, 'ground', 1, 0, 0)
                if r1[0] or r2[0]:
                    #print('secondary ray active!!!')
                    if r1[0]:
                        localRay = r1
                        Start = y1
                        #print('front truck hit')
                    else:
                        localRay = r2
                        Start = y2
                        #print('back truck hit')
                    localHitDist = (Start - localRay[1]).length
                    if localHitDist < .3:
                        own.applyMovement((0, 0, (.3 - localHitDist)), True)
                        #print('moving new rayb')
                        if localRay[2] != [0, 0, -1] and grindHit == 0:
                            own.alignAxisToVect(localRay[2], 2, .25)
                            aligned = True

    else:
        #print('vh')
        Axis = 2
        Distance = -2
        End = Vector.copy(own.worldPosition + (own.worldOrientation.col[Axis]*Distance))
        Start = Vector.copy(own.worldPosition)
        localRay_v = own.rayCast(End, Start, 0, 'vert', 1, 0, 0, 61439)

        if lBump == 1 and rBump == 1:
            own.alignAxisToVect([0.0, 0.0, 1.0], 2, .05)  
        if localRay_v[0] and own['LAST_GRIND'] == False and rBump == 0 and lBump == 0 and ((own['framenum'] - own['last_transition_frame'] < 10) or (own['framenum'] - own['last_vert_frame'] < 10)):
            own['vert'] = 1
            print('vert hit')

            if 'spine' in localRay_v[0] and lUD < -.04:
                print('spine hit!!!!!', lUD)
                
                #print('roting')
                if STANCE == True:
                    own.applyRotation([0,.15,0], True)
                else:    
                    own.applyRotation([0,-.15,0], True)
                own.linearVelocity.x *= .97    
            elif lUD > -.04: 
                own['last_vert_frame'] = own['framenum']
                localHitDist = Vector.copy(Start - localRay_v[1]).length
                own.linearVelocity.x *= .98
                own.linearVelocity.z = .1
                #if localHitDist < .8 and (dict['lUD'] < .04 or dict['lUD'] > -.04):
                #if localHitDist < .8:    
                if localHitDist < 1.5:        
                    #own.applyMovement((0,0,((.4 - (localHitDist) * .1))), True)
                    #own.applyMovement((0, 0, (.3 - localHitDist)), True)
                    #if localHitDist < 1.0:
                    own.applyForce((0, 0, (.4 - localHitDist) *1000), True)

                    print('moving vert ray')
                    if localRay_v[2] != [0, 0, -1] and grindHit == 0:
                    #if localRay_v[2] != [0,0,-1]:
                        #own.alignAxisToVect(localRay_v[2], 2, .4)
                        own.alignAxisToVect(localRay_v[2], 2, .05)
                        aligned = True
            else:
                print('doing nothing')            
        #print(own['lGobj'], 'lGobj')                         




    localHitDist = 1000
    raytof = scene.objects['rayTo_f']
    raytob = scene.objects['rayTo_b']


    if localRay[0]:
        #print('gh')
        localHitDist = (own.worldPosition - localRay[1]).length
    if localHitDist > .3 and frames_since_grinding < 30 and frames_since_grinding > 0 and own['grindTouch'] == False:
        rayf = own.rayCast(raytof.worldPosition, own.worldPosition, 0, 'transition', 1, 0, 0)
        rayb = own.rayCast(raytob.worldPosition, own.worldPosition, 0, 'transition', 1, 0, 0)

        if rayf[0]:
            print('rayf hit')
            own.alignAxisToVect(rayf[2], 2, .2)
            aligned = True
        elif rayb[0]:
            own.alignAxisToVect(rayb[2], 2, .2)
            aligned = True
            print('rayb hit')


    if not aligned or (lBump == 1):
        own.alignAxisToVect([0.0, 0.0, 1.0], 2, .00003)

    if own.linearVelocity.x > 12:
        own.linearVelocity.x = 12
    if (grindDar == False and onGround and own['grindTouch'] == False) or own['jump_timer'] > 40:
        own['grindType'] = ''

    
    if cont.sensors['npc'].triggered:
        cam['walker_manager'].pub.dispatch('path found', ['hit', own, cont.sensors['npc'].hitObject])
        
        #print('npc hit')


    if own['actionState'] in ['frontside_grab_out', 'backside_grab_out', 'frontside_nose_grab_out', 'frontside_tail_grab_out', 'backside_nose_grab_out', 'backside_tail_grab_out', 'reg_walk_air_out', 'reg_air-walk_air', 'reg_judo_out', 'reg_frigid_out', 'reg_fsonefoot_out', 'reg_onefoot_out', 'reg_airwalk_out'] and own['requestAction'] in ['fak_land', 'reg_land']:
        own['requestAction'] = 'reg_land'

    elif own['actionState'] in ['fak_backside_grab_out', 'fak_frontside_grab_out', 'fak_frontside_nose_grab_out', 'fak_frontside_tail_grab_out', 'fak_backside_nose_grab_out', 'fak_backside_tail_grab_out', 'fak_walk_air_out', 'fak_air-walk_air', 'fak_judo_out', 'fak_frigid_out', 'fak_fsonefoot_out', 'fak_onefoot_out'] and own['requestAction'] in  ['reg_land', 'fak_land']:
        own['requestAction'] = 'fak_land'   

