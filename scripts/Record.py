#record
import bge
from bge import logic
from mathutils import Vector
from SortData import *
import boneRecord
import sound_man

cont = logic.getCurrentController()
own = cont.owner
scene = logic.getCurrentScene()
cube = scene.objects["control_cube.002"]
#obj = scene.objects["control_cube.002"]
#sound_empty = scene.objects['replay_sound_empty']
path = logic.expandPath(own["filePath"])

PAF = 0
DAF = 0

deck = scene.objects["b_deck"]
trucks = scene.objects["b_trucks"]

throw_deck_empty = scene.objects["throw_deck_empty"]
wheel1 = scene.objects["wheel1"]
wheel2 = scene.objects["wheel2"]
wheel3 = scene.objects["wheel3"]
wheel4 = scene.objects["wheel4"]

try:
    throw_deck = scene.objects['throw_deck']
except:
    throw_deck = None    

deckact = deck.actuators["Visibility"]
trucksact = trucks.actuators["Visibility"]
wheel1act = wheel1.actuators["Visibility"]
wheel2act = wheel2.actuators["Visibility"]
wheel3act = wheel3.actuators["Visibility"]
wheel4act = wheel4.actuators["Visibility"]

class getData:



    def savePosition(obj, cut):
        position = [Vector(obj.worldPosition)[0],
            Vector(obj.worldPosition)[1],
            Vector(obj.worldPosition)[2]]
        position = str(position).strip("[]")
        position = position.replace(", ",",")
        writeData.addPosition(obj, position, cut)
     
    def saveOrientation(obj, cut): 
        orientation = [Vector(obj.localOrientation.to_euler())[0],
            Vector(obj.localOrientation.to_euler())[1],
            Vector(obj.localOrientation.to_euler())[2]]
        orientation = str(orientation).strip("[]")
        orientation = orientation.replace(", ",",")     
        writeData.addOrientation(obj, orientation, cut)
                    
    def saveScale(obj, cut):  
        scale = [Vector(obj.localScale)[0],
            Vector(obj.localScale)[1],
            Vector(obj.localScale)[2]]
        scale = str(scale).strip("[]")
        scale = scale.replace(", ",",")
        writeData.addScale(obj, scale, cut)
    def saveColor(obj, cut):
        color = [Vector(obj.color)[0],
            Vector(obj.color)[1],
            Vector(obj.color)[2],
            Vector(obj.color)[3]]
        color = str(color).strip("[]")
        color = color.replace(", ",",")
        writeData.addColor(obj, color, cut)
    def saveState(obj, cut):
        
        state = str(obj.state)
        writeData.addState(obj, state, cut)
        
    def saveVisibility(obj, cut):
        visible = obj.visible
        if visible:
            visible = 1
        else:
            visible = 0   
        writeData.addVisibility(obj, str(visible), cut)
    
    def saveSkaterAnim(obj, cut):
        import bge
        scene = bge.logic.getCurrentScene()
        skater = scene.objects["Char4"]
        deck = scene.objects["b_deck"]
        trucks = scene.objects["b_trucks"]
        playing_layer = 0
        for x in range(7):
            l1 = skater.isPlayingAction(x)
            if l1 == True:
                playing_layer = x
                #print(x)
        playing_action = skater.getActionName(playing_layer)
        playing_action_frame = skater.getActionFrame(playing_layer)
        PAF = playing_action_frame
        own['PAF'] = PAF
       #print(playing_action, playing_action_frame)        
        writeData.addSkaterAnim(obj, str(playing_action), cut)
        
    def saveSkaterAnimF(obj, cut):
        PAF = own['PAF']
        writeData.addSkaterAnimF(obj, str(PAF), cut)   
        
    def saveDeckAnim(obj, cut):
        # import bge
        # scene = bge.logic.getCurrentScene()
        # skater = scene.objects["Char4"]
        # deck = scene.objects["b_deck"]
        # trucks = scene.objects["b_trucks"]
        # wheel1 = scene.objects["rollen.000"]
        # wheel2 = scene.objects["rollen.001"]
        # wheel3 = scene.objects["rollen.002"]
        # wheel4 = scene.objects["rollen.003"]        
        # playing_layer = 0
        # for x in range(7):
        #     l1 = deck.isPlayingAction(x)
        #     if l1 == True:
        #         playing_layer = x
        #         #print(x)
        # playing_action = deck.getActionName(playing_layer)
        # playing_action_frame = deck.getActionFrame(playing_layer)
        # DAF = playing_action_frame
        # own['DAF'] = DAF
       #print(playing_action, playing_action_frame)        
        writeData.addDeckAnim(obj, str(1), cut)
        
    def saveDeckAnimF(obj, cut):
        #DAF = own['DAF']
        writeData.addDeckAnimF(obj, str(1), cut)

    def saveSrollVol(obj, cut):
        num = cube['sroll_vol']
        writeData.addSrollVol(obj, str(num), cut) 
    def saveSrollPitch(obj, cut):
        num = cube['sroll_pitch']
        writeData.addSrollPitch(obj, str(num), cut) 
    
    def saveSgrind_cementVol(obj, cut):
        num = cube['grindcement_vol']
        writeData.addSgrind_cementVol(obj, str(num), cut) 
    def saveSgrind_cementPitch(obj, cut):
        num = cube['grindcement_pitch']
        writeData.addSgrind_cementPitch(obj, str(num), cut) 
    def saveSgrind_railVol(obj, cut):
        num = cube['grindrail_vol']
        writeData.addSgrind_railVol(obj, str(num), cut) 
    def saveSgrind_railPitch(obj, cut):
        num = cube['grindrail_pitch']
        writeData.addSgrind_railPitch(obj, str(num), cut)
    def saveSpopsound(obj, cut):
        num = cube['pop_sound']
        writeData.addSpopsound(obj, str(num), cut) 
    def saveSlandsound(obj, cut):
        num = cube['land_sound']
        writeData.addSlandsound(obj, str(num), cut) 
    def saveSdeckvis(obj, cut):
        num = cube['deckvis']
        writeData.addSdeckvis(obj, str(num), cut)  
        
    def savetdPosition(obj, cut):
        try:
            obj = scene.objects['throw_deck']
        except:
            pass    
        position = [Vector(obj.worldPosition)[0],
            Vector(obj.worldPosition)[1],
            Vector(obj.worldPosition)[2]]
        position = str(position).strip("[]")
        position = position.replace(", ",",")
        writeData.addtdPosition(obj, position, cut)
     
    def savetdOrientation(obj, cut): 
        try:
            obj = scene.objects['throw_deck']
        except:
            pass
        orientation = [Vector(obj.localOrientation.to_euler())[0],
            Vector(obj.localOrientation.to_euler())[1],
            Vector(obj.localOrientation.to_euler())[2]]
        orientation = str(orientation).strip("[]")
        orientation = orientation.replace(", ",",")     
        writeData.addtdOrientation(obj, orientation, cut)
    def saveSrevertsound(obj, cut):
        num = cube['revert_sound']
        writeData.addSrevertsound(obj, str(num), cut)                                                                               
              
def loadData():
    
    #Function for loading the data from
    #the disk and setting it.
    dict = bge.logic.globalDict
    objIndex = own["objIndex"]
    own["valueIndex"] = objIndex + 1
    valueIndex = own["valueIndex"]
    playbackSpeed = own["playbackSpeed"]
    loadedPosition = own["loadedPosition"]
    loadedOrientation = own["loadedOrientation"]
    loadedScale = own["loadedScale"]
    loadedColor = own["loadedColor"]
    loadedVisibility = own["loadedVisibility"]
    loadedSkaterAnim = own["loadedSkaterAnim"]
    loadedSkaterAnimf = own["loadedSkaterAnimF"]
    loadedDeckAnim = own["loadedDeckAnim"]
    loadedDeckAnimf = own["loadedDeckAnimF"]
    loadedSrollVol = own["loadedSrollVol"]
    loadedSrollPitch = own["loadedSrollPitch"]
    loadedSgrind_cementlVol = own["loadedSgrind_cementVol"]
    loadedSgrind_cementPitch = own["loadedSgrind_cementPitch"]
    loadedSgrind_railVol = own["loadedSgrind_railVol"]
    loadedSgrind_railPitch = own["loadedSgrind_railPitch"] 
    loadedSpopsound = own["loadedSpopsound"]            
    loadedSlandsound = own["loadedSlandsound"] 
    loadedSdeckvis = own["loadedSdeckvis"]  
    loadedtdPosition = own["loadedtdPosition"]
    loadedtdOrientation = own["loadedtdOrientation"]      
    loadedSrevertsound = own["loadedSrevertsound"] 
    
    skater = scene.objects["Char4"]
    deck = scene.objects["b_deck"]
    trucks = scene.objects["b_trucks"]
    bdeck = scene.objects['deck_arm']
    num = 1
    obj = scene.objects['control_cube.002']
    if num == 1:    
    #for obj in scene.objects:        
        if not "recorder" in obj:
            obj.state = 100
            obj.suspendDynamics()
                     
    readData.loadAll()    #Load the file!!!
    
    #-----Position-----#
    own["lengthPos"] = readData.getLengthPos()
    dict['replay_loop_end'] = own['lengthPos']
    #print('setting replay end', dict['replay_loop_end'])
    lengthPos = own["lengthPos"] 
    
    if lengthPos:
        
        if objIndex > lengthPos-1:
            own["objIndex"] = dict['replay_loop_start']
        if objIndex < dict['replay_loop_start']:
            own["objIndex"] = lengthPos-2

        name, position = readData.returnPosition(objIndex, valueIndex)
        
        if name in scene.objects:
            try:
                scene.objects[name].worldPosition = position
                scene.objects['replay_sound_empty'].worldPosition = position
                #print("replay_sound_empty changing position")
            except:
                pass
    #-----Orientation-----#
    own["lengthOri"] = readData.getLengthOri()   
    lengthOri = own["lengthOri"]
    
    #if lengthPos:
    if lengthOri:
  
        if valueIndex > lengthOri-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthOri-2

        name, orientation = readData.returnOrientation(objIndex, valueIndex)
        
        if name in scene.objects:
            oXYZ = scene.objects[name].localOrientation.to_euler()

            oXYZ[0] = float(orientation[0])
            oXYZ[1] = float(orientation[1])
            oXYZ[2] = float(orientation[2])
            
            try:
                scene.objects[name].localOrientation = oXYZ.to_matrix()
            except:
                pass
    #-----Scale-----#
    own["lengthSca"] = readData.getLengthSca()
    lengthSca = own["lengthSca"]
    
    if lengthSca:
    
        if valueIndex > lengthSca-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSca-2
       
        name, scale = readData.returnScale(objIndex, valueIndex)
        
        if name in scene.objects:
            try:
                scene.objects[name].localScale = scale
            except:
                pass
    #-----Color-----#
    own["lengthCol"] = readData.getLengthCol()   
    lengthCol = own["lengthCol"]
    
    if lengthCol:

        if valueIndex > lengthCol-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthCol-2

        name, color = readData.returnColor(objIndex, valueIndex)
        
        if name in scene.objects:
            try:
                scene.objects[name].color = color
            except:
                pass
    #-----Visibility-----#
    own["lengthVis"] = readData.getLengthVis()
    lengthVis = own["lengthVis"]
    
    if lengthVis:
        pass
        # if valueIndex > lengthVis-1:
        #     own["valueIndex"] = 0
        # if objIndex < 0:
        #     own["objIndex"] = lengthVis-2

        # name, visible = readData.returnVisibility(objIndex, valueIndex)
        
        # if name in scene.objects:
        #     try:
        #         scene.objects[name].visible = int(visible)
        #     except:
        #         pass
    #-----Skater Animation Name-----#
    own["lengthSkaterAnim"] = readData.getLengthSkaterAnim()
    lengthSkaterAnim = own["lengthSkaterAnim"]
    #print("lengthskateranim", lengthSkaterAnim)
    if lengthSkaterAnim:

        if valueIndex > lengthSkaterAnim-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSkaterAnim-2


        skater.stopAction(0)
        skater.stopAction(1)
        skater.stopAction(2)
        skater.stopAction(3)
        skater.stopAction(4)
        skater.stopAction(5)
        skater.stopAction(6)
        skater.stopAction(9999)

        bdeck.stopAction(0)
        bdeck.stopAction(1)
        bdeck.stopAction(2)
        bdeck.stopAction(3)
        bdeck.stopAction(4)
        bdeck.stopAction(5)
        bdeck.stopAction(6)
        bdeck.stopAction(9999)        


        boneRecord.Playback(valueIndex)
        #sound_man.Playback(valueIndex)
        own['sndmgr'].h_Playback(valueIndex)
        #print(valueIndex, 'valueIndex from Record.py')
        #print('recorder', valueIndex)


        # name, skateranim = readData.returnSkaterAnim(objIndex, valueIndex)
        # name, PAF = readData.returnSkaterAnimF(objIndex, valueIndex)
        # #print(PAF)
        # PAF = float(PAF)
        # if name in scene.objects:
        #     #print("name in")
        #     try:
        #         skater.stopAction(0)
        #         skater.stopAction(1)
        #         skater.stopAction(2)
        #         skater.stopAction(3)
        #         skater.stopAction(9999)
        #         if skater != '':
        #             skater.playAction(skateranim, PAF,PAF, layer=9999, play_mode=1, speed=1)
        #         #print("Playing: ", skateranim, PAF)
        #     except:
        #         print("something is wrong")
        #         #pass  
                
                
    #-----Deck Animation Name-----#
    own["lengthDeckAnim"] = readData.getLengthDeckAnim()
    lengthDeckAnim = own["lengthDeckAnim"]
    #print("lengthDeckanim", lengthDeckAnim)
    if lengthDeckAnim:

        if valueIndex > lengthDeckAnim-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthDeckAnim-2

        name, deckanim = readData.returnDeckAnim(objIndex, valueIndex)
        name, DAF = readData.returnDeckAnimF(objIndex, valueIndex)
        #print(DAF)
        DAF = float(DAF)
        if name in scene.objects:
            #print("name in")
            try:
                deck.stopAction(0)
                deck.stopAction(1)
                deck.stopAction(2)
                deck.stopAction(3)
                deck.stopAction(9999)
                #print(deckanim)
                # if deckanim != '':
                #     turnList = ['a_reg_right', 'a_reg_left', 'a_fak_right', 'a_fak_left']
                #     #print(deckanim)
                #     if deckanim not in turnList:
                #         deck.playAction(deckanim, DAF,DAF, layer=9999, play_mode=1, speed=1)
                #         trucks.playAction(deckanim, DAF,DAF, layer=9999, play_mode=1, speed=1)
                #     else:
                #         #print('play a_reg')  
                #         deck.playAction(deckanim, DAF,DAF, layer=9999, play_mode=1, speed=1)
                #         trucks.playAction('a_reg', DAF,DAF, layer=9999, play_mode=1, speed=1)                          
                #print("Playing: ", deckanim, PAF)
            except:
                print("deck something is wrong")
                #pass  

#
    #-----sroll-----#
    own["lengthSrollVol"] = readData.getLengthSrollVol()
    lengthSrollVol = own["lengthSrollVol"]
    if lengthSrollVol:
        if valueIndex > lengthSrollVol-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSrollVol-2
        name, srollVol = readData.returnSrollVol(objIndex, valueIndex)
        name, srollPitch = readData.returnSrollPitch(objIndex, valueIndex)    
        # if name in scene.objects:
        #     try:
        #         cube = scene.objects["control_cube.002"]
        #         srollVol = round(srollVol, 2)
        #         act = cube.actuators["sroll"]
        #         if srollVol < .12:
        #             act.volume = srollVol
        #             act.pitch = srollPitch
        #         act.startSound()
        #     except:
        #         pass
###            
#
    #-----grind cement-----#
    own["lengthSgrind_cementVol"] = readData.getLengthSgrind_cementVol()
    lengthSgrind_cementVol = own["lengthSgrind_cementVol"]
    if lengthSgrind_cementVol:
        if valueIndex > lengthSgrind_cementVol-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSgrind_cementVol-2
        name, sgrind_cementVol = readData.returnSgrind_cementVol(objIndex, valueIndex)
        name, sgrind_cementPitch = readData.returnSgrind_cementPitch(objIndex, valueIndex)    
        # if name in scene.objects:
        #     try:
        #         cube = scene.objects["control_cube.002"]
        #         sgrind_cementVol = round(sgrind_cementVol, 2)
        #         act = cube.actuators["grind_cement"]
        #         if sgrind_cementVol < .2:
        #             act.volume = sgrind_cementVol
        #             act.pitch = sgrind_cementPitch
        #         act.startSound()
                
        #     except:
        #         pass
###    
#
    #-----grind rail-----#
    own["lengthSgrind_railVol"] = readData.getLengthSgrind_railVol()
    lengthSgrind_railVol = own["lengthSgrind_railVol"]
    if lengthSgrind_railVol:
        if valueIndex > lengthSgrind_railVol-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSgrind_railVol-2
        name, sgrind_railVol = readData.returnSgrind_railVol(objIndex, valueIndex)
        name, sgrind_railPitch = readData.returnSgrind_railPitch(objIndex, valueIndex)    
        # if name in scene.objects:
        #     try:
        #         cube = scene.objects["control_cube.002"]
        #         sgrind_railVol = round(sgrind_railVol, 2)
        #         act = cube.actuators["grind_rail"]
        #         if sgrind_railVol < .2:
        #             act.volume = sgrind_railVol
        #             act.pitch = sgrind_railPitch
        #         act.startSound()
        #         #print("grindsound = ", sgrind_railVol, sgrind_railPitch)
        #     except:
        #         pass
###  
#
    #-----pop sound-----#
    own["lengthSpopsound"] = readData.getLengthSpopsound()
    lengthSpopsound = own["lengthSpopsound"]
    if lengthSpopsound:
        if valueIndex > lengthSpopsound-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSpopsound-2
        name, spopsound = readData.returnSpopsound(objIndex, valueIndex)   
        # if name in scene.objects:
        #     #act = sound_empty.actuators["pop"]
        #     try:
        #         #cube = scene.objects[sound_empty]
        #         spopsound = round(spopsound, 2)
        #         act = cube.actuators["pop"]
        #         #act = sound_empty.actuators["pop"]
        #         if spopsound == 1:
        #             #act.volume_maximum = .7
        #             #act.is3D = True
        #             #act.distance_reference = 10.0
        #             #act.distance_maximum = 50.0
        #             act.volume = .6
        #             act.startSound()
        #         #print("grindsound = ", spopsound, sgrind_railPitch)
        #     except:
        #         print("sound passed")
        #         pass
###
#
    #-----land sound-----#
    own["lengthSlandsound"] = readData.getLengthSlandsound()
    lengthSlandsound = own["lengthSlandsound"]
    if lengthSlandsound:
        if valueIndex > lengthSlandsound-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSlandsound-2
        name, slandsound = readData.returnSlandsound(objIndex, valueIndex)   
        if name in scene.objects:
            try:
                cube = scene.objects["control_cube.002"]
                # slandsound = round(slandsound, 2)
                # act = cube.actuators["land"]
                # if slandsound == 1:
                #     act.volume = .6
                #     act.startSound()
                #print("grindsound = ", slandsound, sgrind_railPitch)
            except:
                pass
###  
###
#
    #-----land sound-----#
    own["lengthSdeckvis"] = readData.getLengthSdeckvis()
    lengthSdeckvis = own["lengthSdeckvis"]
    if lengthSdeckvis:
        if valueIndex > lengthSdeckvis-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSdeckvis-2
        name, sdeckvis = readData.returnSdeckvis(objIndex, valueIndex)   
        
        
        if name in scene.objects:
            try:
                cube = scene.objects["control_cube.002"]
                if sdeckvis == 1:
                    #print('setting deck visible')
                    deckact.visibility = True
                    trucksact.visibility = True
                    wheel1act.visibility = True
                    wheel2act.visibility = True
                    wheel3act.visibility = True
                    wheel4act.visibility = True   
                    cont.activate(deck.actuators['Visibility'])
                    cont.activate(trucks.actuators['Visibility'])
                    cont.activate(wheel1.actuators['Visibility'])
                    cont.activate(wheel2.actuators['Visibility'])
                    cont.activate(wheel3.actuators['Visibility'])
                    cont.activate(wheel4.actuators['Visibility'])  
                    for n in scene.objects:
                        if 'throw_deck' in n.name and 'empty' not in n.name:
                            n.endObject() 
#                    if 'throw_deck' in scene.objects:
                            #print('ending td', n)
                            cont.activate(throw_deck.actuators['end_throw_deck'])                   
                     
                    #throwdeck.visibility = False                               
                else:
                    #print('setting deck invisible')
                    deckact.visibility = False
                    trucksact.visibility = False
                    wheel1act.visibility = False
                    wheel2act.visibility = False
                    wheel3act.visibility = False
                    wheel4act.visibility = False   
                    cont.activate(deck.actuators['Visibility'])
                    cont.activate(trucks.actuators['Visibility'])
                    cont.activate(wheel1.actuators['Visibility'])
                    cont.activate(wheel2.actuators['Visibility'])
                    cont.activate(wheel3.actuators['Visibility'])
                    cont.activate(wheel4.actuators['Visibility'])  
                    #if throw_deck == None:
                    if 'throw_deck' not in scene.objects:
                        #print('no throwdeck')
                        #cont.deactivate(throw_deck.actuators['end_throw_deck']) 
                        #throw_deck_empty.wordPosition.z = throw_deck_empty.wordPosition.z + 1
                        cont.activate(throw_deck_empty.actuators['throw_dec_act']) 
                        #scene.addObject('throw_deck')
                    #throwdeck.visibility = True
                    throw_deck.suspendDynamics()

            except:
                pass
##    
    #-----Position-----#
    own["lengthtdPos"] = readData.getLengthtdPos()
    lengthPos = own["lengthtdPos"] 
    
    if lengthPos:
        
        if objIndex > lengthPos-1:
            own["objIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthPos-2

        name, position = readData.returntdPosition(objIndex, valueIndex)
        name = 'throw_deck'
        if name in scene.objects:
            try:
                scene.objects[name].worldPosition = position
                #print('recording tdPos', position)
            except:
                pass
    #-----Orientation-----#
    own["lengthtdOri"] = readData.getLengthtdOri()   
    lengthOri = own["lengthtdOri"]
    
    #if lengthPos:
    if lengthOri:
  
        if valueIndex > lengthOri-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthOri-2

        name, orientation = readData.returntdOrientation(objIndex, valueIndex)
        name = 'throw_deck'
        if name in scene.objects:
            
            oXYZ = scene.objects[name].localOrientation.to_euler()

            oXYZ[0] = float(orientation[0])
            oXYZ[1] = float(orientation[1])
            oXYZ[2] = float(orientation[2])
            
            try:
                #print('recording tdOri')
                scene.objects[name].localOrientation = oXYZ.to_matrix()
            except:
                pass

#
    #-----revert sound-----#
    own["lengthSrevertsound"] = readData.getLengthSrevertsound()
    lengthSrevertsound = own["lengthSrevertsound"]
    if lengthSrevertsound:
        if valueIndex > lengthSrevertsound-1:
            own["valueIndex"] = 0
        if objIndex < 0:
            own["objIndex"] = lengthSrevertsound-2
        name, srevertsound = readData.returnSrevertsound(objIndex, valueIndex)   
        if name in scene.objects:
            try:
                #cube = scene.objects[sound_empty]
                srevertsound = round(srevertsound, 2)
                #act = cube.actuators["revertSound"]
                #if srevertsound == 1:
                    #act.startSound()
                #print("grindsound = ", spopsound, sgrind_railPitch)
            except:
                print("sound passed")
                pass

                                            
                       
def main(recording_cutoff, cc):
    num = 1
    obj = scene.objects["control_cube.002"]
    #own = scene.objects["Camera.003"]
    if num == 1:
        
    #for obj in scene.objects:
        if "record_position" in obj:
            getData.savePosition(obj, recording_cutoff)
        if "record_orientation" in obj:
            getData.saveOrientation(obj, recording_cutoff)
        if "record_scale" in obj:
            getData.saveScale(obj, recording_cutoff)
        if "record_color" in obj:
            getData.saveColor(obj, recording_cutoff)
        if "record_state" in obj:
            getData.saveState(obj, recording_cutoff)
        if "record_visibility" in obj:
            getData.saveVisibility(obj, recording_cutoff)
        if "record_skateranim" in obj:
            getData.saveSkaterAnim(obj, recording_cutoff)
        if "record_skateranimf" in obj:
            getData.saveSkaterAnimF(obj, recording_cutoff) 
        if "record_deckanim" in obj:
            getData.saveDeckAnim(obj, recording_cutoff)
        if "record_deckanimf" in obj:
            getData.saveDeckAnimF(obj, recording_cutoff)    
        #if "record_sroll_vol" in obj:
        getData.saveSrollVol(obj, recording_cutoff)
        #if "record_sroll_pitch" in obj:
        getData.saveSrollPitch(obj, recording_cutoff)
        getData.saveSgrind_cementVol(obj, recording_cutoff)
        getData.saveSgrind_cementPitch(obj, recording_cutoff)
        getData.saveSgrind_railVol(obj, recording_cutoff)
        getData.saveSgrind_railPitch(obj, recording_cutoff)
        getData.saveSpopsound(obj, recording_cutoff)            
        getData.saveSlandsound(obj, recording_cutoff) 
        getData.saveSdeckvis(obj, recording_cutoff) 
        getData.savetdPosition(obj, recording_cutoff)
        getData.savetdOrientation(obj, recording_cutoff)
        getData.saveSrevertsound(obj, recording_cutoff)                                       
        boneRecord.Record() 
        own['sndmgr'].Record()    

        #print('recording cutoff', recording_cutoff)
         
def breakOut():
    num = 1
    obj = scene.objects["control_cube.002"]
    if num == 1:
    #for obj in scene.objects:
        obj.restoreDynamics()
        try:
            name, state = readData.returnState(own["objIndex"], own["valueIndex"])
            obj.state = state
        except:
            pass