import os
import sys

from bge import logic

from . import custom_shader_utils as shader_utils

path = os.path.dirname(__file__)
sys.path.append(path)

shader_lib = path + "/glsl/"


class CustomShader:
    """Basic class for custom shader"""
    
    def __init__(self, material):
        self.material = material
        self.shader = material.shader
        
    def update(self):
        pass