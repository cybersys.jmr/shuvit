import bge
from mathutils import Vector
from collections import OrderedDict

try:
    from custom_shaders import wind as w
    logic = bge.logic
    
except (ImportError, AttributeError):
    pass       
    

class Wind(bge.types.KX_PythonComponent):
    """
    Assign a wind shader to a material.
    
    Args:
        Material: The object material to which the shader should be assigned.
        Fast Lighting: Activate per vertex lighting instead of per pixel lighting.
        Wind Strength: The strength of the wind.
        Wind Speed: The speed of the wind movement.
        Object Deforming: The deforming value that set how much the geometry will be deformed.
        Object Bending: The bending of higher gemometry in comparison to lower level geometry. 
        Ground Level: The height (distance from the object origin) where the geometry begins to move.
        Material Type: Sets the type of the material.
        Texture Slot: The texture slot for the diffuse texture.
        Normalmap Slot: The texture slot for the normalmap texture.
        Normal Factor: Amount normalmap affects normal values. (Similar to the setting in the texture tab)
        Texture Scale: The size of the texture.
    """
    

    args = OrderedDict([
        ("Material", "Material"),
        ("Material Type", {"Diffuse Texture", "Material Color", "Diffuse Texture (Fast Lighting)", "Material Color (Fast Lighting)", "Diffuse Texture + Normalmap", "Material Color + Normalmap"}),
        ("Texture Slot", 0),
        ("Normalmap Slot", 1),
        ("Normal Factor", 0.0),
        ("Texture Scale", Vector((1.0, 1.0))),
        ("Wind Strength", 0.1),
        ("Wind Speed", 1.0),
        ("Object Deforming", 0.0),
        ("Object Bending", 0.0),
        ("Ground Level", 0.0),
    ])

    def start(self, args):
        material = args['Material']
        scale = None        
        lighting = False
        texture = None
        normalmap = None
        normal_factor = 0.0
        
        if args['Material Type'] in ["Diffuse Texture (Fast Lighting)", "Material Color (Fast Lighting)"]:
            lighting = True
        
        if args['Material Type'] in ["Diffuse Texture", "Diffuse Texture (Fast Lighting)", "Diffuse Texture + Normalmap"]:
            texture = args['Texture Slot']
            scale = args['Texture Scale'] 
        
        if args['Material Type'] in ["Diffuse Texture + Normalmap", "Material Color + Normalmap"]:
            normalmap = args['Normalmap Slot']
            normal_factor = args['Normal Factor']
            scale = args['Texture Scale'] 
        
        m = w.shader_utils.get_material(self.object, material)
        self.shader = w.WindShader(m, texture=texture, scale=scale, vertex_lighting=lighting, normalmap=normalmap, normal_factor=normal_factor)
        
        self.shader.strength = args['Wind Strength']
        self.shader.speed = args['Wind Speed']
        self.shader.deforming = args['Object Deforming']
        self.shader.bending = args['Object Bending']
        self.shader.ground = args['Ground Level']

    def update(self):
        #pass
        self.shader.update()