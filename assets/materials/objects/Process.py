import traceback

__all__ = ['Process']

class Process(object):
    '''This is a very basic interface for events'''

    invalid = False

    def pre_draw_setup_dispatch(self, *args):
        return self.pre_draw_setup()
    def pre_draw_setup(self, *args):
        '''Override me !'''

    def pre_draw_dispatch(self, *args):
        return self.pre_draw()
    def pre_draw(self, *args):
        '''Override me !'''

    def post_draw_dispatch(self, *args):
        return self.post_draw()
    def post_draw(self, *args):
        '''Override me !'''

    def error(self, *args):
        traceback.print_exc()
